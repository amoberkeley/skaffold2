% Sets roi values for Andor absorption images

global absorptionCenCav
global absorptionSpanCav
absorptionCenCav = [325, 18];
absorptionSpanCav = [60, 18];

global absorptionCenLink4
global absorptionSpanLink4
absorptionCenLink4 = [285, 30]; % TODO
absorptionSpanLink4 = [70, 20]; % TODO

global absorptionCenLink28
global absorptionSpanLink28
absorptionCenLink28 = [285, 30]; % TODO
absorptionSpanLink28 = [70, 50]; % TODO

global absorptionCenOP
global absorptionSpanOP
absorptionCenOP = [270, 210];
absorptionSpanOP = [200, 140];

global absorptionCenWU
global absorptionSpanWU
absorptionCenWU = [200, 170];
absorptionSpanWU = [210, 140];

global absorptionCenEvap
global absorptionSpanEvap
absorptionCenEvap = [160, 220];
absorptionSpanEvap = [100, 40];
