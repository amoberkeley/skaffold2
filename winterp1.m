function Yi = winterp1(x0,dx,Y,xi,methodflag)
% Performs fast interpolation compared to interp1
%
% winterp1 provides a speedup over interp1 but requires a waveform.  
% As x and y increase in length, the run-time for interp1 increases
% linearly, but the run-time for winterp1 stays constant.  For 
% small-length x, y, and xi, winterp1 runs about 6x faster than interp1.
%
%
% Usage:
%   yi = winterp1(x0,dx,Y,xi)  
%           x0 - the waveform first x value
%           dx - the waveform x spacing
%   yi = winterp1(x,Y,xi,flag)
%           flag = 'nearest'      - Nearest-neighbor
%           flag = 'linear'       - Linear (default)
%           flag = 'pchip', 'spline', etc.  - Passed to interp1
%
% Example:
%   x0 = -5; dx = 0.01; x = x0:dx:-x0; y = exp(-x.^2/2);
%   xi = [-4.23:0.3:4.56];
%   yi = winterp1(x0,dx,y,xi,'linear');
%
% Usage restrictions
%
%    Y may be up to two-dimensional

% Author: N. Brahms
% Copyright 2012, 2006

% Forces vectors to be columns
xi = xi(:);
if isrow(Y); Y = Y(:); end
sY = size(Y);
nY = sY(1);

if nargin>=5
    method=methodflag;
else
    method = 'linear';  % choose nearest-lower-neighbor, linear, etc.
                        % uses integer over string for speed
end

% Pass more difficult interpolations to interp1
if ~( strcmp(method,'nearest') || strcmp(method,'linear') )
    Yi = interp1(x0:dx:x0+dx*(length(Y)-1),Y,xi,method);
    return
end

% Gets the x spacing
ndx = 1/dx;        % one over to perform divide only once
xi = xi - x0;      % subtract minimum of x

% Fills Yi with NaNs
s = size(Y);
if length(s)>2
    error('Y may only be one- or two-dimensional');
end
Yi = NaN(length(xi),s(2));

switch method
    case 'nearest' %nearest-neighbor method
        rxi = round(xi*ndx)+1;        % indices of nearest-neighbors
        flag = rxi<1 | rxi>nY | isnan(xi);
                                      % finds indices out of bounds
        nflag = ~flag;                % finds indices in bounds
        Yi(nflag,:) = Y(rxi(nflag),:);
    case 'linear' %linear interpolation method
        fxi = floor(xi*ndx)+1;          % indices of nearest-lower-neighbors
        flag = fxi<1 | fxi>nY-1 | isnan(xi);
                                        % finds indices out of bounds
        nflag = ~flag;                  % finds indices in bounds
        m1 = repmat((fxi(nflag)-xi(nflag)*ndx),1,size(Yi,2));
        m2 = repmat((1-fxi(nflag)+xi(nflag)*ndx),1,size(Yi,2));
        Yi(nflag,:) = m1.*Y(fxi(nflag),:) + m2.*Y(fxi(nflag)+1,:);
        
%         if s(1)>1 && s(2)>1
%             Yi(nflag,:) = repmat((fxi(nflag)-xi(nflag)*ndx),1,size(Yi,2))...
%                 .*Y(fxi(nflag),:)+...
%                 repmat((1-fxi(nflag)+xi(nflag)*ndx),1,size(Yi,2))...
%                 .*Y(fxi(nflag)+1,:);
%         else
%             Yi(nflag,:) = (fxi(nflag)-xi(nflag)*ndx).*Y(fxi(nflag),:)+...
%                 (1-fxi(nflag)+xi(nflag)*ndx).*Y(fxi(nflag)+1,:);
%         end
    otherwise
        error('winterp1:UnrecognizedMethod',...
            ['Can''t perform waveform interpolation using method ''',method,'''.']);
end