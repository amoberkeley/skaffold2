classdef DataModel < handle
    %Abstract base class for data models
    
    methods (Abstract)
        % Returns specified field from DataModel
        %
        % Throws a skaffold:AttributeError if the
        % field does not exist
        %
        % Usage:
        %   returns... = model.get(field)
        varargout = get(obj,field)
        
        % Sets the value of the specified DataModel field
        %
        % Usage:
        %   model.set(field,args...)        
        set(obj,field,varargin)
        
        % Removes the specified DataModel field.
        %
        % Usage:
        %   model.unset(field)
        unset(o,field)
        
        % Returns true if object has specified field
        %
        % Usage:
        %   test = model.has(field)
        result = has(obj,field)
                
        %Returns a cell array of this object's accessible fields
        %
        %Format:
        %    fieldList = fields(obj)
        %
        %Returns:
        %  fieldList - Cell array of this object's accessible fields
        fieldList = fields(obj)
    end    
    
    methods            
        function copy_fields(o, model, varargin)
            p = inputParser;
            p.addRequired('model',@(x) isa(x, 'DataModel'));
            p.addOptional('fields',{},@(x) isempty(x) || iscellstr(x));
            p.parse(model,varargin{:});
            r = p.Results;
            
            if isempty(r.fields)
                r.fields = model.fields();
            end
            
            for i = 1:length(r.fields)
                field = r.fields{i};
                o.set(field, model.get(field));
            end
        end
        
        function model = clone(o)
            % Returns a copy  of the entire data structure
            %
            % Usage:
            %   duplicate = model.clone()            
            error('skaffold:UsageError',...
                '%s does not support clone()', class(o));
        end

        function result = is_changed(~,field)
            % Returns true if specified field's value has changed
            %
            % Usage:
            %   result = model.is_changed(field) 
            result = true;
        end
        
        function clear_changed(~)
            % Clears changed indicator for all fields
            %
            % Usage:
            %   test = model.clear_changed()            
        end
                
        function sig = pop_signature(o)
            % Returns next Analyzer signature in sequence that was applied
            % to DataModel.  This is only implemented in DataScanFrame.
            sig = '';
        end
    end
end