function a = field2array(s, f)
%FIELD2ARRAY Convert struct array field to array with the same shape
%
%Usage:
%  a = field2array(s,f)
%
%Parameters:
%  s - Struct array
%  f - Field name

    ss = size(s);
    data = [s.(f)];
    a = reshape(data, ss);
end