classdef nlfit < dynamicprops
    %Performs scaled nonlinear regression of models to data.
    %
    %Automatically scales data and parameters for optimal fitting.
    
    properties (SetAccess = protected)
        model     % Model function handle
        result    % Result of fit
        residual  % Fit residuals
        jacobian  % Fit jacobian
        fitinfo   % Fit information structure
        warnings = 'on';  % If 'off', supresses warnings
    end
    
    methods        
        function ci = confint(o,confidence)
            %Calculates confidence interval of fit result
            if nargin==1
                confidence = 0.95;
            end
            if strcmp(o.fitinfo.function,'nlfit')
                if strcmp(o.warnings,'off'), warning off MATLAB:nearlySingularMatrix; end
                ci = o.scaled_ci(o.result,o.residual,o.jacobian,confidence);
                if strcmp(o.warnings,'off'), warning on MATLAB:nearlySingularMatrix; end
            else
                ci = nan(length(o.result),2);
            end
        end
        
        function y = of(o,x)
            %Returns function evaluated at given x data
            %
            %Usage:
            %  y = obj.of(x)
            fm = functions(o.model);
            if strcmp(fm.type,'anonymous')
                m = nlfit.parse_anonymous_args(o.model);
            else
                m = o.model; 
            end
            y = m(o.result,x);
        end
        
        function f = asfunction(o)
            %Returns best-fit model as a function handle.
            %
            %Usage:
            %  f = obj.asfunction()
            p = o.result;   nlfit_model = o.model;
            f = @(x) nlfit_model(p,x);
        end
        
        function disp(o)
            %Displays information regarding the fit 
            %
            %Display the model, fit parameters, and parameter uncertainties.
            so = size(o);
            fprintf('[%dx%d] nlfit object:\n\n',so(1),so(2))
            for i=1:so(1)
                for j=1:so(2)
                    fprintf('nlfit(%d,%d):  Fit to:\n    %s\n',i,j,...
                        func2str(o(i,j).model));
                    fprintf('  Parameters (with 95%% confidence interval):\n');
                    try %Object has dynamically assigned parameter values
                        v = o(i,j).parameters;
                        for k=1:length(v)
                            fprintf('    %s\t= %12.5g \x00B1 %11.5g\n',...
                                v{k},o(i,j).(v{k}),o(i,j).(['d' v{k}]));
                        end
                    catch err %#ok<NASGU> - Only unlabelled parameter vector
                        ci = o(i,j).confint();
                        for k=1:length(o(i,j).result)
                            fprintf('    p(%d)\t= %12.5g \x00B1 %11.5g\n',k,...
                                o(i,j).result(k),abs(diff(ci(k,:)))/2);
                        end
                    end % Try has parameters field
                    fprintf('\n');
                end % Loop over first dimension
            end % Loop over second dimension
        end
    end
    
    methods (Access = protected)
        function o = nlfit(varargin)
            %Create a new nlfit object.
            %
            %This constructor is protected.  Use nlfit.from(...) for
            %public construction.
            %
            %See also nlfit.from, nlfit.fit
            
            %Usage:
            %
            %    o = nlfit(model,result,residual,jacobian,covar,mse)
            %
            %Parameters:
            %  model - Fit model
            %  result - Vector of fit results
            %  residual - Fit residuals
            %  jacobian - Fit Jacobian
            %  fitinfo - Other information about fit
            
            %Build from model result
            o.model = varargin{1};
            o.result = varargin{2};
            o.residual = varargin{3};
            o.jacobian = varargin{4};
            o.fitinfo = varargin{5};
        end
        
%         function P = addprop(obj,prop)
%             P = addprop@dyanmicprops(obj,prop);
%         end
    end
    
    methods (Static)
        function [p,r,J,fitinfo,varargout] = fit(x,y,model,guess,varargin)
            %Performs a scaled nonlinear regression fitting of data
            %
            %Usage:
            %    [p,residual,jacobian,output] = fit(x,y,model,guess)
            %    ... = fit(...,'key',val,...)
            %    [...,y0,model0] = fit(...,'Offset','on')
            %
            %Parameters:
            %  x - x data to pass to fitting routine
            %  y - y data to pass to fitting routine
            %  model - Fitting model (see help for fitting routine)
            %  guess - Initial fit parameters.  In order for nlfit to scale
            %    parameters correctly, no element of guess should be equal
            %    to 0.
            %
            %Keys (first value is default):
            %  'Scale' - 'on' | 'off'
            %     If 'on', scale parameters and model data to the unit
            %     interval
            %  'Function' - 'nlfit' | 'lsqcurvefit'
            %     Fitting routine to use; confidence intervals are
            %     currently only available with nlfit
            %  'Options' - [] | Appropriate options struct
            %     Options struct for fitting routine
            %  'Offset' - 'off' | 'on'
            %     If 'on', centers y data, returning center value in return
            %     variable y0, and offset model in model0
            %  'Warn' - 'on' | 'off'
            %     If 'off', turns off all warnings before fitting, then
            %     turns on ALL warnings
            %
            %Returns:
            %  p - Fit parameters
            %  residual - Fit residuals
            %  jacobian - Jacobian matrix (gradient of y with respect to
            %    parameter vector at each point y_i)
            %  output - Fitting routine output:
            %    For nlfit, has following fields:
            %      covariance - Covariance matrix
            %      mse - Mean square error (analogous to reduced chi-square)
            %      function - 'nlfit'
            %    For lsqcurvefit, is returned 'lambda' field with
            %    additional fields:
            %      exitflag - lsqcurvefit exit flag
            %      mse - Mean square error
            %      function - 'lsqcurvefit'
            %  y0 - Value of scale y offset; only returned if 'Offset' is 
            %    'on'
            %  model0 - Model with y offset; only returned if 'Offset' is
            %    'on'
            
            %Input parsing
            parser = inputParser;
            parser.addParameter('Scale','on',@(x) any(strcmp(x,{'on' 'off'})) );
            parser.addParameter('Function','nlfit',...
                @(x) strcmp(x,'nlfit') || strcmp(x,'lsqcurvefit'));
            parser.addParameter('Options',[],...
                @(x) isempty(x) || isstruct(x));
            parser.addParameter('Offset','off',@(x) any(strcmp(x,{'on' 'off'})) );
            parser.addParameter('Warn','on',@(x) any(strcmp(x,{'on' 'off'})));
            parser.parse(varargin{:});     args = parser.Results;
            
            %Shaping
            guess = guess(:).';
            
            %Warnings
            if strcmp(args.Warn,'off')
                warning off %#ok<WNOFF> - Can't use id for all fit warnings
            end
            
            %Scaling
            if strcmp(args.Scale,'on')
                yScale = max(y)-min(y);     % yOff = mean(y);
                if any(guess==0)
                    warning('nlfit:ScaleWarning',...
                        'One or more initial model parameters are 0, initial value may be badly scaled.');
                end
                pScale = guess.*(guess~=0) + (guess==0);  %Scale 0 parameters by 1
                %Offsets
                if strcmp(args.Offset,'on')
                    y0 = mean(y);       y = y-y0;
                    model0 = @(p,x) model(p) + y0;
                    varargout{0} = y0;  varargout{1} = model0;
                end
            else
                yScale = 1;    pScale = ones(size(guess));
            end
            
            %Scaled fit model
            p0 = guess./pScale;
            f = @(p,x) model(p.*pScale,x)/yScale;
            
            %Perform fit
            if strcmp(args.Function,'nlfit')
                if isempty(args.Options)
                    [p,r,J,covar,mse] = nlinfit(x,y/yScale,f,p0);
                else
                    [p,r,J,covar,mse] = nlinfit(x,y/yScale,f,p0,args.Options);
                end
                % Jacobian is list of dy/dp, so scale by yScale./pScale for
                % each row
                J = J*yScale./repmat(pScale,numel(J)/length(pScale),1);
                % Covariance is matrix of p-p covariances, so scale elements
                % (i,j) by (pScale(i),pScale(j))
                covar = covar.*(pScale.'*pScale);
                mse = mse*yScale^2;
                fitinfo = struct('covariance',covar,'mse',mse,'function','nlfit');
            else
                if isempty(args.Options)
                    [p,mse,r,eflag,lambda,J] = lsqcurvefit(f,p0,x,y/yScale);
                else
                    [p,mse,r,eflag,lambda,J] = lsqcurvefit(f,p0,x,y/yScale,[],[],args.Options);
                end
                % Put in same form as nlfit mse
                mse = mse*yScale^2/(length(y)-length(p0));
                fitinfo = lambda;   fitinfo.exitflag = eflag;
                fitinfo.mse = mse;  fitinfo.function = 'lsqcurvefit';
            end
            
            if strcmp(args.Warn,'off')
                warning on %#ok<WNON> - Can't use id for all fit warnings
            end
            
            %Correct for scaling
            p = p.*pScale;
            r = r*yScale;
            
        end
        
        function obj = from(x,y,model,guess,varargin)
            %Fits data, returning a new nlfit object.
            %
            %Usage:
            %  obj = from(x,y,model,guess)
            %  obj = from(x,y,model,guess,'key',val,...)
            %
            %Parameters and keys are identical to those of nlfit.fit().
            %
            %See also nlfit.fit
            
            fdesc = functions(model);
            hasNamedVars = false;
            
            %Extract variable names from anonymous function
            if strcmp(fdesc.type,'anonymous')
                [f,vars] = nlfit.parse_anonymous_args(model);
                assert(~isempty(vars),'nlfit:InvalidArgument',...
                    'Anonymous function model must be of form @(param1,...,x).');
                if length(vars)==length(guess)
                    hasNamedVars = true;
                end % Otherwise function was vectorized
            else
                f = model;
            end
            
            %Perform fit
            [p,r,J,info] = nlfit.fit(x,y,f,guess,varargin{:});
            
            %Construct output object
            obj = nlfit(model,p,r,J,info);
            
            %Set warnings property
            wsi = find(cellfun(@(x) numel(x)==1,strfind(varargin(1:2:end),'warn')));
            if ~isempty(wsi), obj.warnings = varargin{wsi(1)+1}; end 
            
            %Assign variable fields if appropriate
            if hasNamedVars
                obj.addprop('parameters');  obj.parameters = vars;
                ci = obj.confint();
                for i=1:length(vars)
                    obj.addprop(vars{i});       obj.(vars{i}) = p(i);
                    obj.addprop(['d' vars{i}]); 
                    obj.(['d' vars{i}]) = abs(diff(ci(i,:)))/2;
                end
            end
        end
        
        function ci = scaled_ci(result,residual,jacobian,confidence)
            %Scaled call to nlparci
            
            %Scale confidence interval calculation to unity
            rScale = max(residual);                       %y-data scale
            pScale = repmat(result,length(residual),1);   %parameters scale
            R = residual./rScale;                         %rescaled residuals
            P = ones(size(result));                       %rescaled result
            J = jacobian.*pScale./rScale;
            ci = nlparci(P,R,'jacobian',J,'alpha',1-confidence);
            %Scale back into output units
            ci = ci.*repmat(result.',1,2);
        end
    end
    
    methods (Static, Access = private)
        function [fout,vars] = parse_anonymous_args(f)
            %Gets anonymous function variables and creates vectorized function.
            %
            %Takes an anonymous function of the form
            %  @(<v1>,<v2>,...,x) = <some function of <v1>,<v2>,...,x>,
            %where the <vi> are variables, and does two things.  First, it 
            %returns a vectorized function of the form:
            %  @(p,x) = <same function of p(1),p(2),...,x>
            %where p(1) corresponds to <v1>, etc.  Second, it returns a
            %cell array of the string names in the anonymous function.
            
            % Extract function variable
            s = func2str(f);
            i0 = strfind(s,'@(');                      %Find opening "@("
            iEnd = strfind(s,')');
            iSplit = strfind(s(1:iEnd(1)),',');        %Find variable separators
            vars = cell(1,length(iSplit));
            
            % Loop over anonymous function input parameters
            iSplit = [i0+1 iSplit];
            for j=1:length(iSplit)-1
                vars{j} = s(iSplit(j)+1:iSplit(j+1)-1);
            end
            
            if length(vars)==1
                % Function already parameterized by vector or only one
                % variable
                fout = f;
                return
            end
            
            % Construct vector-parameterized output function
            fouts = '@(p,x) f(';
            for j=1:length(vars)
                fouts = [fouts 'p(' num2str(j,0) '),']; %#ok<AGROW> - Prefer readability to speed
            end
            fouts = [fouts 'x);'];
            fout = eval(fouts);
        end
    end
    
end

