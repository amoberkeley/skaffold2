function h = annotate_data_shape(ax, type, pos, varargin)

    [figPos, visible] = pos_data2fig(ax, pos);
    
    h = annotation(ax.Parent,type,figPos,'Visible',visible,'Units','normalized',varargin{:});
    setappdata(h, 'Coords', pos);

    ls = getappdata(ax, 'DataShapeListener');
    if isempty(ls)
        ls = {};
        ls{1} = addlistener(ax,'XLim','PostSet',@resizeListener);
        ls{2} = addlistener(ax,'YLim','PostSet',@resizeListener);
        ls{3} = addlistener(ax,'Position','PostSet',@resizeListener);
%         ls{4} = addlistener(ax.Parent,'Position','PostSet',@resizeListener);
        setappdata(ax, 'DataShapeListener',ls);
    end
    objs = getappdata(ax, 'DataShapes');
    if isempty(objs)
        objs = [];
    end
    objs = [objs, h];
    setappdata(ax,'DataShapes',objs);  
end

function [figPos, visible] = pos_data2fig(ax, pos)
    % Normalize data units relative to axes
    xends = pos(1) + [0, pos(3)];
    yends = pos(2) + [0, pos(4)];

    xl = ax.XLim;
    yl = ax.YLim;    
    if ax.XAxis.Scale == "log"
        xends = log10(xends);
        xl = log10(xl);
    end
    if ax.YAxis.Scale == "log"
        yends = log10(yends);
        yl = log10(yl);
    end
    
    % Normalize data units relative to axes
    normX = (xends - xl(1))/(xl(2) - xl(1));
    normY = (yends - yl(1))/(yl(2) - yl(1));   
    
    oldUnits = ax.Units;
    ax.Units = 'normalized';
    figX = ax.Position(1) + normX * ax.Position(3);
    figY = ax.Position(2) + normY * ax.Position(4);
    visible = all(figX > ax.Position(1) & figX < ax.Position(1) + ax.Position(3))...
        & all(figY > ax.Position(2) & figY < ax.Position(2) + ax.Position(4));
    ax.Units = oldUnits;
    
    figX = max(0, min(figX, 1));
    figY = max(0, min(figY, 1));
    figPos = [figX(1), figY(1), figX(2) - figX(1), figY(2) - figY(1)];
end

function resizeListener(~,event)
    ax = event.AffectedObject;
    shapes = getappdata(ax, 'DataShapes');
    for idx=1:length(shapes)
        if ~ishandle(shapes)
            continue;
        end
        pos = getappdata(shapes(idx), 'Coords');
        [figPos, visible] = pos_data2fig(ax, pos);
        set(shapes(idx), 'Position', figPos);
        set(shapes(idx), 'Visible', visible);
    end
end