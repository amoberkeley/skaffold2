classdef Scanner < handle
    %Analyzes experimental parameter scans
    
    properties (Constant)
        SCAN_ALL = 0;
    end
    
    properties (SetAccess = protected)
        analyzer        % Analyzer to apply to each iteration of the scan
        nPoints
        nLoops
        maxLoops
        excludeFields
        showGui
        forceAnalysis
        
        nIterations     % Total number of scan iterations
        iteration = 1;  % Current iteration
        aggregator      % AggregatingAnalyzer to call each iteration
        
        aggregate
        loader
  
        state
        stateChanged
        storage
        aggregateFile
        
        restartEnabled
        wait
    end
    
    properties (Transient, SetAccess = protected)
        figureRegistry
        scan        
    end
    
    methods
        function o = Scanner(varargin)
            %Create a new Scanner.
            %
            %Usage:
            %    o = Scanner(analyzer,nIterations)
            %    o = Scanner(analyzer,nIterations,aggregator)
            %
            %Parameters:
            %  analyzer - The analyzer to apply each iteration of the scan.
            %  nIterations - Total number of times to run the analysis pattern.
            %  aggregator - Aggregator to apply each iteration.
                        
            p = inputParser;
            p.addRequired('analyzer', @(x) isa(x,'Analyzer'));
            p.addOptional('nPoints', 1, @(x) isscalar(x) && x > 0);
            p.addOptional('nLoops', 0, @(x) isscalar(x) && x >= 0);
            p.addOptional('aggregator', [], @(x) isempty(x) || isa(x,'Aggregator'))
            p.addParameter('loader',[],@(x) isempty(x) || isa(x, 'Loader'));
            p.addParameter('wait', false, @islogical);
            p.addParameter('storage','',@ischar);
            p.addParameter('state','',@ischar);
            p.addParameter('aggregateFile','',@ischar);
            p.addParameter('maxLoops', 0, @(x) isscalar(x) && x>=0);
            p.addParameter('exclude',{},@iscellstr);
            p.addParameter('showGui',true,@islogical);
            p.addParameter('forceAnalysis',false,@islogical);
            p.addParameter('clearStorage',false,@islogical);
            p.addParameter('precision', 'single', @(x) ismember(x, {'single','double'}));
            p.addParameter('restartEnabled',false,@islogical);
            p.parse(varargin{:});
            args = p.Results;
            
            o.analyzer = args.analyzer;
            o.nPoints = args.nPoints;
            o.nLoops = args.nLoops;

            o.state = args.state;
            o.storage = args.storage;
            o.aggregateFile = args.aggregateFile;
            
            o.aggregator = args.aggregator;
            o.loader = args.loader;

            o.wait = args.wait;
            o.maxLoops = args.maxLoops;
            o.excludeFields = args.exclude;
            o.showGui = args.showGui;
            o.forceAnalysis = args.forceAnalysis;
            
            o.restartEnabled = args.restartEnabled;

            if ~isempty(o.storage)
                if args.clearStorage
                    DataScan.delete_scan(o.storage);
                end
                
                o.scan = DataScan(o.storage, o.nPoints, 'maxLoops', o.maxLoops, 'exclude', o.excludeFields, 'precision', args.precision);
                
                if ~isempty(o.loader)
                    triggers = o.scan.get_loader_data('trigger_status');
                    o.loader.set_trigger_status(triggers);
                end
            else
                o.scan = [];
            end
            
            o.aggregate = DataFrame();
            o.aggregate.set('nPoints',o.nPoints);
            o.nIterations = o.nPoints * o.nLoops;
            
            o.figureRegistry = containers.Map;
            
            global scanner
            scanner = o;
        end

        function [res, aggregate, scan] = run(o)
            %Executes analysis pattern and aggregating analyzer.
            %then increments iteration by one    
            
            res = Result.SUCCESS;
            o.stateChanged = 1;
            
            forceLoad = {};
            
            if ~isempty(o.scan)                
                % Collect cell array of analyzer signatures, and register them
                % in the DataScan.  The returned key will be set in each
                % DataFrame, to indicate which set of analyzers were last
                % run on it.
                signatures = o.analyzer.collect_signatures();    
                analysisKey = o.scan.register_signatures(signatures);
            else
                analysisKey = [];
            end
            
            % Global interrupt flag, used by GUI to signal Scanner to stop.
            global skaffoldInterrupt skaffoldRestart skaffoldSave
            skaffoldInterrupt = false;
            skaffoldRestart = false;
            skaffoldSave = false;
            if o.showGui
                % Create GUI with stop button, which sets interrupt flag
                if o.restartEnabled
                    gui = restartGui();
                else
                    gui = stopGui();
                end
            else
                gui = [];
            end
            
            while o.nIterations == o.SCAN_ALL || o.iteration < o.nIterations+1                

                [loop, point] = o.get_loop_point();                
                
                if ~isempty(o.loader) && o.wait && ~o.loader.has_data() && ~skaffoldInterrupt
                    % If loader is set, and currently out of data, flush
                    % the scan file to disk, then wait for new data.
                    
                    o.flush_state();
                    
                    fprintf('Waiting for data... ');
                    
                    while ~o.loader.has_data() && ~skaffoldInterrupt
                        pause(0.1);
                    end
                elseif skaffoldSave
                    fprintf('Saving state... \n');
                    o.flush_state();
                end
                
                % Check for GUI interrupt signal
                if skaffoldInterrupt
                    fprintf('Interrupted\n');
                    break;
                end
                
                if skaffoldRestart && o.restartEnabled
                    o.iteration = 1;
                    o.aggregate = DataFrame();
                    [loop, point] = deal(1);
                    close all
                    skaffoldRestart = false;
                end
                
                fprintf('processing file ''%s'' (loop %d, point %d)\n', o.loader.get_source(), loop, point);
                
                if ~isempty(o.scan) && o.scan.has_frame(loop, point)
                    % Load DataFrame from DataScan, if exists      
                    model = o.scan.read_frame(loop, point);
                    for k=1:length(forceLoad)
                        % Reset fields marked for 'forceLoad' to empty, setting the changed flag in the
                        % DataModel, which analyze.Load checks to force reloading raw data.
                        model.set(forceLoad{k}, []);
                    end
                else
                    % Start with empty DataFrame
                    model = DataFrame();
                end         
                
                if ~isempty(o.loader)
                    % Check all triggers in this iteration to exclude extra
                    % ones, and abort if one is missing.
                    res = o.loader.preload_iteration();
                else
                    res = Result.SUCCESS; 
                end
                
                if res == Result.SUCCESS
                    % Run analyzers on the model
                    try
                        res = o.analyzer.run(model, loop, point, 'force', o.forceAnalysis);
                    catch e
                        switch e.identifier
                            case 'skaffold:ExcludedField'
                                % Catch exception thrown when the analysis tries to access a field of the
                                % DataScanFrame which was excluded from the DataScan.
                                if ismember(forceLoad, e.message) % If we already tried to force loading, pass error upstream
                                    rethrow(e)
                                else
                                    fprintf('Analysis needs ''%s'' excluded from DataScan, attempting to reload...\n', e.message);
                                    forceLoad = [forceLoad, {e.message}];
                                    if ~isempty(o.loader) % Reset loader to try again
                                        o.loader.reset_iteration();
                                    end                                    
                                    continue
                                end
                            otherwise
                                rethrow(e)
                        end                        
                    end
                end

                switch res
                    case Result.SUCCESS
                        if ~isempty(o.aggregator)
                            % Run aggregator on analyzed DataModel
                            o.aggregator.run(o.aggregate, loop, point, model);
                        end
                        if ~isempty(o.scan)
                           % Write changes to the DataScan database
                           o.scan.write_frame(loop, point, model, analysisKey);                            
                        end
                        
                    case Result.SKIP                                        
                        if ~isempty(o.aggregator)
                            % Give aggregators a chance to cleanup, if
                            % skipping this iteration
                            o.aggregator.skip(o.aggregate, loop, point);
                        end
                        if ~isempty(o.scan)
                           % Write any changes back to the DataScan
                           o.scan.write_frame(loop, point, model);                            
                        end
                        % reset result, in case this is the last iteration
                        res = Result.SUCCESS; 

                    case Result.ABORT
                        if ~isempty(o.aggregator)
                            % Give aggregators a chance to cleanup, if
                            % skipping this iteration
                            o.aggregator.skip(o.aggregate, loop, point);
                        end
                        if ~isempty(o.scan)
                            o.scan.clear_frame(loop,point);
                        end
                        res = Result.SUCCESS; 
                        
                    case Result.FAIL
                        fprintf('Fatal error occured during Analysis\n');
                        break
                        
                    case Result.OUT_OF_DATA
                        if o.wait
%                             warning('Missing file, trying again in 0.1 s...');
                            pause(0.1);                            
                            if ~isempty(o.loader)
                                % Ensure loader is reset to first trigger
                                % in iteration
                                o.loader.reset_iteration();
                            end                            
                            continue
                        else
                            break
                        end
                end
                
                if ~isempty(o.loader)
                    % Ensure loader is advanced to first trigger of next
                    % iteration
                    o.loader.advance_iteration();
                end
                
                o.iteration = o.iteration + 1;
                drawnow;
                o.stateChanged = 1;
            end            
            
            if o.showGui && ishandle(gui)
                % Close 'stop' GUI
                close(gui);
            end
            
            o.flush_state();
            
            aggregate = o.aggregate;
            scan = o.scan;
        end

        function reset(o)
            %Reset scan to 1st iteration; also resets aggregation
            o.iteration = 1;
            o.aggregate = DataFrame();
            if ~isempty(o.loader)
                o.loader.reset();
            end
        end
        
        function agg = get_aggregate(o)
            agg = o.aggregate;
        end
        
        function scan = get_scan(o)
            scan = o.scan;
        end
        
        function [loop, point] = get_loop_point(o)
            loop = floor((o.iteration-1)/o.nPoints) + 1;
            point = mod((o.iteration-1),o.nPoints) + 1;
        end
        
        function it = get_iteration(o)
            it = o.iteration;
        end
        
        function set_n_loops(o, n)
            %Set the total number of loops and iterations
            assert(isscalar(n) && n >= 0,'skaffold:ArgumentError',...
                'Can only set number of loops to a positive integer');
            o.nLoops = n;
            o.nIterations = o.nPoints * o.nLoops;
        end
        
        function loader = get_loader(o)
            loader = o.loader;
        end
        
        function save_state(o, filename)
            save(filename, 'o','-v7'); 
        end
        
        function save_figures(o)
            paths = o.figureRegistry.keys();
            for i=1:length(paths)
                hFig = o.figureRegistry(paths{i});
                if ~isempty(paths{i}) && ishandle(hFig)
                    [~, ~, ext] = fileparts(paths{i});
                    ext = ext(2:end);
                    if isempty(ext); ext = 'png'; end
                    saveas(hFig, paths{i}, ext);
                else
                    o.figureRegistry.remove(paths{i});
                end
            end
        end
        
        function register_save(o, hFig, path)
             o.figureRegistry(path) = hFig;
        end
    end        
     
    methods (Access=protected)
        function flush_state(o)
            global skaffoldSave;
            if ~isempty(o.scan)
                if ~isempty(o.loader)
                    % Save status of triggers to scan, so future analysis
                    % can be run without needing access to raw data files
                    triggers = o.loader.get_trigger_status();
                    o.scan.set_loader_data('trigger_status', triggers);
                end

                % Flush any changes in DataScan to disk
                o.scan.flush();
            end
            
            if ~o.stateChanged
                return
            end
            o.stateChanged = 0;
            
            try
                o.save_figures();
            catch e
                fprintf('Failed saving figures: %s\n', e.message);
            end

            % Save state of Scanner
            if ~isempty(o.state)
                o.save_state(o.state);
            end
            
            % Save aggregated data to file
            if ~isempty(o.aggregateFile)
                try
                    o.aggregate.save(o.aggregateFile);
                catch e
                    fprintf('Failed saving aggregate: %s\n', e.message);
                end
            end
            
            skaffoldSave = false;
        end
    end
    
    methods (Static)
        function s = load_state(filename, storage)
            data = load(filename);          
            s = data.o;
            
            if nargin > 1
                s.storage = storage;
            end
            
            if ~isempty(s.storage)
                s.scan = DataScan(s.storage, s.nPoints, 'maxLoops', s.maxLoops, 'exclude', s.excludeFields);
            else
                s.scan = [];
            end
            s.figureRegistry = containers.Map;
            global scanner;            
            scanner = s;
        end
    end
end

