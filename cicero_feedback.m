function cicero_feedback(varargin)
    p = inputParser;
    p.addParamValue('pathname','', @ischar);
    p.addParamValue('pause',1,@(x) isscalar(x) && isreal(x) && x > 0);
    p.addParamValue('stateFile','cicero_feedback_state.mat', @ischar);
    p.addParamValue('overrideFile','C:\Documents and Settings\E3\My Documents\cicero_override.txt', @ischar);
    p.addParamValue('dataRoot','E:\Data\', @ischar);
    p.addParamValue('analysisRoot','E:\Analysis\', @ischar);
    p.addParamValue('logFile','%s_feedback.log', @ischar);
    p.addParamValue('plotFile','%s_feedback.png', @ischar);
    p.addParamValue('wwwPlotFile','Z:\E3\www\cicero_feedback.png', @ischar);
    p.addParamValue('showGui',true,@islogical);
    p.parse(varargin{:});
    r = p.Results;
    
%% Control parameters

    % Manually set pathname (comment out for prompt)
    date = '2015/May/21';
    run = 'larmor_resonance3';
    r.pathname = e3.get_data_path(date, run);
    
    % Resets skaffold analyzers, and triggers reanalysis of most recent
    % 'startLoops' loops.  Necessary for any changes to sideband fit
    % parameters to take effect.
    resetAnalyzers = 0;
    
    % Resets PID loops. Happens automatically when data path changes.  
    % Otherwise, can tune all lock parameters without resetting.
    resetLoops = 0;
       
%% Lock Parameters
    deltaPcEnable = true;
    deltaPcParameters = {...
            'setPoint', 0e6,...0e3,...%1000e3,... %1250e3, ...
            'initial', -0.1502,...%-0.0586,...%-0.5160,... %-0.7599, ...
            'range', [-1, 1],...
            'gain', 0.2 * 0.3604e-6, ...
            'tInt', 20};
                
    sidebandEnable = false;
    sidebandFmParameters = {...
            'setPoint', 129e3, ...
            'initial', 5.9103, ...%6.3428, ...
            'range', 6 + [-0.8, 0.8],...
            'gain', 0.1 * -0.0758e-3, ...%0.18 * -0.0758e-3, ...
            'tInt', 30};
        
%% Sideband fit parameters
    sidebandAverage = 20; % number of previous shots to use in weighted, moving average
    minAvg = 5; % Minimum number of interations to average for sideband fit
    sidebandGuessFm = 129e3;
    sidebandGuessGm = 4e3;
    sidebandRange = sidebandGuessFm + [-5e3, 5e3]; % frequency range to use in sideband fit.  Should exclude other oscillators
    plotRange = sidebandGuessFm + [-22e3, 22e3];
    driveFreq = 139e3;
    driveRep = 1 / 2e-3;

    suppressDrive = [];%suppress_sidebands(driveFreq, driveRep, sidebandRange);
   
    edet = .080;
    nTriggers = 2;
    triggerPattern = [31, 1];
    nPoints = 1; % Doesn't currently support nPoints ~= 1
    startLoops = sidebandAverage;
    threshold = 0.3;
    
    tLoad = [0, 0.029];
    tDeltaPC = [0.001, 0.020];
    tSidebandFm = [0.0065, 0.0115];
        
%% Loop and Analyzer Methods
    function pids = initialize_loops()
        % Create new FeedbackLoop objects
        pids = struct('deltaPc',0,'sidebandFm',0);
        if deltaPcEnable
            pids.deltaPc = FeedbackLoop('deltaPc','slNbar',...
                deltaPcParameters{:},...
                'tSample',30,... % Sampling interval (sequences length) do not change
                'tBW', 240);
        end
        
        if sidebandEnable
            pids.sidebandFm = FeedbackLoop('sidebandFm','odtPwr',...
                sidebandFmParameters{:},...
                'tSample',30,... % Sampling interval (sequences length) do not change
                'tBW', 300);
        end
    end
    

    function tune_loops()
        % Update feedback parameters for FeedbackLoops, without reseting
        % history
        if deltaPcEnable
            state.pids(1).deltaPc.tune(deltaPcParameters{:});
        end
        if sidebandEnable
            state.pids(1).sidebandFm.tune(sidebandFmParameters{:});
        end
    end
    
    function [scanner, gsl] = build_analyzers(filenumber, skipFiles)
        % Construct analyzers
        gsl = loader.Gagescope(state.pathname, filenumber,'dataRate',80e6,'skip', skipFiles,...
            'defaultRange',tLoad,'triggerPattern',triggerPattern);

        analyzer = analyze.Block({...
            analyze.Load(gsl, {'raw', 'source'}, {'channel', 1}, 'increment', nTriggers),...
            analyze.RescaleHeterodyne('raw', 'transimpedance', 28.8e3, 'gain', 30.1, 'impedance',1e6),...
            analyze.FFT('raw','rawFft','range', tDeltaPC, 'type', 'powerDensity'),...
            analyze.HetModSidelock('rawFft',{'deltaPc','nBar','sn'},...
                'fCar',10e6,'fMod',2.87e6,'kappa',2.15e6,'kappaN',1.82e6,'rbGain',1.14,...
                'snRange',[3.5e5 4.5e5],'edet',edet,'close',true),...
        });
    
        if sidebandEnable
            analyzer.add(analyze.Heterodyne('raw','carrierFreq',10e6,'filterBw',10e3));
            analyzer.add(analyze.FFT('qQuad','qQuadPower','range',tSidebandFm,'type','powerDensity'));
            analyzer.add(analyze.FFT('iQuad','iQuadPower','range',tSidebandFm,'type','powerDensity'));            
        end

        verifiers = {...
            verify.Range('nBar',[0.5,100]),...
        };            

        aggregator = aggregate.Block({...
            aggregate.Verifier('valid',verifiers),...
            aggregate.Collect({'deltaPc'},'if','valid'),...
        });    
    
        if sidebandEnable
            aggregator.add(aggregate.Memory('qQuadPower','qQuadPowerCache', sidebandAverage,'if','valid'));
            aggregator.add(analyze.FitSidebands('qQuadPowerCache',{'sidebandFm','sidebandGm','sidebandR2'},'threshold',threshold,...
                'guessFm',sidebandGuessFm,'guessGm',sidebandGuessGm,'range',sidebandRange,...
                'plot',true,'plotRange',plotRange,'suppressDrive',suppressDrive,...
                'method','adaptive','minAvg',minAvg,'if','valid'));
            aggregator.add(aggregate.Collect({'sidebandFm','sidebandR2'},'if','valid'));
        end
    
        scanner = Scanner(analyzer, nPoints, Scanner.SCAN_ALL, aggregator, 'loader', gsl, 'showGui', false);
    end

    function suppress = suppress_sidebands(driveFreq, driveRep, sidebandRange)
        suppress = driveFreq + [-50, 50];  % on-resonance drive frequency band to be suppressed in fits
        nSuppress = floor(diff(sidebandRange)/(2*driveRep));
        for index=1:nSuppress
           suppress = [
                suppress;...
                driveFreq + index*driveRep + [-50, 50];...% drive repitition sidebands to be suppressed in fits
                driveFreq - index*driveRep + [-50, 50];...           
            ];
        end        
    end
    
%% Initialize UI
    global skaffoldInterrupt
    skaffoldInterrupt = false;
    inLoop = true;
    
    if r.showGui
        % Create GUI with stop button, which sets interrupt flag
        gui = stopGui();
    else
        gui = [];
    end

    % Initiailze state variables
    % State variables
    state = struct('pathname','');
    
%% BEGIN MAIN SCRIPT
    
    % Load state from stateFile
    if exist(r.stateFile,'file')
        state = load(r.stateFile);
    end
         
    % Prompt user for data path
    if isempty(r.pathname)
        % Use path dialog to get pathname
        pathname = pathname_dialog(state.pathname);
        if ~ischar(pathname) || isempty(pathname)
            return;
        end
    else
        pathname = r.pathname;
    end
    
    % If selected pathname differs from saved state, force reset
    if ~strcmp(pathname, state.pathname)
        resetAnalyzers = 1;
        resetLoops = 1;
        state.pathname = pathname;
        
        % Split parts of data path, and build target paths
        parts = regexpi(state.pathname, ['^(.+)\' filesep '(.+)_CH(\d+)\' filesep '?'], 'tokens');
        state.dataPath = parts{1}{1};
        state.run = parts{1}{2};

        state.analysisPath = strrep(state.dataPath, r.dataRoot, r.analysisRoot);
        % Auto-create analysis directory, if it doesn't exist
        if exist(state.analysisPath, 'dir') ~= 7
            mkdir(state.analysisPath);
        end        
        
        state.plotFile = fullfile(state.analysisPath, sprintf(r.plotFile, state.run));       
        state.logFile = fullfile(state.dataPath, sprintf(r.logFile, state.run));
    end

    % Initialize new PID loops, or tune existing ones if no reset
    if resetLoops || ~isfield(state, 'pids') || isempty(state.pids)
        state.pids = initialize_loops();        
    else
        tune_loops();
    end
    
    if resetAnalyzers || ~isfield(state, 'scanner') || isempty(state.scanner)
        % Only read most recent 10 files, if initializing new scanner
        currentFile = e3.count_sources(state.pathname);
        state.startFile = max(0, currentFile - nPoints*nTriggers*startLoops);
            
        fprintf('Starting at file AS_CH01-%05d.sig\n', state.startFile+1);
        
        numFiles = currentFile - state.startFile;
        nLoops = floor(numFiles/(nPoints*nTriggers));
        fprintf('Processing previous %d loops\n', nLoops);
        
        [state.scanner, state.gsl] = build_analyzers(state.startFile+1, []);
        state.scanner.run();        
        state.lastFile = state.gsl.fileNumber;
        
        update_locks();
        
        save_state();
    end
           
    update_plots();
    
%% Main Program loop    
    fprintf('Entering main loop\n');
       
    while ~skaffoldInterrupt
        if ~state.gsl.has_data()
            % If loader is set, and currently out of data, flush
            % the scan file to disk, then wait for new data.
            fprintf('Waiting for data... ');

            % Save state of Scanner
            save_state();
            
            while ~state.gsl.has_data() && ~skaffoldInterrupt
                pause(r.pause);
            end
            fprintf('processing file ''%s''\n', state.gsl.get_source());
        end
        
        if skaffoldInterrupt
            fprintf('Interrupted\n');
            break;
        end

        state.scanner.run();

        state.lastFile = state.gsl.fileNumber;

        if inLoop
            update_locks();
        end

        update_plots();

        save_state();
    end
    
%     if ishandle(gui)
%         % Close 'stop' GUI
%         close(gui);
%     end
    
%% Methods    
    
    function update_locks()
        agg = state.scanner.get_aggregate();
        iteration = floor(state.lastFile/nTriggers);
        logData = [];
        
%         names = fieldnames(state.pids);
%         for i = 1:length(names)
%             lockName = names{i};
%             lock = state.pids.(lockName);
%         
%             values = agg.get(lock.get_input_name());
%             output = lock.update(values, iteration);
%             fprintf('%2 = %0.4f\n', lock.get_output_name(), output);            
%             cicero_override(lock.get_output_name(), output);
%             logData = [logData, values(end), output];
%         end

        if deltaPcEnable
            lockName = 'deltaPc';
            lock = state.pids.(lockName);

            values = agg.get(lock.get_input_name());
            output = lock.update(values, iteration);
            fprintf('%s = %0.4f, %s = %0.4f\n', lock.get_input_name(), values(end), lock.get_output_name(), output);            
            cicero_override(lock.get_output_name(), output);
            logData = [logData, values(end), output];
        end
         
        if sidebandEnable
            lockName = 'sidebandFm';
            lock = state.pids.(lockName);

            values = agg.get(lock.get_input_name());       
            r2 = agg.get('sidebandR2');
            output = lock.update(values, iteration);
            fprintf('%s = %0.4f, %s = %0.4f, %s = %0.4f\n', lock.get_input_name(), values(end), 'sidebandR2', r2(end), lock.get_output_name(), output);            
            cicero_override(lock.get_output_name(), output);   
            logData = [logData, values(end), output];
        end
        
        write_log(iteration, logData);
    end

    function fid = init_log()
        % Create log file, and write header to first line
        fid = fopen(state.logFile, 'a+t');
        if (fid >= 0) 
            fprintf(fid, 'timestamp,shot');

            names = fieldnames(state.pids);
            for i = 1:length(names)
                lock = state.pids.(names{i});
                if lock == 0
                    continue;
                end
                fprintf(fid, ',%s,%s', lock.get_input_name(), lock.get_output_name());
            end
            fprintf(fid, '\n');
        end
    end

    function write_log(shot, logData)
        % Write data point to log file
        try
            if isempty(state.logFile)
                return;
            end;

            if exist(state.logFile, 'file') ~= 2
                % If no log file, write header first
                fid = init_log();
            else
                fid = fopen(state.logFile, 'a+t');
            end

            if (fid >= 0)
                timestamp = datestr(clock, 31);
                logText = sprintf(',%.4g',logData);
                fprintf(fid,'''%s'',%d%s\n', timestamp, shot, logText);
                fclose(fid);
            end        
        catch e
            fprintf('Error writing log: %s', e.getReport());
        end        
    end

    function update_plots()
        h = ufigure('Feedback plots');
        names = fieldnames(state.pids);
        for i = 1:length(names)
            lockName = names{i};
            lock = state.pids.(lockName);
            if lock == 0
                continue;
            end
        
            subplot(length(names), 1, i);
            input = lock.get_input();     
            output = lock.get_output();    
            if isempty(input)
                continue;
            end
            
            [haxes,hLine1,hLine2] = plotyy(1:length(input),input,1:length(output),output,'plot','plot');
            set(hLine1,'Marker','.')
            set(hLine1,'LineStyle','--')
            set(hLine2,'Marker','.')
            set(hLine2,'LineStyle','--')
            ylabel(haxes(1), lock.get_input_name()) % label left y-axis
            ylabel(haxes(2), lock.get_output_name()) % label right y-axis
            xlabel(haxes(2), 'Iteration') % label x-axis
        end
        
        if ~isempty(state.plotFile)
            try
                saveas(h, state.plotFile, 'png');
            catch e
                fprintf('Error saving plot: %s', e.getReport());
            end            
        end

        if ~isempty(r.wwwPlotFile)
            try
                saveas(h, r.wwwPlotFile, 'png');
            catch e
                fprintf('Error saving plot: %s', e.getReport());
            end
        end        
    end

    function pathname = pathname_dialog(pathname)
        % Prompt user for data path
        if isempty(pathname)
            pathname = r.dataRoot;
        end
        
        [f,pathname] = uigetfile(['*','.sig'],'Choose GageScope data folder', [pathname, '']);
        
        if ischar(f)
            return;
        else % Aborted UI
            pathname = false;
        end
    end

    function save_state()
        % Save script state to file
        save(r.stateFile, '-struct', 'state');
    end

    function cicero_override(name, value)
        % Write name = value update to cicero override file
        fid = fopen(r.overrideFile, 'at+');
        if (fid >= 0)
            fprintf(fid, '%s = %0.4f\n', name, value);
            fclose(fid);
        end
    end
end