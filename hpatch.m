function hPatches=hpatch(varargin)
%Shortcut to plot hoizontal patch(es). Automatically resizes patches to match 
% x-limits of the axes.
%
%Usage:
%  hPatches = hpatch([ax],y,...)
%
%Parameters:
%  ax - Optional axis handle, if not provided defaults to current axis.
%  y - vector or matrix of y-value(s)
%  ... - All additional parameters will be passed directly to 'plot'
%
%Returns:
%  hPatches - Handles of created patches

    [ax,args,nargs] = axescheck(varargin{:});
    if isempty(ax)
        ax = gca();
    end
     
    y = args{1};
    if nargs > 1
        opts = args(2:end);
    else
        opts = {};
    end
    
    wasHeld=ishold(ax);
    hold(ax,'on');

    x=get(ax,'XLim');
    
    for idx=1:size(y,1)
        h(idx)=patch([x,fliplr(x)], y(idx,[1 1 2 2]), opts{:},'Parent',ax);
    end

    if ~wasHeld
        hold(ax,'off')
    end
    
%     set(h,'handlevisibility','off')
   
    listener = getappdata(ax, 'HPatchListener');
    if isempty(listener)
        listener = addlistener(ax,'XLim','PostSet',@xLimListener);
        setappdata(ax, 'HPatchListener',listener);
    end
    patches = getappdata(ax, 'HPatches');
    if isempty(patches)
        patches = [];
    end
    patches = [patches, h];
    setappdata(ax,'HPatches',patches);  
    
    if nargout
        hPatches=h;
    end
end

function xLimListener(~,event)
    ax = event.AffectedObject;
    xl = get(ax, 'XLim');
    patches = getappdata(ax, 'HPatches');
    invalid = false(size(patches));
    for idx=1:length(patches)
        if ~ishandle(patches(idx))
            invalid(idx) = true;
            continue;
        end
        vert = get(patches(idx), 'Vertices');
        vert(:,1) = [xl, fliplr(xl)];
        set(patches(idx), 'Vertices', vert);
    end
    setappdata(ax, 'HPatches', patches(~invalid));
end
