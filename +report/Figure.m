classdef Figure < Reporter
    % Report that creates subplots for each of it's sub
    
    properties (SetAccess = protected)
        name            % Name of figure on which to plot.        
        elements       % Array of PlotReporter objects for each subplot
        locations
        rows
        cols
        update
        save
        raise
        linkx
                
        axesProperties      % Axes properties
        figureProperties    % Figure properties        
    end
    
    properties (Transient,SetAccess = protected)
        hAxes
        hFigure
        raiseStart = false
    end
    
    methods
        function o = Figure(name,varargin)
            %Construct a new Figure reporter
            %
            %Usage:
            %   o = report.Figure(name, subplots);
            %
            %Parameters:
            %  name - Name of figure.
            %  subplots - An array of PlotReporter objects for each
            %    subplot.  Note that PlotReporters must NOT change the axes
            %    from the current axes in order for this implementation to
            %    work.  The default PlotReporter follows this contract.

            p = inputParser;
            p.addRequired('name',@(x) ischar(x) && ~isempty(x));
            p.addRequired('elements',@(x) iscell(x) && all( cellfun(@(y) isempty(y) || isa(y,'Reporter'),x(:)) ) );
            p.addParameter('dimensions',[],@(x) isnumeric(x) && numel(x)==0 || numel(x)==2);
            p.addParameter('locations',{},@(x) iscell(x) &&...
                all(cellfun(@isnumeric,x)));
            p.addParameter('figureProperties',{},@isparamval);
            p.addParameter('axesProperties',{},@isparamval);
            p.addParameter('update',false,@islogical);
            p.addParameter('save',{},@(x) ischar(x) || iscellstr(x));
            p.addParameter('raise','off',@(x) ismember(x,{'off','start','update'}));
            p.addParameter('linkx',{},@(x) iscell(x) || isnumeric(x) || strcmp(x, 'all'));
            p.parse(name,varargin{:});
            args = p.Results;
            
            o.elements = args.elements;
            o.update = args.update;
            
            o.set_name(args.name);
            
            if isempty(args.dimensions)
                dims = size(o.elements);
                o.rows = dims(1);
                o.cols = dims(2);
            else
                o.rows = args.dimensions(1);
                o.cols = args.dimensions(2);
            end
            
            if isempty(args.locations)
                o.locations = num2cell(1:numel(o.elements));
            else
                o.locations = args.locations;
            end
            
            if ischar(args.save)
                o.save = {args.save};
            else
                o.save = args.save;
            end
            
            o.figureProperties = [report.Plot.FIGURE_PROPERTIES, args.figureProperties];
            o.axesProperties = [report.Plot.AXES_PROPERTIES, args.axesProperties];

            o.raise = args.raise;
            
            o.linkx = args.linkx;
            
            if strcmp(o.linkx,'all')
                o.linkx = 1:numel(o.elements);
            end
            
            if ~iscell(o.linkx)
                o.linkx = {o.linkx};
            end
        end
                
        function set_name(o,name)
            o.name = name;
            
            for index = 1:length(o.elements)
                if ismethod(o.elements{index}, 'set_name')
                    o.elements{index}.set_name(o.name);
                end
            end            
        end
        
        function find_figure(o)
            if ~isempty(o.hFigure) && ishandle(o.hFigure)
               return
            end
            o.hFigure = findfigure(o.name, o.figureProperties{:});
            clf(o.hFigure);
        end
        
        function result = run(o, model, varargin)
            %Applies each PlotReporter in its appropriate subplot.
            p = inputParser();
            p.KeepUnmatched = true;
            p.addOptional('loop',0,@(x) isnumeric(x) && isscalar(x));
            p.addOptional('point',0,@(x) isnumeric(x) && isscalar(x));
            p.addOptional('frame',[],@(x) isempty(x) || isa(x, 'DataModel'));
            p.parse(varargin{:});
            args = p.Results;
            
            o.find_figure();
            if strcmp(o.raise,'update') || (strcmp(o.raise,'start') && ~o.raiseStart)
                figure(o.hFigure);
                o.raiseStart = true;
            end            

            for index=1:numel(o.elements)
                if isempty(o.elements{index})
                    continue
                end
                
                if length(o.hAxes) < index || ~ishandle(o.hAxes(index))
                    o.hAxes(index) = subplot(o.cols, o.rows, o.locations{index}, 'Parent', o.hFigure);          
                    
                    if ~isempty(o.axesProperties)
                        set(o.hAxes(index), o.axesProperties{:});
                    end                    
                end
                
                o.elements{index}.run(model, varargin{:}, 'axes', o.hAxes(index));
            end
            
            for iAxis = 1:length(o.linkx)
                axes = o.linkx{iAxis};
                linkaxes(o.hAxes(axes), 'x');
            end
            
            %Update plot if requested
            if o.update
                drawnow;
            end          
            
            global scanner;
            if ~isempty(o.save) && ~isempty(scanner)
                for index=1:length(o.save)
                    scanner.register_save(o.hFigure, o.save{index});
                end
            end
            
            result = Result.SUCCESS;
        end
    end
end

