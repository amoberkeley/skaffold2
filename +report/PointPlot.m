classdef PointPlot < Reporter
    % Reporter implementation for plotting DataModel fields.
    
    properties (Constant)
        FIGURE_PROPERTIES = {'Color',[0.9 0.9 1],'PaperUnits','inches'};
        AXES_PROPERTIES = {'FontSize',11,'Box','on','NextPlot','add'};        
        LINE_PROPERTIES = {'Marker','.','LineStyle','none','MarkerSize',12};        
        MEAN_PROPERTIES = {'Marker','.','LineStyle','none','MarkerSize',12,'Color','k'};
        LABEL_PROPERTIES = {'FontSize',11};
    end
    
    properties (SetAccess = protected)
        name
        yField
        xValues
        complex
        
        xLabel              % x-label text or ''
        yLabel              % y-label text or ''

        axesProperties      % Axes properties
        figureProperties    % Figure properties
        lineProperties      % Line properties
        meanProperties
        xLabelProperties    % xLabel properties
        yLabelProperties    % yLabel properties        
        axisCommand         % Either numeric array or valid string command to axis()
        
        plotMean
        plotOnlyMean
        
        update
    end
        
    properties (Transient,SetAccess = protected)
        hAxes
        hLine
        hMean
    end
    
    methods
        function o = PointPlot(yField,varargin)
            p = inputParser;
            p.addRequired('yField',@ischar)
            p.addParameter('xValues',[],@(x) isnumeric(x) && isvector(x));
            p.addParameter('complex',[],@(x) isempty(x) || ismember(x,{'mag','phase'}));
            p.addParameter('name','',@ischar);
            p.addParameter('yLim', [],@(x) isnumeric(x) && isvector(x));
            p.addParameter('xLim', [],@(x) isnumeric(x) && isvector(x));
            p.addParameter('xLabel','Point',@ischar)
            p.addParameter('yLabel','',@ischar)
            p.addParameter('plotMean',false,@(x) islogical(x) || ischar(x));
            p.addParameter('plotOnlyMean',false,@(x) islogical(x) || ischar(x));
            p.addParameter('figureProperties',{},@isparamval);
            p.addParameter('axesProperties',{},@isparamval);
            p.addParameter('labelProperties',{},@isparamval);
            p.addParameter('lineProperties',{},@isparamval);
            p.addParameter('meanProperties',{},@isparamval);
            p.addParameter('xLabelProperties',{},@isparamval);
            p.addParameter('yLabelProperties',{},@isparamval);
            p.addParameter('axisCommand','normal',@(x) isnumeric(x) && isvector(x) ||...
                ischar(x));
            p.addParameter('update',false,@islogical);
            p.parse(yField,varargin{:});
            args = p.Results;
            
            o.yField = args.yField;
            o.xValues = args.xValues;
            if isempty(args.name)
                o.name = o.yField;
            else
                o.name = args.name;
            end
            o.complex = args.complex;
            
            o.xLabel = args.xLabel;
            if ~isempty(args.yLabel)
                o.yLabel = args.yLabel;
            else
                o.yLabel = o.yField;
            end
            
            o.plotMean = args.plotMean;
            o.plotOnlyMean = args.plotOnlyMean;
            
            if (ischar(args.plotOnlyMean) || args.plotOnlyMean) && isempty(args.meanProperties) && ~isempty(args.lineProperties)
                args.meanProperties = args.lineProperties;
            end
            
            o.figureProperties = [o.FIGURE_PROPERTIES, args.figureProperties];
            o.axesProperties = [o.AXES_PROPERTIES, args.axesProperties];
            o.lineProperties = [o.LINE_PROPERTIES, args.lineProperties];
            o.meanProperties = [o.MEAN_PROPERTIES, args.meanProperties];
            o.xLabelProperties = [o.LABEL_PROPERTIES, args.labelProperties, args.xLabelProperties];
            o.yLabelProperties = [o.LABEL_PROPERTIES, args.labelProperties, args.yLabelProperties];            
            
            if ~isempty(args.yLim) 
                o.axesProperties = [o.axesProperties, 'YLim', args.yLim];
            end
            
            if ~isempty(args.xLim) 
                o.axesProperties = [o.axesProperties, 'XLim', args.xLim];
            end
            
            o.update = args.update;
        end       
        
        function set_name(o,name)
            o.name = name;
        end
                
        function result = run(o,model,varargin)
            p = inputParser;
            p.KeepUnmatched = true;
            p.addOptional('loop',0,@(x) isnumeric(x) && isscalar(x));
            p.addOptional('point',1,@(x) isnumeric(x) && isscalar(x));
            p.addOptional('frame',[],@(x) isempty(x) || isa(x, 'DataModel'));
            p.addParameter('axes',[], @(x) ishandle(x) || isempty(x));
            p.parse(varargin{:});
            args = p.Results;

            result = Result.SUCCESS;
            
            % Verify axes handle, or create new figure
            if isempty(o.hAxes) || ~ishandle(o.hAxes) ||...
                       (~isempty(args.axes) && o.hAxes ~= args.axes)
                if ishandle(args.axes) 
                    o.hAxes = args.axes;
                else                    
                    f = findfigure(o.name, o.figureProperties{:});
                    clf(f);
                    o.hAxes = axes('Parent',f);
                end
                
                o.init_axes();
            end
                            
            % Load plot data from data model
            [hasData, xData, yData] = o.get_plot_data(model);
            if ~hasData
                return
            end
            
            % Take value of nPoints from 2nd dimension of plot data
            nPoints = size(yData,2);
            nLoops = size(yData,1);
            % Plot data may not have as many datasets as the current point
            % number (i.e. if plotting single shot data)
            loop = min(args.loop, nLoops);
            
            % Update or create plot line objects with new data
            if ~isempty(o.hLine) && loop <= length(o.hLine) && ishandle(o.hLine(loop))
                if size(xData, 2) > 1
                    if size(xData, 1) == 1 && size(xData, 2) == size(yData, 2)
                        xData = xData';
                    else
                        xData = xData(loop,:);
                    end
                end
                
                if ~(ischar(o.plotOnlyMean) || o.plotOnlyMean)
                    % Update existing line
                    o.hLine(loop) = o.update_line(o.hLine(loop), xData, yData(loop,:));
                end
            else
                if ~(ischar(o.plotOnlyMean) || o.plotOnlyMean)
                    % Create new lines.  First make sure old lines are all gone
                    for line=o.hLine
                        if ishandle(o.hLine)
                            delete(o.hLine);
                        end
                    end
                    o.hLine = o.plot_line(o.hAxes, xData, yData, o.lineProperties);
                end
            end
            
            doPlotMean = false;
            if (ischar(o.plotMean) || o.plotMean) && nLoops > 1
                doPlotMean = true;
                plotMean = o.plotMean;
            end
            
            if ischar(o.plotOnlyMean) || o.plotOnlyMean
                doPlotMean = true;
                plotMean = o.plotOnlyMean;
            end
                
            if doPlotMean
                if islogical(plotMean)
                    valid = true(size(yData));
                else
                    valid = model.get(plotMean);
                end
                
                xAvg = xData;
                xErr = [];
                if size(xData, 2) > 1
                    xAvg = nanmean(nanmask(xData, logical(sum(valid))), 1);
                    xErr = nanstderr(nanmask(xData, logical(sum(valid))), 1);
                end
                yAvg = nanmean(nanmask(yData, valid), 1);
                yErr = nanstderr(nanmask(yData, valid), 1);
                if ~isempty(o.hMean) && ishandle(o.hMean)
                    % Update existing errorbars
                    o.hMean = o.update_mean(o.hMean, xAvg, xErr, yAvg, yErr);
                else
                    o.hMean = o.plot_mean(o.hAxes, xAvg, xErr, yAvg, yErr, o.meanProperties);
                end
            end
            
            if ~any(strcmp(o.axesProperties, 'XLim'))
                if isempty(xData)
                    xLim = [0, nPoints] + 0.5;
                else
                    xLim = [min(xData)-0.1*abs(min(xData)), max(xData)+0.1*abs(max(xData))];
                end
                set(o.hAxes, 'XLim', xLim);
            end
            
            % Update plot if requested
            if o.update
                drawnow;
            end            
        end
    end
    
    methods (Access = protected)
        function [result, xData, yData] = get_plot_data(o, model)           
            xData = [];  yData = [];
            if ~model.has(o.yField)
                warning('skaffold:ReportError', 'Missing field for plot ''%s''.', o.yField);
                result = false;
                return;
            end
            
            yData = model.get(o.yField);            
            if isa(yData,'Waveform')
                warning('skaffold:ReportError', 'PointPlot cannot plot Waveform ''%s''.', o.yField);
                result = false;
                return;
            end

            if ~isempty(o.complex)
                switch o.complex
                    case 'mag'
                        yData = abs(yData);
                        
                    case 'phase'
                        yData = atan2(imag(yData),real(yData));
                end
            end
            
            xData = [];
            if ~isempty(o.xValues)
                xData = o.xValues;
            end

            result = true;
        end
        
        function init_axes(o)
            %Apply axis command
            if ~isempty(o.axisCommand)
                axis(o.hAxes,o.axisCommand);
            end

            %Apply axis properties
            if ~isempty(o.axesProperties)
                set(o.hAxes, o.axesProperties{:});
            end

            % Set X Label and properties
            if ~isempty(o.xLabel)
                xlabel(o.hAxes,o.xLabel);
                if ~isempty(o.xLabelProperties)
                    set(get(o.hAxes,'XLabel'), o.xLabelProperties{:});                    
                end
            end

            % Set Y Label and properties
            if ~isempty(o.yLabel)
                ylabel(o.hAxes,o.yLabel);
                if ~isempty(o.yLabelProperties)
                    set(get(o.hAxes,'YLabel'), o.yLabelProperties{:});                    
                end
            end            
        end
                
    end
    
    methods (Static)        
        function h = plot_line(hAxes, xData, yData, props)
            %Plots the specified data.

            if isempty(xData)
                h = line(1:size(yData, 2), yData, 'Parent', hAxes, props{:});
            else
                h = line(xData(1:size(yData, 2))', yData, 'Parent', hAxes, props{:});
            end
        end
        
        function h = update_line(h, xData, yData)
            %Plots the specified data.
            
            if isempty(xData)
                set(h, 'XData', 1:size(yData,2), 'YData', yData);
            else
                set(h, 'XData', xData(1:size(yData, 2))', 'YData', yData);
            end            
        end
        
        function h = plot_mean(hAxes, xData, xErr, yData, yErr, props)
            %Plots the specified data.

            nLoops = size(yData, 2);
            if isempty(xData)
                h = errorbar(hAxes, 1:nLoops, yData', yErr', props{:});
            else
                h = errorbar(hAxes, xData(1:nLoops)', yData', yErr', yErr', xErr(1:nLoops)', xErr(1:nLoops)', props{:});
            end
        end        
        
        function h = update_mean(h, xData, xErr, yData, yErr)
            %Plots the specified data.
            
            if isempty(xData)
                set(h, 'XData', 1:size(yData,2), 'YData', yData,...
                    'XNegativeDelta', [], 'XPositiveDelta', [], 'YNegativeDelta', yErr, 'YPositiveDelta', yErr);
            else
                set(h, 'XData', xData, 'YData', yData,...
                    'XNegativeDelta', xErr, 'XPositiveDelta', xErr, 'YNegativeDelta', yErr, 'YPositiveDelta', yErr);
            end            
        end        
    end
end

