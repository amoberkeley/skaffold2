classdef ColorPlot2D < Reporter
    % Reporter implementation for plotting Spectrograms
    
    properties (Constant)
        FIGURE_PROPERTIES = {'Color',[0.9 0.9 1],'PaperUnits','inches'};
        AXES_PROPERTIES = {'FontSize',11,'Box','on'};        
        LINE_PROPERTIES = {'MarkerSize',12};        
        LABEL_PROPERTIES = {'FontSize',11};
    end
    
    properties (SetAccess = protected)
        name
        complex

        cField
        cLim
        xField
        xLim
        yField
        yLim

        xLabel              % x-label text or ''
        yLabel              % y-label text or ''
        axesProperties      % Axes properties
        figureProperties    % Figure properties
        lineProperties      % Line properties
        xLabelProperties    % xLabel properties
        yLabelProperties    % yLabel properties        
        axisCommand         % Either numeric array or valid string command to axis()
        
        update
        save
    end
        
    properties (Transient,SetAccess = protected)
        hFigure
        hAxes
        hSurf
    end
    
    methods
        function o = ColorPlot2D(cField,varargin)
            
            p = inputParser;
            p.addRequired('cField',@ischar)
            p.addParameter('nPoints',0,@(x) x>0);
            p.addParameter('yField','',@ischar)
            p.addParameter('xField','',@ischar)
            p.addParameter('complex',[],@(x) isempty(x) || ismember(x,{'mag','phase'}));
            p.addParameter('name','',@ischar)
            p.addParameter('yLim', [], @(x) isnumeric(x) && isvector(x));
            p.addParameter('xLim', [], @(x) isnumeric(x) && isvector(x));
            p.addParameter('cLim', [], @(x) isnumeric(x) && isvector(x));
            p.addParameter('xLabel','',@ischar)
            p.addParameter('yLabel','',@ischar)
            p.addParameter('figureProperties',{},@isparamval);
            p.addParameter('axesProperties',{},@isparamval);
            p.addParameter('labelProperties',{},@isparamval);
            p.addParameter('lineProperties',{},@isparamval);
            p.addParameter('xLabelProperties',{},@isparamval);
            p.addParameter('yLabelProperties',{},@isparamval);
            p.addParameter('axisCommand','normal',@(x) isnumeric(x) && isvector(x) ||...
                ischar(x));
            p.addParameter('update',false,@islogical);
            p.addParameter('save',{},@(x) ischar(x) || iscellstr(x));
            p.parse(cField,varargin{:});
            args = p.Results;
            
            o.cField = args.cField;
            o.yField = args.yField;
            o.xField = args.xField;
            if isempty(args.name)
                o.name = o.cField;
            else
                o.name = args.name;
            end

            o.complex = args.complex;
            
            o.init_default_properties();
            
            if ~isempty(args.xLabel)
                o.xLabel = args.xLabel;
            end
            
            if ~isempty(args.yLabel)
                o.yLabel = args.yLabel;
            else
                o.yLabel = o.yField;
            end

            o.figureProperties = [o.figureProperties, args.figureProperties];
            o.axesProperties = [o.axesProperties, args.axesProperties];
            o.lineProperties = [o.lineProperties, args.lineProperties];
            o.xLabelProperties = [o.xLabelProperties, args.labelProperties, args.xLabelProperties];
            o.yLabelProperties = [o.yLabelProperties, args.labelProperties, args.yLabelProperties];              
            if ~isempty(args.xLim)
                o.xLim = args.xLim;
                o.axesProperties = [o.axesProperties, 'XLim', args.xLim];
            end
            if ~isempty(args.yLim)
                o.yLim = args.yLim;
                o.axesProperties = [o.axesProperties, 'YLim', args.yLim];
            end
            if ~isempty(args.cLim)
                o.cLim = args.cLim;
                o.axesProperties = [o.axesProperties, 'CLim', args.cLim];
            end
            
            o.update = args.update;
            if ischar(args.save)
                o.save = {args.save};
            else
                o.save = args.save;
            end            
        end       
        
        function set_name(o,name)
            o.name = name;
        end
                
        function result = run(o,model,varargin)
            p = inputParser;
            p.KeepUnmatched = true;
            p.addOptional('loop',0,@(x) isnumeric(x) && isscalar(x));
            p.addOptional('point',1,@(x) isnumeric(x) && isscalar(x));
            p.addOptional('frame',[],@(x) isempty(x) || isa(x, 'DataModel'));
            p.addParameter('axes',[], @(x) ishandle(x) || isempty(x));
            p.parse(varargin{:});
            args = p.Results;

            result = Result.SUCCESS;
            
            % Verify figure handle, or create new figure
            if isempty(o.hFigure) || ~ishandle(o.hFigure)
                o.hFigure = findfigure(o.name, o.figureProperties{:});
                clf(o.hFigure);
            end
            
            global scanner
            % Verify axes handle, or create new axes
            for index=1:scanner.nPoints
                if length(o.hAxes) < index || ~ishandle(o.hAxes(index))
                    o.hAxes(index) = e3.tileplot(scanner.nPoints, index, 'Parent', o.hFigure);
                    o.init_axes(o.hAxes(index));           
                end
            end            
               
            point = min(args.point, scanner.nPoints);
            % Load plot data from data model
            [hasData, cData, xData, yData, cLimAuto] = o.get_plot_data(model);
            if ~hasData || size(cData,3) < point || (~isvector(xData) && size(xData,1) < point)
                return
            end            
           
            if ~isvector(xData)
                xData = xData(point,:);
            end
            
            % Update or create surface plot objects with new data
            if ~isempty(o.hSurf) && point <= length(o.hSurf) && ishandle(o.hSurf(point)) && o.hSurf(point) ~= 0
                % Update existing surface plot
                
                o.hSurf(point) = o.update_pcolor(o.hSurf(point), xData, yData, cData(:,:,point));
            else
                % Create new surface plot
                o.hSurf(point) = o.plot_pcolor(o.hAxes(point), xData, yData, cData(:,:,point), {});                
            end
            if ~isempty(o.cLim)
                caxis(o.hAxes(point), o.cLim);
            else
                caxis(o.hAxes(point), cLimAuto);
            end

            %Update plot if requested
            if o.update
                drawnow;
            end          
            
            if ~isempty(o.save) && ~isempty(scanner)
                for index=1:length(o.save)
                    scanner.register_save(o.hFigure, o.save{index});
                end
            end
            
            result = Result.SUCCESS;   
        end
    end
    
    methods (Access = protected)
        function [result, cData, xData, yData, cLim] = get_plot_data(o, model)           
            cData = []; xData = [];  yData = []; cLim = [];
            if ~model.has(o.cField)
                fprintf('WARNING: Missing field ''%s'' for plot.\n', o.cField);
                result = false;
                return;
            end          
            cData = model.get(o.cField);
            
            if ~isempty(o.xField)
                if ~model.has(o.xField)
                    fprintf('WARNING: Missing field ''%s'' for plot.\n', o.xField);
                    result = false;
                    return;                    
                end
                
                xData = model.get(o.xField);
            end
            
            if ~isempty(o.yField)
                if ~model.has(o.yField)
                    fprintf('WARNING: Missing field ''%s'' for plot.\n', o.yField);
                    result = false;
                    return;                    
                end
                
                yData = model.get(o.yField);
            end            
                        
            if isa(yData, 'Waveform')
                yData = yData.data;
            end
            if isa(xData, 'Waveform')
                xData = xData.data;
            end
            if isa(cData,'Waveform')
                if isempty(yData)                    
                    yData = cData.get_x()';
                end                
                if ~isempty(o.yLim)
                    dataRange = cData.slice_x(o.yLim).data;
                    cLim = [min(dataRange(:)), max(dataRange(:))];
                else
                    dataRange = cData.data;
                    cLim = [min(dataRange(:)), max(dataRange(:))];
                end
                cData = cData.data;
            else
                if ~isempty(o.yLim)
                    yIndices = yData > o.yLim(1) & yData < o.yLim(2);
                    dataRange = cData(:,yIndices);
                    cLim = [min(dataRange(:)), max(dataRange(:))];
                else
                    dataRange = cData(:, :);
                    cLim = [min(dataRange(:)), max(dataRange(:))];
                end                
            end

            if ~isempty(o.complex)
                switch o.complex
                    case 'mag'
                        cData = abs(cData);
                        
                    case 'phase'
                        cData = atan2(imag(cData),real(cData));
                end
            end

            result = true;
        end
        
        function init_axes(o, hAxes)
            %Apply axis command
            if ~isempty(o.axisCommand)
                axis(hAxes,o.axisCommand);
            end

            %Apply axis properties
            if ~isempty(o.axesProperties)
                set(hAxes, o.axesProperties{:});
            end

            % Set X Label and properties
            if ~isempty(o.xLabel)
                xlabel(hAxes,o.xLabel);
                if ~isempty(o.xLabelProperties)
                    set(get(hAxes,'XLabel'), o.xLabelProperties{:});                    
                end
            end

            % Set Y Label and properties
            if ~isempty(o.yLabel)
                ylabel(hAxes,o.yLabel);
                if ~isempty(o.yLabelProperties)
                    set(get(hAxes,'YLabel'), o.yLabelProperties{:});                    
                end
            end            
        end
        
        function h = plot_pcolor(o, hAxes, xData, yData, cData, props)
            %Plots the specified data.

            if isempty(xData)               
                xData = 1:size(cData,2);
            elseif all(xData == 0)
                h = -1;
                return
            end
            
            h = e3.colorplot(hAxes, xData, yData, cData, props{:}); 
            xlim(hAxes, [min(xData), max(xData)]);
            ylim(hAxes, [min(yData), max(yData)]);
            set(hAxes, o.axesProperties{:});
        end
        
        function hSurf = update_pcolor(o, hSurf, xData, yData, cData)
            %Plots the specified data.
            
            if isempty(xData)               
                xData = 1:size(cData,2);
            end
            
            set(hSurf, 'XData', xData, 'YData', yData, 'CData', cData);
        end
        
        function init_default_properties(o)
            o.figureProperties = [o.FIGURE_PROPERTIES];
            o.axesProperties = [o.AXES_PROPERTIES];
            o.lineProperties = [o.LINE_PROPERTIES];
            o.xLabelProperties = [o.LABEL_PROPERTIES];
            o.yLabelProperties = [o.LABEL_PROPERTIES];
        end
    end    
end

