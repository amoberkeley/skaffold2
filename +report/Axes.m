classdef Axes < Reporter
    % Reporter representing axes object, which can display multiple 

    properties (SetAccess = protected)
        name
        elements    % Array of plot elements for axes
        update
        raise
        
        axesProperties      % Axes properties
        figureProperties    % Figure properties
        xLabel              % x-label text or ''
        yLabel              % y-label text or ''
        xLabelProperties    % xLabel properties
        yLabelProperties    % yLabel properties        
        axisCommand         % Either numeric array or valid string command to axis()
        forceProperties     % Force Axes properties to overtake enclosed Reporter properties
    end
            
    properties (Transient,SetAccess = protected)
        hAxes
        raiseStart = false
    end
    
    methods
        function o = Axes(elements,varargin)            

            p = inputParser;
            p.addRequired('elements',@(x) iscell(x) && all( cellfun(@(y) isa(y,'Reporter'),x) ) );            
            p.addParameter('name','',@(x) ischar(x) && ~isempty(x));
            p.addParameter('yLim', [], @(x) isnumeric(x) && isvector(x));
            p.addParameter('xLim', [], @(x) isnumeric(x) && isvector(x));
            p.addParameter('xLabel','',@ischar);
            p.addParameter('yLabel','',@ischar);
            p.addParameter('figureProperties',{},@isparamval);
            p.addParameter('axesProperties',{},@isparamval);
            p.addParameter('labelProperties',{},@isparamval);
            p.addParameter('xLabelProperties',{},@isparamval);
            p.addParameter('yLabelProperties',{},@isparamval);
            p.addParameter('axisCommand','normal',@(x) isnumeric(x) && isvector(x) ||...
                ischar(x));
            p.addParameter('forceProperties', false, @islogical);
            p.addParameter('update',false,@islogical);
            p.addParameter('raise','off',@(x) ismember(x,{'off','start','update'}));
            p.parse(elements, varargin{:});
            args = p.Results;            
                                   
            o.elements = args.elements;
            o.update = args.update;         

            if ~isempty(args.name)
                o.set_name(args.name);
            end
            
            o.forceProperties = args.forceProperties;
            
            o.xLabel = args.xLabel;
            o.yLabel = args.yLabel;

            o.figureProperties = [report.Plot.FIGURE_PROPERTIES, args.figureProperties];
            o.axesProperties = [report.Plot.AXES_PROPERTIES, args.axesProperties];
            o.xLabelProperties = [report.Plot.LABEL_PROPERTIES, args.labelProperties, args.xLabelProperties];
            o.yLabelProperties = [report.Plot.LABEL_PROPERTIES, args.labelProperties, args.yLabelProperties];
            
            if ~isempty(args.xLim) 
                o.axesProperties = [o.axesProperties, 'XLim', args.xLim];
            end
            if ~isempty(args.yLim) 
                o.axesProperties = [o.axesProperties, 'YLim', args.yLim];
            end
            
            o.axisCommand = args.axisCommand;
            
            o.raise = args.raise;            
        end
        
        function set_name(o,name)
            o.name = name;
                        
            for index = 1:length(o.elements)
                if ismethod(o.elements{index}, 'set_name')
                    o.elements{index}.set_name(o.name);
                end
            end                        
        end
        
        function find_axes(o, ax)
            if ~isempty(o.hAxes) && ishandle(o.hAxes) && (isempty(ax) || o.hAxes == ax)
                return
            end           
            % If old handle is not set, no longer exists, or args.axes is a
            % different axes, reinitialize axes properties.
            
            if ishandle(ax) 
                o.hAxes = ax;
                cla(o.hAxes);                    
            else
                f = findfigure(o.name, o.figureProperties{:});
                clf(f);
                o.hAxes = axes('Parent',f);       
            end

            o.init_axes();
        end

        function result = run(o,model,varargin)
            p = inputParser;
            p.KeepUnmatched = true;
            p.addOptional('loop',0,@(x) isnumeric(x) && isscalar(x));
            p.addOptional('point',0,@(x) isnumeric(x) && isscalar(x));
            p.addOptional('frame',[],@(x) isempty(x) || isa(x, 'DataModel'));
            p.addParameter('axes',[], @(x) ishandle(x) || isempty(x));
            p.parse(varargin{:});
            args = p.Results;
            
            o.find_axes(args.axes);
            if strcmp(o.raise,'update') || (strcmp(o.raise,'start') && ~o.raiseStart)
                axes(o.hAxes); %This raises the plot window
                o.raiseStart = true;
            end         

            for index=1:length(o.elements)
                o.elements{index}.run(model, varargin{:}, 'axes', o.hAxes);
            end
            
            if o.forceProperties
                o.apply_properties();
            end
            
            %Update plot if requested
            if o.update
                drawnow;
            end            
            result = Result.SUCCESS;
        end
    end    
    
    methods (Access = protected)
        
        function init_axes(o)
            %Apply axis command
            if ~isempty(o.axisCommand)
                axis(o.hAxes,o.axisCommand);
            end

            o.apply_properties();
        end
        
        function apply_properties(o)
            %Apply axis properties
            if ~isempty(o.axesProperties)
                set(o.hAxes, o.axesProperties{:});
            end

            % Set X Label and properties
            if ~isempty(o.xLabel)
                xlabel(o.hAxes, o.xLabel);
                if ~isempty(o.xLabelProperties)
                    set(get(o.hAxes,'XLabel'), o.xLabelProperties{:});                    
                end
            end

            % Set Y Label and properties
            if ~isempty(o.yLabel)
                ylabel(o.hAxes, o.yLabel);
                if ~isempty(o.yLabelProperties)
                    set(get(o.hAxes,'YLabel'), o.yLabelProperties{:});                    
                end
            end
        end
                
    end
end

