classdef Print < Reporter
   
    properties (SetAccess = protected)
        format
        fields
    end
    
    methods
        function o = Print(format,fields,varargin)
            p = inputParser;
            p.addRequired('format',@ischar)
            p.addRequired('fields',@(x) ischar(x) || iscellstr(x))
            p.parse(format,fields,varargin{:});
            args = p.Results;
            
            o.format = args.format;
            if ischar(args.fields)
                o.fields = {args.fields};
            else
                o.fields = args.fields;
            end
        end
            
        function result = run(o,model,varargin)
            p = inputParser;
            p.KeepUnmatched = true;
            p.addOptional('loop',0,@(x) isnumeric(x) && isscalar(x));
            p.addOptional('point',0,@(x) isnumeric(x) && isscalar(x));
            p.addOptional('frame',[],@(x) isempty(x) || isa(x, 'DataModel'));
            p.parse(varargin{:});
            %args = p.Results;
            
            data = cell(size(o.fields));
            for index=1:length(o.fields)
%                 parts = strsplit(o.fields{index},'.');
%                 data{index} = model.get(parts{1});
%                 if length(parts) > 1
%                     data{index} = data{index}.(parts{2});
%                 end
                 data{index} = model.get(o.fields{index});
            end
            
            fprintf(o.format,data{:});
            
            result = Result.SUCCESS;
        end
    end
    
end

