classdef Plot < Reporter
    % Reporter implementation for plotting DataModel fields.
    
    properties (Constant)
        FIGURE_PROPERTIES = {'Color',[0.9 0.9 1],'PaperUnits','inches'};
        AXES_PROPERTIES = {'FontSize',11,'Box','on','NextPlot','add'};        
        LINE_PROPERTIES = {'MarkerSize',12};        
        LABEL_PROPERTIES = {'FontSize',11};
    end
    
    properties (SetAccess = protected)
        name
        yField
        xField
        complex
                
        axesProperties      % Axes properties
        figureProperties    % Figure properties
        lineProperties      % Line properties
        xLabel              % x-label text or ''
        yLabel              % y-label text or ''
        xLabelProperties    % xLabel properties
        yLabelProperties    % yLabel properties        
        axisCommand         % Either numeric array or valid string command to axis()
        xValues             % xData against which to plot if no xField is supplied
        
        update
    end
        
    properties (Transient,SetAccess = protected)
        hAxes
        hLine        
    end
    
    methods
        function o = Plot(yField,varargin)
            
            p = inputParser;
            p.addRequired('yField',@ischar)
            p.addParameter('xField','',@ischar)
            p.addParameter('xValues', [], @(x) isnumeric(x) && isvector(x));
            p.addParameter('complex',[],@(x) isempty(x) || ismember(x,{'mag','phase'}));
            p.addParameter('name','',@ischar)
            p.addParameter('yLim', [], @(x) isnumeric(x) && isvector(x));
            p.addParameter('xLim', [], @(x) isnumeric(x) && isvector(x));
            p.addParameter('xLabel','',@ischar)
            p.addParameter('yLabel','',@ischar)
            p.addParameter('figureProperties',{},@isparamval);
            p.addParameter('axesProperties',{},@isparamval);
            p.addParameter('labelProperties',{},@isparamval);
            p.addParameter('lineProperties',{},@isparamval);
            p.addParameter('xLabelProperties',{},@isparamval);
            p.addParameter('yLabelProperties',{},@isparamval);
            p.addParameter('axisCommand','normal',@(x) isnumeric(x) && isvector(x) ||...
                ischar(x));
            p.addParameter('update',false,@islogical);
            p.parse(yField,varargin{:});
            args = p.Results;
            
            o.yField = args.yField;
            o.xField = args.xField;
            if isempty(args.name)
                o.name = o.yField;
            else
                o.name = args.name;
            end
            o.complex = args.complex;

            o.init_default_properties();
            
            if ~isempty(args.xLabel)
                o.xLabel = args.xLabel;
            end
            
            if ~isempty(args.yLabel)
                o.yLabel = args.yLabel;
            end
            
            o.xValues = args.xValues;

            o.figureProperties = [o.figureProperties, args.figureProperties];
            o.axesProperties = [o.axesProperties, args.axesProperties];
            o.lineProperties = [o.lineProperties, args.lineProperties];
            o.xLabelProperties = [o.xLabelProperties, args.labelProperties, args.xLabelProperties];
            o.yLabelProperties = [o.yLabelProperties, args.labelProperties, args.yLabelProperties];              
            if ~isempty(args.xLim) 
                o.axesProperties = [o.axesProperties, 'XLim', args.xLim];
            end
            if ~isempty(args.yLim) 
                o.axesProperties = [o.axesProperties, 'YLim', args.yLim];
            end
            
            o.update = args.update;
        end       
        
        function set_name(o,name)
            o.name = name;
        end
                
        function result = run(o,model,varargin)
            p = inputParser;
            p.KeepUnmatched = true;
            p.addOptional('loop',0,@(x) isnumeric(x) && isscalar(x));
            p.addOptional('point',1,@(x) isnumeric(x) && isscalar(x));
            p.addOptional('frame',[],@(x) isempty(x) || isa(x, 'DataModel'));
            p.addParameter('axes',[], @(x) ishandle(x) || isempty(x));
            p.parse(varargin{:});
            args = p.Results;

            result = Result.SUCCESS;
            
            % Verify axes handle, or create new figure
            if isempty(o.hAxes) || ~ishandle(o.hAxes) ||...
                       (~isempty(args.axes) && o.hAxes ~= args.axes)
                if ishandle(args.axes) 
                    o.hAxes = args.axes;
%                    cla(o.hAxes);                    
                else                    
                    f = findfigure(o.name, o.figureProperties{:});
                    clf(f);
                    o.hAxes = axes('Parent',f);
                end
                
                o.init_axes();
            end
                            
            % Load plot data from data model
            [hasData, xData, yData] = o.get_plot_data(model);
            if ~hasData
                return
            end
            
            % Take value of nPoints from 2nd dimension of plot data
            nPoints = size(yData,2);
            % Plot data may not have as many datasets as the current point
            % number (i.e. if plotting single shot data)
            point = min(args.point, nPoints);
            
            % Update or create plot line objects with new data
            if ~isempty(o.hLine) && point <= length(o.hLine) && ishandle(o.hLine(point))
                if size(xData, 2) > 1
                    xData = xData(:,point);
                end
                
                % Update existing line
                o.hLine(point) = o.update_line(o.hLine(point), xData, yData(:,point));                
            else
                % Create new lines.  First make sure old lines are all gone
                for line=o.hLine
                    if ishandle(o.hLine)
                        delete(o.hLine);
                    end
                end
                o.hLine = o.plot_line(o.hAxes, xData, yData, o.lineProperties);                
            end            
            
            % Update plot if requested
            if o.update
                drawnow;
            end            
        end
    end
    
    methods (Access = protected)
        function [result, xData, yData] = get_plot_data(o, model)           
            xData = [];  yData = [];
            if ~model.has(o.yField)                
                fprintf('WARNING: Missing field ''%s'' for plot.\n', o.yField);
                result = false;
                return;
            end
            
            yData = model.get(o.yField);
            if ~isempty(o.xField)
                if ~model.has(o.xField)
                    warning('skaffold:ReportError', 'Missing field for plot ''%s''.', o.xField);                    
                    result = false;
                    return;                    
                end
                
                xData = model.get(o.xField);
            end
            
            if isa(yData,'Waveform')
                if isempty(xData)                    
                    xData = yData.get_x()';
                end
                
                yData = yData.data;
            elseif iscell(yData)
                yData = cell2mat(yData);
            end
            
            if isempty(xData)
                xData = o.xValues;
                
                if length(xData) > length(yData)
                    xData = xData(1:length(yData));
                end
            end
            
            if isa(xData, 'Waveform')
                xData = xData.data;
            end

            if ~isempty(o.complex)
                switch o.complex
                    case 'mag'
                        yData = abs(yData);
                        
                    case 'phase'
                        yData = atan2(imag(yData),real(yData));
                end
            end

            result = true;
        end
        
        function init_axes(o)
            %Apply axis command
            if ~isempty(o.axisCommand)
                axis(o.hAxes,o.axisCommand);
            end

            %Apply axis properties
            if ~isempty(o.axesProperties)
                set(o.hAxes, o.axesProperties{:});
            end

            % Set X Label and properties
            if ~isempty(o.xLabel)
                xlabel(o.hAxes,o.xLabel);
                if ~isempty(o.xLabelProperties)
                    set(get(o.hAxes,'XLabel'), o.xLabelProperties{:});                    
                end
            end

            % Set Y Label and properties
            hYLabel = get(o.hAxes,'YLabel');
            if ~isempty(o.yLabel) || isempty(hYLabel) || isempty(get(hYLabel, 'String'))
                if ~isempty(o.yLabel)
                    ylabel(o.hAxes,o.yLabel);
                else
                    ylabel(o.hAxes,o.yField);
                end
                
                if ~isempty(o.yLabelProperties)
                    set(hYLabel, o.yLabelProperties{:});                    
                end
            end            
        end
        
        function init_default_properties(o)
            o.figureProperties = [o.FIGURE_PROPERTIES];
            o.axesProperties = [o.AXES_PROPERTIES];
            o.lineProperties = [o.LINE_PROPERTIES];
            o.xLabelProperties = [o.LABEL_PROPERTIES];
            o.yLabelProperties = [o.LABEL_PROPERTIES];
        end
    end
    
    methods (Static)        
        function h = plot_line(hAxes, xData, yData, props)
            %Plots the specified data.

            if isempty(xData)
                h = line(1:size(yData,1), yData, 'Parent', hAxes, props{:});
            else
                h = line(xData, yData, 'Parent', hAxes, props{:});
            end
        end
        
        function h = update_line(h, xData, yData)
            %Plots the specified data.
            
            if isempty(xData)
                set(h, 'XData', 1:size(yData,1), 'YData', yData);
            else
                set(h, 'XData', xData, 'YData', yData);
            end            
        end
    end
end

