classdef LoopPlot < report.Plot
    methods
        function o = LoopPlot(yField,varargin)
            o@report.Plot(yField,varargin{:});
        end       
    end
    methods (Access = protected)
        function init_default_properties(o)
            init_default_properties@report.Plot(o);
            o.xLabel = 'Loop';
            o.lineProperties = [o.lineProperties, {'Marker','.','LineStyle','none'}];
        end        
    end
end
