classdef Write < Reporter
   
    properties (SetAccess = protected)
        format
        fields
        path
    end
    
    methods
        function o = Write(path, format, fields, varargin)
            p = inputParser;
            p.addRequired('path', @ischar)
            p.addRequired('format', @ischar)
            p.addRequired('fields', @(x) ischar(x) || iscellstr(x))
            p.parse(path, format, fields, varargin{:});
            args = p.Results;
            
            o.path = args.path;
            o.format = args.format;
            if ischar(args.fields)
                o.fields = {args.fields};
            else
                o.fields = args.fields;
            end
        end
            
        function result = run(o, model, varargin)
            p = inputParser;
            p.KeepUnmatched = true;
            p.addOptional('loop', 0, @(x) isnumeric(x) && isscalar(x));
            p.addOptional('point', 0, @(x) isnumeric(x) && isscalar(x));
            p.addOptional('frame', [], @(x) isempty(x) || isa(x, 'DataModel'));
            p.parse(varargin{:});
            %args = p.Results;
            
            data = cell(size(o.fields));
            for index=1:length(o.fields)
%                 parts = strsplit(o.fields{index}, '.');
%                 data{index} = model.get(parts{1});
%                 if length(parts) > 1
%                     data{index} = data{index}.(parts{2});
%                 end
                 data{index} = model.get(o.fields{index});
            end
            
            fid = fopen(o.path, 'w');
            fprintf(fid, o.format, data{:});
            fclose(fid);
            
            result = Result.SUCCESS;
        end
    end
    
end

