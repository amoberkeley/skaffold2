function hLines=vline(varargin)
%Shortcut to plot vertical line(s).  
%
%Usage:
%  hLines = vline([ax],x,...)
%
%Parameters:
%  ax - Optional axis handle, if not provided defaults to current axis.
%  x - Scalar or vector of x-value(s)
%  ... - All additional parameters will be passed directly to 'plot'
%
%Returns:
%  hLines - Handles of created lines

    [ax,args,nargs] = axescheck(varargin{:});
    if isempty(ax)
        ax = gca();
    end
   
    x = args{1};
    if nargs > 1
        opts = args(2:end);
    else
        opts = {};
    end
    
    wasHeld=ishold(ax);
    hold(ax,'on');

    y=get(ax,'ylim');
    
    for idx=1:length(x)
        h(idx)=plot(ax, [x(idx) x(idx)], y, opts{:});
    end
    
    if ~wasHeld
        hold(ax,'off')
    end
    
%     set(h,'handlevisibility','off') % This causes cla(...) to ignore this line

    listener = getappdata(ax, 'VLineListener');
    if isempty(listener)
        listener = addlistener(ax,'YLim','PostSet',@yLimListener);
        setappdata(ax, 'VLineListener',listener);
    end
    lines = getappdata(ax, 'VLines');
    if isempty(lines)
        lines = [];
    end
    lines = [lines, h];
    setappdata(ax,'VLines',lines);  
    
    if nargout
        hLines=h;
    end
end

function yLimListener(~,event)
    ax = event.AffectedObject;
    yl = get(ax, 'YLim');
    lines = getappdata(ax, 'VLines');
    invalid = false(size(lines));
    for idx=1:length(lines)
        if ~ishandle(lines(idx))
            invalid(idx) = true;
            continue;
        end
        set(lines(idx), 'YData', yl);
    end    
    setappdata(ax, 'VLines', lines(~invalid));
end
