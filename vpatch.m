function hPatches=vpatch(varargin)
%Shortcut to plot vertical patch(es). Automatically resizes patches to match 
% y-limits of the axes.
%
%Usage:
%  hPatches = vpatch([ax],x,...)
%
%Parameters:
%  ax - Optional axis handle, if not provided defaults to current axis.
%  x - Scalar or vector of x-value(s)
%  ... - All additional parameters will be passed directly to 'patch'
%
%Returns:
%  hPatches - Handles of created patches

    [ax,args,nargs] = axescheck(varargin{:});
    if isempty(ax)
        ax = gca();
    end
     
    x = args{1};
    if nargs > 1
        opts = args(2:end);
    else
        opts = {};
    end
    
    wasHeld=ishold(ax);
    hold(ax,'on');

    y=get(ax, 'YLim');
    
    for idx=1:size(x,1)
        h(idx)=patch(x(idx,[1 1 2 2]), [y,fliplr(y)], opts{:},'Parent',ax);
    end

    if ~wasHeld
        hold(ax,'off')
    end
    
%     set(h,'handlevisibility','off')
   
    listener = getappdata(ax, 'VPatchListener');
    if isempty(listener)
        listener = addlistener(ax,'YLim','PostSet',@yLimListener);
        setappdata(ax, 'VPatchListener',listener);
    end
    patches = getappdata(ax, 'VPatches');
    if isempty(patches)
        patches = [];
    end
    patches = [patches, h];
    setappdata(ax,'VPatches',patches);  
    
    if nargout
        hPatches=h;
    end
end

function yLimListener(~,event)
    ax = event.AffectedObject;
    yl = get(ax, 'YLim');
    patches = getappdata(ax, 'VPatches');
    invalid = false(size(patches));
    for idx=1:length(patches)
        if ~ishandle(patches(idx))
            invalid(idx) = true;
            continue;
        end
        vert = get(patches(idx), 'Vertices');
        vert(:,2) = [yl, fliplr(yl)];
        set(patches(idx), 'Vertices', vert);
    end 
    setappdata(ax, 'VPatches', patches(~invalid));
end
