classdef Aggregator < Task
    % An Aggregator is a state-less object that aggregates data from each
    % iteration of a DataScan into a aggregate DataModel.  It should not
    % change the original model
    
    methods (Abstract)
        % Aggregates new data into the aggregate DataModel.
        %
        % Usage:
        %   result = o.run(aggModel, loop, point, model)
        result = run(o, aggModel, loop, point, model, varargin)
    end
    
    methods
        function skip(o,aggModel,loop,point,varargin)
        end
    end
    
end

