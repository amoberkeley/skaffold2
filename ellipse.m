function [P] = ellipse(varargin)

    [ax,args,~] = axescheck(varargin{:});

    if isempty(ax)
        ax = gca();
    end
    
    p = inputParser;
    p.addRequired('pos', @(x) isvector(x) && numel(x) == 2);
    p.addRequired('cov', @(x) all(size(x) == [2,2]));
    p.addParameter('FaceColor',[1,.5,.5]);
    p.addParameter('LineStyle','none');
    p.addParameter('EdgeColor',[]);
    p.KeepUnmatched = 1;
    p.parse(args{:});
    args = p.Results;
    plotOpts = struct2params(p.Unmatched);
  
    phase = linspace(0, 2*pi, 200);

    [eigVec, eigVal] = eig(args.cov); % Solve eigensystem
    
    v = [cos(phase'),sin(phase')] * eigVal * eigVec'; % Transformation
    X = args.pos(1) + v(:,1);
    Y = args.pos(2) + v(:,2);
    
    if isempty(args.EdgeColor)
        args.EdgeColor = args.FaceColor;
    end
    
    P = patch(ax,X,Y,args.FaceColor,'LineStyle',args.LineStyle,'EdgeColor',args.EdgeColor,plotOpts{:});
end
