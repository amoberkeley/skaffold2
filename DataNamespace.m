classdef DataNamespace < DataModel
    % Exposes subset of model's fields, prefixed by 'namespace_'.
    % Optionally allows cascading access to fields without the prefix, if a
    % prefixed field of the same name doesn't exist in the namespace.
    
    properties (SetAccess = protected)
        model
        namespace
        cascade
    end

    methods
        function o = DataNamespace(model, namespace, varargin)
            %Create a new DataNamespace, which exposes fields of a
            %DataModel sharing the same prefix.
            %
            %Usage:
            %    o = DataNamespace(model, namespace, 'key',value,...)
            %
            %Parameters:
            %  model - DataModel to encapsulate
            %  namespace - Field namespace prefix
            %
            %Keys:
            %  cascade - logical [false]
            %    Whether to cascade outside the namespace if no field
            %    exists with the given name.
            
            p = inputParser;
            p.addRequired('model', @(x) isa(x,'DataModel'));
            p.addRequired('namespace', @ischar);
            p.addParameter('cascade', false, @islogical);
            p.parse(model,namespace,varargin{:});
            args = p.Results;
            
            o.model = args.model;
            o.namespace = args.namespace;
            o.cascade = args.cascade;
        end

        function varargout = get(o,field)
            % Get field's value within the namespace, optionally cascading
            % out of the namespace, if no field with this name exists
            % within it.
            
            subfield = [o.namespace, '_', field];
            if ~o.cascade || o.model.has(subfield)
                varargout = {o.model.get(subfield)};
            else
                varargout = {o.model.get(field)};
            end
        end
        
        function set(o,field,varargin)
            % DataNamespace.set always sets the field within the configured
            % namespace
            
            subfield = [o.namespace, '_', field];
            o.model.set(subfield, varargin{:});
        end
 
        function unset(o,field)
            % DataNamespace.unset always unsets the field within the configured
            % namespace
            
            subfield = [o.namespace, '_', field];
            o.model.unset(subfield);
        end  
        
        function test = has(o,field)
            subfield = [o.namespace, '_', field];
            test = o.model.has(subfield);
            if ~test && o.cascade
                test = o.model.has(field);
            end
        end
        
        function test = is_changed(o,field)
            subfield = [o.namespace, '_', field];
            if ~o.cascade || o.model.has(subfield)
                test = o.model.is_changed(subfield);
            else
                test = o.model.is_changed(field);
            end
        end

        function fields = fields(o)
            % Does not reveal fields outside namespace, even if cascade is
            % true.  This might want to be implemented in the future, but
            % not clear it's needed.
            
            prefix = [o.namespace, '_'];
            fields = o.model.fields();
            sub = strncmp(prefix,fields,length(prefix));
            fields = fields{sub};
        end
        
        function sig = pop_signature(o)
            sig = o.model.pop_signature();
        end
    end
    
end

