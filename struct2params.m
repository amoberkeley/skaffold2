function c = struct2params(s)
%STRUCT2PARAMS Convert scalar struct to cell array of name, values
% parameters
%
%Usage:
%  c = struct2params(s)
%
%Parameters:
%  s - Struct array
        
    names = fieldnames(s);
    values = struct2cell(s);
    c = reshape([names(:), values(:)]', 1, 2*numel(values));
end