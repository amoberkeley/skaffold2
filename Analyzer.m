classdef Analyzer < Task
    % An Analyzer is a state-less object that represents a task to be run
    % on a single DataModel.  The properties of an Analyzer should be
    % considered constant once the object is created, and should not be
    % changed during Analysis.
    
    properties (Access = protected)
        a_sourceFields
        a_signature
    end
    
    methods (Abstract)       
        result = analyze(o, model, loop, point)
        % Analyze a DataModel object, modifying the object in place, and
        % returning a code indicating the result        
    end
    
    methods
        function skip(o, model, loop, point)
        end
        
        function sig = signature(o)
            % Returns analyzer signature, representing a unique combination
            % of Analyzer properties to be used in determining if a model
            % needs reanalysis

            if ~isempty(o.a_signature)
                sig = o.a_signature;
                return;
            end
            
            sig = '';
            p = properties(o);
            for i = 1:length(p)                
                data = o.(p{i});
                sig = [sig, Analyzer.prop_str(data)];
            end
            sig = [class(o), md5(sig)];
            o.a_signature = sig;
        end
        
        function signatures = collect_signatures(o)
            % Collect analyzer signature for this and all potential
            % sub-analyzers.  This is called once and stored per DataScan, 
            % and assumed to be constant for all iterations of the scan.
            
            signatures = {o.signature()};
        end
                
        function result = run(o,model,varargin)
            p = inputParser;
            p.addOptional('loop',0,@(x) isscalar(x) && x>=0);
            p.addOptional('point',0,@(x) isscalar(x) && x>=0);
            p.addOptional('frame',[],@(x) isempty(x) || isa(x, 'DataModel'));
            p.addParameter('force',false,@islogical);
            p.addParameter('skip',false,@islogical);
            p.parse(varargin{:});
            args = p.Results;            
            
            % Pop next analyzer signature from DataFrame, and check to see
            % if we need to reanalyze
            sig = model.pop_signature();
            run = args.force || o.reanalyze(model, sig);
            
            if run && ~args.skip
                % Analyze model
                result = o.analyze(model, args.loop, args.point);
            else
                % Skip analysis on this model
                o.skip(model, args.loop, args.point);
                result = Result.SUCCESS;
            end
        end
        
        function test = reanalyze(o,model,signature)
            % Returns logical value indicating whether model needs reanalysis
            
            if ~strcmp(signature, o.signature())
                % If Analyzer signature has changed, reanalyze
                test = true;
            else
                test = false;
                % If any source fields have changed, reanalyze
                for i=1:length(o.a_sourceFields)
                    if ~isempty(o.a_sourceFields{i}) && model.is_changed(o.a_sourceFields{i})
                        test = true;
                        break;
                    end
                end
            end
        end
    end
    
    methods (Access = protected)
        function set_source_fields(o, sourceFields)
            o.a_sourceFields = sourceFields;        
        end
        
        function set_signature(o, sig)
            o.a_signature = sig;
        end
    end
    
    methods (Static, Access = protected)
        function str = prop_str(data)
            % Creates str representation of property data, used to
            % construct analyzer signatures.
            
            if ischar(data)
                str = reshape(data,1,[]);
            elseif ismatrix(data) && isnumeric(data)
                str = mat2str(data);
            elseif isstruct(data)
                c = struct2cell(data);
                c = cellfun(@Analyzer.prop_str, reshape(c,1,[]), 'UniformOutput', false);
                str = strjoin(c, '');
            elseif iscell(data)
                c = cellfun(@Analyzer.prop_str, reshape(data,1,[]), 'UniformOutput', false);
                str = strjoin(c, '');
            elseif islogical(data)
                if data
                    str = 'T';
                else
                    str = 'F';
                end
            elseif isa(data, 'Loader') || isa(data, 'loader.Andor')
                str = '';
            else
                str = char(data);
            end
        end
    end
end