classdef Result
    % Enumeration of analyzer/aggregator/loader result codes
    
    enumeration
        SUCCESS     % Task successfully run, execution should continue as normal
        SKIP        % Indicates all following tasks should be skipped (calling their 'skip' method)
        ABORT       % Indicates non-fatal failure, processing of this iteration should immediately terminate, advancing to the next
        OUT_OF_DATA % Indicates all data is processed, should wait for more or complete.
        FAIL        % Indicates fatal failure, which should immediately terminate processing the entire scan
    end
end

