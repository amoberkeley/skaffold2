% Sets dataRoot path, configures single-iteration analysis, and appends
% monitor figure output for live analysis

global fig_files

% Configure local paths to data and analysis
dataRoot = 'E:\Data'; % Location of GageScope data
analysisRoot = 'E:\Analysis'; % Location of analysis code
storageRoot = 'S:\'; % Location of HDF5 data storage
imgRoot = 'E:\Andor'; % Location of Andor absorption images

% Configure for single-file analysis mode
if exist('single','var') && single
    nLoops = 1;
    nPoints = 1;
    filenumber = 0; % Prompt for file
    storage = '';
    statename = '';
    dataname = '';
    live = 0;
end

% For live analysis, configure target paths for figures
% if exist('live','var') && live
%     fig_files = struct();
%     fig_files.agg = {'Z:\E3\www\run.png'};
%     fig_files.single = {'Z:\E3\www\single.png'};
%     fig_files.atoms = {'Z:\E3\www\atoms.png'};
% end

warning('off','MATLAB:Figure:FigureSavedToMATFile')