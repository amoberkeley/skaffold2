classdef Loader < handle
    %Abstract base class for raw data loader
    
    methods (Abstract)
        result = has_data(o) %!
        % Checks if new data is available for analysis, beyond any
        % preloaded iteration
        
        increment(o, offset)
        % Advances to next source file for
                
        [result, data, source, timestamp] = load(o,varargin);
        % Loads data from current source file
        
        source = get_source(o,varargin); %!
        % Returns filename of current source
                         
        result = preload_iteration(o); %!
        % Looks ahead at next iteration, and checks trigger status

        reset_iteration(o); %!
        % Reset to beginning of current iteration.  Used if analysis is
        % interrupted (missing file, etc.), but we want to try again.        
        
        advance_iteration(o); %!
        % Called after loading/analyzing each iteration.  
        % Moves to next iteration, skipping any remaining source files

        triggers = get_trigger_status(o) %!
        % Gets numerical array of trigger status for each source file
        % processed
        
        set_trigger_status(o, triggers) %!
        % Sets numerical array of trigger status for each source file
        % processed
        
        reset(o) %!
        % Resets loader to first data file
    end   
end