function vecrast(figureHandle, filename, resolution, stack, exportType, rasterTargets)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Theodoros Michelis, 6 October 2017
% TUDelft, Aerospace Engineering, Aerodynamics
% t.michelis@tudelft.nl
%
% Modified: Jonathan Kohler, 8 December 2017
% 
% D E S C R I P T I O N:
% vecrast is a function that allows to automatically save a figure with
% mixed vector and raster content. More specifically, two copies of the
% figure of interest are created, rasterFigure and vectorFigure. Patches,
% surfaces, contours, images, and lights are kept in rasterFigure but
% removed from vectorFigure. rasterFigure is then saved as a temporary
% .png image with the required resolution. The .png file is subsequently
% inserted into the vectorFigure, and the result is saved in a single
% vector file.
%
% 
% I N P U T:
% vecrast(figureHandle, filename, resolution, stack, exportType)
%   figureHandle:   Handle of the figure of interest
%   filename:       Baseline name string of output file WITHOUT the extension.
%   resolution:     Desired resolution of rasterising in dpi
%   stack:          'top' or 'bottom'. Stacking of raster image with
%                       respect to axis in vector figure, see examples below.
%   exportType:     'pdf' or 'eps'. Export file type for the output file.
% 
% 
% N O T E S:
% - The graphics smoothing (anti-aliasing) is turned off for the raster
%   figure. This improves sharpness at the borders of the image and at the
%   same time greatly reduces file size. You may change this option in the
%   script by setting 'GraphicsSmoothing', 'on' (line 82).
% - A resolution of no less than 300 dpi is advised. This ensures that
%   interpolation at the edges of the raster image does not cause the image
%   to bleed outside the prescribed axis (make a test with 20dpi on the
%   first example and you will see what I mean).
% - The stacking option has been introduced to accomodate 2D and 3D plots
%   which require the image behind or in front the axis, respectively. This
%   difference can be seen in the examples below.
% - I strongly advise to take a look at the tightPlots function that allows
%   setting exact sizes of figures.

% E X A M P L E   1:
%   clear all; close all; clc;
%   Z = peaks(20);
%   contourf(Z,10)
%   vecrast(gcf, 'example1', 300, 'bottom', 'pdf');

% E X A M P L E   2:
%   clear all; close all; clc;
%   [X,Y] = meshgrid(1:0.4:10, 1:0.4:20);
%   Z = sin(X) + cos(Y);
%   surf(X,Y,Z)
%   vecrast(gcf, 'example2', 300, 'top', 'pdf');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Some checks of the input ------------------------------------------------
if ~ismember(stack, {'top','bottom'})
    error('Stack must be ''top'' or ''bottom''');
end
if ~ismember(exportType, {'pdf','eps'})
    error('Export type must be ''pdf'' or ''eps''');
end

if nargin < 6
    rasterTargets = {'patch','surface','contour','image','light'};
end

% Ensure figure has finished drawing
drawnow;

% Set figure units to points
set(figureHandle, 'units', 'points');

% Ensure figure size and paper size are the same
figurePosition = get(figureHandle, 'Position');
set(figureHandle, 'PaperUnits', 'points', 'PaperSize', [figurePosition(3) figurePosition(4)])
set(figureHandle, 'PaperPositionMode', 'manual', 'PaperPosition', [0 0 figurePosition(3) figurePosition(4)]);

% Create a copy of the figure and remove smoothness in raster figure
rasterFigure = copyobj(figureHandle, groot);

% Tag the elements to raster
for j = 1:length(rasterTargets)
    if ischar(rasterTargets{j})
        elements = findall(rasterFigure, 'type', rasterTargets{j});
    elseif ishandle(rasterTargets{j})
        elements = rasterTargets{j};
    else
        error('Invalid element handle or type');
    end
        
    set(elements, 'tag', 'raster');
end

vectorFigure = copyobj(rasterFigure, groot);
set(rasterFigure, 'GraphicsSmoothing', 'off');
set(vectorFigure, 'GraphicsSmoothing', 'on');

% Fix vector image axis limits based on the original figure
% (this step is necessary if these limits have not been defined)
axesHandle = findall(vectorFigure, 'type', 'axes');
for i = 1:length(axesHandle)
    xlim(axesHandle(i), 'manual');
    ylim(axesHandle(i), 'manual');
    zlim(axesHandle(i), 'manual');
end

% Ensure fontsizes are the same in all figures
figText = findall(figureHandle, 'type', 'text');
rastText = findall(rasterFigure, 'type', 'text');
vecText = findall(vectorFigure, 'type', 'text');
for i=1:length(figText)
    set(rastText(i), 'FontSize', get(figText(i), 'FontSize'));
    set(vecText(i), 'FontSize', get(figText(i), 'FontSize'));
end

% Raster Figure ----------------------------------------------------------
% Select what to remove from raster figure
axesHandle = findall(rasterFigure, 'type', 'axes');
for i = 1:length(axesHandle)
    contents = findall(axesHandle(i));
    set(contents, 'visible', 'off');
end

% Hide all annotations, colorbars, and legends from raster figure
hideAnnotations = {'textarrow','arrow','doublearrow','line','rectangle',...
    'textbox','rectangle','ellipse','colorbar','legend'};
for idx=1:length(hideAnnotations)
    set(findall(rasterFigure, 'type', hideAnnotations{idx}), 'visible', 'off');
end

toRaster = findall(rasterFigure,'tag','raster');
set(toRaster, 'visible', 'on');

set(rasterFigure, 'Color', 'w');

% Print rasterFigure on temporary .png
% ('-loose' ensures that the bounding box of the figure is not cropped)
tempFilename = [filename 'temp.png'];
print(rasterFigure, tempFilename, '-dpng', ['-r' num2str(resolution) ], '-opengl');
close(rasterFigure);

% Vector Figure -----------------------------------------------------------

% Remove rastered elements from vector figure
toRemove = findall(vectorFigure,'tag','raster');
set(toRemove, 'visible', 'off');

% axesHandle = findall(vectorFigure, 'type', 'axes');
% set(axesHandle, 'color', 'none');
for idx=1:length(toRemove)
    ax = ancestor(toRemove(idx), 'axes');
    if ~isempty(ax)
        set(ax, 'color', 'none');
    end
end

% Create axis in vector figure to fill with raster image
rasterAxis = axes(vectorFigure, 'Color', 'none', 'Box', 'off', 'Units', 'points');
set(rasterAxis, 'position', [0 0 figurePosition(3) figurePosition(4)]);
uistack(rasterAxis, stack);

% Insert Raster image into the vector figure
A = imread(tempFilename);
image(rasterAxis, A);
axis(rasterAxis, 'off');
xlim(rasterAxis, [0, size(A, 2)]);
ylim(rasterAxis, [0, size(A, 1)]);

% Finalise ----------------------------------------------------------------
% Remove raster image from directory
delete(tempFilename); % COMMENT THIS IF YOU WANT TO KEEP PNG FILE

set(vectorFigure, 'Color', 'w');

% Print and close the combined vector-raster figure
if strcmp(exportType, 'pdf') == 1
    print(vectorFigure, [filename '.pdf'], '-dpdf', '-painters', sprintf('-r%d', resolution));
elseif strcmp(exportType, 'eps') == 1
    print(vectorFigure, [filename '.eps'], '-depsc', '-painters', sprintf('-r%d', resolution));
end
%%
close(vectorFigure);

end