function varargout = stopGui(varargin)
% STOPGUI MATLAB code for stopGui.fig
%      STOPGUI, by itself, creates a new STOPGUI or raises the existing
%      singleton*.
%
%      H = STOPGUI returns the handle to a new STOPGUI or the handle to
%      the existing singleton*.
%
%      STOPGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in STOPGUI.M with the given input arguments.
%
%      STOPGUI('Property','Value',...) creates a new STOPGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before stopGui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stopGui.  All inputs are passed to stopGui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help stopGui

% Last Modified by GUIDE v2.5 31-Jan-2018 15:14:33

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @stopGui_OpeningFcn, ...
                   'gui_OutputFcn',  @stopGui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before stopGui is made visible.
function stopGui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to stopGui (see VARARGIN)

% Choose default command line output for stopGui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes stopGui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = stopGui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in stopGui.
function stop_Callback(hObject, eventdata, handles)
% hObject    handle to stopGui (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global skaffoldInterrupt
    skaffoldInterrupt = true;


% --------------------------------------------------------------------
function empty_Callback(hObject, eventdata, handles)
% hObject    handle to empty (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Action_Callback(hObject, eventdata, handles)
% hObject    handle to Action (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Stop_Callback(hObject, eventdata, handles)
% hObject    handle to Stop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global skaffoldInterrupt
    skaffoldInterrupt = true;

% --- Executes on button press in save.
function save_Callback(hObject, eventdata, handles)
% hObject    handle to save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    global skaffoldSave
    skaffoldSave = true;
