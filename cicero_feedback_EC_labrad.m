function cicero_feedback_EC_labrad(varargin)
    p = inputParser;
    p.addParamValue('pathname','', @ischar);
    p.addParamValue('pause',1,@(x) isscalar(x) && isreal(x) && x > 0);
    p.addParamValue('stateFile','cicero_feedback_state.mat', @ischar);
    p.addParamValue('overrideFile','C:\Documents and Settings\E3\My Documents\cicero_override.txt', @ischar);
    p.addParamValue('labradFile','C:\Documents and Settings\E3\My Documents\labrad_updates.txt', @ischar);
    p.addParamValue('dataRoot','E:\Data\', @ischar);
    p.addParamValue('analysisRoot','E:\Analysis\', @ischar);
    p.addParamValue('logFile','%s_feedback.log', @ischar);
    p.addParamValue('plotFile','%s_feedback.png', @ischar);
    p.addParamValue('wwwPlotFile','Z:\E3\www\cicero_feedback.png', @ischar);
    p.addParamValue('showGui',true,@islogical);
    p.parse(varargin{:});
    r = p.Results;
    
%% Control parameters

    % Manually set pathname (comment out for prompt)
    date = '2017/Mar/16';
    run = 'cal2';
    r.pathname = e3.get_data_path(date, run);

    % Resets skaffold analyzers, and triggers reanalysis of most recent
    % 'startLoops' loops.  Necessary for any changes to sideband fit
    % parameters to take effect.
    resetAnalyzers = 1;

    % Resets PID loops. Happens automatically when data path changes.  
    % Otherwise, can tune all lock parameters without resetting.
    resetLoops = 1;
    
    startLoops = 1;
       
%% Lock Parameters

    deltaPcEnable = true;
    deltaPcParameters = {...
            'setPoint', +000e3,...
            'initial',  -0.1430,...
            'range', [-1, 1],...
            'gain', 5e-8,...
            'tInt', 20};

    larmorEnable = false;
    mechEnable = false;
        
%%    
    edet = .065;
    nTriggers = 1;
    triggerPattern = [1];
    nPoints = 1; % Doesn't currently support nPoints ~= 1
    threshold = 0.25;
    
    tLoad = [0.001, 0.013]; %[0.065, 0.080];
    tDeltaPC = [0.008, 0.010];
        
%% Loop and Analyzer Methods
    function pids = initialize_loops()
        % Create new FeedbackLoop objects
        pids = struct('deltaPc',0,'larmorF',0,'mechF',0);
        if deltaPcEnable
            pids.deltaPc = FeedbackLoop('deltaPc','slSetpoint',...
                'dest', 'labrad',...
                deltaPcParameters{:},...
                'tSample',30,... % Sampling interval (sequences length) do not change
                'tBW', 240);
        end
        
        if larmorEnable
            pids.larmorF = FeedbackLoop('larmorF','imgBiasAmps',...
                larmorParameters{:},...
                'tSample',30,... % Sampling interval (sequences length) do not change
                'tBW', 900);
        end
        
        if mechEnable
            pids.mechF = FeedbackLoop('mechF','odtPwr',...
                mechParameters{:},...
                'tSample',30,... % Sampling interval (sequences length) do not change
                'tBW', 300);
        end        
    end
    

    function tune_loops()
        % Update feedback parameters for FeedbackLoops, without reseting
        % history
        if deltaPcEnable
            state.pids(1).deltaPc.tune(deltaPcParameters{:});
        end
        if larmorEnable
            state.pids(1).larmorF.tune(larmorParameters{:});
        end
        if mechEnable
            state.pids(1).mechF.tune(mechParameters{:});
        end
    end
    
    function [scanner, gsl] = build_analyzers(filenumber, skipFiles)
        % Construct analyzers
        gsl = loader.Gagescope(state.pathname, filenumber,'dataRate',80e6,'skip', skipFiles,...
            'defaultRange',tLoad,'triggerPattern',triggerPattern);

        analyzer = analyze.Block({...
            analyze.Load(gsl, {'raw', 'source'}, {'channel', 1}, 'increment', nTriggers),...
            analyze.RescaleHeterodyne('raw', 'transimpedance', 28.8e3, 'gain', 30.1, 'impedance',1e6),...
        });
    
        if deltaPcEnable
            analyzer.add(analyze.FFT('raw','rawFft','range', tDeltaPC, 'type', 'powerDensity'));
            analyzer.add(analyze.HetModSidelock('rawFft',{'deltaPc','nBar','sn'},...
                'fCar',10e6,'fMod',2.87e6,'kappa',2.15e6,'kappaN',1.82e6,'rbGain',1.14,...
                'snRange',[3.5e5 4.5e5],'edet',edet,'close',true));
        end
    
        if larmorEnable || mechEnable
            analyzer.add(analyze.Heterodyne('raw','carrierFreq',10e6,'filterBw',10e3));
            analyzer.add(analyze.FFT('qQuad','qQuadPower','range',tLarmor,'type','powerDensity'));
        end

        if larmorEnable
            analyzer.add(analyze.Heterodyne('raw','carrierFreq',10e6,'filterBw',10e3));
            analyzer.add(analyze.FFT('qQuad','larmorPower','range',tLarmor,'type','powerDensity'));
        end
        
        if mechEnable
            analyzer.add(analyze.FFT('qQuad','mechPower','range',tMech,'type','powerDensity'));
        end        
        
        verifiers = {...
            verify.Range('nBar',[0.0,100]),...
        };            

        aggregator = aggregate.Block({...
            aggregate.Verifier('valid',verifiers),...
            aggregate.Collect({'deltaPc'},'if','valid'),...
        });    
    
        if larmorEnable
            aggregator.add(aggregate.Memory('larmorPower','larmorCache', larmorAverage,'if','valid'));
            aggregator.add(analyze.FitSidebands('larmorCache',{'larmorF','larmorG','larmorR2'},'threshold',threshold,...
                'guessFm',larmorGuessFm,'guessGm',larmorGuessGm,'range',larmorRange,...
                'plot','Fit Larmor','plotRange',larmorPlotRange,'suppressDrive',larmorSuppress,...
                'method','adaptive','minAvg',larmorMinAvg,'if','valid'));
            aggregator.add(aggregate.Collect({'larmorF','larmorR2'},'if','valid'));
        end
    
        if mechEnable
            aggregator.add(aggregate.Memory('mechPower','mechCache', mechAverage,'if','valid'));
            aggregator.add(analyze.FitSidebands('mechCache',{'mechF','mechG','mechR2'},'threshold',threshold,...
                'guessFm',mechGuessFm,'guessGm',mechGuessGm,'range',mechRange,...
                'plot','Fit Mech.','plotRange',mechPlotRange,'suppressDrive',mechSuppress,...
                'method','adaptive','minAvg',mechMinAvg,'if','valid'));
            aggregator.add(aggregate.Collect({'mechF','mechR2'},'if','valid'));
        end       
        
        scanner = Scanner(analyzer, nPoints, Scanner.SCAN_ALL, aggregator, 'loader', gsl, 'showGui', false);
    end

    function suppress = suppress_sidebands(driveFreq, driveRep, sidebandRange)
        suppress = driveFreq + [-50, 50];  % on-resonance drive frequency band to be suppressed in fits
        nSuppress = floor(diff(sidebandRange)/(2*driveRep));
        for index=1:nSuppress
           suppress = [
                suppress;...
                driveFreq + index*driveRep + [-50, 50];...% drive repitition sidebands to be suppressed in fits
                driveFreq - index*driveRep + [-50, 50];...           
            ];
        end        
    end
    
%% Initialize UI
    global skaffoldInterrupt
    skaffoldInterrupt = false;
    inLoop = true;
    
    if r.showGui
        % Create GUI with stop button, which sets interrupt flag
        gui = stopGui();
    else
        gui = [];
    end

    % Initiailze state variables
    % State variables
    state = struct('pathname','');
    
%% BEGIN MAIN SCRIPT
    
    % Load state from stateFile
    if exist(r.stateFile,'file')
        state = load(r.stateFile);
    end
         
    % Prompt user for data path
    if isempty(r.pathname)
        % Use path dialog to get pathname
        pathname = pathname_dialog(state.pathname);
        if ~ischar(pathname) || isempty(pathname)
            return;
        end
    else
        pathname = r.pathname;
    end
    
    % If selected pathname differs from saved state, force reset
    if ~strcmp(pathname, state.pathname)
        resetAnalyzers = 1;
        resetLoops = 1;
        state.pathname = pathname;
        
        % Split parts of data path, and build target paths
        parts = regexpi(state.pathname, ['^(.+)\' filesep '(.+)_CH(\d+)\' filesep '?'], 'tokens');
        state.dataPath = parts{1}{1};
        state.run = parts{1}{2};

        state.analysisPath = strrep(state.dataPath, r.dataRoot, r.analysisRoot);
        % Auto-create analysis directory, if it doesn't exist
        if exist(state.analysisPath, 'dir') ~= 7
            mkdir(state.analysisPath);
        end        
        
        state.plotFile = fullfile(state.analysisPath, sprintf(r.plotFile, state.run));       
        state.logFile = fullfile(state.dataPath, sprintf(r.logFile, state.run));
    end

    % Initialize new PID loops, or tune existing ones if no reset
    if resetLoops || ~isfield(state, 'pids') || isempty(state.pids)
        state.pids = initialize_loops();        
    else
        tune_loops();
    end
    
    if resetAnalyzers || ~isfield(state, 'scanner') || isempty(state.scanner)
        % Only read most recent 10 files, if initializing new scanner
        currentFile = e3.count_sources(state.pathname);
        state.startFile = max(0, currentFile - nPoints*nTriggers*startLoops);
            
        fprintf('Starting at file AS_CH01-%05d.sig\n', state.startFile+1);
        
        numFiles = currentFile - state.startFile;
        nLoops = floor(numFiles/(nPoints*nTriggers));
        fprintf('Processing previous %d loops\n', nLoops);
        
        [state.scanner, state.gsl] = build_analyzers(state.startFile+1, []);
%         if state.gsl.has_data()
%             state.scanner.run();        
%             state.lastFile = state.gsl.fileNumber;
%             update_locks();
%         end
            
        save_state();
    end
           
    update_plots();
    
%% Main Program loop    
    fprintf('Entering main loop\n');
       
    while ~skaffoldInterrupt
        if ~state.gsl.has_data()
            % If loader is set, and currently out of data, flush
            % the scan file to disk, then wait for new data.
            fprintf('Waiting for data... ');

            % Save state of Scanner
            save_state();
            
            while ~state.gsl.has_data() && ~skaffoldInterrupt
                pause(r.pause);
            end
            
            if ~skaffoldInterrupt
                fprintf('processing file ''%s''\n', state.gsl.get_source());
            end
        end
        
        if skaffoldInterrupt
            fprintf('Interrupted\n');
            break;
        end

        state.scanner.run();
        state.lastFile = state.gsl.fileNumber;

        if inLoop
            update_locks();
        end
        update_plots();
        save_state();
    end
    
%     if ishandle(gui)
%         % Close 'stop' GUI
%         close(gui);
%     end
    
%% Methods    
    
    function update_locks()
        agg = state.scanner.get_aggregate();
        iteration = floor(state.lastFile/nTriggers);
        logData = [];
        
%         names = fieldnames(state.pids);
%         for i = 1:length(names)
%             lockName = names{i};
%             lock = state.pids.(lockName);
%         
%             values = agg.get(lock.get_input_name());
%             output = lock.update(values, iteration);
%             fprintf('%2 = %0.4f\n', lock.get_output_name(), output);            
%             cicero_override(lock.get_output_name(), output);
%             logData = [logData, values(end), output];
%         end

        if deltaPcEnable
            lockName = 'deltaPc';
            lock = state.pids.(lockName);

            values = agg.get(lock.get_input_name());
            output = lock.update(values, iteration);
            fprintf('%s = %0.4f, %s = %0.4f\n', lock.get_input_name(), values(end), lock.get_output_name(), output);            
            output_value(lock.get_output_name(), output, lock.get_dest());
            logData = [logData, values(end), output];
        end
         
        if larmorEnable
            lockName = 'larmorF';
            lock = state.pids.(lockName);

            values = agg.get(lock.get_input_name());       
            r2 = agg.get('larmorR2');
            output = lock.update(values, iteration);
            fprintf('%s = %0.4f, %s = %0.4f, %s = %0.4f\n', lock.get_input_name(), values(end), 'larmorR2', r2(end), lock.get_output_name(), output);
            output_value(lock.get_output_name(), output, lock.get_dest());
            logData = [logData, values(end), output];
        end
        
        if mechEnable
            lockName = 'mechF';
            lock = state.pids.(lockName);

            values = agg.get(lock.get_input_name());       
            r2 = agg.get('mechR2');
            output = lock.update(values, iteration);
            fprintf('%s = %0.4f, %s = %0.4f, %s = %0.4f\n', lock.get_input_name(), values(end), 'mechR2', r2(end), lock.get_output_name(), output);
            output_value(lock.get_output_name(), output, lock.get_dest());
            logData = [logData, values(end), output];
        end
        
        write_log(iteration, logData);
    end

    function fid = init_log()
        % Create log file, and write header to first line
        fid = fopen(state.logFile, 'a+t');
        if (fid >= 0) 
            fprintf(fid, 'timestamp,shot');

            names = fieldnames(state.pids);
            for i = 1:length(names)
                lock = state.pids.(names{i});
                if lock == 0
                    continue;
                end
                fprintf(fid, ',%s,%s', lock.get_input_name(), lock.get_output_name());
            end
            fprintf(fid, '\n');
        end
    end

    function write_log(shot, logData)
        % Write data point to log file
        try
            if isempty(state.logFile)
                return;
            end;

            if exist(state.logFile, 'file') ~= 2
                % If no log file, write header first
                fid = init_log();
            else
                fid = fopen(state.logFile, 'a+t');
            end

            if (fid >= 0)
                timestamp = datestr(clock, 31);
                logText = sprintf(',%.4g',logData);
                fprintf(fid,'''%s'',%d%s\n', timestamp, shot, logText);
                fclose(fid);
            end        
        catch e
            fprintf('Error writing log: %s', e.getReport());
        end        
    end

    function update_plots()
        h = findfigure('Feedback plots');
        names = fieldnames(state.pids);
        for i = 1:length(names)
            lockName = names{i};
            lock = state.pids.(lockName);
            if lock == 0
                continue;
            end
        
            ax = subplot(length(names), 1, i, 'Parent', h);
            input = lock.get_input();     
            output = lock.get_output();    
            if isempty(input)
                continue;
            end
            
            [haxes,hLine1,hLine2] = plotyy(ax, 1:length(input),input,1:length(output),output,'plot','plot');
            set(hLine1,'Marker','.')
            set(hLine1,'LineStyle','--')
            set(hLine2,'Marker','.')
            set(hLine2,'LineStyle','--')
            ylabel(haxes(1), lock.get_input_name()) % label left y-axis
            ylabel(haxes(2), lock.get_output_name()) % label right y-axis
            xlabel(haxes(2), 'Iteration') % label x-axis
        end
        
        if ~isempty(state.plotFile)
            try
                saveas(h, state.plotFile, 'png');
            catch e
                fprintf('Error saving plot: %s', e.getReport());
            end            
        end

        if ~isempty(r.wwwPlotFile)
            try
                saveas(h, r.wwwPlotFile, 'png');
            catch e
                fprintf('Error saving plot: %s', e.getReport());
            end
        end        
    end

    function pathname = pathname_dialog(pathname)
        % Prompt user for data path
        if isempty(pathname)
            pathname = r.dataRoot;
        end
        
        [f,pathname] = uigetfile(['*','.sig'],'Choose GageScope data folder', [pathname, '']);
        
        if ischar(f)
            return;
        else % Aborted UI
            pathname = false;
        end
    end

    function save_state()
        % Save script state to file
        save(r.stateFile, '-struct', 'state');
    end

    function output_value(name, value, dest)
        switch dest
            case 'cicero'
                cicero_override(name, value);
                
            case 'labrad'
                % write to both
                cicero_override(name, value);
                labrad_update(name, value);
        end
    end

    function cicero_override(name, value)
        % Write name = value update to cicero override file
        fid = fopen(r.overrideFile, 'at+');
        if (fid >= 0)
            fprintf(fid, '%s = %0.4f\n', name, value);
            fclose(fid);
        end
    end

    function labrad_update(name, value)
        % Write name = value update to labrad update file
        fid = fopen(r.labradFile, 'at+');
        if (fid >= 0)
            fprintf(fid, '%s = %0.4f\n', name, value);
            fclose(fid);
        end
    end        
end