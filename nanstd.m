function out = nanstd(X, varargin)

out = std(X, varargin{:}, 'omitnan');

end