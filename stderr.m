function se = stderr(x, d)
%STDERR Standard Error on the Mean
%    Calculates the standard error of the mean for values in x along dimension d
%
%Usage:
%  se = stderr(x,[d])
%
%Parameters:
%  x - Vector or matrix of sample values
%  d - Dimension along which to calculate.  Defaults to first non-singleton
%     dimension
%
    if nargin < 2
        d = find(size(x) ~= 1, 1);    
    end
    se = std(x, [], d)./sqrt(size(x, d));
end