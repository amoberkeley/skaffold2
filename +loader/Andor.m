classdef Andor < Loader
    properties (Constant = true)
        DIR_PATTERN = ['%04d' filesep '%02d' filesep '%02d'];
        FILE_PATTERN = '%04d-%02d-%02d-%02d%02d%02d';
        FILE_REGEX = '(\d+)-(\d+)-(\d+)-(\d+).(txt|h5)';
    end

    properties (SetAccess = protected)       
        dataRoot      % Source path
        fileExt
        mode
        retries
        wait
        offset
        tolerance
        
        files
        fileNumber
        curIteration
        nextIteration
        
        timestamp
    end
   
    methods
        function o = Andor(dataRoot, varargin)
            p = inputParser();
            p.addRequired('dataRoot', @ischar);
            p.addParameter('fileExt','.h5',@(x) ismember(x, {'.txt','.h5'}));
            p.addParameter('mode','timestamp',@(x) ismember(x,{'timestamp','scan'}));
            p.addParameter('wait',false,@islogical);
            p.addParameter('retries',3,@isnumerical);
            p.addParameter('offset',0,@(x) isscalar(x) && isreal(x));
            p.addParameter('tolerance',5,@(x) isscalar(x) && isreal(x));
            p.parse(dataRoot, varargin{:});
            args = p.Results;

            o.dataRoot = args.dataRoot;
            o.fileExt = args.fileExt;
            o.mode = args.mode;
            o.wait = args.wait;
            o.retries = args.retries;
            o.offset = args.offset;
            o.tolerance = args.tolerance;
            
            o.timestamp = 0;
            o.fileNumber = 1;
            
            switch o.mode
                    
                case 'scan'
                    o.rescan_files();
                    o.curIteration = 1;
                    o.nextIteration = 1;
                    
                case 'timestamp'
                    
            end
        end
                
        function test = has_data(o)
            % Checks if new data is available for analysis, beyond any
            % preloaded iteration
           
            switch o.mode
                case 'scan'
                    if length(o.files) >= o.fileNumber
                        test = true;
                    else
                        o.rescan_files();
                        test = length(o.files) >= o.fileNumber;
                    end                                   
                    
                case 'timestamp'
                    error('Not supported in timestamp mode');
            end
        end
                
        function fileNumber = increment(o, offset)
            if nargin < 2
                offset = 1;
            elseif islogical(offset)
                offset = 1;
            end

            switch o.mode
                case 'scan'
                    o.fileNumber = o.fileNumber + offset;
                    fileNumber = o.fileNumber;
 
            end
        end        
                        
        function [result, data, source, timestamp] = load(o,varargin)
            % Loads data from current source file
            
            switch o.mode
                case 'timestamp'
                    [result, data, source, timestamp] = o.load_timestamp(varargin{:});
                    
                case 'scan'
                    [result, data, source, timestamp] = o.load_next(varargin{:});
                    
            end
        end
        
        function source = get_source(o,varargin)
            % Returns filename of current source
            
            switch (o.mode)
                case 'scan'
                    source = o.files{o.fileNumber};
                    
                case 'timestamp'
                    source = o.get_source_timestamp(o.timestamp);
            end
        end

                         
        function result = preload_iteration(o)
            % Looks ahead at next iteration, and checks trigger status
            result = Result.SUCCESS;                                    
            switch (o.mode)
                case 'scan'
                    o.nextIteration = max(o.fileNumber,o.curIteration)+1;
                  
            end
        end
        
        function reset_iteration(o)
            % Reset to beginning of current iteration.  Used if analysis is
            % interrupted (missing file, etc.), but we want to try again.
            
            switch (o.mode)
                case 'scan'
                    o.fileNumber = o.curIteration;

            end
        end
        
        function advance_iteration(o)
            % Advance to beginning of next iteration, if preload_iteration
            % was called before loading this iteration.
            
            switch (o.mode)
                case 'scan'
                    o.fileNumber = max(o.fileNumber, o.nextIteration);
                    o.curIteration = o.fileNumber;

            end
        end        
        
        function reset(o)
            switch (o.mode)
                case 'scan'
                    o.fileNumber = 1;
                    o.curIteration = 1;
                    o.nextIteration = 1;
            end            
        end
        
        function rescan_files(o)
            fileStruct = dir(fullfile(o.dataRoot, '*' + o.fileExt));
            o.files = cellfun(@(x) [x{1} '-' x{2} '-' x{3} '-' x{4}], regexp([fileStruct.name],...
                loader.Andor.FILE_REGEX,'tokens'), 'UniformOutput',false);
        end
        
        function [result, data, source, timestamp] = load_timestamp(o,iterTimestamp,varargin)
            data = struct();

            for attempt=1:o.retries
                [source, timestamp] = o.find_image(iterTimestamp);

                if isempty(source) && o.wait
                    pause(1);
                    continue
                end
                break
            end
            
            if isempty(source)
                % give up
                warning('Andor:noImage', ['No image could be found at time:' iterTimestamp]);
                result = Result.SKIP;
                return
            end
            
            o.timestamp = datenum(timestamp);
            
            switch o.fileExt
                case '.txt'
                    [result, data] = o.load_csv(source);
                    
                case '.h5'
                    [result, data] = o.load_h5(source);
            end    
        end
        
        function [result, data, source, timestamp] = load_next(o,varargin)
            data = struct();
            source = '';
            timestamp = zeros(1,6);
            
            if o.has_data()
                source = o.files{o.fileNumber};
            else
                result = Result.OUT_OF_DATA;
                return
            end
            
            switch o.fileExt
                case '.txt'
                    [result, data] = o.load_csv(source);
                    
                case '.h5'
                    [result, data] = o.load_h5(source);
            end            
        end        

        function [result, data, timestamp] = load_csv(o, source)
            img_file = fullfile(o.dataRoot, [source '.txt']);
            signal_file = fullfile(o.dataRoot, [source '-sig.txt']);
            reference_file = fullfile(o.dataRoot, [source '-ref.txt']);
            background_file = fullfile(o.dataRoot, [source '-bkg.txt']);

            data.tof = csvread(img_file,1,1);
            data.sig = csvread(signal_file,1,1);
            data.ref = csvread(reference_file,1,1);
            data.bkg = csvread(background_file,1,1);            
            
            fileInfo = dir(img_file);
            timestamp = datevec(fileInfo.datenum);
            
            result = Result.SUCCESS;
        end
        
        function [result, data, timestamp] = load_h5(o, source)
            img_file = fullfile(o.dataRoot, [source '.h5']);
            
            data.sig = h5read(img_file, '/sig')';
            data.ref = h5read(img_file, '/ref')';
            data.bkg = h5read(img_file, '/bkg')';
            data.tof = double(data.sig - data.bkg)./double(data.ref - data.bkg);
            
            timestampStr = h5readatt(img_file, '/', 'timestamp');            
            timestamp = datevec(timestampStr,'yyyy-mm-ddTHH:MM:SS');

            result = Result.SUCCESS;
        end
                
        
        function [source, timestamp] = find_image(o, iterTimestamp)
            if ischar(iterTimestamp) || ~isscalar(iterTimestamp)
                iterTimestamp = datenum(iterTimestamp);
            end
            
            source = [];
            timestamp = zeros(1,6);
            for scanOffset=-o.tolerance:o.tolerance
                testtime = addtodate(iterTimestamp, o.offset + scanOffset, 'second');
                [testname, testdir] = o.get_source_timestamp(testtime);
                filepath = fullfile(o.dataRoot, testdir, [testname o.fileExt]);
                if exist(filepath,'file') == 2
                    source = fullfile(testdir, testname);
                    timestamp = datevec(testtime);
                    return
                end
            end            
        end
        
        function [filename, dir] = get_source_timestamp(o, timestamp)
            if ischar(timestamp) || isscalar(timestamp)
                timestamp = datevec(timestamp);
            end
            ts = num2cell(timestamp);
            filename = sprintf(o.FILE_PATTERN, ts{:});
            pattern = regexprep(o.DIR_PATTERN,'\\','\\\\'); % Escape backslash for Windows...           
            dir = sprintf(pattern, ts{1:3});
        end        
               
        function root = get_root(o)
            root = o.dataRoot;
        end

        function triggers = get_trigger_status(o)
            % Gets numerical array of trigger status for each source file
            % processed
            triggers = [];
        end
        
        function set_trigger_status(o, triggers) %!
            % Sets numerical array of trigger status for each source file
            % processed
        end
                
    end
end