classdef AndorMonitor < Loader

    properties (SetAccess = protected)       
        dataRoot      % Source path
        monitor
        
        fileNumber
        curIteration
        nextIteration
        
        timestamp
    end
   
    methods
        function o = AndorMonitor(dataRoot, varargin)
            p = inputParser();
            p.addRequired('dataRoot', @ischar);
            p.addParameter('monitor',{},@iscellstr);
            p.parse(dataRoot, varargin{:});
            args = p.Results;

            o.dataRoot = args.dataRoot;
            o.monitor = args.monitor;
            
            o.timestamp = 0;
            o.fileNumber = 1;

            o.curIteration = 0;
            o.nextIteration = 0;
                    
        end
                
        function test = has_data(o)
            % Checks if new data is available for analysis, beyond any
            % preloaded iteration
           
            for idx=1:length(o.monitor)
                source = fullfile(o.dataRoot, o.monitor{idx});
                fileInfo = dir(source);
                test = (fileInfo.datenum > o.timestamp);
                if ~test
                    break
                end
            end
        end
                
        function fileNumber = increment(o, ~)
            source = fullfile(o.dataRoot, o.monitor{end});
            fileInfo = dir(source);
            o.timestamp = fileInfo.datenum;
            fileNumber = 0;
        end        
                        
        function [result, data, source, timestamp] = load(o,varargin)
            % Loads data from current source file
            
            data = struct();
            source = o.monitor{1};
            timestamp = zeros(1,6);
            
            if ~o.has_data()
                result = Result.OUT_OF_DATA;
                return
            end
            
            
            [~,~,ext] = fileparts(o.monitor{1});
            
            switch ext
                case '.txt'
                    [result, data, timestamp] = o.load_csv();
                    
                case '.h5'
                    [result, data, timestamp] = o.load_h5();
            end
            

        end
        
        function [result, data, timestamp] = load_csv(o)
            img_file = fullfile(o.dataRoot, o.monitor{1});
            data.tof = csvread(img_file,1,1);
            
            [fid, message] = fopen(img_file, 'a+');
            while fid<0
                warning('loader:Andor','Failed opening file %s: %s. Trying again in 1 s...', img_file, message);
                pause(1);
                [fid, message] = fopen(img_file, 'a+');
            end      
            fclose(fid);
            
            fileInfo = dir(img_file);
            o.timestamp = fileInfo.datenum;
            timestamp = datevec(fileInfo.datenum);
            
            signal_file = fullfile(o.dataRoot, o.monitor{2});
            data.sig = csvread(signal_file,1,1);
            
            consistent = all(size(data.tof) == size(data.sig));
            
            if numel(o.monitor) > 2
                reference_file = fullfile(o.dataRoot, o.monitor{3});
                data.ref = csvread(reference_file,1,1); 
                consistent = consistent && all(size(data.tof) == size(data.ref));
            end
            
            if numel(o.monitor) > 3
                background_file = fullfile(o.dataRoot, o.monitor{4});
                data.bkg = csvread(background_file,1,1);
                consistent = consistent && all(size(data.tof) == size(data.bkg));
            end

            if ~consistent
                result = Result.OUT_OF_DATA;
            else
                result = Result.SUCCESS;
            end            
        end
        
        function [result, data, timestamp] = load_h5(o)
            img_file = fullfile(o.dataRoot, o.monitor{1});
            
            consistent = true;
            for i = 1:5
                try
                    info = h5info(img_file);
                    break;
                catch ME
                    switch ME.identifier
                        case 'MATLAB:imagesci:h5info:fileOpenErr'
                            pause(0.5);
                        otherwise
                            rethrow(ME);
                    end
                end
            end
            datasets = {info.Datasets.Name};
            
            data.sig = h5read(img_file, '/sig')';
            if ismember('ref',datasets)
                data.ref = h5read(img_file, '/ref')';
                consistent = all(size(data.sig) == size(data.ref));
            end
            if ismember('bkg',datasets)
                data.bkg = h5read(img_file, '/bkg')';
                consistent = consistent && all(size(data.sig) == size(data.bkg));
            end

            timestampStr = h5readatt(img_file, '/','timestamp');           
            timestampNum = datenum(timestampStr,'yyyy-mm-ddTHH:MM:SS');
            o.timestamp = timestampNum;
            timestamp = datevec(timestampNum);

            if ~consistent
                result = Result.OUT_OF_DATA;
            else
                result = Result.SUCCESS;
            end            
        end
        
        function source = get_source(o,varargin)
            % Returns filename of current source

            source = fullfile(o.dataRoot, o.monitor{1});
        end

        function result = preload_iteration(o)
            % Looks ahead at next iteration, and checks trigger status
            result = Result.SUCCESS;                                    

            source = fullfile(o.dataRoot, o.monitor{end});
            fileInfo = dir(source);
            o.nextIteration = fileInfo.datenum;                    
        end
        
        function reset_iteration(o)
            % Reset to beginning of current iteration.  Used if analysis is
            % interrupted (missing file, etc.), but we want to try again.
            
            o.timestamp = o.curIteration;
        end
        
        function advance_iteration(o)
            % Advance to beginning of next iteration, if preload_iteration
            % was called before loading this iteration.
            
            o.timestamp = max(o.timestamp, o.nextIteration);
            o.curIteration = o.timestamp;
        end        
        
        function reset(o)
            o.timestamp = 0;
            o.curIteration = 0;
            o.nextIteration = 0;
        end

        function root = get_root(o)
            root = o.dataRoot;
        end

        function triggers = get_trigger_status(o)
            % Gets numerical array of trigger status for each source file
            % processed
            triggers = [];
        end
        
        function set_trigger_status(~) %!
            % Sets numerical array of trigger status for each source file
            % processed
        end
                
    end
end