classdef GageHDF5 < Loader
    %Loads HDF5 files, storing output in a Waveform
    
    properties (Constant = true)        
        FILE_PATTERN = 'iteration%05d.h5';
        FILE_REGEX = 'iteration(\d+).h5';
        FILE_EXT = 'h5';
        
        PATH_REGEX = ['(.*)\', filesep, '([^\', filesep, ']+)_gs'];
    end
    
    properties (SetAccess = protected)
        path      % Source path
        dataRoot
        runName
        startFile
        fileNumber
        
        skipFiles     % User specified source files to skip
        
        nextIteration
        curIteration
        
        filePattern
    end
    
    methods
        function o = GageHDF5(varargin)
            %Creates a new GageHDF5 loader
            %
            %Usage:
            %  o = loader.GageHDF5(path, file, 'key', val,...)
            %
            %Parameters
            %  file - Starting file name or number
            %  path - Location of file
            %
            %Keys
            %  skip - File numbers to skip loading
            %  filePattern - Override default file pattern

            p = inputParser();
            p.addOptional('path', pwd, @ischar);
            p.addOptional('file', '', @(x) ischar(x) || (isscalar(x) && uint32(x) == x));            
            p.addParameter('skip', [], @(x) all(uint32(x)==x))
            p.addParameter('filePattern', [], @(x) isempty(x) || ischar(x));
            p.parse(varargin{:});
            args = p.Results;
                                             
            file = args.file;     
            o.path = args.path;
            
            if isempty(o.path) || isempty(file) || file == 0
                [success, o.path, file] = o.fileui(o.path);
                if ~success % Gets filename and path from UI, stores in o
                    error('skaffold:AbortedByUser','File loading aborted by user.');
                end
            end            
            
            [o.dataRoot, o.runName] = o.parse_pathname(o.path);                
            
            if ischar(file)
                o.startFile = o.parse_filename(file);
            else
                o.startFile = uint32(file);
            end
            o.fileNumber = o.startFile;
            
            o.skipFiles = args.skip;

            o.nextIteration = o.startFile;
            o.curIteration = o.startFile;
            
            if isempty(args.filePattern)
                o.filePattern = loader.GageHDF5.FILE_PATTERN;
            else
                o.filePattern = args.filePattern;
            end
        end
        
        function set_skip_files(o, skip)
            assert(isempty(skip) || (all(uint32(skip)==skip) && isvector(skip)), 'skaffold:GageHDF5',...
                'loader.GageHDF5.set_skip_files must be provided with a vector of file numbers.');
            
            o.skipFiles = skip;
        end
        
        function skip = get_skip_files(o)
            skip = o.skipFiles;
        end
        
        function add_skip_files(o, skip)
            assert(isempty(skip) || (all(uint32(skip)==skip) && isvector(skip)), 'skaffold:GageHDF5',...
                'loader.GageHDF5.add_skip_files must be provided with a vector of file numbers.');
                        
            o.skipFiles = unique([o.skipFiles, skip]);
        end
        
        function triggers = get_trigger_status(~)
            % Missed/extra trigger detection is handled internally by gage_acquire_h5.py.
            triggers = [];
        end
        
        function set_trigger_status(~, ~)
            % Missed/extra trigger detection is handled internally by gage_acquire_h5.py.
        end
        
        function reset(o)
            o.fileNumber = o.startFile;
            o.nextIteration = o.startFile;
            o.curIteration = o.startFile;
        end
        
        function increment(o, offset)
            if nargin < 2
                offset = 1;
            elseif islogical(offset)
                offset = 1;
            end
            
            for index=1:offset
                o.fileNumber = o.fileNumber + 1;
                % skips unwanted files before loading wanted ones
                while any(o.skipFiles == o.fileNumber) 
                    fprintf('Skipping file ''%s''\n', o.get_filename(o.fileNumber))                
                    o.fileNumber = o.fileNumber + 1;
                end     
            end
        end
        
        function test = has_data(o)
            % Check if at least nTriggers more, unloaded data files exist.
            
            checkFile = max(o.nextIteration, o.fileNumber);
            test = (exist(o.get_file_path(checkFile), 'file') == 2);
        end
        
        function result = preload_iteration(o)
            result = Result.SUCCESS;
            
            if o.fileNumber >= o.nextIteration
                nextFile = o.fileNumber + 1;
            else
                nextFile = o.curIteration + 1;
            end
            
            % skip any files in user-defined o.skipFiles
            while any(o.skipFiles == nextFile)
                nextFile = nextFile + 1;
            end     
            o.nextIteration = nextFile;
        end
        
        function reset_iteration(o)
            % Reset to beginning of current iteration.  Used if analysis is
            % interrupted (missing file, etc.), but we want to try again.
            
            o.fileNumber = o.curIteration;
        end
        
        function advance_iteration(o)
            % Advance to beginning of next iteration, if preload_iteration
            % was called before loading this iteration.
            
            o.fileNumber = max(o.fileNumber, o.nextIteration);
            o.curIteration = o.fileNumber;
        end
        

        function [fields] = get_fields(o)
            % Lookup fieldnames in file

            fields = {};
            
            source_path = o.get_file_path(o.fileNumber);
            if exist(source_path, 'file') ~= 2
                return;
            end

            info = h5info(source_path);
            ch_info = info.Groups;

            for idx=1:numel(ch_info)
                ch = ch_info(idx);
                
                if ~isempty(ch.Datasets)
                    fields = [fields, {ch.Datasets.Name}];
                end
            end
            
        end      
        
        function [result, data, source, timestamp] = load(o,varargin)
            % Loads data from the file           
            data = containers.Map();
            timestamp = 0;
            
            source = o.get_filename(o.fileNumber);
            source_path = o.get_file_path(o.fileNumber);
            if exist(source_path, 'file') ~= 2
                result = Result.OUT_OF_DATA;
                return;
            end

            for i = 1:5
                try
                    info = h5info(source_path);
                    break;
                catch ME
                    switch ME.identifier
                        case 'MATLAB:imagesci:h5info:fileOpenErr'
                            pause(0.5);
                        otherwise
                            rethrow(ME);
                    end
                end
            end
            ch_info = info.Groups;

            for idx=1:numel(ch_info)
                ch = ch_info(idx);
                ch_atts = containers.Map({ch.Attributes.Name},{ch.Attributes.Value});

                range_mVpp = double(ch_atts('input_range'));
                offset_mV = double(ch_atts('dc_offset'));
                resolution = double(ch_atts('sample_res'));
                sample_offset = double(ch_atts('sample_offset'));

                for seg_idx = 1:numel(ch.Datasets)
                    seg_ds = ch.Datasets(seg_idx);

                    sample_data = h5read(source_path, [ch.Name '/' seg_ds.Name]);

                    seg_atts = containers.Map({seg_ds.Attributes.Name},{seg_ds.Attributes.Value});

                    x0 = seg_atts('x0');
                    dx = seg_atts('dx');

                    scaled_data = (sample_offset - double(sample_data))/resolution * range_mVpp / 2000.0 + offset_mV / 1000.0;
                    data(seg_ds.Name) =  Waveform(x0,dx,scaled_data);
                end
            end
            
            file_atts = containers.Map({info.Attributes.Name},{info.Attributes.Value});
            timestamp = file_atts('timestamp');
            if iscell(timestamp)
                timestamp = timestamp{1};
            end
            timestamp = datestr(datenum(timestamp,'yyyy-mm-ddTHH:MM:SS')); % parse to MATLAB datenum 

            result = Result.SUCCESS;
        end      
        
        function source = get_source(o, varargin)  
            source = o.get_filename(o.fileNumber);
        end
        
        function path = get_file_path(o, fileNumber)
            path = fullfile(o.path, o.get_filename(fileNumber));
        end
        
        function path = get_path(o)
            path = o.path;
        end
        
        function root = get_root(o)
            root = o.dataRoot;
        end
        
        function run = get_run(o)
            run = o.runName;
        end        
        
        function filename = get_filename(o, fileNumber)
            filename = sprintf(o.filePattern, fileNumber);
        end
        
    end % methods

    methods (Static)

        function [rootDir, runName] = parse_pathname(pathname)
            tokens = regexpi(pathname, loader.GageHDF5.PATH_REGEX, 'tokens');
            rootDir = tokens{1}{1};
            runName = tokens{1}{2};
            
            if isempty(rootDir) || isempty(runName)
                throw(MException('loader:GageHDF5:parse_pathname',...
                    ['Invalid GageHDF5 data path ''', pathname]));
            end            
        end
        
        function [fileNumber] = parse_filename(filename)
            tokens = regexpi(filename, loader.GageHDF5.FILE_REGEX, 'tokens');
            fileNumber = tokens{1}{1};
            
            if isempty(fileNumber)
                throw(MException('loader:GageHDF5:parse_filename',...
                    ['Invalid GageHDF5 filename ''', filename,...
                    '''.  Expecting file of the format ''', loader.GageHDF5.get_file_name(1), '''.']));
            end   
            
            fileNumber = str2double(fileNumber);
        end    
                
        function [success, path, file] = fileui(path)
            [file, path] = uigetfile(['*.',loader.GageHDF5.FILE_EXT],'Choose GageHDF5 signal file', path);
            if ischar(file)
                success = true;
            else % Aborted UI
                success = false;
                path = '';
                file = '';
            end
        end
    end
end

