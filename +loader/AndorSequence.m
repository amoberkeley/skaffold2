classdef AndorSequence < Loader
    properties (Constant = true)
        FILE_PATTERN = 'image%d';
    end

    properties (SetAccess = protected)       
        dataRoot      % Source path
        filePattern
        
        fileExts
        startFile
        fileNumber
        curIteration
        nextIteration
    end
   
    methods
        function o = AndorSequence(dataRoot, file, varargin)
            p = inputParser();
            p.addRequired('dataRoot', @ischar);
            p.addOptional('file', 1, @(x) isscalar(x) && uint32(x) == x);
            p.addParameter('fileExts', {'.h5'}, @iscellstr);
            p.addParameter('filePattern', [], @(x) isempty(x) || ischar(x));
            p.parse(dataRoot, file, varargin{:});
            args = p.Results;

            o.dataRoot = args.dataRoot;
            o.startFile = args.file;
            o.fileExts = args.fileExts;

            o.fileNumber = o.startFile;
            o.nextIteration = o.startFile;
            o.curIteration = o.startFile;      
            
            if isempty(args.filePattern)
                o.filePattern = loader.AndorSequence.FILE_PATTERN;
            else
                o.filePattern = args.filePattern;
            end            
        end
                
        function test = has_data(o)
            % Checks if new data is available for analysis, beyond any
            % preloaded iteration

            checkFile = max(o.nextIteration, o.fileNumber);
            
            nFiles = length(o.fileExts);
%             fileSize = zeros(1,nFiles);
            for idx=1:nFiles
                checkPath = [o.get_file_path(checkFile), o.fileExts{idx}];
                test = (exist(checkPath, 'file') == 2);
                
                if ~test; return; end
                
%                 fileInfo = dir(checkPath);
%                 fileSize(idx) = fileInfo.bytes;
            end
            
%             test = all(diff(fileSize) == 0);
        end
                
        function fileNumber = increment(o, offset)
            if nargin < 2
                offset = 1;
            elseif islogical(offset)
                offset = 1;
            end

            o.fileNumber = o.fileNumber + offset;
            fileNumber = o.fileNumber;
        end        
                        
        function [result, data, source, timestamp] = load(o,varargin)
            % Loads data from current source file
            
            data = struct();
            source = o.get_filename(o.fileNumber);      
            source_path = o.get_file_path(o.fileNumber);      
            timestamp = zeros(1,6);
            
            img_file = [source_path, o.fileExts{1}];
                
            if ~(exist(img_file, 'file') == 2)
                result = Result.OUT_OF_DATA;
                return
            end
      
            [~,~,ext] = fileparts(img_file);
            
            switch ext
                case '.txt'
                    [result, data, timestamp] = o.load_csv(source_path);
                    
                case '.h5'
                    [result, data, timestamp] = o.load_h5(source_path);
            end
        end
        
        function [result, data, timestamp] = load_csv(o, source_path)
            img_file = [source_path, o.fileExts{1}];

            try
                data.tof = csvread(img_file,1,1);

                fileInfo = dir(img_file);
                timestamp = datevec(fileInfo.datenum);

                signal_file = [source_path, o.fileExts{2}];
                data.sig = csvread(signal_file,1,1);

                consistent = all(size(data.tof) == size(data.sig));

                if numel(o.fileExts) > 2
                    reference_file = [source_path, o.fileExts{3}];                    
                    data.ref = csvread(reference_file,1,1); 
                    consistent = consistent && all(size(data.tof) == size(data.ref));
                end

                if numel(o.fileExts) > 3
                    background_file = [source_path, o.fileExts{4}];                    
                    data.bkg = csvread(background_file,1,1);
                    consistent = consistent && all(size(data.tof) == size(data.bkg));
                end
            catch e
                result = Result.OUT_OF_DATA;
                return
            end
        
            if ~consistent
                result = Result.OUT_OF_DATA;
            else
                result = Result.SUCCESS;
            end            
        end

        function [result, data, timestamp] = load_h5(o, source_path)
            img_file = [source_path, o.fileExts{1}];
            
            for i=1:5
                try
                    info = h5info(img_file);
                    datasets = {info.Datasets.Name};
                    attributes = {info.Attributes.Name};
                catch exc
                    print('Error reading img_file; trying again...')
                    pause(1)
                    if i < 5
                        continue
                    else
                        rethrow(exc)
                    end
                end
                break
            end
            
            if ismember('sig',datasets)
                data.sig = h5read(img_file, '/sig')';
            end            
            if ismember('ref',datasets)
                data.ref = h5read(img_file, '/ref')';
                consistent = all(size(data.sig) == size(data.ref));
            end
            if ismember('bkg',datasets)
                data.bkg = h5read(img_file, '/bkg')';
                consistent = consistent && all(size(data.sig) == size(data.bkg));
            end
            if ismember('abs',datasets)
                data.tof = h5read(img_file, '/abs')';
            else
                data.tof = double(data.sig - data.bkg)./double(data.ref - data.bkg);                
            end    
            
            index = find(strcmp(attributes,'timestamp'));
            timestampStr = info.Attributes(index).Value;           
            timestamp = datevec(timestampStr,'yyyy-mm-ddTHH:MM:SS');

            if ~consistent
                result = Result.OUT_OF_DATA;
            else
                result = Result.SUCCESS;
            end            
        end
                
        
        function source = get_source(o,varargin)
            % Returns filename of current source

            source = o.get_filename(o.fileNumber);
        end

                         
        function result = preload_iteration(o)
            % Looks ahead at next iteration, and checks trigger status
            result = Result.SUCCESS;                                    
            o.nextIteration = max(o.fileNumber,o.curIteration)+1;
        end
        
        function reset_iteration(o)
            % Reset to beginning of current iteration.  Used if analysis is
            % interrupted (missing file, etc.), but we want to try again.

            o.fileNumber = o.curIteration; 
            o.nextIteration = o.curIteration;
        end
        
        function advance_iteration(o)
            % Advance to beginning of next iteration, if preload_iteration
            % was called before loading this iteration.

            o.fileNumber = max(o.fileNumber, o.nextIteration);
            o.curIteration = o.fileNumber;
        end        
        
        function reset(o)
            o.fileNumber = o.startFile;
            o.nextIteration = o.startFile;
            o.curIteration = o.startFile;      
        end   
                
        function path = get_file_path(o, fileNumber)
            path = fullfile(o.dataRoot, o.get_filename(fileNumber));
        end
        
        function root = get_root(o)
            root = o.dataRoot;
        end
        
        function filename = get_filename(o, fileNumber)
            filename = sprintf(o.filePattern, fileNumber);
        end
        

        function triggers = get_trigger_status(o)
            % Gets numerical array of trigger status for each source file
            % processed
            triggers = [];
        end
        
        function set_trigger_status(o, triggers) %!
            % Sets numerical array of trigger status for each source file
            % processed
        end
                
    end
    
end

