classdef Gagescope < Loader
    %Loads Gagescope files, storing output in a Waveform
    
    properties (Constant = true)        
        FILE_PATTERN = 'AS_CH%02d-%05d.sig';
        FILE_REGEX = 'AS_CH(\d+)-(\d+).sig';
        FILE_EXT = 'sig';

        PATH_PATTERN = ['%s_CH%02d' filesep 'Folder.00001'];
        PATH_REGEX = ['(.*)\', filesep, '([^\', filesep, ']+)_CH(\d+)\', filesep, 'Folder.00001\', filesep, '?'];
        GAIN_RANGE = [20, 10, 4, 2, 1, .4, .2]; % Corresponding Peak-to-Peak voltage range for GageScope file captured_gain setting
		SAMPLE_RATES = [1.0, 2.0, 5.0, 10.0, 20.0, 50.0, 100.0, 200.0, 500.0, ...
                1e3, 2e3, 5e3, 1e4, 2e4, 5e4, 1e5, 2e5, 5e5, ...
				1e6, 2e6, 2.5e6, 5e6, 1e7, 1.25e7, 2e7, 2.5e7, 3e7, 4e7, 5e7, 6e7, 6.5e7, 8e7, ...
				1e8, 1.2e8, 1.25e8, 1.3e8, 1.5e8, 2e8, 2.5e8, 3e8, 5e8, 1e9, 2e9, 4e9, 5e9, 8e9, 1e10];
    end
    
    properties (SetAccess = protected)
        dataRate      % Sample rate (s^-1).  Default is 1e6 / s.
        nGageBits     % Number of bits in Gagescope data
        
        path
        dataRoot      % Source path
        runName
        startFile
        fileNumber
        defaultChannel
        defaultRange  % Time range of data to load
        
        skipFiles     % User specified source files to skip

        triggerPattern
        nTriggers
        lastTimestamp
        triggerStatus % Detected bad trigger files to skip        
        nextIteration
        curIteration
        
        filePattern
    end
    
    methods
        function o = Gagescope(varargin)
            %Creates a new GageScope loader
            %
            %Usage:
            %  o = loader.Gagescope(path, file, 'key', val,...)
            %
            %Parameters
            %  file - Starting file name or number
            %  path - Location of file
            %
            %Keys
            %  skipFiles - file numbers to skip loading
            %  dataRate - [] | scalar
            %    Data sampling rate, only used to give a warning if this
            %    differs from the value encoded in each file.
            %  nGageBits - 16 | scalar
            %    Number of bits per datum

            p = inputParser();
            p.addOptional('path', pwd, @ischar);
            p.addOptional('file', '', @(x) ischar(x) || (isscalar(x) && uint32(x) == x));            
            p.addParameter('skip', [], @(x) all(uint32(x)==x))
            p.addParameter('dataRate', [], @(x) isempty(x) || (isscalar(x) && x>0));
            p.addParameter('nGageBits', 16, @(x) isscalar(x) && x>0);
            p.addParameter('defaultRange', [], @(x) isreal(x) && (isempty(x) || numel(x)==2));
            p.addParameter('defaultChannel', 0, @(x) isscalar(x) && x>=0);
            p.addParameter('triggerPattern', [], @(x) isvector(x) && all(x>0));
            p.addParameter('filePattern', [], @(x) isempty(x) || ischar(x));
            p.parse(varargin{:});
            args = p.Results;
                                             
            file = args.file;     
            o.path = args.path;
            
            if isempty(o.path) || isempty(file) || file == 0
                [success, o.path, file] = o.fileui(o.path);
                if ~success % Gets filename and path from UI, stores in o
                    error('skaffold:AbortedByUser','File loading aborted by user.');
                end
            end            
            
            [o.dataRoot, o.runName, o.defaultChannel] = o.parse_pathname(o.path);            
            if ischar(file)
                [o.startFile, o.defaultChannel] = o.parse_filename(file);
            else
                o.startFile = uint32(file);
            end
            o.fileNumber = o.startFile;
            
            o.skipFiles = args.skip;

            o.dataRate = args.dataRate;
            o.nGageBits = args.nGageBits;
            
            o.defaultRange = args.defaultRange;
            
            o.triggerPattern = args.triggerPattern;
            o.nTriggers = length(o.triggerPattern);
            o.triggerStatus = [];
            o.nextIteration = o.startFile;
            o.curIteration = o.startFile;
            
            if args.defaultChannel > 0
                o.defaultChannel = args.defaultChannel;
            end
            
            if isempty(args.filePattern)
                o.filePattern = loader.Gagescope.FILE_PATTERN;
            else
                o.filePattern = args.filePattern;
            end
        end
        
        function set_skip_files(o, skip)
            assert(isempty(skip) || (all(uint32(skip)==skip) && isvector(skip)), 'skaffold:Gagescope',...
                'loader.Gagescope.set_skip_files must be provided with a vector of file numbers.');
            
            o.skipFiles = skip;
        end
        
        function skip = get_skip_files(o)
            skip = o.skipFiles;
        end
        
        function add_skip_files(o, skip)
            assert(isempty(skip) || (all(uint32(skip)==skip) && isvector(skip)), 'skaffold:Gagescope',...
                'loader.Gagescope.add_skip_files must be provided with a vector of file numbers.');
                        
            o.skipFiles = unique([o.skipFiles, skip]);
        end
        
        function triggers = get_trigger_status(o)
            triggers = o.triggerStatus;
        end
        
        function set_trigger_status(o, triggers)
            assert(isempty(triggers) || (all(int32(triggers)==triggers) && isvector(triggers)), 'skaffold:Gagescope',...
                'loader.Gagescope.set_trigger_status must be provided with a vector of trigger counts.');
            o.triggerStatus = triggers;
        end
        
        function reset(o)
            o.fileNumber = o.startFile;
            o.nextIteration = o.startFile;
            o.curIteration = o.startFile;
        end
        
        function increment(o, offset)
            if nargin < 2
                offset = 1;
            elseif islogical(offset)
                offset = 1;
            end
            
            for index=1:offset
                o.fileNumber = o.fileNumber + 1;
                % skips unwanted files before loading wanted ones
                while any(o.skipFiles == o.fileNumber) ||...
                        (length(o.triggerStatus) >= o.fileNumber && o.triggerStatus(o.fileNumber) == -1)
                    fprintf('Skipping file ''%s''\n', o.get_filename(o.defaultChannel, o.fileNumber))                
                    o.fileNumber = o.fileNumber + 1;
                end     
            end
        end
        
        function test = has_data(o)
            % Check if at least nTriggers more, unloaded data files exist.
            
            checkFile = max(o.nextIteration, o.fileNumber) + o.nTriggers - 1; % Check for last trigger file in pattern
            test = (exist(o.get_file_path(o.defaultChannel, checkFile), 'file') == 2);
        end
        
        function result = preload_iteration(o)
            result = Result.SUCCESS;
            
            step = 1;
            if o.fileNumber >= o.nextIteration
                nextFile = o.fileNumber;
            else
                nextFile = o.curIteration;
            end
            while step <= o.nTriggers                
                if nextFile > length(o.triggerStatus) || o.triggerStatus(nextFile) == 0
                    % triggerStatus does not contain information on this
                    % trigger.  Check source file.
                    [result, header] = o.load_header(nextFile);
                    if result == Result.OUT_OF_DATA
                        return
                    end

                    o.check_trigger(nextFile, step, header);
                end
                    
                if o.triggerStatus(nextFile) == -1
                    % Trigger is marked as extra, skip it
                    % Don't advance to next step
                    fprintf('Extra trigger at file ''%s''.\n', o.get_filename(1, nextFile));     
                elseif o.triggerStatus(nextFile) ~= step
                    % Gap in trigger status steps indicates missed
                    % trigger, set result to abort analysis
                    fprintf('Missed trigger before file ''%s'', skipping iteration... \n', o.get_filename(1, nextFile));
                    result = Result.ABORT;
                    step = step + 1; % Advance to next step   
                    continue % Don't advance to next file               
                else
                    step = step + 1; % Advance to next step                         
                end
                
                nextFile = nextFile + 1; % Advance to next file
                % skip any files in user-defined o.skipFiles
                while any(o.skipFiles == nextFile)
                    o.triggerStatus(nextFile) = 0;
                    nextFile = nextFile + 1;
                end     
            end
            
            o.nextIteration = nextFile;
        end

        function [result, header] = load_header(o, nextFile)
            result = Result.SUCCESS;
            header = [];
            source_path = o.get_file_path(1, nextFile);
            if exist(source_path, 'file') ~= 2
                result = Result.OUT_OF_DATA;
                return;
            end

            fid = fopen(source_path);
            if fid<0
                result = Result.OUT_OF_DATA;
                return;
            end

            header = loader.Gagescope.parse_header(fid);
            fclose(fid);            
        end

        function check_trigger(o, nextFile, step, header)
            % Check trigger timestamp against trigger pattern, to match
            % bad triggers.
            if ~isempty(o.lastTimestamp)
                elapsed = etime(header.trigger_timestamp, o.lastTimestamp);
                error = elapsed - o.triggerPattern(step);
                if error < -2
                    % Elapsed time too short, must be an extra trigger.
                    % Skip this file, and continue with the next file.
                    o.triggerStatus(nextFile) = -1;
                elseif error > 2 && step > 1  % GS timestamp precision is 2 seconds
                    % Elapsed time too long, and not the first trigger.
                    % (Assume sequence can be interrupted only before
                    % first trigger).  A trigger must have been missed,
                    % in which case the current file is the next
                    % trigger in the sequence, or the first trigger of
                    % the next sequence.  In either case, don't
                    % increment file, but continue to check next step.

                    if step >= o.nTriggers
                        % If we've advanced beyond the final step in the
                        % trigger pattern, assume this is the first file of
                        % the next sequence
                        o.triggerStatus(nextFile) = 1;
                        o.lastTimestamp = header.trigger_timestamp; 
                    else
                        % Recurse to check same file against next step

                        % Update lastTimestamp to expected trigger
                        % position, so the current file can be checked
                        % against the next step in the trigger pattern

                        o.lastTimestamp = o.lastTimestamp + [0, 0, 0, 0, 0, o.triggerPattern(step)];
                        o.check_trigger(nextFile, step + 1, header);
                    end
                else
                    o.triggerStatus(nextFile) = step;
                    o.lastTimestamp = header.trigger_timestamp;                   
                end   
            else
                o.triggerStatus(nextFile) = step;
                o.lastTimestamp = header.trigger_timestamp;                  
            end                            
        end        
        
        function reset_iteration(o)
            % Reset to beginning of current iteration.  Used if analysis is
            % interrupted (missing file, etc.), but we want to try again.
            
            o.fileNumber = o.curIteration;
        end
        
        function advance_iteration(o)
            % Advance to beginning of next iteration, if preload_iteration
            % was called before loading this iteration.
            
            o.fileNumber = max(o.fileNumber, o.nextIteration);
            o.curIteration = o.fileNumber;
        end
        
        function [result, data, source, timestamp] = load(o,varargin)
            % Loads data from the file           
            data = [];
            timestamp = 0;
            
            p = inputParser();
            p.addOptional('channel',o.defaultChannel,@(x) isscalar(x) && x>0);
            p.addParameter('range', o.defaultRange, @(x) isreal(x) && (isempty(x) || numel(x)==2));
            p.parse(varargin{:});
            args = p.Results;
            
            source = o.get_filename(args.channel, o.fileNumber);
            source_path = o.get_file_path(args.channel, o.fileNumber);
            if exist(source_path, 'file') ~= 2
                result = Result.OUT_OF_DATA;
                return;
            end

            [fid, message] = fopen(source_path);
            while fid<0
                warning('loader:Gagescope','Failed opening file %s: %s. Trying again in 5 s...', source_path, message);
                pause(5);
                [fid, message] = fopen(source_path);
            end           

            % Skip GageScope header
            %fseek(fid, 512, 'bof');
           
            % Load GageScope header
            header = loader.Gagescope.parse_header(fid);
            timestamp = datestr(header.trigger_timestamp);
            
            if header.sample_rate_index < 47
                sample_rate = loader.Gagescope.SAMPLE_RATES(header.sample_rate_index + 1);
            else
                sample_rate = header.external_clock_rate;
            end
            
            if ~isempty(o.dataRate) && sample_rate ~= o.dataRate
                warning(['GageScope sample rate encoded in file (%.2e) does not match manually configured value (%.2e).'...
                    ' Using the file value.'], sample_rate, o.dataRate);
            end
            
            if isempty(args.range)
                raw = fread(fid, 'int16');             % Reads to end of file
                tStart = 0.0;
            else
                tStart = min(args.range);
                tStop = max(args.range);
                fseek(fid, 2*floor(tStart*sample_rate), 'cof');
                raw = fread(fid, ceil((tStop - tStart)*sample_rate), 'int16');             % Reads to end of file
            end
            fclose(fid);

            vRange = loader.Gagescope.GAIN_RANGE(header.captured_gain + 1);            

            % This below is the 'textbook' conversion of the raw GageScope
            % samples, according to the SDK manual. However, the GageScope
            % software does not save the correct value for
            % 'sample_resolution' in the header, as compared to the raw
            % binary values saved in the file. The header indicates a
            % resolution consistent with a 14bit range (-2^13), however the values
            % in the file are scaled to a full 16bit range.
%             scaled = (header.sample_offset - raw)/header.sample_resolution * vRange/2;

            % Hence, this amended calculation is used to maintain
            % consistency between *.sig files saved from GageScope, and
            % those saved via the csAPI through Python. This assumes the
            % data are saved with the minimum (maximum) voltages matching
            % the smallest (largest) value of a 16bit signed integer, with
            % vRange the full scale, peak-to-peak voltage range.
            scaled = (raw - double(header.sample_offset))/2^(o.nGageBits)*vRange;

            data = Waveform(tStart, 1/sample_rate, scaled);
            result = Result.SUCCESS;
        end      
        
        function source = get_source(o, varargin)  
            p = inputParser();
            p.KeepUnmatched = true;
            p.addOptional('channel',o.defaultChannel,@(x) isscalar(x) && x>0);
            p.parse(varargin{:});
            args = p.Results;
            
            source = o.get_filename(args.channel, o.fileNumber);
        end
        
        function path = get_file_path(o, channel, fileNumber)
            path = fullfile(o.dataRoot, o.get_pathname(o.runName, channel), o.get_filename(channel, fileNumber));
        end
        
        function path = get_path(o, channel)
            path = fullfile(o.dataRoot, o.get_pathname(o.runName, channel));
        end
        
        function root = get_root(o)
            root = o.dataRoot;
        end
        
        function run = get_run(o)
            run = o.runName;
        end
        
        function filename = get_filename(o, channel, fileNumber)
            filename = sprintf(o.filePattern, channel, fileNumber);
        end
        
    end % methods

    methods (Static)       
        function pathname = get_pathname(runName, channel)
            pattern = regexprep(loader.Gagescope.PATH_PATTERN,'\\','\\\\'); % Escape forward slash for Windows...
            pathname = sprintf(pattern, runName, channel);
        end        

        function [rootDir, runName, channel] = parse_pathname(pathname)
            tokens = regexpi(pathname, loader.Gagescope.PATH_REGEX, 'tokens');
            rootDir = tokens{1}{1};
            runName = tokens{1}{2};
            channel = tokens{1}{3};
            
            if isempty(rootDir) || isempty(runName) || isempty(channel)
                throw(MException('loader:Gagescope:parse_pathname',...
                    ['Invalid GageScope data path ''', pathname,...
                    '''.  Expecting path of the format ''', loader.Gagescope.get_pathname('runN', 1), '''.']));
            end            
            
            channel = str2double(channel);
        end
        
        function [fileNumber, channel] = parse_filename(filename)
            tokens = regexpi(filename, loader.Gagescope.FILE_REGEX, 'tokens');
            channel = tokens{1}{1};
            fileNumber = tokens{1}{2};
            
            if isempty(channel) || isempty(fileNumber)
                throw(MException('loader:Gagescope:parse_filename',...
                    ['Invalid GageScope filename ''', filename,...
                    '''.  Expecting file of the format ''', loader.Gagescope.get_file_name(1, 1), '''.']));
            end   
            
            channel = str2double(channel);
            fileNumber = str2double(fileNumber);
        end    
                
        function [success, path, file] = fileui(path)
            [file, path] = uigetfile(['*.',loader.Gagescope.FILE_EXT],'Choose GageScope signal file', path);
            if ischar(file)
                success = true;
            else % Aborted UI
                success = false;
                path = '';
                file = '';
            end
        end
        
        function metadata = parse_header(fid)
            metadata = struct();
            metadata.file_version = fread(fid, 14, '*char')';
            fseek(fid, 2, 0);
            metadata.name = fread(fid, 9, '*char')';
            fseek(fid, 2, 0);
            metadata.comment = fread(fid, 256, '*char')';
            fseek(fid, 4, 0);
            metadata.sample_rate_index = fread(fid, 1, '*int16');
            metadata.operation_mode = fread(fid, 1, '*int16');
            metadata.trigger_depth = fread(fid, 1, '*int32');
            metadata.trigger_slope = fread(fid, 1, '*int16');
            metadata.trigger_source = fread(fid, 1, '*int16');
            metadata.trigger_level = fread(fid, 1, '*int16');
            
            metadata.sample_depth = fread(fid, 1, '*int32');
            metadata.captured_gain = fread(fid, 1, '*int16');
            metadata.captured_coupling = fread(fid, 1, '*int16');
            metadata.current_mem_ptr = fread(fid, 1, '*int32');
            metadata.starting_address = fread(fid, 1, '*int32');
            metadata.trigger_address = fread(fid, 1, '*int32');
            metadata.ending_address = fread(fid, 1, '*int32');
            
            metadata.trigger_time = fread(fid, 1, '*uint16');
            metadata.trigger_date = fread(fid, 1, '*uint16');
            metadata.trigger_coupling = fread(fid, 1, '*int16');
            metadata.trigger_gain = fread(fid, 1, '*int16');
            
            metadata.probe = fread(fid, 1, '*int16');
            metadata.inverted_data = fread(fid, 1, '*int16');
            metadata.board_type = fread(fid, 1, '*uint16');
            metadata.resolution_12_bits = fread(fid, 1, '*int16');
            metadata.multiple_record = fread(fid, 1, '*int16');
            metadata.trigger_probe = fread(fid, 1, '*int16');
            
            metadata.sample_offset = fread(fid, 1, '*int16');
            metadata.sample_resolution = fread(fid, 1, '*int16');
            metadata.sample_bits = fread(fid, 1, '*int16');
            metadata.extended_trigger_time = fread(fid, 1, '*uint32');
            
            metadata.imped_a = fread(fid, 1, '*int16');
            metadata.imped_b = fread(fid, 1, '*int16');
            metadata.external_tbs = fread(fid, 1, '*float');
            metadata.external_clock_rate = fread(fid, 1, '*float');
            
            metadata.file_options = fread(fid, 1, '*int32');
            metadata.version = fread(fid, 1, '*uint16');
            metadata.eeprom_options = fread(fid, 1, '*uint32');
            metadata.trigger_hardware = fread(fid, 1, '*uint32');
            metadata.record_depth = fread(fid, 1, '*uint32');
            fseek(fid, 127, 0);
            
            % Reconstruct date and time
            time = metadata.trigger_time;
            date = metadata.trigger_date;
            
            day = bitand(date, uint16(31));
            month = bitshift(bitand(date, bitshift(uint16(15),5)),-5);
            year = bitshift(date, -9) + 1980;

            second = bitand(time, uint16(31))*2;
            minute = bitshift(bitand(time, bitshift(uint16(63),5)),-5);
            hour = bitshift(time, -11);
            metadata.trigger_timestamp = double([year, month, day, hour, minute, second]);
        end        
    end
end

