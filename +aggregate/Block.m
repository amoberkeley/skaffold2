classdef Block < Aggregator
    % Block of aggregators to run
    
    properties (SetAccess = protected)
        aggregators
        ifField
    end
    
    methods
        function o = Block(aggregators,varargin)
            %Constructs a new Mean aggregator
            %
            %Usage:
            %
            %    o = aggregate.Block(sources)
            %
            %Parameters:
            %  source - {} | cell array of Aggregators
            %    List of aggregators to run.
            %
            %Keys:
            %  if - '' | string
            %    Field name with logical array of verification results.
            %    Block doesn't run if this is false for a given loop and point
            
            p = inputParser;
            p.addRequired('aggregators', @(x) isa(x, 'Task') || ...
                (iscell(x) && ~isempty(x) && all(cellfun(@(y) isa(y,'Task'),x))))
            p.addParameter('if', '', @ischar);
            p.parse(aggregators,varargin{:})
            v = p.Results;
            
            if iscell(v.aggregators)
                o.aggregators = v.aggregators;
            else
                o.aggregators = {v.aggregators};
            end
            o.ifField = v.if;
        end
        
        function result = run(o,aggModel,loop,point,frame,varargin)
            result = Result.SUCCESS;
            if ~isempty(o.ifField)
                valid = aggModel.get(o.ifField);
                if ~valid(loop, point)
                    % Call 'skip' method of aggregators if validation
                    % failed
                    o.skip(aggModel,loop,point,varargin{:});
                    return;
                end
            end

            for i=1:length(o.aggregators)
                result = o.aggregators{i}.run(aggModel, loop, point, frame, varargin{:});
                
                switch result
                    case Result.SKIP
                        % reset result status (block succeeds, even if a
                        % member skips
                        result = Result.SUCCESS;
                        
                    case Result.ABORT 
                        % Immediately interrupts execution for this iteration only, and doesn't call 'skip'
                        return
                        
                    case Result.FAIL
                        % Immediately interrupts execution for entire scan
                        return
                end
            end
        end
        
        function skip(o,aggModel,loop,point,varargin)
            for i=1:length(o.aggregators)
                o.aggregators{i}.skip(aggModel, loop, point, varargin{:});
            end            
        end
        
        function add(o, aggregator)
            % Add an Aggregator into the block.
            %
            % Usage:
            %  obj.add(aggregator)
            %
            %Parameters:
            %  aggregator - Aggregator to insert

            
            assert(isa(aggregator,'Aggregator'),'skaffold:TypeError',...
                'Can only insert Aggregator objects into aggregator.Block.');

            o.aggregators{length(o.aggregators)+1} = aggregator;
        end
    end
    
end

