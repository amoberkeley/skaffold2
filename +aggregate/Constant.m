classdef Constant < Aggregator
    % Sets field of aggregate model to constant value
    
    properties (SetAccess = protected)                
        values
    end
    
    methods
        function o = Constant(varargin)
            %Constructs a new Constant aggregator
            %
            %Usage:
            %
            %    o = aggregate.Constant('target',value,...)
            %
            %Parameters:
            %  target - field name
            %    Name of field to set
            %  value - any
            %    Value to set
            
            p = inputParser;
            p.KeepUnmatched = true;
            p.parse(varargin{:})
            
            o.values = p.Unmatched;
        end
        
        function result = run(o,aggModel,~,~,~,varargin)
            fields = fieldnames(o.values);
            for i=1:length(fields)
                field = fields{i};
                aggModel.set(field, o.values.(field));
            end
            
            result = Result.SUCCESS;            
        end
       

        function result = skip(o,aggModel,~,~,varargin)  
            fields = fieldnames(o.values);
            for i=1:length(fields)
                field = fields{i};
                aggModel.set(field, o.values.(field));
            end
            
            result = Result.SUCCESS;            
        end        
    end
end

