classdef Verifier < Aggregator
    % Runs a list of Verifiers on each DataModel, storing the result in the
    % aggregate.
    
    properties (SetAccess = protected)
        target
        verifiers
    end
    
    methods
        function o = Verifier(target, verifiers)
            %Constructs a new Verifier aggregator
            %
            %Usage:
            %
            %    o = aggregate.Verifier(target, verifiers)
            %
            %Parameters:
            %  target - name of target aggregate DataModel field
            %    Name of field to aggregate results of verification into.
            %  verifiers - cell array of Verifier
            %    List of Verifiers to run
            %
            
            p = inputParser;
            p.addRequired('target', @ischar)
            p.addRequired('verifiers', @(x) isa(x,'Verifier') || ...
                (iscell(x) && all(cellfun(@(y) isa(y,'Verifier'),x))));
            p.parse(target, verifiers)
            v = p.Results;
            
            o.target = v.target;
            o.verifiers = v.verifiers;
        end
        
        function result = run(o,aggModel,loop,point,frame,varargin)            
            if aggModel.has(o.target)
                valid = aggModel.get(o.target);
            else
                valid = [];
            end
            
            failed = false;
            for i=1:length(o.verifiers)
                [result, message] = o.verifiers{i}.verify(frame, loop, point);
                if ~result
                    if ~failed
                        fprintf('Model on loop %d, point %d, failed verification:\n',loop,point);
                    end
                    fprintf('\t%s\n',message);    
                    failed = true;
                end
            end
                       
            valid(loop, point) = ~failed;
            
            aggModel.set(o.target, valid);
            aggModel.set([o.target,'_verifiers'], o.verifiers);
            
            result = Result.SUCCESS;
        end
    end
end

