classdef Collect < Aggregator
    % Aggregates an array of (verified) data from DataModel fields
    %
    % aggregate.Collect builds arrays of model field data.  The built array has
    % dimensions of (<number of loops>,<number of points>), where it is
    % assumed that the scan was a repeating number of loops over a number of
    % unique experimental settings, called points.
    %
    % If the model field is a single-element object, the aggregated field
    % will consist of a (<nLoops>,<nPoints>) element array of those objects.
    % If the model field is a multi-element array of objects, the aggregated
    % field will consist of a (<nLoops>,<nPoints>) element cell array of 
    % the multi-element arrays.
    
    properties (SetAccess = protected)
        sources       % Cell array of source fields to aggregate
        targets
        ifField
        debug         % One of 'none', 'point', or 'loop' (see process())
        failedValues
    end
    
    methods
        function o = Collect(source,varargin)
            %Constructs a new Collect aggregator
            %
            %Usage:
            %
            %    o = aggregate.Collect(sources,'key',value,...)
            %
            %Parameters:
            %  source - {} | cell array of strings
            %    Name of field(s) to aggregate.  If empty, aggregate all
            %    settable fields in DataModel.
            %
            %Keys:
            %  debug - 'none' | 'loop' | 'point'
            %    Specifies when to print information regarding to the scan
            %  failedValue - struct
            %    Values to set if verification fails.
            
            p = inputParser;
            p.addRequired('sources', @(x) ischar(x) || (iscellstr(x) && ~isempty(x)));
            p.addParameter('targets', {}, @(x) ischar(x) || iscellstr(x));
            p.addParameter('if', '', @ischar);            
            p.addParameter('else', struct(), @isstruct);
            p.addParameter('debug','none',@(x) any(strcmp(x,...
                {'none', 'loop', 'point'}) ) );
            p.parse(source,varargin{:})
            v = p.Results;
            
            
            if ischar(v.sources)
                o.sources = {v.sources};
            else
                o.sources = v.sources;
            end
            
            o.targets = o.sources;
            if ischar(v.targets)
                o.targets{1} = v.targets;
            elseif ~isempty(v.targets)
                o.targets(1:length(v.targets)) = v.targets;
            end
            
            o.ifField = v.if;            
            o.debug = v.debug;
            o.failedValues = v.else;
        end
        
        function result = run(o,aggModel,loop,point,frame,varargin)
            %Aggregates data from a single DataModel into the aggregate DataModel.
            %
            %Usage:
            %     aggModel = obj.run(aggModel,loop,point,frame)
            %
            %Prints information regarding the progress of the scan
            %according to the value of obj.debug:
            %  'none' - Suppress printing
            %  'loop' - Indicate progress at start of every loop
            %  'point' - Indicate progress for every call to process()
                                       
            switch o.debug
                case 'point'
                    fprintf('Aggregating loop: %d\tpoint: %d\n',loop,point)
                case 'loop'
                    if point==1, fprintf('Aggregating loop: %d\n',loop); end
            end
            
            %Determine whether model passed verification
            if ~isempty(o.ifField)
                valid = aggModel.get(o.ifField);
                valid = valid(loop, point);
            else
                valid = true;
            end
            
            %Aggregate all fields
            for i=1:length(o.sources)
                if valid
                    data = frame.get(o.sources{i});
                elseif isfield(o.failedValues, o.sources{i})
                    data = o.failedValues.(o.sources{i});
                else
                    data = NaN;
                end

                if aggModel.has(o.targets{i})
                    aggData = aggModel.get(o.targets{i});                 
                elseif isstruct(data)
                    aggData = struct();
                else
                    aggData = [];
                end
                
                aggData = o.scan_aggregate(aggData, data, loop, point);
                aggModel.set(o.targets{i}, aggData);
            end % for i over sources
            
            result = Result.SUCCESS;            
        end
       

        function result = skip(o,aggModel,loop,point,varargin)  
            switch o.debug
                case 'point'
                    fprintf('Skipping loop: %d\tpoint: %d\n',loop,point)
                case 'loop'
                    if point==1, fprintf('Skipping loop: %d\n',loop); end
            end
            
            %Aggregate all fields
            for i=1:length(o.sources)
                if aggModel.has(o.targets{i})
                    aggData = aggModel.get(o.targets{i});                 
                else
                    aggData = [];
                end
                
                if isfield(o.failedValues, o.sources{i})
                    aggData = o.failedValues.(o.sources{i});
                elseif isstruct(aggData)
                    
                elseif iscell(aggData)
                    aggData(loop,point) = {NaN};
                else
                    aggData(loop,point) = NaN;
                end
                
                aggModel.set(o.targets{i}, aggData);
            end % for i over sources
            
            result = Result.SUCCESS;            
        end        
        
    end
        
    methods (Static)
        function aggData = scan_aggregate(aggData,data,loop,point)
            %Implements aggregation algorithm.
            %
            %If data is a scalar number, aggregates into simple array;
            %otherwise, aggregates into cell array.
            %
            %Usage:
            %
            %    agg = ScanAggregator.scan_aggregate(...
            %             agg, new, loop, point)
            %
            %Parameters:
            %  agg - Either:  1) An empty matrix, or 2) old aggregate data
            %    from earlier calls to average_in.
            %  new - New data to aggregate.
            %  loop - Scan loop.
            %  point - Index of unique scan point.
                
            if loop==1 && point==1
                if (isscalar(data) && isnumeric(data)) || isstruct(data)
                    aggData = data;
                elseif isstruct(data)
                    aggData = data;
                else
                    aggData = {data};
                end
            end
                         
            if ~iscell(aggData)
%                 if size(aggData,1) < loop
%                     aggData(loop,:) = NaN;
%                 end
%                 if size(aggData,2) < point
%                     aggData(:,point) = NaN;
%                 end
                
                aggData(loop,point) = data;
            else
                aggData(loop,point) = {data};
            end            
        end
    end
    
end

