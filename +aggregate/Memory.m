classdef Memory < Aggregator
   
    properties (SetAccess = protected)
        source       % source field to cache
        targets
        ifField
        nCache        % Number of loops to keep in memory cache
        points
        weighting
    end
    
    methods
        function o = Memory(varargin)
            p = inputParser;
            p.addRequired('source', @(x) ischar(x) && ~isempty(x) );
            p.addRequired('targets', @(x) (ischar(x) || iscellstr(x)) && ~isempty(x) );
            p.addParameter('if', '', @ischar);
            p.addRequired('nCache', @(x) isscalar(x) && x>0);
            p.addParameter('points','separate',@(x) ismember(x, {'separate','ignore'}));
            p.addParameter('weighting','flat',@(x) ismember(x, {'flat','linear'}));
            
            p.parse(varargin{:})
            v = p.Results;
            
            o.source = v.source;
            
            if iscellstr(v.targets)
                o.targets = v.targets;
            else
                o.targets = {v.targets};
            end
            
            o.ifField = v.if;    
            o.nCache = v.nCache;
            o.points = v.points;
            o.weighting = v.weighting;
        end
        
        function result = run(o,aggModel,loop,point,model)
            result = Result.SUCCESS;
            
            % Get number of good points
            if ~isempty(o.ifField)
                isValid = aggModel.get(o.ifField);
                isValid = isValid(loop, point);
            else
                isValid = true;
            end
            
            iteration = (loop-1)*point + point;
            if strcmp(o.points, 'ignore')
                point = 1;
                loop = iteration;
            end

            datum = model.get(o.source);

            if aggModel.has(o.targets{1})
                cacheData = aggModel.get(o.targets{1});
            else
                if isscalar(datum) && isnumeric(datum)
                    cacheData = [];  
                else
                    cacheData = {};
                end
            end
            
            if isValid
                % Aggregate new data into aggregate data model cache field

                % Ensure cache grows no longer then the current loop number, the
                % nCache limit, or one more then the current length
                cacheLength = min([loop, o.nCache, size(cacheData,1)+1]);

                if iscell(cacheData)
                    datum = {datum};
                end

                if cacheLength == 1
                    cacheData(1,point) = datum;
                else
                    cacheData(1:cacheLength,point) = [datum; cacheData(1:cacheLength-1,point)];
                end            

                aggModel.set(o.targets{1}, cacheData);
            end

            cacheLength = size(cacheData,1);
            
            if cacheLength > 1 && length(o.targets) > 1
                % Average target provided, calculate average
                
                switch o.weighting
                    % Calculate data weights for averaging
                    case 'linear'
                        weights = (cacheLength:-1:1)';
                        weights = weights / sum(weights); % Normalize
                    case 'flat'
                        weights = repmat(1 / cacheLength, cacheLength, 1);
                end

                if aggModel.has(o.targets{2})
                    avgData = aggModel.get(o.targets{2});
                else
                    avgData = {};
                end
                
                % Calculate weighted average
                if iscell(cacheData)
                    avgData{point} = cacheData{1,point} * 0;
                    for j = 1:cacheLength
                        datum = cacheData{j,point};
                        avgData{point} = avgData{point} + datum * weights(j);
                    end
                else
                    avgData{point} = sum(cacheData(1:cacheLength,point).*weights);
                end           

                % Set average in aggregated datamodel
                aggModel.set(o.targets{2}, avgData);
            end
        end
    end   
end

