classdef Mean < Aggregator
    % Aggregates the Mean of DataModel fields
    
    properties (SetAccess = protected)
        sources       % Cell array of source fields to average
        targets
        ifField        
        debug        % One of 'none', 'point', or 'loop' (see process())        
    end
    
    methods
        function o = Mean(source,varargin)
            %Constructs a new Mean aggregator
            %
            %Usage:
            %    o = aggregate.Mean(sources,'key',value,...)
            %
            %Parameters:
            %  source - {} | cell array of strings
            %    Name of field(s) to aggregate.  If empty, aggregate all
            %    waveforms in DataModel.
            %
            %Keys:
            %  update - 'none' | 'loop' | 'point'
            %    Specifies when to print information regarding to the scan
            
            p = inputParser;
            p.addRequired('sources', @(x) ischar(x) || (iscellstr(x) && ~isempty(x)))
            p.addParameter('targets', {}, @(x) ischar(x) || iscellstr(x));            
            p.addParameter('if', '', @ischar);            
            p.addParameter('debug','none',@(x) any(strcmp(x,...
                {'none', 'loop', 'point'}) ) );
            p.parse(source,varargin{:})
            args = p.Results;
            
            if ischar(args.sources)
                o.sources = {args.sources};
            else
                o.sources = args.sources;
            end
                        
            o.targets = o.sources;
            if ischar(args.targets)
                o.targets{1} = args.targets;
            elseif ~isempty(args.targets)
                o.targets{1:length(args.targets)} = args.targets;
            end
            
            o.ifField = args.if;            
            o.debug = args.debug;
        end
        
        function result = run(o,aggModel,loop,point,frame,varargin)
            % Averages in data from a new model.
            %
            % See 'average_in' for description of the aggregation algorithm.
            %
            % Usage:
            %     result = obj.run(aggModel,loop,point,frame)
            %
            % Prints information regarding the progress of the scan
            % according to the value of obj.debug:
            %  'none' - Suppress printing
            %  'loop' - Indicate progress at start of every loop
            %  'point' - Indicate progress for every call to process()
            
            result = Result.SUCCESS;
            
            switch o.debug
                case 'point'
                    fprintf('Aggregating loop: %d\tpoint: %d\n',loop,point)
                case 'loop'
                    if point==1, fprintf('Aggregating loop: %d\n',loop); end
            end
             
            % Get number of good points
            if ~isempty(o.ifField)
                valid = aggModel.get(o.ifField);
                if ~valid(loop, point)
                    return
                end
                % Count number of good iterations on this point up to the
                % current loop
                nGood = sum(logical(valid(1:loop,point)));
            else
                % Assume all iterations up till now are good.
                nGood = loop;
            end
                    
            %Average all fields
            for i=1:length(o.sources)
                data = frame.get(o.sources{i});
                target = o.targets{i};
                
                if aggModel.has(target)
                    aggData = aggModel.get(target);
                else
                    %Initialize empty aggregate 
                    if isa(data, 'Waveform')
                        aggData = Waveform(data.x0, data.dx, []);
                    else
                        aggData = [];
                    end
                end
                
                if isa(data, 'Waveform')
                    aggData.data = o.average_in(aggData.data, data.data, nGood, point);
                else
                    aggData = o.average_in(aggData, data, nGood, point);
                end                    

                aggModel.set(target, aggData);
            end % for i over sources
        end
    end
    
    methods (Static)
        function aggData = average_in(aggData,newData,nAvg,point)
            %Implements aggregation algorithm.
            %
            %If aggregating a row vector, averages data with aggregate data
            %at row indexed by 'point'.  Likewise if aggregating a column
            %vector, averages data with aggregate data at column indexed by
            %'point'.
            %
            %If aggregating a multidimensional array having n dimensions,
            %averages data with those aggregate data having their
            %n+1-dimensional index equal to 'point'.
            %
            %In either case, automatically initializes new dimensions of
            %the aggregate data if necessary.
            %
            %The algorithm used to average in new data at a specific point
            %is:
            %
            %   aggregate = ((nAvg-1)*aggregate + new)/nAvg
            %
            %Usage:
            %
            %    agg = aggregate.Mean.average_in(...
            %             agg, new, nAvg, point)
            %
            %Parameters:
            %
            %  agg - Either:  1) An empty matrix, or 2) old aggregate data
            %    from earlier calls to average_in.
            %  new - New data to aggregate.
            %  nAvg - Number of samples in old average.
            %  point - Index of unique scan point.
            %
            %Examples:
            %
            %  Average in noisy sine waves:
            %
            %  agg = [];
            %  t = linspace(0,2*pi,101);
            %  for loop=1:10
            %    for point=1:4
            %      data = sin(t+point*pi/2)+randn(size(t));
            %      agg = aggregate.Mean.average_in(...
            %              agg,data,loop,point);
            %    end
            %  end
            %  plot(t,agg)
            %
            %  Average images:
            %
            %  agg = [];
            %  for point=1:4
            %    for loop=1:10
            %      D = <get an image>
            %      agg = aggregate.Mean.average_in(...
            %              agg,D,loop,point);
            %    end
            %    figure(point)
            %    imagesc(agg(:,:,point))
            %  end
                
            if isscalar(newData)
                if length(aggData) < point
                    aggData(1,point) = newData;
                else
                    aggData(1,point) = ( (nAvg-1)*aggData(1,point) + newData ) / nAvg;
                end
            elseif isvector(newData) % Averaging vectors               
                if isrow(newData)
                    if size(aggData,1) < point
                        aggData(point,:) = newData;
                    else
                        aggData(point,:) = ( (nAvg-1)*aggData(point,:) + newData ) / nAvg;    
                    end
                else
                    if size(aggData,2) < point
                        aggData(:,point) = newData;
                    else
                        aggData(:,point) = ( (nAvg-1)*aggData(:,point) + newData ) / nAvg;    
                    end
                end
            else % Averaging arrays
                catDim = ndims(newData)+1;                               
                dims = repmat({':'}, 1, ndims(newData));
                if isempty(aggData) || size(aggData, catDim) < point
                    aggData(dims{:}, point) = newData;
                else
                    aggData(dims{:}, point) = ( (nAvg-1)*aggData(dims{:}, point) + newData ) / nAvg;    
                end                                   
            end
        end
    end
end

