classdef Waveform < matlab.mixin.CustomDisplay
    %Stores data waveforms.
    %
    %A Waveform object is used to store evenly sampled data.  The object
    %contains three variables:
    %  data - The sampled data array.
    %  x0 - The abcissa datum corresponding to the first element of the
    %       sampled data.
    %  dx - The data sampling interval.
    %
    %The purpose of storing data this way is twofold:  First, we can
    %generally reduce the storage space for these sorts of data.  Second,
    %some operations can experience a significant performance increase 
    %(linear interpolation, for instance, can skip its time-consuming 
    %sorting and searching steps, reducing the time for interpolation from 
    %O(n log n) to O(1)).
    
    %% Data variables
    properties
        data
        x0
        dx
    end
    
    %% Methods
    methods
        
        %% Methods: Constructor
        
        function o = Waveform(varargin)
            %Builds a new Waveform
            %
            %Usage:
            %
            %    o = Waveform()
            %
            %  Creates a default Waveform.
            %
            %    o = Waveform(waveform)
            %
            %  Duplicates waveform.
            %
            %    o = Waveform(x,data)
            %
            %  Computes x0 and dx from evenly-spaced x.  Takes O(n) to
            %  execute.
            %
            %    o = Waveform(x0,dx,data)
            %
            %  Fills object fields directly.
            %
            %    o = Waveform(x0,dx,length,function)
            %
            %  Creates waveform data from function.  Algorithmic scaling
            %  depends on function (most will execute in O(n)).
            %
            %  Example:
            %    o = Waveform(0,2*pi*0.01,101,@sin);
            %    o.plot()  % Plots sine wave from 0 to 2 pi
            %  
            %Parameters:
            %
            %  x - Evenly-spaced array of same length as data
            %  x0 - First abcissa datum
            %  dx - Abcissa step
            %  data - Waveform data
            %  length - Number of waveform data to create
            %  function - Data creation function
            if nargin==0
                o.x0 = 0; o.dx = 1; o.data = [];
            elseif nargin==1
                w = varargin{1};
                assert(isa(w,'Waveform'),'Waveform:TypeError',...
                        'Input to Waveform(%s) must be of type Waveform',...
                        inputname(1));
                o.x0 = w.x0; o.dx = w.dx; o.data = w.data;
            elseif nargin==2
                x = varargin{1};    y = varargin{2};    sy = size(y);
                assert(...
                    isvector(x) && (length(x)==sy(1) || length(x)==sy(2)) &&...
                      Waveform.isevenspaced(x) ,...
                    'Waveform:ArgumentError',...
                    'Input %s to Waveform(%s,%s) must be evenly spaced and same length as %s',...
                    inputname(1),inputname(1),inputname(2),inputname(2));
                o.data = y;
                o.x0 = x(1);       o.dx = mean(diff(x));
            else
                o.x0 = varargin{1}; o.dx = varargin{2};
                assert(...
                    isscalar(o.x0) && isnumeric(o.x0) &&...
                    isscalar(o.dx) && isnumeric(o.dx),...
                    'Waveform:ArgumentError',...
                    'First two input arguments to Waveform(x0,dx,...) must be scalar numbers.');
                if nargin==3
                    assert(isnumeric(varargin{3}),'Waveform:TypeError',...
                      'Third input to Waveform(x0,dx,y) must be a numeric array.');
                    o.data = varargin{3};
                elseif nargin==4
                    assert(isscalar(varargin{3}) && isa(varargin{4},'function_handle'),...
                        'Waveform:ArgumentError',...
                        'Arguments to Waveform(x0,dx,n,fun) must be scalar, scalar, scalar, function handle.');
                    t = o.x0:o.dx:o.x0+o.dx*(varargin{3}-1);
                    f = varargin{4};
                    o.data = f(t);
                else
                    error('Waveform:ArgumentError','Too many arguments to Waveform.');
                end
            end
        end
        
        %% Methods: Set / get
        
        function x = get_x(o)
            % Returns an array of abcissa data corresponding to ordinate data
            assert(numel(o) == 1, 'Waveform:IllegalArrayOperation', ...
                '''get_x'' cannot operate on a Waveform array');

            x = linspace(o.x0, o.x0+o.dx*(o.length()-1), o.length()); % This produces more evenly spaced steps
        end
        
        function xend = get_x_end(o)
            assert(numel(o) == 1, 'Waveform:IllegalArrayOperation', ...
                '''get_x_end'' cannot operate on a Waveform array');
            
            % Returns the last abcissa datum corresponding to data(end)
            xend = o.x0+o.dx*(o.length()-1);    
        end
        
        function len = length(o)
            assert(numel(o) == 1, 'Waveform:IllegalArrayOperation', ...
                '''length'' cannot operate on a Waveform array');
            
            if isvector(o.data)
                len = length(o.data);
            else
                % Multidimensional arrays always index along first
                % dimension.
                len = size(o.data,1);
            end
        end
        
        function s = dsize(o,d)
            assert(numel(o) == 1, 'Waveform:IllegalArrayOperation', ...
                '''dsize'' cannot operate on a Waveform array');
            
            if nargin > 1
                s = size(o.data,d);
            else
                s = size(o.data);
            end
        end
        
        function o = set_x(o,x)
            % Sets x0 and dx using an equally spaced array
            %
            % x0 is set to min(x), dx to mean(diff(x))
            assert(numel(o) == 1, 'Waveform:IllegalArrayOperation', ...
                '''set_x'' cannot operate on a Waveform array');
            
            o.x0 = min(x);  
            o.dx = mean(diff(x));
        end
        
        function o = set_x_end(o,xe)            
            % Sets Waveform x0 and dx to span specified range.
            %
            % x is scaled such that the x datum corresponding to data(end)
            % is equal to xe.
            %
            % Usage:
            %   w = w.set_x_end(xe)
            %
            % Parameters:
            %   xe - Value of x corresponding to data(end).
            assert(numel(o) == 1, 'Waveform:IllegalArrayOperation', ...
                '''set_x_end'' cannot operate on a Waveform array');
            
            o.dx = (xe-o.x0)/(o.length()+1);
        end
        
        function index = get_index(o, x)
            % Returns index of data in the Waveform's domain with coordinate 
            % closest to the requested value.
            
            assert(numel(o) == 1, 'Waveform:IllegalArrayOperation', ...
                '''get_index'' cannot operate on a Waveform array');
            
            err = 0.01; % estimate round off error
            index = round((x - o.x0)/o.dx + err) + 1;
            
            assert(all(index > 0 & index <= o.length()), 'Waveform:IndexOutOfBounds',...
                'Requested data range is outside of this Waveform''s domain.');    
        end
        
        function [imin, imax] = get_index_bounds(o, xmin, xmax)
            % Returns index bounds for data in the Waveform's domain containing
            % the requested inclusive range
            
            assert(numel(o) == 1, 'Waveform:IllegalArrayOperation', ...
                '''get_index_bounds'' cannot operate on a Waveform array');
            
            if (nargin < 3 && numel(xmin) == 2)
                xmax = max(xmin);
                xmin = min(xmin);
            end
            assert(isscalar(xmax) && isscalar(xmin), 'Waveform:IllegalArguments',...
                'Range bounds must be scalar.');
            assert(xmax >= xmin, 'Waveform:IllegalBounds',...
                'Range maximum must not be less then minimum.');
            
            err = 0.01; % estimate round off error
            imin = floor((xmin-o.x0)/o.dx + err) + 1;
            imax = ceil((xmax-o.x0)/o.dx - err) + 1;

            len = o.length();
            if imax == len+1 % Tolerate final index one larger than length, and coerce down
                imax = len;
            end
            if imin == 0 % Tolerate inital index 0, and coerce to 1
                imin = 1;
            end
            
            assert(imin > 0 && imax <= len, 'Waveform:IndexOutOfBounds',...
                'Requested data range is outside of this Waveform''s domain.');            
        end
        
        %% Methods: Restructuring
        
        function o = select(o,varargin)
            for idx=1:numel(o) % Support operating on an array of Waveforms
                if isvector(o(idx).data)
                    o(idx).data = o(idx).data(:);
                else
                    o(idx).data = o(idx).data(:, varargin{:});
                end
            end            
        end
        
        function r = condense(o)           
            % Condenses a waveform array into a single Waveform object,
            % with data concatenated in additional dimensions of the data
            % array.  Warns if domain and resolution of array elements
            % differ.
            
            if numel(o) == 1
                r = o;
            else
                if ~all(abs([o.x0] - o(1).x0) <= 1e-9) || ~all(abs([o.dx] - o(1).dx) <= 1e-9)
                    warning('Waveform:condense','Waveform array elements do not have identical x0 and dx');
                end            

                catdim = ndims(o(1).data)+1;
                rdata = squeeze(reshape(cat(catdim, o.data), [size(o(1).data), size(o)]));
                r = Waveform(o(1).x0, o(1).dx, rdata); 
            end
        end
        
        function o = squeeze(o)
            % Removes singleton dimesions from extra dimesions in waveform
            % data.  Never modifies first dimension (Waveform domain).
            
            for idx=1:numel(o) % Support operating on an array of Waveforms                
                if ~ismatrix(o(idx).data)
                    s = size(o(idx).data);
                    singletonDims = (s==1);
                    singletonDims(1) = false; % Always leave first dimension untouched
                    s(singletonDims) = []; % Remove singleton dimensions.
                    s = [s ones(1,2-length(s))]; % Make sure siz is at least 2-D
                    o(idx).data = reshape(o(idx).data, s);
                end
            end
        end
        
        function o = slice(o,imin,imax)
            %Returns a duplicate waveform with subset of data
            %
            %Usage:
            %    subset = obj.slice(imin,imax)
            %
            %Parameters:
            %  imin - Minimum data index
            %  imax - Maximum data index
                        
            if (nargin < 3 && numel(imin) == 2)
                imax = max(imin);
                imin = min(imin);
            end
            
            assert(imax >= imin, 'Waveform:IllegalBounds',...
                'Range maximum must not be less then minimum.');
            
            for idx=1:numel(o) % Support operating on an array of Waveforms
                assert(imin > 0 && imax <= o(idx).length(), 'Waveform:IndexOutOfBounds',...
                    'Requested data range is outside of this Waveform''s domain.');                    
                
                if isvector(o(idx).data)
                    o(idx).data = o(idx).data(imin:imax);
                else
                    % Preserve other dimensions
                    dims = repmat({':'}, 1, ndims(o(idx).data)-1);
                    o(idx).data = o(idx).data(imin:imax,dims{:});
                end
                o(idx).x0 = o(idx).x0 + o(idx).dx*(imin-1);
            end                   
        end
        
        function o = slice_x(o,xmin,xmax)
            %Returns a duplicate waveform with data in specified domain.
            %
            %Usage:
            %    subset = obj.slice(xmin,xmax)
            %
            %  Returns a waveform object with data whose indices
            %  correspond to all indices in the inclusive range
            %  [xmin,xmax].
            %
            %Parameters:
            %  xmin - Minimum x
            %  xmax - Maximum x          

            if (nargin < 3 && numel(xmin) == 2)
                xmax = max(xmin);
                xmin = min(xmin);
            end
            
            % Returns data spanning the inclusive range [xmin,xmax]
            for idx=1:numel(o) % Support operating on an array of Waveforms
                [imin, imax] = o(idx).get_index_bounds(xmin, xmax);
                o(idx) = o(idx).slice(imin, imax);
            end            
        end

        function [o, xStart] = stack_slices(o, iMin, iLength, iRep, nRep)
            assert(numel(o) == 1, 'Waveform:IllegalArrayOperation', ...
                '''stack_slices'' cannot operate on a Waveform array');
            
            if nargin < 4
                iRep = iLength;
            end
            nRepMax = floor((o.length()-iMin-iLength)/iRep+1.001);
            if nargin < 5
                nRep = nRepMax;
            else
                assert(nRep <= nRepMax, 'Waveform:IndexOutOfBounds',...
                    'Waveform length is insufficient')
            end            

            iStart = iMin + (0:nRep-1) * iRep;
            iStop = iStart + iLength - 1;
            if isvector(o.data)
                stacked_data = zeros(iLength, nRep);
                for i=1:nRep
                    stacked_data(:,i) = o.data(iStart(i):iStop(i));
                end
            else
                % Preserve other dimensions
                dims = size(o.data);               
                stacked_data = zeros([iLength, dims(2:end), nRep]);                
                idx = repmat({':'}, 1, ndims(o.data)-1);
                for i=1:nRep
                    stacked_data(:,idx{:},i) = o.data(iStart(i):iStop(i),idx{:});
                end
            end
            
            o.data = stacked_data;
            o.x0 = 0;
            xStart = o.x0 + (iStart-1) * o.dx;    
        end        
        
        function [o, xStart] = stack_slices_x(o, x0, xLen, xRep, nRep)
            assert(numel(o) == 1, 'Waveform:IllegalArrayOperation', ...
                '''stack_slices_x'' cannot operate on a Waveform array');
            
            if nargin < 4
                xRep = xLen;
            end 
            iStart = o.get_index(x0);
            nRepMax = floor(((o.length()-iStart)*o.dx-xLen)/xRep+1.001);
            if nargin < 5
                nRep = nRepMax;
            else
                assert(nRep <= nRepMax, 'Waveform:IndexOutOfBounds',...
                    'Waveform length is insufficient')                
            end
            
            iLength = ceil(xLen/o.dx);
            xStart = linspace(x0, x0 + (nRep - 1) * xRep, nRep);
            iStart = o.get_index(xStart);
            iStop = iStart + iLength - 1;
            
            if isvector(o.data)
                stacked_data = zeros(iLength, nRep);
                for i=1:nRep
                    stacked_data(:,i) = o.data(iStart(i):iStop(i));
                end
            else
                % Preserve other dimensions
                dims = size(o.data);               
                stacked_data = zeros([iLength, dims(2:end), nRep]);                
                idx = repmat({':'}, 1, ndims(o.data)-1);
                for i=1:nRep                   
                    stacked_data(:,idx{:},i) = o.data(iStart(i):iStop(i),idx{:});
                end
            end
            
            o.data = stacked_data;
            o.x0 = 0;      
        end
        
        function y = interp(o,xi,method)
            %Interpolate data from this Waveform.
            %
            %Data are returned as a simple array corresponding to the
            %elements of xi.
            %To create a new Waveform by interpolation, use resample().
            %
            %Usage:
            %  y = o.interp(xi)
            %  y = o.interp(xi,method)
            %
            %Parameters:
            %  xi - Interpolating array
            %  method - 'nearest' | {'linear'}
            %      Interpolation method
            %
            %Returns:
            %  y - Interpolated data
            assert(numel(o) == 1, 'Waveform:IllegalArrayOperation', ...
                '''interp'' cannot operate on a Waveform array');
            
            if nargin<3
                method = 'linear';
            end
            
            if isvector(o.data)
                y = winterp1(o.x0,o.dx,o.data,xi,method);
            else
                [~, points] = size(o.data);
                dims = size(o.data);

                dims(1) = length(xi);
                y = nan(dims);
    
                for point=1:points
                    y(:,point) = winterp1(o.x0, o.dx, o.data(:,point), xi, method);
                end
            end                
        end
        
        function o = filter(o,b,a,varargin)
            %Filters a waveform
            %
            %Usage:
            %    filtered = o.filter(b,a,...)
            %
            %Parameters:
            %  b - numerator coefficient vector
            %  a - denominator coefficient vector
            %  ... - Parameters passed to filter command
            %
            %Returns:
            %  filtered - Filtered Waveform
            
            for idx=1:numel(o) % Support operating on an array of Waveforms
                o(idx).data = filter(b,a,o(idx).data,varargin{:});
            end
        end
        
        function o = filtfilt(o,b,a)
            %Zero-phase two-pass filtering of a waveform
            %
            %Usage:
            %    filtered = o.filtfilt(b,a)
            %    filtered = o.filtfilt(SOS,G)
            %
            %Parameters:
            %  b - numerator coefficient vector
            %  a - denominator coefficient vector
            %  SOS - second-order section matrix
            %  G - biquad scale values
            %
            %Returns:
            %  filtered - Filtered Waveform

            for idx=1:numel(o) % Support operating on an array of Waveforms
                o(idx).data = filtfilt(b, a, o(idx).data);
            end
        end

        function o = decimate(o,r,varargin)
            %Decimates (filters and downsamples) a waveform
            %
            %Usage:
            %    od = o.decimate(r)
            %    od = o.decimate(r,...)
            %
            %Parameters:
            %  r - Integer number by which to reduce sampling
            %  ... - Parameters passed to Signal Processing Toolbox's
            %     'decimate' command.
            %
            %Returns:
            %  od - Decimated Waveform            
            
            if r == 1
                return
            end
            
            for idx=1:numel(o) % Support operating on an array of Waveforms                
                if isvector(o(idx).data)
                    [o(idx).data, offset] = e3.decimate(o(idx).data,r,varargin{:});
                else
                    [~, points] = size(o(idx).data);
                    dims = size(o(idx).data);
                
                    dims(1) = ceil(dims(1)/r);
                    newData = zeros(dims);
                
                    for point=1:points
                        [newData(:,point), offset] = e3.decimate(o(idx).data(:,point),r,varargin{:});
                    end
                    o(idx).data = newData;
                end
                                
                % Fix initial time to account for indexing offset in
                % decimation.  offset = 1 for all FIR filters, however
                % offset is in range [1 r] for IIR filters, because the
                % decimate algorithm matches the original and decimated
                % data at the end of the vector.
                o(idx).x0 = o(idx).x0 + (offset-1)*o(idx).dx;           
                o(idx).dx = r*o(idx).dx;            
            end
        end
        
        function o = downsample(o,r,varargin)
            %Downsamples a waveform
            %
            %Usage:
            %    od = o.downsample(r)
            %    od = o.downsample(r,...)
            %
            %Parameters:
            %  r - Integer number by which to reduce sampling
            %  ... - Parameters passed to Signal Processing Toolbox's
            %     'downsample' command.
            %
            %Returns:
            %  od - Downsampled Waveform
            
            for idx=1:numel(o) % Support operating on an array of Waveforms
                o(idx).data = downsample(o(idx).data,r,varargin{:});
                o(idx).dx = r*o(idx).dx;
            end     
        end
        
        function o = resample(o,p,q,varargin)
            %Rational resampling of a waveform
            %
            %Usage:
            %    od = o.downsample(p,q)
            %    od = o.downsample(p,q,...)
            %
            %Parameters:
            %  p - Integer by which to increase sampling
            %  q - Integer by which to decrease sampling
            %  ... - Parameters passed to Signal Processing Toolbox's
            %     'resample' command.
            %
            %Returns:
            %  od - Resampled Waveform
            
            for idx=1:numel(o) % Support operating on an array of Waveforms
                o(idx).data = resample(o(idx).data,p,q,varargin{:});
                o(idx).dx = q/p*o(idx).dx;
            end            
        end
        
        function varargout = fft(o, varargin)
            %Take a Fourier transform of this Waveform
            %
            %Output is a Waveform containing the transformed data.  For
            %complex output types, output is two-sided (negative and
            %positive frequencies).  For power output types, output is
            %single-sided (zero and positive frequencies only).
            %
            %Usage:
            %  transformed = obj.fft()
            %  transformed = obj.fft(type)
            %
            %Parameters:
            %  type - Output format, one of:
            %    'complex' - Returns the complex scientific transform,
            %              definined by
            %
            %                  yf = 1/N*fftshift(fft(y))
            %
            %              this transform is double-sided
            %    'complexDensity' - Same as above, except normalized by the
            %              square root of the Fourier bandwidth;
            %              double-sided
            %    'power' - Returns the square magnitude of the complex
            %              transform; single-sided
            %    'powerDensity' - As above, but normalized by the Fourier
            %              bandwidth; single-sided
            
            assert(numel(o) == 1, 'Waveform:IllegalArrayOperation', ...
                '''fft'' cannot operate on a Waveform array');
            
            if nargin<2
                types = {'complex'};
            else
                types = varargin(1:nargin-1);
            end
                        
            assert(all(ismember(types,{'complex' 'complexDensity' 'power' 'powerDensity'})),...
                'Waveform:FftType',...
                'Waveform FFT type must be one of ''complex'' ''complexDensity'' ''power'' or ''powerDensity''.')
            
            len = o.length();
            yf = fft(o.data)/len;
            if isvector(o.data)
                yf = fftshift(yf);
            else
                yf = fftshift(yf,1);
            end

            df = 1/len/o.dx;      %This is Fourier BW
            
            results(size(types)) = Waveform();
            for i=1:length(types)
                switch types{i}
                    case 'complexDensity'
                        yfs = yf/sqrt(df);
                    case 'complex'
                        yfs = yf;% Already done
                    case 'power'
                        yfs = yf.*conj(yf);
                    case 'powerDensity'
                        yfs = yf.*conj(yf)/df;
                end

                if strcmp(types{i},'power') || strcmp(types{i},'powerDensity')
                    % Take powers from negative frequencies, then invert wave
                    % index order.  This works for both even and odd length
                    % data.
                    if isvector(yfs)
                        yfs = yfs(floor(len/2)+1:-1:1);
                    else
                        idx = repmat({':'}, 1, ndims(yfs)-1);                        
                        yfs = yfs(floor(len/2)+1:-1:1,idx{:});
                    end
                    results(i) = Waveform(0,df,yfs);
                else
                    f0 = -floor(len/2)/len/o.dx;
                    results(i) = Waveform(f0,df,yfs);
                end

            end
            
            if nargout <= 1
                varargout{1} = results;
            else
                varargout{nargout} = results(nargout:end);
                for i=1:nargout-1
                    varargout{i} = results(i);
                end
            end
        end
        
        function result = ifft(o, varargin)

            assert(numel(o) == 1, 'Waveform:IllegalArrayOperation', ...
                '''ifft'' cannot operate on a Waveform array');
            
            len = o.length();
            
            if isvector(o.data)
                dShift = ifftshift(o.data);
            else
                dShift = ifftshift(o.data,1);
            end            
            yf = ifft(dShift)*len;
            dt = 1/o.dx;      %This is Fourier BW
            
            result = Waveform(0,dt,yf);
        end                
        
        function varargout = spectrogram(o,tStep,types,tWindow,windowFunction,Nfft)
            
            assert(numel(o) == 1, 'Waveform:IllegalArrayOperation', ...
                '''spectrogram'' cannot operate on a Waveform array');
            
            if nargin<3
                types = {'complex'};
            elseif ischar(types)
                types = {types};
            end
            assert(all(ismember(types,{'complex' 'complexDensity' 'power' 'powerDensity'})),...
                'Waveform:SpectrogramType',...
                'Waveform FFT type must be one of ''complex'' ''complexDensity'' ''power'' or ''powerDensity''.')

            if nargin<4
                tWindow = tStep;
            end
            assert(tWindow >= tStep, 'Waveform:SpectrogramWindow',...
                'Spectrogram window must not be less then time step,');
            
            if nargin<5
                windowFunction = @hamming;
            end
            
            windowLen = round(tWindow / o.dx);
            stepLen = round(tStep / o.dx);
            
            if nargin<6
                Nfft = windowLen;
            end
            
            % Frequency resolution of windowed transform
            df = 1 / o.dx / Nfft;  

            nSteps = floor((o.length() - windowLen)/stepLen)+1;
            
            windowVector = windowFunction(windowLen);
            
            % Ask for the 'twosided' spectrogram, because it returns
            % data similar to the built-in 'fft' function, with frequency
            % bins labeled by 0:Fs*(Nfft - 1)/2, which I can then center
            % using fftshift.  I do this to match the Waveform.fft
            % method, since the 'centered' output from the spectrogram
            % method differs from fftshift(fft(x)) in whether the Nyquist
            % point is mapped to -Fs/2 (at the first index) or Fs/2 (at
            % the final index)
            if isvector(o.data)
                sdata = spectrogram(o.data, windowVector, windowLen - stepLen, Nfft, 'twosided');
            else
                [~, points] = size(o.data);
                dims = size(o.data);
                sdata = zeros([Nfft, nSteps, dims(2:end)]);
                for point=1:points
                    sdata(:,:,point) = spectrogram(o.data(:,point), windowVector, windowLen - stepLen, Nfft, 'twosided');
                end
            end
            
            %Center the DC component between the positive and negative frequencies
            sdata = fftshift(sdata,1);
            
            %Calculate the normalization, accounting for the windowing.  In
            %the case of a rectangular window, this reduces to the normal
            %FFT normalization
            normalization = sum(windowVector.^2) * Nfft;                

            results(size(types)) = Waveform();
            for i=1:length(types)
                switch types{i}
                    case 'complex'
                        result = sdata/sqrt(normalization);
                    case 'complexDensity'
                        result = sdata/sqrt(normalization)/sqrt(df);
                    case 'power'
                        result = sdata.*conj(sdata)/normalization;
                    case 'powerDensity'
                        result = sdata.*conj(sdata)/normalization/df;                        
                end                

                if strcmp(types{i},'power') || strcmp(types{i},'powerDensity')
                    % Take powers from negative frequencies, then invert wave
                    % index order.  This works for both even and odd length
                    % data, capturing the Nyquist point for even lengths.

                    idx = repmat({':'}, 1, ndims(result)-1);                        
                    results(i) = Waveform(0,df,result(floor(Nfft/2)+1:-1:1,idx{:}));
                else
                    f0 = -df*floor(Nfft/2);
                    results(i) = Waveform(f0,df,result);
                end
            end            
            
            varargout{1} = results;
            if nargout > 1
                % Return time vector, marking center of each time window
                % (assuming a symmetric window function...)
                t0 = o.x0 + (windowLen * o.dx)/2;
                t = t0 + tStep*(0:nSteps - 1);
                varargout{2} = t;
            end
        end
        
        %% Methods: Plot
        
        function varargout = plot(o,varargin)
            %Plots waveform data
            %
            %Usage:
            %
            %    o.plot()
            %  
            %  Plots waveform data.
            %
            %    o.plot(xData)   or    o.plot(xWaveform)
            %
            %  Plots waveform data vs. specified x data.
            %
            %    o.plot(...plot options...)
            %    o.plot(Waveform,...plot options...)
            %
            %  Plots with the specified plot options.
            %
            %    h = o.plot(...)
            %
            %  Returns the plot handle associated with this plot.

            assert(numel(o) == 1, 'Waveform:IllegalArrayOperation', ...
                '''plot'' cannot operate on a Waveform array');            

            [ax,args,nargs] = axescheck(varargin{:});            
            
            if isempty(ax)
                ax = gca();
            end

            if nargs>=2 && isa(args{1},'Waveform')
                w = args{1};    x = w.data;     
                args = args(2:end);
            elseif nargs>=2 && isnumeric(args{1})
                x = args{1};    
                args = args(2:end);
            else
                x = o.get_x();
            end
            
            % Use of varargout suppresses return of h when function
            % called with no assignment.
            [varargout{1:nargout}] = plot(ax,x,o.data,args{:});
        end
        
        function h = pcolor(~)
            h = [];
            error('Waveform.pcolor has been renamed Waveform.colorplot');
        end
            
        function h = colorplot(o,varargin)
            assert(numel(o) == 1, 'Waveform:IllegalArrayOperation', ...
                '''colorplot'' cannot operate on a Waveform array');
            
            [ax,args,nargs] = axescheck(varargin{:});            
            
            if isempty(ax)
                ax = gca();
            end
            
            if nargs>=1 && isa(args{1},'Waveform')
                w = args{1};
                x = w.data;     
                args = args(2:end);
            elseif nargs>=1 && isnumeric(args{1})
                x = args{1};    
                args = args(2:end);
            else
                x = 1:size(o.data,2);
            end
            
            y = o.get_x();
            
            h = e3.colorplot(ax, x, y, o.data, args{:});
        end
        
        function varargout = fplot(o,varargin)
            assert(numel(o) == 1, 'Waveform:IllegalArrayOperation', ...
                '''fplot'' cannot operate on a Waveform array');
            
            figure;
            [varargout{1:nargout}] = o.plot(varargin{:});
        end

        function tileplot(o,varargin)

            if numel(varargin) >= 1 && isa(varargin{1},'matlab.ui.Figure')
                fig = varargin{1};
                if numel(varargin) > 1
                    args = varargin(2:end);
                else
                    args = {};
                end
            else
                fig = figure();
                args = varargin;
            end
            
            if numel(o) > 1
                nPlots = numel(o);
                for idx=1:nPlots
                    e3.tileplot(nPlots,idx,'Parent',fig);
                    o(idx).plot(args{:});
                end
            elseif ndims(o.data) == 2
                [~,nPlots] = size(o.data);
                for idx=1:nPlots
                    e3.tileplot(nPlots,idx,'Parent',fig);
                    o.select(idx).plot(args{:});
                end
            else
                [~,~,nPlots] = size(o.data);
                for idx=1:nPlots
                    e3.tileplot(nPlots,idx,'Parent',fig);
                    o.select(':',idx).plot(args{:});
                end
            end
        end

        
        %% Methods: Operator overrides
        function o = plus(a,b)
            %Implements + operator
            %
            %Usage:
            %    o + b
            %  Here 'o' is a Waveform object, and 'b' is one of: a Waveform
            %  object with identical x0,dx, and data size as o, or is a
            %  numeric array with same size as o.data.
            o = Waveform.operator_helper(a,b,@plus);
        end
        function o = minus(a,b)
            %Implements binary - operator
            %
            %See help for plus
            o = Waveform.operator_helper(a,b,@minus);
        end
        function o = uminus(o)
            %Implements unary - operator
            o.data = -o.data;
        end
        function o = mtimes(a,b)
            %Implements * operator
            %
            %Restrictions are as for plus, except that o.data and b.data
            %need not have identical lengths, but only be valid inputs to
            %matrix multiplication
            o = Waveform.operator_helper(a,b,@mtimes);
        end
        function o = times(a,b)
            %Implements .* operator
            %
            %See help for plus
            o = Waveform.operator_helper(a,b,@times);
        end
        function o = mrdivide(a,b)
            %Implements / operator
            %
            %See help for plus
            o = Waveform.operator_helper(a,b,@mrdivide);
        end
        function o = rdivide(a,b)
            %Implements ./ operator
            %
            %See help for plus
            o = Waveform.operator_helper(a,b,@rdivide);
        end
        function o = mldivide(a,b)
            %Implements \ operator
            %
            %See help for plus
            o = Waveform.operator_helper(a,b,@mldivide);
        end
        function o = ldivide(a,b)
            %Implements .\ operator
            %
            %See help for plus
            o = Waveform.operator_helper(a,b,@ldivide);
        end
        function o = mpower(o,b)
            %Implements ^ operator
            %
            %Usage:
            %  o^b
            %
            %Applies ^b to each element of o.data
            for idx=1:numel(o) % Support operating on an array of Waveforms
                o(idx).data = o(idx).data^b;
            end
        end
        function o = power(o,b)
            %Implements .^ operator
            %
            %See help for mpower
            for idx=1:numel(o) % Support operating on an array of Waveforms
                o(idx).data = o(idx).data.^b;
            end
        end
        function o = lt(a,b)
            %Implements < operator
            %
            %See help for plus
            o = Waveform.operator_helper(a,b,@lt);
        end
        function o = gt(a,b)
            %Implements > operator
            %
            %See help for plus
            o = Waveform.operator_helper(a,b,@gt);
        end
        function o = le(a,b)
            %Implements <= operator
            %
            %See help for plus
            o = Waveform.operator_helper(a,b,@le);
        end
        function o = ge(a,b)
            %Implements >= operator
            %
            %See help for plus
            o = Waveform.operator_helper(a,b,@ge);
        end
        function o = ne(a,b)
            %Implements ~= operator
            %
            %See help for plus
            o = Waveform.operator_helper(a,b,@ne);
        end
        function o = eq(a,b)
            %Implements == operator
            %
            %See help for plus
            o = Waveform.operator_helper(a,b,@eq);
        end
        function o = and(a,b)
            %Implements & operator
            %
            %See help for plus
            o = Waveform.operator_helper(a,b,@and);
        end
        function o = or(a,b)
            %Implements | operator
            %
            %See help for plus
            o = Waveform.operator_helper(a,b,@or);
        end
        function o = not(o)
            %Implements ~ operator
            %
            %Applies ~ to each element of o.data
            o.data = ~o.data;
        end
        function o = cat(a,b,dim,noX0)
            %Concatenates Waveforms
            %
            %Usage:
            %
            %    cat(o,b,dim)
            %
            %  Concatenates Waveform data or array 'b' to o.data along 
            %  dimension 'dim'.  'o' and 'b' (if Waveform) must have
            %  identical x0 and dx.
            %
            %    cat(o,b,dim,true)
            %
            %  If true, does not require 'o' and 'b' to have identical x0.
            if nargin<=2
                dim = 1;
            end
            if nargin<=3
                noX0 = false;
            end
            o = Waveform.operator_helper(a,b,@(a,b) cat(dim,a,b),noX0);
            
        end
        function o = horzcat(a,b)
            %Implements horizontal concatenation
            %
            %See help for plus
            o = Waveform.operator_helper(a,b,@horzcat);
        end
        function o = vertcat(a,b)
            %Implements vertical concatenation
            %
            %See help for plus
            o = Waveform.operator_helper(a,b,@vertcat);
        end
        function o = conj(o)
            %Implements conjugation
            for idx=1:numel(o) % Support operating on an array of Waveforms
                o(idx).data = conj(o(idx).data);
            end            
        end
        
        function o = ctranspose(o)
            % Conjugate transpose the higher dimensions (only applicable for a 3 data array with 3
            % dimensions
            for idx=1:numel(o) % Support operating on an array of Waveforms
                if ndims(o(idx).data) <= 3
                    o(idx).data = conj(permute(o(idx).data, [1,3,2]));
                else
                    error('Transpose on 4+D Waveform is not defined. Use PERMUTE instead.');
                end
            end            
        end        
        
        
        function o = transpose(o)
            % Transpose the higher dimensions (only applicable for a 3 data array with 3
            % dimensions
            for idx=1:numel(o) % Support operating on an array of Waveforms
                if ndims(o(idx).data) <= 3
                    o(idx).data = permute(o(idx).data, [1,3,2]);
                else
                    error('Transpose on 4+D Waveform is not defined. Use PERMUTE instead.');
                end
            end            
        end        
        
        function o = permute(o,order)
            % Permutes the higher dimensions
            for idx=1:numel(o) % Support operating on an array of Waveforms
                data_order = [1, order+1];
                o(idx).data = permute(o(idx).data, data_order);
            end
        end
        %% Methods: Statistical
        
        function o = sum(o,varargin)
            %Sums Waveform data, returning new Waveform
            for idx=1:numel(o) % Support operating on an array of Waveforms
                o(idx).data = sum(o(idx).data,varargin{:});
            end
        end
        function o = cumsum(o,varargin)
            %Takes cumsum of Waveform data, returning new Waveform
            for idx=1:numel(o) % Support operating on an array of Waveforms
                o(idx).data = cumsum(o(idx).data,varargin{:});
            end            
        end
        function o = mean(o,varargin)
            %Takes mean of Waveform data, returning new Waveform
            for idx=1:numel(o) % Support operating on an array of Waveforms
                o(idx).data = mean(o(idx).data,varargin{:});
            end
        end
        function o = nanmean(o,varargin)
            %Takes mean of Waveform data, returning new Waveform
            for idx=1:numel(o) % Support operating on an array of Waveforms
                o(idx).data = nanmean(o(idx).data,varargin{:});
            end
        end        
        function o = std(o,varargin)
            %Takes std of Waveform data, returning new Waveform
            for idx=1:numel(o) % Support operating on an array of Waveforms
                o(idx).data = std(o(idx).data,varargin{:});
            end               
        end
        function o = diff(o,varargin)
            %Takes diff of Waveform data, returning new Waveform
            for idx=1:numel(o) % Support operating on an array of Waveforms
                o(idx).data = diff(o(idx).data,varargin{:});
            end               
        end
        function o = prod(o,varargin)
            %Takes product of Waveform data, returning new Waveform
            for idx=1:numel(o) % Support operating on an array of Waveforms
                o(idx).data = prod(o(idx).data,varargin{:});
            end                 
        end
        function o = abs(o)
            %Takes absolute value of Waveform data, returning new Waveform
            for idx=1:numel(o) % Support operating on an array of Waveforms
                o(idx).data = abs(o(idx).data);
            end            
        end        
        function o = mag(o)
            %Takes square magnitude of Waveform data, returning new Waveform
            for idx=1:numel(o) % Support operating on an array of Waveforms
                o(idx).data = o(idx).data .* conj(o(idx).data);
            end
        end       
        function o = arg(o)
            %Takes argument of Waveform data, returning new Waveform
            for idx=1:numel(o) % Support operating on an array of Waveforms
                o(idx).data = atan2(imag(o(idx).data),real(o(idx).data));
            end
        end        
        function o = phase(o)
            %Takes argument of Waveform data, returning new Waveform
            for idx=1:numel(o) % Support operating on an array of Waveforms
                o(idx).data = e3.phaseN(o(idx).data,1,true);
            end
        end                
        function o = real(o)
            %Takes real part of Waveform data, returning new Waveform
            for idx=1:numel(o) % Support operating on an array of Waveforms
                o(idx).data = real(o(idx).data);
            end
        end 
        function o = imag(o)
            % Takes imaginary part of Waveform data, returning new Waveform
            for idx=1:numel(o) % Support operating on an array of Waveforms
                o(idx).data = imag(o(idx).data);
            end
        end            
        function o = log(o)
            %Takes natural log of Waveform data, returning new Waveform
            for idx=1:numel(o) % Support operating on an array of Waveforms
                o(idx).data = log(o(idx).data);
            end            
        end
        function o = log10(o)
            %Takes log base 10 of Waveform data, returning new Waveform
            for idx=1:numel(o) % Support operating on an array of Waveforms
                o(idx).data = log10(o(idx).data);
            end            
        end        
        function o = sqrt(o)
            %Takes the square root of the Waveform data, returning new
            %Waveform
            for idx=1:numel(o) % Support operating on an array of Waveforms
                o(idx).data = sqrt(o(idx).data);
            end            
        end
        
        %% Methods: Casts
        function s = struct(o)
            %Convert waveform object to struct array with the same data
            s = struct('x0',{o.x0},'dx',{o.dx},'data',{o.data});
        end
        
    end
    
    methods (Access = protected)
        
        function s = getHeader(o)
            name = matlab.mixin.CustomDisplay.getClassNameForHeader(o);            
            if ~isscalar(o)
                dims = matlab.mixin.CustomDisplay.convertDimensionsToString(o);
                s = sprintf('%s %s array:\n', dims, name);
            else
                s = sprintf('%s:\n', name);            
            end
        end
        
        function propgrp = getPropertyGroups(o)
            summary = struct();
            if ~isscalar(o)
                desc = struct();
                if ~all(abs([o.x0] - o(1).x0) <= 1e-9) || ~all(abs([o.dx] - o(1).dx) <= 1e-9)
                    summary.x = 'mixed';
                    desc.dx = 'mixed';
                    desc.x0 = 'mixed';
                else
                    summary.x = [o(1).x0, o(1).get_x_end()];
                    desc.dx = o(1).dx;
                    desc.x0 = o(1).x0;
                end

                sizes = arrayfun(@(x)size(x.data),o,'UniformOutput',false);
                if numel(sizes)<=1 || isequal(sizes{:})
                    desc.data = o(1).data;
                else
                    desc.data = 'mixed';
                end
                propgrp(2) = matlab.mixin.util.PropertyGroup(desc,'Properties');
            else
                summary.x = [o.x0, o.get_x_end()];
                propgrp(2) = matlab.mixin.util.PropertyGroup({'x0','dx','data'},'Properties');
            end
            propgrp(1) = matlab.mixin.util.PropertyGroup(summary,'Summary');
        end
                
    end
    
    %% Protected / helper methods
    methods (Static, Access = protected)
        function o=operator_helper(o,b,f,noX0)
            %Automatically perform an operation on a Waveform and one of
            %either: 1) another Waveform, or 2) a numerical array.
            %If both are Waveforms, requires Waveforms to share identical
            %x0 and dx.
            %
            %Usage:
            %  w = operator_helper(a,b,f,noX0)
            %
            %Parameters:
            %  a - Either a Waveform or numeric array.
            %  b - Either a Waveform or numeric array.  One of a or b must
            %      be a Waveform.
            %  f - Binary operation to perform, typically:
            %        f = @(a,b) operation(a,b)
            %  noX0 - If true, does not require a and b waveforms to have
            %      identical x0.
            
            if nargin<4, noX0 = false; end
            
            if ~isa(o,'Waveform') % Correct for inverted argument order
                o = Waveform.operator_helper(b,o,@(b,a) f(a,b),noX0);
                return
            end

            for idx=1:numel(o) % Support operating on an array of Waveforms
                if isa(b,'Waveform')
                    if numel(b) == 1
                        bIdx = b;
                    else
                        bIdx = b(idx);
                    end
                    
                    testX = (noX0 || abs(o(idx).x0 - bIdx.x0) <= 1e-9) && abs(o(idx).dx - bIdx.dx) < 1e-9; % Avoid round-off errors
                    if ~testX
                        msg = 'Waveforms must have identical dx';
                        if nargin<4 || ~noX0, msg = [msg,' and x0']; end
                        error('Waveform:OperatorError',msg);
                    end
                    o(idx).data = f(o(idx).data, bIdx.data);
                elseif isnumeric(b)
                    o(idx).data = f(o(idx).data, b);
                else
                    error('Waveform:OperatorError',...
                        'Expected either numeric array or Waveform as argument in overriden operation.');
                end
            end
        end
    end
    
    %% Static methods
    methods (Static)
        % Waveform factory
        function varargout = of(varargin)
            % Makes multiple waveforms with common x0 and dx.
            %
            % Usage:
            %   Waveform.of(x,A,B,...)
            %   Waveform.of(x0,dx,A,B,...)
            %
            % Parameters:
            %   x - X-array.
            %   x0 - Waveform x0.
            %   dx - Waveform dx.
            %   A, B, etc. - Data arrays.
            assert(isscalar(varargin{1}) && isscalar(varargin{2}) ||...
                cellfun(@(x) numel(x)==numel(varargin{1}), varargin),...
                'Waveform:ArgumentError','Illegal arguments passed to Waveform.of()');
            
            if isscalar(varargin{1})
                varargout = varargin(3:end);
                for i=3:length(varargin)
                    varargout{i-2} = Waveform(varargin{1},varargin{2},...
                        varargin{i});
                end
            else
                varargout = varargin(2:end);
                for i=2:length(varargin)
                    varargout{i-1} = Waveform(varargin{1},varargin{i});
                end
            end
        end
        
        % Static plot implementation
        function varargout = plot_from(varargin)
            % Plots one or more waveforms.
            %
            % Always uses direct calls to MATLAB's plot function (does not
            % call Waveform.plot).
            %
            % Usage:
            %     Waveform.plot_from(w)
            %   Plots waveform data.
            %     Waveform.plot_from(w1,w2,...)
            %   Plot multiple waveforms.
            %     Waveform.plot_from(w1,spec1,...)
            %   Plot waveform with line specification.
            %     Waveform.plot_from(...,'Property',value,...)
            %   Applies given line properties.
            %     h = plot_from(...)
            %   Returns line handles.
            
            % Construct plot command - expand every Waveform instance as x
            % and y data.
            c = {};
            for i=1:nargin % Don't need to worry about speed since c is small
                d = varargin{i};
                if isa(d,'Waveform')
                    c = [c {d.get_x() d.data}];
                else
                    c = [c {d}];
                end
            end
            
            h = plot(c{:});
            if nargout>0
                varargout = h;
            end
        end
    end
    
    methods (Static, Access = private)
        function test = isevenspaced(x)
            %Test for even spacing
            %
            %Takes O(n)
            
            %From MATLAB Central forums
            
            x = x(:).';                              % Force row vector
            xe = linspace(x(1),x(end),length(x));    % Known evenly-spaced vector            
            test = all( abs(x-xe) <= abs(eps(xe)) ); % Tolerate rounding errors
            
            %Example of above in action:
            %  x = 0:0.01:1
            %  xe = linspace(0,1,101);
            %  plot(xe,abs(x-xe),xe,abs(eps(xe)))
        end
    end
end

