classdef Reporter < Task
    %Base abstract class for creating reports of DataModels
    
    methods (Abstract)
        result = run(o, model, varargin)
        %Generate a report on a DataModel
    end
    
end

