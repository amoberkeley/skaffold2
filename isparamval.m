function test = isparamval(c)
%Tests if a cell array is a list of parameter-value pairs.
%
%Usage:
%    test = isparamvalue(c)
%
%Params:
%  c - the cell array to be tested.
%
%Returns:
%  test -
%    true iff c is a cell array with an even number of entries AND all odd
%    entries are strings; returns false otherwise.  Note that by this
%    definition, an empty array will return true.  Use 
%       @(c) isparamval(c) && ~isempty(c)
%    (in this order) to test for nonempty parameter-value array.

test = iscell(c) && mod(length(c),2)==0 && iscellstr(c(1:2:end));

end

