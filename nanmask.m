function x = nanmask(x, m)
    x(~m) = nan;
end