classdef Spectrogram < Analyzer
    % Makes Fourier spectrograms.
    %
    % A spectrogram is an array of time-sequential Fourier transforms.
    
    properties (SetAccess = protected)
        source          % DataModel field to transform
        target
        timeTarget
        
        range           % Time range start
        rangeField      % Name of DataModel field containing range to use in fit
        step            % Spectrogram time step
        window          % Window size (defaults to twice step size)
        windowFunction  % Function handle to produce window function, given window length
        oversample
        type            % One of 'complex', 'power', 'complexDensity', or 'powerDensity'
        builtin;
    end

    methods
        function o = Spectrogram(source,target,varargin)
            %Constructs a new Spectrogram analyzer
            %
            %Usage:
            %
            %  o = analyze.Spectrogram(source,'key',value,...)
            %
            %Keys (default first):
            %  source - Name of the DataModel field to transform
            %  target - DataModel field in which to store spectrogram
            %  range - [] | 2-element double array
            %    Start and stop time for creating spectgrogram.  If empty, transforms entire source.
            %  tWin - 1e-3 | scalar
            %    Fourier window time
            %  tStep - NaN | scalar
            %    Chunk step size.  If set to NaN, uses tWin.
            %  resultType - 'complex' | 'power' | 'complexDensity' |
            %               {'powerDensity'}
            %    See help for FourierChunkAnalyzer.transform()
            %  meanOnly - 'off' | 'on'
            %    If 'off', saves both the spectrogram and its time average
            %    to the DataModel.  If 'on', only saves the time average.
            
            p = inputParser();
            p.addRequired('source',@ischar);
            p.addRequired('target', @(x) ischar(x) || iscellstr(x));
            p.addParameter('timeTarget', [], @(x) ischar(x));
            p.addParameter('range',[],@(x) isnumeric(x) && numel(x)==0 || numel(x)==2);
            p.addParameter('rangeField', '', @ischar);
            p.addParameter('step',NaN,@(x) isscalar(x) && x > 0);
            p.addParameter('window',NaN,@isscalar);
            p.addParameter('windowFunction',@hamming,@(x) isa(x, 'function_handle'));
            p.addParameter('oversample',NaN,@isscalar);
            
            p.addParameter('type','powerDensity',...
                @(x) all(ismember(x,{'complex','power','complexDensity','powerDensity'})));
            p.parse(source,target,varargin{:});
            args = p.Results;
            
            o.source = args.source;
            if ischar(args.target)
                o.target = {args.target};
            else
                o.target = args.target;
            end
            o.timeTarget = args.timeTarget;
            
            o.range = args.range;
            o.rangeField = args.rangeField;
            o.step = args.step;
            o.window = args.window;
            if args.window > 0
                o.window = args.window;
            else
                o.window = o.step * 2;
            end
            o.windowFunction = args.windowFunction;
            o.oversample = args.oversample;
            
            if ischar(args.type)
                o.type = {args.type};
            else
                o.type = args.type;
            end
            
            o.set_source_fields({o.source});
        end
        
        function result = analyze(o, model, ~, ~)

            data = model.get(o.source);

            % Slice, if requested
            if ~isempty(o.range)
                data = data.slice_x(o.range); 
            end
            if ~isempty(o.rangeField)
                data = data.slice_x(get(model, o.rangeField));
            end
            
            % Perform the transformation
            if ~isnan(o.oversample)
                NFft = round(o.oversample * o.window / data.dx);
                [results,t] = data.spectrogram(o.step, o.type, o.window, o.windowFunction, NFft);
            else
                [results,t] = data.spectrogram(o.step, o.type, o.window, o.windowFunction);
            end

            % Save output
            nResults = length(results);
            for i=1:min(length(o.target), nResults)
                model.set(o.target{i}, results(i));
            end            
            if ~isempty(o.timeTarget)
                model.set(o.timeTarget, t);
            end

            result = Result.SUCCESS;
        end
    end
end

