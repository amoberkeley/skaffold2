classdef Mean < Analyzer
    %Calculates the mean of a Waveform or vector over time range
    
    properties (SetAccess = protected)
        source  % DataModel field to average
        target  % DataModel field for result
        range
        rangeType
        nan
    end
    
    methods
        function o = Mean(source,target,varargin)
            %Constructs a Mean analyzer
            %
            %Usage:
            %  o = analyze.Mean(source,target,'key',value,...)
            %
            % Parameters:
            %  source - Name of the DataModel field to transform
            %  target - Name of the DataModel field to store transform in
            %
            % Keys (default first):
            %  range - [] | 2-element double array
            %     Range of data over which to compute average
            %  rangeType - 'time' | 'index'
            %     Indicates whether 'range' is given as the dependent
            %     varialbe of a waveform, or by index
            %
            p = inputParser();
            p.addRequired('source',@ischar);
            p.addRequired('target',@ischar);
            p.addParameter('range', [], @(x) isnumeric(x) && numel(x)==0 || numel(x)==2);
            p.addParameter('rangeType', 'time', @(x) ismember(x, {'time','index'}));
            p.addParameter('nan', false, @islogical);
            p.parse(source,target,varargin{:});
            args = p.Results;
            
            o.source = args.source;
            o.target = args.target;
            o.range = args.range;
            o.rangeType = args.rangeType;
            o.nan = args.nan;
            
            o.set_source_fields({o.source});
        end
        
        function result = analyze(o, model, ~, ~)
            data = model.get(o.source);
            
            if isa(data, 'Waveform')
                if ~isempty(o.range)
                    switch o.rangeType
                        case 'time'
                            data = data.slice_x(min(o.range), max(o.range)).data;
                        case 'index'
                            data = data.slice(min(o.range), max(o.range)).data;
                    end
                end
            else
                assert(strcmp(o.rangeType, 'index'), 'skaffold:TypeError',...
                ['DataModel field ''',o.source,''' must be a Waveform for analyze.Mean to slice over time.']);
            
                data = data(min(o.range):max(o.range));
            end
            
            if o.nan
                avg = nanmean(data);
            else
                avg = mean(data);
            end
            model.set(o.target, avg);
            
            result = Result.SUCCESS;
        end
        
    end
end

