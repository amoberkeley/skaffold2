classdef Slice < Analyzer
    %analyze.Slice slices a waveform in a DataModel field
    
    properties (SetAccess = protected)
        source
        target
        sliceType
        range
    end
    
    methods
        function o = Slice(source,target,range,varargin)
            %Construct a Slice analyzer
            %
            %Usage:
            %  obj = analyze.Slice(source,target,range)
            %
            %  source - Source field in DataModel
            %  target - Destination field in DataModel
            %  range - A two-element array of slice boundaries
                        
            p = inputParser;
            p.addRequired('source', @ischar);
            p.addRequired('target', @ischar);
            p.addRequired('range',@(x) isnumeric(x) && numel(x)==2);
            p.parse(source,target,range,varargin{:})
            
            args = p.Results;

            o.source = args.source;
            o.target = args.target;            
            o.range = args.range;
            
            o.set_source_fields({o.source});
        end
        
        function result = analyze(o, model, ~, ~)
            data = model.get(o.source);
            if isa(data, 'Waveform')
                data = data.slice_x(min(o.range),max(o.range));
            end
            model.set(o.target, data);            
            result = Result.SUCCESS;
        end
    end
    
end

