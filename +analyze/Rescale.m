classdef Rescale < Analyzer
    %Rescales waveform x or y data
    %
    %Use this to apply an arbitrary function to waveform data in a data
    %model.
    %
    %How to use:
    %  1. Create a new Rescale analyzer.  Call:
    %
    %       obj = analyze.Rescale(source[,target],function,...)
    %
    %     Here 'source' is the name of the waveform to rescale.  'Function'
    %     is a function handle to apply to the data.
    %
    %  2. "Analyze" the data mode by calling
    %
    %       result = obj.analyze(model)
    
    properties (SetAccess = protected)
        axis    % 'x' or 'y'
        source  % DataModel field to rescale
        scale   % Function handle for rescaling
    end
    
    methods
        function o = Rescale(source,varargin)
            % Builds a new Rescale analyzer
            %
            % Usage:
            %    o = analyze.Rescale(source,[target],'key',value,...)
            %
            % Parameters:
            %  source - Field to rescale
            %  target - Field in which to store results
            %
            % Keys (default first):
            %  range - [] | 2-element double array
            %    Start and stop time for transform  If empty, transforms 
            %               entire source.
            %  type - 'complex' | 'power' | 'complexDensity' | 'powerDensity'
            
            p = inputParser();
            p.addRequired('source',@ischar);
            p.addParameter('target','',@ischar);
            p.addParameter('scale',0,@(x) isa(x,'function_handle'));
            p.addParameter('axis','y',@(x) any(strcmp(x,{'x','y'})));      
            p.parse(source,varargin{:});
            args = p.Results;
            
            o.source = args.source;
            o.scale = args.scale;
            if args.target
                o.target = args.target;
            else
                o.target = o.source;
            end
            
            o.axis = args.axis;
            
            o.set_source_fields({o.source});
        end
        
        function result = analyze(o, model, ~, ~)
            %Applies scale function to model source.
            f = model.get(o.source);
            switch o.axis
                case 'x'
                    f = f.set_x(o.scale(f.get_x()));
                case 'y'
                    f.data = o.scale(f.data);
            end
            model.set(o.target,f);
            result = Result.SUCCESS;
        end
    end
    
end

