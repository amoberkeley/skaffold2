classdef HetModSidelock < Analyzer
    % Calculates the probe lock point.
    
    properties (SetAccess = protected)
        source
        targets
         
        fCar    % Carrier frequency
        fMod    % Modulation frequency
        kappa   % Broadened cavity linewidth (cyclic)
        kappaN  % Natural cavity linewidth (cyclic)        
        rbGain  % Relative detection gain of red vs. blue sideband
        snRange % Range over which to evaluate shot noise
        
        window
        
        edet    % Heterodyne detection efficiency
        close   % true if the lock point is within the sidebands, false otherwise
        
        debug   %If 'all', prints debug messages
    end
    
    methods
        function o = HetModSidelock(source,targets,varargin)
            %Constructs a HetModSidelock analyzer
            %
            %Usage:
            %  o = analyze.HetModSidelock(source,targets,'key',value,...)
            %
            %Parameters:
            %  source - DataModel source field containing raw heterodyne
            %           signal FFT
            %  targets - Destination fields for deltaPc, nBar, and SN,
            %            respectively
            %
            %Keys:
            %  fCar       - Carrier frequency
            %  fMod       - Modulation frequency
            %  kappa      - Broadened cavity linewidth (cyclic)
            %  kappaN     - Natural cavity linewidth (cyclic)
            %  rbGain     - Relative detection gain of red vs. blue sideband
            %  snRange    - 2-element array indicating range of detuning
            %               range over which to evaluate shot noise
            %  eDet       - Heterodyne detection efficiency            
            %  close      - true if the lock point is within the sidebands,
            %               false otherwise
            %  update     - 'none' | 'all'
            %                If 'all', prints update messages to screen

            p = inputParser;
            p.addRequired('source', @ischar);
            p.addRequired('targets', @(x) ~isempty(x) && (ischar(x) || iscellstr(x)));
            
            p.addParameter('fCar', NaN, @(x) (isscalar(x) && x > 0) || (isvector(x) && all(x>0)));
            p.addParameter('fMod', NaN, @(x) x > 0);
            p.addParameter('kappa', NaN, @(x) x > 0);
            p.addParameter('kappaN', NaN, @(x) x > 0);
            p.addParameter('rbGain', NaN, @(x) x > 0);
            p.addParameter('snRange', [], @(x) isnumeric(x) && numel(x)==2);
            p.addParameter('window', 1e3, @isscalar);            
            p.addParameter('edet', NaN, @(x) x > 0);
            p.addParameter('close', true, @islogical);
            p.addParameter('debug', false, @islogical);
            p.parse(source,targets,varargin{:});
            args = p.Results;
            
            o.source = args.source;
            if ~iscellstr(args.targets)
                o.targets = {args.targets};
            else
                o.targets = args.targets;
            end
            
            o.fCar = args.fCar;
            o.fMod = args.fMod;
            o.kappa = args.kappa;
            o.kappaN = args.kappaN;
            o.rbGain = args.rbGain;
            o.snRange = args.snRange;
            o.window = args.window;
            o.edet = args.edet;
            o.close = args.close;
            
            o.debug = args.debug;
            
            o.set_source_fields({o.source});
        end
        
        function result = analyze(o, model, ~, point)
            % Implements lock detection algorithm for heterodyne
            % data with marker sidebands (which are assumed to be
            % equal height before the cavity).
            %
            % Define 'Sr' to be the height of the red sideband, and
            % 'Sb' to be the height of the blue sideband.  We'll
            % correct the relative height for any post-detection
            % gain.  Now define the relative asymmetry of these
            % heights as:
            %   r = (Sb-Sr)/(Sb+Sr)
            %
            % If we are locking closer to resonance than f_mod,
            %
            %   Delta_pc = -f_mod + rt( f_mod� - r� (f_mod� + kappa�) )
            %
            % where kappa is the cavity linewidth.
            %
            % If instead we are locked farther from resonance than
            % f_mod, use
            %
            %   Delta_pc = -f_mod - rt( f_mod� - r� (f_mod� + kappa�) )
            %       

            if length(o.fCar) > 1
                carrier = o.fCar(point);
            else
                carrier = o.fCar(1);
            end
            
            data = model.get(o.source);
            
            if ~isempty(o.targets{1})
                
                powerBlue = o.rbGain * o.get_window_power(data, carrier + o.fMod);
                powerRed = o.get_window_power(data, carrier - o.fMod);
                r = (powerBlue-powerRed)/(powerBlue+powerRed);
                if o.close
                    deltaPc = (-o.fMod + sqrt(o.fMod^2 - r^2*(o.fMod^2 + o.kappa^2)))/r;
                else
                    deltaPc = (-o.fMod - sqrt(o.fMod^2 - r^2*(o.fMod^2 + o.kappa^2)))/r;
                end

                if ~isreal(deltaPc)
                    warning('LockType:ImaginaryDetuning',...
                        'LockType returned an imaginary detuning... setting to NaN.');
                    deltaPc = NaN;
                end

                model.set(o.targets{1}, deltaPc);

                if o.debug
                    fprintf('powerBlue = %0.3g, powerRed = %0.3g, deltaPc = %0.0f\n',...
                        powerBlue,powerRed,deltaPc/1e3);
                end            
            end
            

            if length(o.targets) > 1
                [psdCarrier, nWin] = o.get_window_power(data, carrier);            
                snWf1 = data.slice_x(carrier + min(o.snRange), carrier+max(o.snRange));
                snWf2 = data.slice_x(carrier - max(o.snRange), carrier-min(o.snRange));            
                psdSN = mean(snWf1.data)/2 + mean(snWf2.data)/2;
                nbar = (psdCarrier/psdSN - nWin)*data.dx/8/pi/o.kappaN/o.edet;
                
                model.set(o.targets{2}, nbar);
                if length(o.targets) > 2
                    model.set(o.targets{3}, psdSN);
                end               

                if o.debug
                    fprintf('psdCarrier = %0.3g, psdSN = %0.3g, nbar = %0.2f\n',...
                        psdCarrier,psdSN,nbar);
                end                       
            end   

            result = Result.SUCCESS;
        end
        
        function [power, nWin] = get_window_power(o, wf, freq)
            if o.window > 0
                [iMin, iMax] = wf.get_index_bounds(freq - o.window/2, freq + o.window/2);                
                nWin = iMax - iMin + 1;
                power = sum(wf.data(iMin:iMax));
            else
                nWin = 1;
                power = wf.interp(freq, 'nearest');
            end            
        end
    end
end

