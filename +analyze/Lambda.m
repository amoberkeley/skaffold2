classdef Lambda < Analyzer
    % Applies arbitrary functions to properties of a Data Model
    
    properties (SetAccess = protected)
        sources
        target
        lambda
        parameters
    end
    
    methods
        
        function o = Lambda(sources, target, lambda, varargin)
            % Constructs a Lambda Analyzer
            
            p = inputParser();
            
            p.addRequired('sources', @(x) ischar(x) || iscellstr(x));
            p.addRequired('target', @ischar)
            p.addRequired('lambda', @(x) isa(x, 'function_handle'));
            p.addParameter('parameters', {}, ...
                @(x) isa(x, 'numeric') || ...
                (iscell(x) && all(cellfun(@(y) isa(y, 'numeric'), x))));
            
            p.parse(sources, target, lambda, varargin{:});
            args = p.Results;
            
            if iscell(args.sources)
                o.sources = args.sources;
            else
                o.sources = {args.sources};
            end
            
            if iscell(args.parameters)
                o.parameters = args.parameters;
            else
                o.parameters = {args.parameters};
            end
            
            o.target = args.target;
            o.lambda = args.lambda;
            
            o.set_source_fields(o.sources)
            
        end
        
        function r = analyze(o, model, ~, ~)
            % Applies the lambda function associated with the Lambda
            % Analyzer `o` to the Data Model `model`
            
            argumentss = num2cell(cellfun(@model.get, o.sources, 'UniformOutput', false));
            if ~isempty(argumentss)
                argumentss = [argumentss{:}];
            end
            model.set(o.target, o.lambda(argumentss{:}, o.parameters{:}));
            
            r = Result.SUCCESS;
            
        end
        
    end
    
end
