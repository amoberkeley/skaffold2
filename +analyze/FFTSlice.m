classdef FFTSlice < Analyzer
    %Makes Fourier transform over segmented time slices of a source.
    
    properties (SetAccess = protected)
        source      % DataModel field to transform
        target      % DataModel field for result
        range       % Slice time range
        tRep        % Time between slices
        nRep        % Number of slices to take      
        type        % One of 'complex', 'power', 'complexDensity', or 'powerDensity'
    end
    
    methods
        function o = FFTSlice(source,target,varargin)
            %Constructs a new FFTSlice analyzer
            %
            %Usage:
            %
            %  o = analyze.FFTSlice(source,target,'key',value,...)
            %
            %Keys (default first):
            %  source - Name of the DataModel field to transform
            %  target - Name of the DataModel field to store transform in
            %  range - [] | 2-element double array
            %    Start and stop time for first transform slice. If empty, transforms 
            %               entire source.
            %  tRep - Step time between slices to transform.
            %  nRep - number of slice repetitions
            %  type - 'complex' | 'power' | 'complexDensity' | 'powerDensity'
                        
            p = inputParser();
            p.addRequired('source', @ischar);
            p.addRequired('target', @ischar);
            p.addParameter('range',[], @(x) isnumeric(x) && numel(x)==2);
            p.addParameter('nRep', 1, @(x) isscalar(x) && x >= 1);
            p.addParameter('tRep', 0, @(x) isscalar(x) && x >= 0);
            p.addParameter('type','powerDensity',...
                @(x) ismember(x,{'complex','power','complexDensity','powerDensity'}));
            p.parse(source,target,varargin{:});
            args = p.Results;
            
            o.source = args.source;
            o.target = args.target;
            o.range = args.range;
            o.nRep = args.nRep;
            o.tRep = args.tRep;
            o.type = args.type;
            
            if o.nRep > 1 && o.tRep == 0
                error('skaffold:FFTSlice', 'tRep must be > 0 if nRep > 1.');
            end
            
            o.set_source_fields({o.source});
        end
        
        function result = analyze(o, model, ~, ~)
            wf = model.get(o.source);

            % Slice
            wfSlice = wf.slice_x(o.range(1), o.range(2)).data;
            for iRep=1:o.nRep-1
                wfSlice = [wfSlice; wf.slice_x(o.range(1)+iRep*o.tRep, o.range(2)+iRep*o.tRep).data];
            end
            wfSlice = Waveform(0,wf.dx,wfSlice);
            
            % Perform the transformation
            fft = wfSlice.fft(o.type);
            
            % Save output
            model.set(o.target, fft);
            result = Result.SUCCESS;
        end
    end    
end

