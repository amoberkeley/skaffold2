classdef Demodulate < Analyzer

    properties (SetAccess = protected)
        source      % Name of the field holding the heterodyne signal
        targets     % Names of target fields for demodulated complex amlitude
        power       % Name of target field for demodulated power
        range       % Time range in which to calculate waveforms
        freq        % Demodulation frequencies
        freqPerPoint
        phase       % Initial demodulation phase at t=t0
        t0          % Define time reference for demodulation 
        bw          % Filter bandwidth after demodulation        
    end
    
    methods
        function o = Demodulate(varargin)
            % Creates a new Demodulate analyzer
            %
            % Usage:
            %   obj = analze.Demodulate(source, targets, 'key', value,...)
            %
            % Parameters:
            %   sources - 'raw'
            %       Name of DataModel fields for the raw heterodyne signal 
            %   targets - {'data1', 'data2',...}
            %       Destination fields for demodulated complex waveforms
            %
            % Keys:
            %   freq - double
            %       Vector of demodulation frequencies, corresponding to
            %       list of targets
            %   range - [] | 2-element double array
            %       Start and stop time for analysis.  If empty, analyzes 
            %       entire source.
            %   bw - double
            %       Low-pass bandwidth
            
            p = inputParser();
            p.addRequired('source', @ischar);
            p.addRequired('targets', @(x) ~isempty(x) && (ischar(x) || iscellstr(x)));
            p.addParameter('power', {}, @(x) ~isempty(x) && (ischar(x) || iscellstr(x)));
            p.addParameter('freq', [], @(x) isnumeric(x) && numel(x) > 0);
            p.addParameter('freqPerPoint', [], @(x) isnumeric(x) && numel(x) > 0);
            p.addParameter('phase', 0, @(x) isnumeric(x));
            p.addParameter('t0', 0, @(x) isnumeric(x));            
            p.addParameter('range', [], @(x) isnumeric(x) && numel(x)==0 || numel(x)==2);
            p.addParameter('bw', NaN, @isscalar)            
            p.parse(varargin{:});
            args = p.Results;
            
            o.source = args.source;
            if ~iscellstr(args.targets)
                o.targets = {args.targets};
            else
                o.targets = args.targets;
            end

            if ~iscellstr(args.power)
                o.power = {args.power};
            else
                o.power = args.power;
            end
            
            o.freq = args.freq;
            o.freqPerPoint = args.freqPerPoint;
            o.phase = args.phase;
            o.t0 = args.t0;            
            o.range = args.range;
            o.bw = args.bw;
            
            o.set_source_fields({o.source});
        end
        
        % Implements abstract analyze()
        function result = analyze(o, model, ~, point)
            wf = model.get(o.source);
            if ~isempty(o.range)
                wf = wf.slice_x(o.range);
            end            

            t = wf.get_x()' - o.t0;
            r = ceil(1/wf.dx/o.bw);
            quadDt = r*wf.dx;
            
            demodFreq = o.freq;
            if ~isempty(o.freqPerPoint)
                demodFreq = o.freqPerPoint(:, point);
            end

            for idx=1:numel(demodFreq)
                f = demodFreq(idx);

                % Demodulate heterodyne signal at sideband frequency, then
                % low pass filter and decimate to recover time-varying amplitude of sideband
                sbAmp = e3.decimate(wf.data .* exp(1i * (2*pi*f*t + o.phase)), r);
                model.set(o.targets{idx}, Waveform(wf.x0, quadDt, sbAmp));

                if numel(o.power) >= idx && ~isempty(o.power{idx})
                    sbPower = real(sbAmp).^2 + imag(sbAmp).^2;
                    model.set(o.power{idx}, Waveform(wf.x0, quadDt, sbPower));
                end
            end
            
            result = Result.SUCCESS;
        end
    end
end

