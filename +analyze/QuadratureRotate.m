classdef QuadratureRotate < Analyzer
    % QuadratureRotate analyzer
    %
    % Rotates the Q and I quadratures
    
    properties
        sources
        targets
        phase        
    end
    
    methods
        function o = QuadratureRotate(sources,targets,phase,varargin) 
            %Constructs a new QuadratureRotate Analyzer
            %
            %Usage:
            %  obj = analyze.QuadratureRotate(sources,targets,phase,'key',value,...)
            %
            %Parameters:
            %  sources - {'iQuad', 'qQuad'}
            %            Source of in-phase and quadrature-phase, respectively
            %  targets - {'iQuadRot', 'qQuadRot'}
            %            DataModel fields in which to store rotated quadratures
            %  phase -   real
            %            Quadrature angle to by which to rotate
            %

            p = inputParser;
            p.addRequired('sources',@(x) iscellstr(x) && numel(x)==2);
            p.addRequired('targets',@(x) iscellstr(x) && (numel(x)==1 || numel(x)==2));
            p.addRequired('phase',@(x) isreal(x) && isscalar(x));
            p.parse(sources,targets,phase,varargin{:});   
            r = p.Results;
            
            o.sources = r.sources;
            o.targets = r.targets;
            o.phase = r.phase;
            
            o.set_source_fields(o.sources);            
        end
        
        function result = analyze(o, model, ~, ~)
            iQuad = model.get(o.sources{1});
            qQuad = model.get(o.sources{2});
            
            iRot = iQuad * cos(o.phase) + qQuad * sin(o.phase);
            qRot = - iQuad * sin(o.phase) + qQuad * cos(o.phase);
   
            model.set(o.targets{1}, iRot);
            if length(o.targets) > 1
                model.set(o.targets{2}, qRot);
            end
            
            result = Result.SUCCESS;           
        end
    end
    
end

