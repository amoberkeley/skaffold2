classdef DeltaPCSweeps < Analyzer
    % DeltaPCSweeps calculates the detuning between the resonance condition
    % (found by sweeping across resonance) and the probe frequency during
    % science.
    % 
    
    properties (SetAccess = protected)
        sweepSources
        vcoSource
        target
        errTarget
        range
        rangeType
        shift
        shiftField
        shiftErrField
    end
    
    methods
        function o = DeltaPCSweeps(source,target,varargin)
            %Construct a DeltaPCSweeps analyzer
            %
            %Usage:
            %  obj = analyzer.DeltaPCSweeps(sweepSources, vcoSource, ...
            %      target, 'key', value, ...)
            
            p = inputParser();
            p.addRequired('sweepSources', @(x) ischar(x) || iscellstr(x));
            p.addRequired('vcoSource', @ischar);
            p.addRequired('target', @ischar);
            p.addParameter('errTarget', '', @ischar);
            p.addParameter('range', [], @(x) isempty(x) || (isnumeric(x) && numel(x) == 2));
            p.addParameter('rangeType', 'time', @(x) ismember(x, {'time', 'index'}));
            p.addParameter('shift', 0, @isnumeric);
            p.addParameter('shiftField', '', @ischar);
            p.addParameter('shiftErrField', '', @ischar);
            p.parse(source, target, varargin{:});
            args = p.Results;
            
            o.vcoSource = args.vcoSource;
            o.target = args.target;
            o.errTarget = args.errTarget;
            o.range = args.range;
            o.rangeType = args.rangeType;
            o.shift = args.shift;
            o.shiftField = args.shiftField;
            o.shiftErrField = args.shiftErrField;
            
            if iscell(args.sweepSources)
                o.sweepSources = args.sweepSources;
            else
                o.sweepSources = {args.sweepSources};
            end

            o.set_source_fields({o.sweepSources{:}, o.vcoSource});
        end
        
        function result = analyze(o, model, ~, point)
            x0 = zeros(size(o.sweepSources));
            x0Err = zeros(size(o.sweepSources));
            for index=1:length(o.sweepSources)
                field = o.sweepSources{index};
                x0(index) = model.get(field).x0;
                x0Err(index) = model.get(field).dx0;
            end
            
            weights = 1 ./ x0Err.^2;
            x0Mean = sum(x0 .* weights) ./ sum(weights);
            model.set(o.target, x0Mean);
            
            if ~isempty(o.errTarget)
                n = numel(o.fitFields);
                if n > 1
                    sigmaSquared = sum((x0 - x0Mean).^2) / (n-1);
                    sumWeights = sum(weights) / n;
                    sumSquaredWeights = sum(weights.^2) / n;
                    x0Uncertainty = sqrt(sigmaSquared * sumSquaredWeights / n / sumWeights.^2); % std(x0);
                else
                    x0Uncertainty = x0Err;
                end
            end
            
            vcoData = model.get(o.vcoSource);
            
            if isa(vcoData, 'Waveform')
                if ~isempty(o.range)
                    switch o.rangeType
                        case 'time'
                            vcoData = vcoData.slice_x(min(o.range), max(o.range)).data;
                        case 'index'
                            vcoData = vcoData.slice(min(o.range), max(o.range)).data;
                    end
                end
            else
                assert(strcmp(o.rangeType, 'index'), 'skaffold:TypeError',...
                ['DataModel field ''',o.source,''' must be a Waveform for analyze.Mean to slice over time.']);
            
                vcoData = vcoData(min(o.range):max(o.range));
            end
            
            vcoBar = mean(vcoData);
            
            correction = o.shift;
            if ~isempty(o.shiftField)
                correction = get(model, o.shiftField);
            end
            if iscell(correction)
                correction = correction{point};
            end
            
            deltaPc = vcoBar - x0Mean - correction;

            model.set(o.target, deltaPc);
            
            if ~isempty(o.errTarget)
                if ~isempty(o.shiftErrField)
                    uncertainty = x0Uncertainty;
                else
                    shiftErr = get(model, o.shiftErrField);
                    uncertainty = sqrt(x0Uncertainty.^2 + shiftErr.^2);
                end
                
                model.set(o.errTarget, uncertainty);
            end

            result = Result.SUCCESS;
        end
    end
    
end