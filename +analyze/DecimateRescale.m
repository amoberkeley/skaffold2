classdef DecimateRescale < Analyzer
    % Decimates and rescales a waveform
    
    properties (SetAccess = protected)
        source   % DataModel field with the raw waveform
        target   % DataModel field to store scaled results
        bw       % Low-pass filter BW.
        scaleFun % Scale function to apply
    end
    
    methods
        function o = DecimateRescale(source,varargin)
            %Create a new DecimateRescale analyzer
            %
            %Usage:
            %  o = analyze.DecimateRescale(source,[target],'key',value,...)
            %
            %Parameters:
            %  source - DataModel source field containing signal
            %  target - Destination fields for result (defaults to
            %  'source')
            %
            %Keys (default value first):
            %    bw - 100e3 | number
            %       Low-pass filter BW.
            %    scaleFun - 1 | function
            %       Function to apply to data
            
            %Leverage Analyzer.parse() helper function
            p = inputParser();
            p.addRequired('source',@ischar);
            p.addParameter('target','',@ischar);
            p.addParameter('bw',100e3,@isscalar);
            p.addParameter('scaleFun',@(x) x,@(x) isa(x,'function_handle'));
            p.parse(source, varargin{:});
            args = p.Results;
            
            o.source = args.source;
            if ~isempty(args.target)
                o.target = args.target;
            else
                o.target = args.source;
            end
            
            o.bw = args.bw;
            o.scaleFun = args.scaleFun;
            
            o.set_source_fields({o.source});
        end
        
        function result = analyze(o, model, ~, ~)
            data = model.get(o.source);
            r = round(1/data.dx/o.bw);
            data = data.decimate(r);                %Filter and downsample
            data = o.scaleFun(data);                %Scale data
            model.set(o.target, data);
            
            result = Result.SUCCESS;
        end
        
    end
    
end

