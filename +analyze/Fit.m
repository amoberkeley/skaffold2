classdef Fit < Analyzer
    %Fits data in a DataModel.
    %
    %How to use:
    %
    %  Construct a function handle to use to fit data.  Then construct this
    %  object with the names of the DataModel x- and y-data fields, the
    %  fit model function handle, and a function to create an array of
    %  guess parameters.
    %
    %  When this object's analyze() function is called, the fit will be
    %  applied and stored in a FitDataModel (in a field named
    %  '<yField>Fit').
    %
    %This implementation currently uses nlfit.fit() as the fitting function.  
    %
    %See also nlfit
    
    properties (SetAccess = protected)
        yField        % Name of DataModel field containing y data to fit
        xField        % Name of DataModel field containing x data to fit
        target        % Target field for fit results
        fitModel      % Fitting function or fitModel object
        fitOptions    % Options for fitting
        paramTargets
        startFunction % Function to apply to generate guess parameters
        range         % Data range to use in fit 
        rangeField    % Name of DataModel field containing range to use in fit
%         warn        % If 'off', suppresses warnings.  'on' otherwise
        targetWf      % Target field for waveform containing the fit results
    end
    
    methods
        function o = Fit(varargin)
            %Builds a new Fit analyzer
            %
            %Usage:
            %    o = analyze.Fit(xField,yField,target,fitModel)
            %    o = analyze.Fit(...,fitOptions)
            %Parameters:
            %   xField - Name of DataModel field holding x data; or, set to
            %     '', in which case x data will be extracted from y
            %     Waveform.
            %   yField - Name of DataModel field holding y data
            %   fitModel - CurveFittingToolbox fitModel function or object
            %   fitOptions - Cell array of options to pass to nlfit
            %   range - Data range to use in fit
            
            p = inputParser();
            p.addRequired('xField',@ischar);
            p.addRequired('yField',@ischar);
            p.addRequired('target',@ischar);
            p.addRequired('fitModel',@(x) ischar(x) || ...
                isa(x,'function_handle') || isa(x,'fitModel') );
            p.addRequired('startFunction',@(x) isa(x,'function_handle') );
            p.addOptional('paramTargets',{}, @iscellstr);
            p.addOptional('fitOptions',{},@(x)  ...
                iscell(x) && mod(numel(x),2)==0 &&...
                all(cellfun(@ischar, x(1:2:end))) );
            p.addParameter('range',[], @(x) (isnumeric(x) && numel(x)==0 || numel(x)==2) ||...
                iscell(x) && all(cellfun(@(y) isnumeric(y) && numel(y)==2,x)));
            p.addParameter('rangeField', '', @ischar);
%             p.addOptional('warn','on',@(x) strcmp(x,'on') || strcmp(x,'off'));
            p.addParameter('targetWf', '', @ischar);
            p.parse(varargin{:});
            args = p.Results;
            
            o.xField = args.xField;
            o.yField = args.yField;
            o.target = args.target;
            o.paramTargets = args.paramTargets;
            
            o.fitModel = args.fitModel;
            o.startFunction = args.startFunction;
            o.fitOptions = args.fitOptions;            
            o.range = args.range;
            o.rangeField = args.rangeField;
            o.targetWf = args.targetWf;
            
            o.set_source_fields({o.xField, o.yField});
        end
        
        function result = analyze(o, model, ~, point)
            %Fits data in a DataModel.
            
            % Construct data arrays
            y = get(model,o.yField);

            rng = o.range;
            if ~isempty(o.rangeField)
                rng = get(model, o.rangeField);
            end
            if iscell(rng)
                rng = rng{point};
            end            
            
            if ~isempty(rng)
                assert(isa(y,'Waveform'),'skaffold:InputError',...
                    'Fit range can only be used in analyze.Fit if yField is a Waveform.');

                y = y.slice_x(min(rng), max(rng));
            end
            
            if ~isempty(o.xField)
                x = get(model,o.xField);
                if ~isempty(rng)
                    assert(isa(x,'Waveform'),'skaffold:InputError',...
                        'Fit range can only be used in analyze.Fit if xField is a Waveform.');

                    x = x.slice_x(min(rng), max(rng));
                end

                if isa(x,'Waveform'), x = x.data; end
            else
                assert(isa(y,'Waveform'),'skaffold:InputError',...
                    'Empty string can only be used for FitAnalyzer.xField if y field is a Waveform.');
                x = y.get_x()';
            end
            if isa(y,'Waveform'), y = y.data; end
            
            guess = o.startFunction(x,y);
            
            tN = min(length(x),length(y));
            
            fobj = nlfit.from(x(1:tN), y(1:tN), o.fitModel, guess, o.fitOptions{:});
            errs = 2*sqrt(diag(fobj.fitinfo.covariance));
            
            results =  struct();
            try
                for index = 1:length(fobj.parameters)
                    param = fobj.parameters{index};

                    results(1).(param) = fobj.(param);
                    results(1).(['d' param]) = errs(index);

                    if length(o.paramTargets) >= index && ~isempty(o.paramTargets{index})
                        model.set(o.paramTargets{index}, fobj.(param));
                        model.set(['d' o.paramTargets{index}], errs(index));
                    end
                end
            catch MExc
                if ~strcmp(MExc.identifier, 'MATLAB:noSuchMethodOrField')
                    rethrow(MExc)
                end
                    
                for index = 1:length(fobj.result)
                    param = sprintf('p%d', index);
                    
                    results(1).(param) = fobj.result(index);
                    results(1).(['d' param]) = errs(index);
                    
                    if length(o.paramTargets) >= index && ~isempty(o.paramTargets{index})
                        model.set(o.paramTargets{index}, fobj.result(index));
                        model.set(['d' o.paramTargets{index}], errs(index));
                    end
                end
            end
            
            if ~isempty(o.targetWf)
                xdataInterp = linspace(x(1), x(tN), tN);
                fr = num2cell(fobj.result);
                try
                    resultsWf = Waveform(xdataInterp', o.fitModel(fr{:}, xdataInterp)');
                catch MExc
                    if ~strcmp(MExc.identifier, 'MATLAB:TooManyInputs')
                        rethrow(MExc)
                    end
                    
                    resultsWf = Waveform(xdataInterp', o.fitModel([fr{:}], xdataInterp)');
                end
                
                model.set(o.targetWf, resultsWf);
            end
            
            model.set(o.target, results);
            result = Result.SUCCESS;
        end
    end
    
end

