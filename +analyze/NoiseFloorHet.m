classdef NoiseFloorHet < Analyzer
    % Applies arbitrary functions to properties of a Data Model
    
    properties (SetAccess = protected)
        sweeps
        het
        target
        
        range
        lp
    end
    
    methods
        
        function o = NoiseFloorHet(sweeps, het, target, varargin)
            % Constructs a NoiseFloorHet Analyzer
            
            p = inputParser();
            
            p.addRequired('sweeps', @(x) ischar(x) || iscellstr(x));
            p.addRequired('het', @ischar);
            p.addRequired('target', @ischar);
            p.addOptional('range', [], @isnumeric);
            p.addOptional('lp', nan, @(x) isscalar(x) && isnumeric(x));
            
            p.parse(sweeps, het, target, varargin{:});
            args = p.Results;
            
            if iscell(args.sweeps)
                o.sweeps = args.sweeps;
            else
                o.sweeps = {args.sweeps};
            end
            
            o.het = args.het;
            o.target = args.target;
            o.range = args.range;
            o.lp = args.lp;
            
            o.set_source_fields({o.sweeps{:}, o.het});
            
        end
        
        function r = analyze(o, model, ~, ~)
            % Calculates the noise floor on the heterodyne signal, given the supplied
            % calibration sweeps
            
            swp = cellfun(@model.get, o.sweeps);
            
            y0Mean = mean([swp.('y0')]);
            aMean = mean([swp.('A')]);
            gMean = mean([swp.('G')]);
            x0Mean = mean([swp.('x0')]);
            
            hm = model.get(o.het);
            if ~isnan(o.lp)
                hm = hm.decimate(floor(1/(o.lp*hm.dx)));
            end
            if ~isempty(o.range)
                hm = hm.slice_x(o.range);
            end
            
            invLor = @(lor) x0Mean + gMean/2 .* sqrt(abs(aMean ./ (lor - y0Mean)));
            
            noiseDist = fitdist(invLor(hm.data), 'Normal');
            noiseFloor = noiseDist.('sigma');
        
            model.set(o.target, noiseFloor);
            
            r = Result.SUCCESS;
            
        end
        
    end
    
end
