classdef DemodSidelock < Analyzer
    % Calculates probe detuning from cavity resonance from the transmission
    % of phase-modulated sidebands, recorded in the heterodyne signal.
    % This method makes use of both the amplitude and phase information
    % contained in the heterodyne signal, which is an improvement over the
    % previous method, which just compared the power in each sideband.
    %
    % The result is something like a digital, heterodyne Pound-Drever-Hall 
    % error signal, represented in the complex plane.  By using information
    % in both quadratures, the ambiguity of which solution branch ('close' 
    % or 'far') to use is avoided, so the detuning can be continuously
    % estimated over any range.  Additionally, the imaginary part of the
    % solution directly gives a measure of the cavity half-linewidth (as 
    % measured in frequency space for the current probe polarization.  This
    % may differ from the cavity decay rate due to linear birefringence of
    % the cavity mirrors.)
    %
    % This calculation is insensitive to drifts in
    % the LO phase below the analysis band-width, because they are common to both
    % sidebands.  The modulation phase is recovered from the heterodyne
    % carrier.  The cavity linewidth is only needed to estimate the
    % cavity phase delay of the modulation signal, giving the correct
    % rotation in the complex plane.
    %
    % This analyzer requires the signal processing toolbox
    
    properties (SetAccess = protected)
        source     % Name of the field holding the heterodyne signal
        targets     % Names of target fields for demodulated data
        range       % Time range in which to calculate waveforms
        steps       % Time steps in which to calculate mean
        bw          % Filter bandwidth after demodulation
        fCar        % Carrier beat frequency
        fMod        % Modulation frequency
        kappa
        rbGain
        
        initPhase
        initTime
    end
    
    methods
        function o = DemodSidelock(varargin)
            % Creates a new DemodSidelock analyzer
            %
            % Usage:
            %   obj = analze.DemodSidelock(source, targets, 'key',value,...)
            %
            % Parameters:
            %   source - 'raw'
            %       DataModel fields for the raw heterodyne signal
            %   targets - {'deltaPcWf', 'kappaWf'}
            %       Destination fields for deltaPc waveform, and optionally 
            %       cavity half-linewidth waveform, respectively
            %
            % Keys:
            %   range - [] | 2-element double array
            %       Start and stop time for analysis.  If empty, analyzes 
            %       entire source.
            %   steps - {'target', [tStart, tStop],...}
            %       Time-steps over which to compute average detuning. 
            %       Specified as cell array of DataModel target fields
            %       followed by corresponding time window.
            %   bw - double
            %       Low-pass bandwidth
            %   fCar - double
            %       Heterodyne carrier frequency
            %   fMod - double
            %       Sideband modulation frequency          
            %   kappa - double
            %       Broadened cavity half-linewidth, accounting for birefringence (cyclic)
            %   rbGain - double
            %       Relative (power) detection efficiency for red vs. blue sideband
            
            p = inputParser();
            p.addRequired('source',@(x) ischar(x))
            p.addRequired('targets', @(x) ~isempty(x) && (ischar(x) || iscellstr(x)));
            p.addParameter('initPhase', 0, @(x) isnumeric(x));
            p.addParameter('initTime', 0, @(x) isnumeric(x));
            p.addParameter('range', [], @(x) isnumeric(x) && numel(x)==0 || numel(x)==2);
            p.addParameter('steps', {}, @(x) isstruct(x) || iscell(x));
            p.addParameter('bw',40e3,@isscalar)
            p.addParameter('fCar',10e6,@isscalar)
            p.addParameter('fMod',2e6,@isscalar)
            p.addParameter('kappa', NaN, @(x) x > 0);
            p.addParameter('rbGain', NaN, @(x) x > 0);
            p.parse(varargin{:});
            args = p.Results;
            
            o.source = args.source;
            if ~iscellstr(args.targets)
                o.targets = {args.targets};
            else
                o.targets = args.targets;
            end
            
            o.range = args.range;
            
            if ~isstruct(args.steps)
                o.steps = struct(args.steps{:});
            end

            % Check that step definitions are valid
            stepTargets = fieldnames(o.steps);
            for i=1:numel(stepTargets)
                stepRange = o.steps.(stepTargets{i});
                assert(isnumeric(stepRange) && numel(stepRange)==2, 'DemodSidelock:InvalidStepRange', 'Invalid step range ''%s''', stepTargets{i});
            end
            
            o.bw = args.bw;
            o.fCar = args.fCar;
            o.fMod = args.fMod;
            o.kappa = args.kappa;
            o.rbGain = args.rbGain;
            
            o.initPhase = args.initPhase;
            o.initTime = args.initTime;
            
            o.set_source_fields({o.source});
        end
        
        % Implements abstract analyze()
        function result = analyze(o, model, ~, ~)
            wf = model.get(o.source);

            if ~isempty(o.range)
                wf = wf.slice_x(o.range);
            end            

%             carrierPhase = 2*pi*o.fMod*qQuad.get_x()';  % Phase evolution of sideband modulation
%             aModAvg = nanmean(qQuad.data .* exp(1i * carrierPhase));  % Demodulate sideband signal in heterodyne phase quadrature
%             modPhase = angle(aModAvg) - atan2(o.fMod,o.kappa);  % Find phase of average sideband state, including cavity delay

            modPhase = o.initPhase;

            t = wf.get_x()' - o.initTime;
            r = ceil(1/wf.dx/o.bw);
            quadDt = r*wf.dx;

            fBlue = o.fCar + o.fMod; % Blue sideband frequency
            carrierPhase = 2*pi*fBlue*t + modPhase;  % Phase evolution, offset by modulation phase
            aBlue = wf.data .* exp(1i * carrierPhase); % Demodulate heterodyne signal at sideband frequency
            aBlue = sqrt(o.rbGain) * e3.decimate(aBlue, r); % Low pass filter and decimate to recover time-varying complex state of sideband

            fRed = o.fCar - o.fMod; % Red sideband frequency
            carrierPhase = 2*pi*fRed*t - modPhase; % Phase evolution, offset by modulation phase
            aRed = wf.data(:) .* exp(1i * carrierPhase); % Demodulate heterodyne signal at sideband frequency
            aRed = e3.decimate(aRed, r); % Low pass filter and decimate to recover time-varying complex state of sideband
           
            deltaPc = o.fMod .* (aRed - aBlue)./(aRed + aBlue); % Calculate complex detuning from normalized difference of sideband states.

            deltaPcWf = Waveform(wf.x0, quadDt, real(deltaPc)); % Real part is detuning
            model.set(o.targets{1}, deltaPcWf);
            if length(o.targets) > 1
                kappaWf = Waveform(wf.x0, quadDt, imag(deltaPc));  % Imaginary part is cavity half-linewidth
                model.set(o.targets{2}, kappaWf);
            end
            if length(o.targets) > 2
                model.set(o.targets{3}, Waveform(wf.x0, quadDt, aRed));
            end
            if length(o.targets) > 3
                model.set(o.targets{4}, Waveform(wf.x0, quadDt, aBlue));
            end

            % Calculate averages within step ranges.  Average after
            % calculating deltaPc above.  Averaging aRed or aBlue
            % independently prevents a time-varying LO-phase from canceling
            % out properly in the ratio.
            stepTargets = fieldnames(o.steps);
            for i=1:numel(stepTargets)
                stepRange = o.steps.(stepTargets{i});
                model.set(stepTargets{i}, deltaPcWf.slice_x(stepRange).mean().data);
            end   
            
            result = Result.SUCCESS;
        end
    end
end

