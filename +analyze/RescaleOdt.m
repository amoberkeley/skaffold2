classdef RescaleOdt < analyze.DecimateRescale
    % Rescales ODT intensity data
    %
    % In order to plot averaged heterodyne data vs. ODT intensity data, you
    % should choose 'bw' to be twice the heterodyne low-pass BW.
  
    methods
        function o = RescaleOdt(source,varargin)
            %Create a new RescaleOdt analyzer
            %
            %Usage:
            %  o = analyze.RescaleOdt(source,'key',value,...)
            %
            %Parameters:
            %  source - DataModel source field containing signal in volts
            %
            %Keys (default value first):
            %  target - Destination fields for result (defaults to
            %    'source')
            %    bw - 100e3 | number
            %       Low-pass filter BW for the ODT intensity.
            %    scaleFun - 1 | function
            %       Function to convert voltage to frequency.
                        
            o = o@analyze.DecimateRescale(source, varargin{:});
        end
    end      
end

