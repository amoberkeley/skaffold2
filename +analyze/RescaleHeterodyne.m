classdef RescaleHeterodyne < Analyzer
    % Converts voltage from heterodyne detector to heterodyne watts
    
    properties (SetAccess = protected)
        sources           % Fields to rescale
        targets           % Fields to store results in, if not sources

        impedance        % Measurement impedance (Ohms, typically 50 or inf)
        transimpedance   % Detector transimpedance (V/W)
        gain             % Classical gain between detector and acquisition (dB)
        label            % New output label
%       lopower          % Power of local oscillator
    end
    
    methods
        function o = RescaleHeterodyne(sources,varargin)
            % Constructs a new RescaleHeterodyne analyzer
            %
            %Usage:
            %  o = analyze.RescaleHeterodyne(sources,[targets],'key',value,...)
            %
            %Parameters:
            %  sources - DataModel source fields containing signals in volts
            %  targets - Destination fields for result (defaults to 'sources')
            %
            %Keys:
            %  transimpedance - Detector transimpedance (V/W)
            %  impedance      - Measurement impedance (Ohms, typically 50 or inf)
            %  gain           - Classical gain between detector and acquisition (dB)

            p = inputParser;
            p.addRequired('sources', @(s) ischar(s) || iscellstr(s));
            p.addParameter('targets', '', @(s) ischar(s) || iscellstr(s));
            p.addParameter('transimpedance', 0, @(x) isscalar(x) && x > 0);
            p.addParameter('impedance', 50, @(x) isscalar(x) && x > 0);
            p.addParameter('gain', 0, @(x) isscalar(x) && isreal(x));
            p.addParameter('label','Heterodyne signal (W)',@ischar);
            p.parse(sources,varargin{:});
            args = p.Results;
            
            if iscell(args.sources)
                o.sources = args.sources;
            else
                o.sources = {args.sources};
            end
           
            if ~isempty(args.targets)
                if iscell(args.targets)
                    o.targets = args.targets;
                else
                    o.targets = {args.targets};
                end
            else
                o.targets = o.sources;
            end
            
            assert(length(o.targets) == length(o.sources), ...
                'Number of targets (if given) must be equal to number of sources.');
            
            o.impedance = args.impedance;
            o.transimpedance = args.transimpedance;
            o.gain = args.gain;

            o.set_source_fields(o.sources);
        end
        
        function result = analyze(o, model, ~, ~)
            %Rescales heterodyne data from V to W
            impAtten = 1-50/(50+o.impedance); % Attenuation due to impedance
            gain = 10^(o.gain/20);
            
            for i = 1:length(o.sources)
                data = model.get(o.sources{i});
                data = data/o.transimpedance/impAtten/gain;
                model.set(o.targets{i}, data);
            end
            
            result = Result.SUCCESS;
        end
    end
    
end

