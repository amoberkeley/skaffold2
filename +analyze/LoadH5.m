classdef LoadH5 < Analyzer
    % Loads data from an HDF5 loader into the DataModel
    
    properties (SetAccess = protected)
        loader
        fields
        targets
        increment
    end
    
    methods
        function o = LoadH5(loader,targets,varargin)
            %Constructs a new LoadH5 analyzer.
            %
            %Usage:
            %    obj = analyze.LoadH5(loader,targets,[fields],'key',value,...)
            %
            %    Creates a LoadH5 analyzer to load data from 'loader' into
            %    'target' field in data model.  
            %
            %Params:
            %  loader - Loader object
            %  targets - Cell array of target fields in data model for source and timestamp
            %  fields - Cell array of key/value pairs to pass to loader.load()
            %
            %Keys
            %  increment - Logical flag indicating whether to increment the loader after this analyzer is run
            
            p = inputParser;
            p.addRequired('loader', @(x) isa(x, 'Loader'));
            p.addRequired('targets',@(x) ischar(x) || (iscellstr(x) && ~isempty(x)));
            p.addOptional('fields',{},@(x) ischar(x) || (iscellstr(x) && ~isempty(x)));            
            p.addParameter('increment',false,@(x) islogical(x) || x>0);
            p.parse(loader,targets,varargin{:});
            args = p.Results;
            
            o.loader = args.loader;
            if ischar(args.targets)
                o.targets = {args.targets};
            else
                o.targets = args.targets;
            end

            if ischar(args.fields)
                o.fields = {args.fields};
            else
                o.fields = args.fields;
            end
            
            o.increment = args.increment;
        end
        
        function test = reanalyze(o,model,signature)
            test = reanalyze@Analyzer(o, model, signature); % Check whether this analyzers signature has changed
            if test; return; end
            
            if ~isempty(o.fields)
                fields = o.fields;
            else
                fields = o.loader.get_fields();
            end

            % Check if Scanner has reset/changed target field to force loading
            for field=fields
                test = test || model.is_changed(field{1});
            end
            
            if ~isempty(o.targets)
                % Check the source field (1st target) against the current loader source
                % filename.
                source = o.loader.get_source();
                test = test || ~strcmp(source, model.get(o.targets{1}));
            end
        end
        
        function result = analyze(o, model, ~, ~)
            [result, data, source, timestamp] = o.loader.load();
            
            switch result
                case Result.SUCCESS
                    if o.increment > 0
                        o.loader.increment(o.increment);
                    end

                    if ~isempty(o.fields)
                        fields = o.fields;
                    else
                        fields = keys(data);
                    end                    
                    
                    for field = fields
                        model.set(field{1}, data(field{1}));
                    end

                    if ~isempty(o.targets)
                        % Set source filename to 1st target, if provided
                        model.set(o.targets{1}, source);
                    end
                    if length(o.targets) > 1
                        % Set source timestamp to 2nd target, if provided
                        model.set(o.targets{2}, timestamp);
                    end                    
                    
                case Result.SKIP
                    if o.increment > 0
                        o.loader.increment(o.increment);
                    end

                    if ~isempty(o.targets)
                        model.set(o.targets{1}, '');
                    end
                    if length(o.targets) > 1
                        model.set(o.targets{2}, '');
                    end                    
        
            end
        end

        function skip(o, ~, ~, ~)
            if o.increment > 0
                % Increment here.  This is reached if reanalyze returns
                % false, and so loading is being skipped.
                
                o.loader.increment(o.increment);
            end
        end
    end
    
end

