classdef Block < Analyzer
    %A Block of multiple analyzer objects.
    %
    %Block analyzer stores a cell array of Analyzers.  To set up a
    %Block analyzer object, either:
    %
    %1. Construct a Block analyzer using the struct array constructor.  Each
    %element in this struct array automatically builds a new Analyzer (this
    %uses MATLAB's eval command).
    %
    %2. Construct an empty Block analyzer (call its constructor with no
    %arguments or its default parameter struct).  Then use add() to attach
    %each Analyzer.
    
    properties (SetAccess = protected)
        analyzers  % Array of Analyzer objects
        namespace
    end
    
    methods
        function o = Block(varargin)
            %Constructs a new Block analyzer.
            %
            %Usage:
            %
            %    obj = analyze.Block()
            %
            %  Creates a Block analyzer with no attached Analyzer objects.
            %
            %    obj = analyze.Block(analyzers)
            %
            %  Creates a Block analyzer with the specified list of
            %  analyzers.
            %
            %Params:
            %  analyzers - Cell array of Analyzer objects.
            
            p = inputParser;
            p.addOptional('analyzers',{},@(x) iscell(x) &&...
                all(cellfun(@(y) isa(y,'Analyzer') || isa(y, 'Reporter'),x)) );
            p.addParameter('namespace','',@ischar);
            p.parse(varargin{:});
            args = p.Results;
            
                        
            if iscell(args.analyzers)
                o.analyzers = args.analyzers;
            else
                o.analyzers = {args.analyzers};
            end
            o.namespace = args.namespace;
            
            sig = class(o);
            if ~isempty(o.namespace)
                sig = [sig,'_',o.namespace];
            end
            o.set_signature(sig);            
        end
        
        function add(o,analyzer)
            %Adds an analysis to the analysis pattern
            if ~isa(analyzer, 'Analyzer') && ~isa(analyzer, 'Reporter')
                error('skaffold:TypeError','Only Analyzers and Reporters can be added to an analysis Block.');
            end
            o.analyzers{length(o.analyzers)+1} = analyzer;
        end
        
        function signatures = collect_signatures(o)
            signatures = {o.signature()};
            for i=1:length(o.analyzers)
                analyzer = o.analyzers{i};
                if isa(analyzer, 'Analyzer')
                    signatures = [signatures, analyzer.collect_signatures()];
                end
            end            
        end

        function result = run(o,model,varargin)
            p = inputParser;
            p.addOptional('loop',0,@(x) isnumeric(x) && isscalar(x));
            p.addOptional('point',0,@(x) isnumeric(x) && isscalar(x));
            p.addOptional('frame',[],@(x) isempty(x) || isa(x, 'DataModel'));
            p.addParameter('force',false,@islogical);
            p.addParameter('skip',false,@islogical);   
            p.parse(varargin{:});
            args = p.Results;            
            
            % If this Block analyzer's signature has changed, force the
            % entire block of analyzers to run, regardless of their
            % signatures.  This keeps things consistent if the Block's
            % namespace is changed.
            sig = model.pop_signature();
            force = args.force || o.reanalyze(model, sig);
            
            if ~isempty(o.namespace)
                subspace = DataNamespace(model, o.namespace);
            else
                subspace = model;
            end

            skip = args.skip;
            for i=1:length(o.analyzers)
                result = o.analyzers{i}.run(subspace, args.loop, args.point, 'force', force, 'skip', skip);

                switch result
                    case Result.SKIP
                        % reset result status (block succeeds, even if a
                        % member skips
                        result = Result.SUCCESS;                         
                    case Result.ABORT
                        % Non-fatal error, abort remaining analyzers on
                        % this iteration
                        return
                    case Result.OUT_OF_DATA
                        % No more data available, abort current analysis
                        % and return to wait for more data, or complete
                        % scan
                        return
                    case Result.FAIL
                        % Fatal error, abort all analysis for entire scan.
                        return
                end
            end
        end        
        
        function result = analyze(~,~,~,~)
            % Not used in Block analyzer
            error('skaffold:UsageError',...
                'analyze.Block does not support analyze()');   
        end
    end
    
end

