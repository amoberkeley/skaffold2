classdef FFTPoint < Analyzer
    % Extracts the value of a point in an FFT, optionally calculating the
    % rectangular or polar components
    
    properties (SetAccess = protected)
        source % DataModel source field
        target % DataModel target field for point value
        rect   % DataModel target fields for rectangular componenets
        polar  % DataModel target fields for magnitude and phase
        
        fPoint  % Point(s) in Fourier space from which to extract mag and phase

        debug
    end
    
    methods
        function o = FFTPoint(source,targets,varargin)
            %Constructs an FFTPoint analyzer
            %
            %Usage:
            %  o = analyze.FFTPoint(source,targets,fPoint,'key',value,...)
            %
            %Parameters:
            %  source - DataModel source field containing (complex or real) FFT
            %  target - Destination field for the real or complex value of the point
            %
            %Keys:
            %  rect   - Destination fields for the real and imaginary components, respectively. 
            %           If source refers to a real FFT, imaginary will always be 0.
            %  polar  - Destination fields for the magnitude and phase, respectively. 
            %           If source refers to a real FFT, phase will always be 0.
            %  fPoint - frequency of point in spectrum to analyze, or list
            %           frequency to analyze for given point in sequence loop
            %  debug  - true|false
            %           If true, prints update messages to screen

            p = inputParser;
            p.addRequired('source', @ischar);
            p.addOptional('target', '', @(x) ischar(x));           
            p.addParameter('rect', {}, @(x) iscellstr(x) && (isempty(x) || numel(x) == 2));
            p.addParameter('polar', {}, @(x) iscellstr(x) && (isempty(x) || numel(x) == 2));
            
            p.addParameter('fPoint', NaN, @(x) (isscalar(x) && x > 0) || (isvector(x) && all(x>0)));
            
            p.addParameter('debug', false, @islogical);
            p.parse(source,targets,varargin{:});
            args = p.Results;
            
            o.source = args.source;
            o.target = args.target;
            o.rect = args.rect;
            o.polar = args.polar;
            
            o.fPoint = args.fPoint;
            
            o.debug = args.debug;
            
            o.set_source_fields({o.source});
        end
        
        function result = analyze(o, model, ~, point)
            data = model.get(o.source);
            
            if length(o.fPoint) >= 1
                freq = o.fPoint(point);
            else
                freq = o.fPoint(1);
            end
            value = data.interp(freq,'nearest');   
            
            if ~isempty(o.target)
                model.set(o.target, value);
            end
            if o.debug
                fprintf('pointValue = %s' , num2str(value));
            end
            
            if ~isempty(o.rect)
                x1 = real(value);
                x2 = imag(value);

                model.set(o.rect{1}, x1);
                model.set(o.rect{2}, x2); 

                if o.debug
                    fprintf(', pointX1 = %0.3g, pointX2 = %0.3g', x1, x2);
                end            
            end

            if ~isempty(o.polar)
                mag = sqrt(real(value)^2+imag(value)^2);
                phase = atan2(imag(value),real(value));

                model.set(o.polar{1}, mag);
                model.set(o.polar{2}, phase);                

                if o.debug
                    fprintf(', pointMag = %0.3g, pointPhase = %0.3g', mag, phase);
                end            
            end     
            
            if o.debug
                fprintf('\n');
            end 
            
            result = Result.SUCCESS;
        end
    end
    
end

