classdef VcoShift < Analyzer
    % VcoShift calculates the VCO shift between w/atoms and empty cavity
    %   
    % In order to use VcoShiftAnalyzer, the data model structure must have
    % the following fields:
    %   'vco' - result from VcoLoader applied to w/atoms shot
    %   'vcoBar' - result from VcoAnalyzer applied to w/atoms shot
    %   emptyBranch - Branch containing empty cavity results, must contain
    %    'vco' and 'vcoBar' - As above
    
    properties (SetAccess = protected)
        source
        target
        range
        emptyRange
        emptyNamespace
    end
    
    methods
        function o = VcoShift(source,target,varargin)
            %Construct a VcoShift analyzer
            %
            %Usage:
            %  obj = analyzer.VcoShift(source,target,'key',value,...)
            
            p = inputParser();
            p.addRequired('source',@ischar);
            p.addRequired('target',@ischar);
            p.addParameter('range', [2.5e-3, 3e-3],@(x) isnumeric(x) && numel(x)==2);
            p.addParameter('emptyRange', [], @(x) isempty(x) || (isnumeric(x) && numel(x)==2));
            p.addParameter('emptyNamespace', 'empty',  @(x) ischar(x))
            p.parse(source,target,varargin{:});
            args = p.Results;
            
            o.source = args.source;
            o.target = args.target;
            o.range = args.range;
            o.emptyNamespace = args.emptyNamespace;
            
            if isempty(args.emptyRange)
                o.emptyRange = args.range;
            else
                o.emptyRange = args.emptyRange;
            end
            
            o.set_source_fields({o.source});
        end
        
        function result = analyze(o, model, ~, ~)
            vcoAtoms = model.get(o.source);
            vcoEmpty = DataNamespace(model, o.emptyNamespace).get(o.source);

            if isa(vcoAtoms, 'Waveform')
                vcoAtomsMean = mean(vcoAtoms.slice_x(min(o.range), max(o.range)).data);
            else
                vcoAtomsMean = mean(vcoAtoms);
            end
            
            if isa(vcoEmpty, 'Waveform')
                vcoEmptyMean = mean(vcoEmpty.slice_x(min(o.emptyRange), max(o.emptyRange)).data);
            else
                vcoEmptyMean = mean(vcoEmpty);
            end                            
            
            % dvAll = vcoAtoms - vcoEmpty;
            % dv = dvAll.slice_x(min(o.range), max(o.range);

            dvBar = vcoAtomsMean - vcoEmptyMean;          

            model.set(o.target, dvBar);

            result = Result.SUCCESS;
        end
    end
    
end