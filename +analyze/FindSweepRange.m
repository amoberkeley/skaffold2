classdef FindSweepRange < analyze.FindRange
    % Finds the time window during which probe VCO sweeps occur.
    
    methods
        function o = FindSweepRange(vcoSource, target, vcoMin, vcoMax, dir, varargin)
            
            switch dir
                case 'up'
                    start = @(x) x > vcoMin;
                    stop  = @(x) x > vcoMax;
                case 'down'
                    start = @(x) x < vcoMax;
                    stop  = @(x) x < vcoMin;
                otherwise
                    error('skaffold:InputError', 'FindSweepRange dir must be one of ''up'' or ''down''.');
            end
            
            o = o@analyze.FindRange({vcoSource}, target, start, stop, varargin{:});
            
        end
        
    end

end
