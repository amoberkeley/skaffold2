classdef SidelockPower < Analyzer

    properties (SetAccess = protected)
        source     % Name of the field holding the heterodyne signal
        targets     % Names of target fields for demodulated data
        range       % Time range in which to calculate waveforms
        steps       % Time steps in which to calculate mean
        bw          % Filter bandwidth after demodulation
        fCar        % Carrier beat frequency
        fMod        % Modulation frequency
        kappa
        rbGain
        close       % true if the lock point is within the sidebands, false otherwise
    end
    
    methods
        function o = SidelockPower(varargin)
            % Creates a new SidelockPower analyzer
            %
            % Usage:
            %   obj = analze.SidelockPower(sources, targets, 'key',value,...)
            %
            % Parameters:
            %   source - 'raw'
            %       Name of DataModel fields for the raw heterodyne signal
            %   targets - {'deltaPcWf', 'kappaWf'}
            %       Destination fields for deltaPc waveform, and optionally 
            %       cavity half-linewidth waveform, respectively
            %
            % Keys:
            %   range - [] | 2-element double array
            %       Start and stop time for analysis.  If empty, analyzes 
            %       entire source.
            %   steps - {'target', [tStart, tStop],...}
            %       Time-steps over which to compute average detuning. 
            %       Specified as cell array of DataModel target fields
            %       followed by corresponding time window.
            %   bw - double
            %       Low-pass bandwidth
            %   fCar - double
            %       Heterodyne carrier frequency
            %   fMod - double
            %       Sideband modulation frequency          
            %   kappa - double
            %       Broadened cavity half-linewidth, accounting for birefringence (cyclic)
            %   rbGain - double
            %       Relative (power) detection efficiency for red vs. blue sideband
            %   close - true if the lock point is within the sidebands,
            %               false otherwise
            
            p = inputParser();
            p.addRequired('source', @ischar);
            p.addRequired('targets', @(x) ~isempty(x) && (ischar(x) || iscellstr(x)));
            p.addParameter('range', [], @(x) isnumeric(x) && numel(x)==0 || numel(x)==2);
            p.addParameter('steps', {}, @(x) isstruct(x) || iscell(x));
            p.addParameter('bw', NaN, @isscalar);
            p.addParameter('fCar', NaN, @isscalar);
            p.addParameter('fMod', NaN, @isscalar);
            p.addParameter('kappa', NaN, @(x) x > 0);
            p.addParameter('rbGain', NaN, @(x) x > 0);
            p.addParameter('close', true, @islogical);
            p.parse(varargin{:});
            args = p.Results;
            
            o.source = args.source;
            if ~iscellstr(args.targets)
                o.targets = {args.targets};
            else
                o.targets = args.targets;
            end
            
            o.range = args.range;
            
            if ~isstruct(args.steps)
                o.steps = struct(args.steps{:});
            end

            % Check that step definitions are valid
            stepTargets = fieldnames(o.steps);
            for i=1:numel(stepTargets)
                stepRange = o.steps.(stepTargets{i});
                assert(isnumeric(stepRange) && numel(stepRange)==2, 'SidelockPower:InvalidStepRange', 'Invalid step range ''%s''', stepTargets{i});
            end            
            
            o.bw = args.bw;
            o.fCar = args.fCar;
            o.fMod = args.fMod;
            o.kappa = args.kappa;
            o.rbGain = args.rbGain;
            
            o.set_source_fields({o.source});
        end
        
        % Implements abstract analyze()
        function result = analyze(o, model, ~, ~)
            wf = model.get(o.source);
            if ~isempty(o.range)
                wf = wf.slice_x(o.range);
            end            

            t = wf.get_x()';
            r = ceil(1/wf.dx/o.bw);
            quadDt = r*wf.dx;

            % Demodulate heterodyne signal at blue sideband frequency, then
            % low pass filter and decimate to recover time-varying amplitude of sideband
            aBlue = e3.decimate(wf.data .* exp(1i * 2*pi*(o.fCar+o.fMod)*t), r);            
            pBlue = o.rbGain*(real(aBlue).^2 + imag(aBlue).^2);

            % Demodulate heterodyne signal at red sideband frequency, then
            % low pass filter and decimate to recover time-varying complex state of sideband
            aRed = e3.decimate(wf.data(:) .* exp(1i * 2*pi*(o.fCar-o.fMod)*t), r); 
            pRed = (real(aRed).^2 + imag(aRed).^2);            
           
            redWf = Waveform(wf.x0, quadDt, pRed);
            blueWf = Waveform(wf.x0, quadDt, pBlue);
            
            model.set(o.targets{1}, redWf);
            model.set(o.targets{2}, blueWf);
            
            if numel(o.targets) >= 3
                % Compute sideband imbalance
                r = (pBlue-pRed)./(pBlue+pRed);
                if o.close
                    deltaPc = (-o.fMod + sqrt(o.fMod^2 - r.^2*(o.fMod^2 + o.kappa^2)))./r;
                else
                    deltaPc = (-o.fMod - sqrt(o.fMod^2 - r.^2*(o.fMod^2 + o.kappa^2)))./r;
                end          
                
                deltaPcWf = Waveform(wf.x0, quadDt, real(deltaPc));
                model.set(o.targets{3}, deltaPcWf);
            end

            % Calculate averages within step ranges. 
            stepTargets = fieldnames(o.steps);
            for i=1:numel(stepTargets)
                stepRange = o.steps.(stepTargets{i});

                powerRed = redWf.slice_x(stepRange).mean().data;
                powerBlue = blueWf.slice_x(stepRange).mean().data;

                % Compute sideband imbalance
                r = (powerBlue-powerRed)./(powerBlue+powerRed);
                if o.close
                    deltaPc = (-o.fMod + sqrt(o.fMod^2 - r.^2*(o.fMod^2 + o.kappa^2)))./r;
                else
                    deltaPc = (-o.fMod - sqrt(o.fMod^2 - r.^2*(o.fMod^2 + o.kappa^2)))./r;
                end          
               
                model.set(stepTargets{i}, real(deltaPc));
            end   
            
            result = Result.SUCCESS;
        end
    end
end

