classdef Load < Analyzer
    % Loads data from a loader into datamodel
    
    properties (SetAccess = protected)
        loader
        targets
        parameters
        increment
    end
    
    methods
        function o = Load(loader,target,varargin)
            %Constructs a new Load analyzer.
            %
            %Usage:
            %    obj = analyze.Load(loader,target,[parameters],'key',value,...)
            %
            %    Creates a Load analyzer to load data from 'loader' into
            %    'target' field in data model.  
            %
            %Params:
            %  loader - Loader object
            %  target - Target field in data model
            %  parameters - Cell array of key/value pairs to pass to loader.load()
            %
            %Keys
            %  increment - Logical flag indicating whether to increment the loader after this analyzer is run
            
            p = inputParser;
            p.addRequired('loader', @(x) isa(x, 'Loader'));
            p.addRequired('targets',@(x) ischar(x) || (iscellstr(x) && ~isempty(x)));
            p.addOptional('parameters',{},@iscell);
            p.addParameter('increment',false,@(x) islogical(x) || x>0);
            p.parse(loader,target,varargin{:});
            args = p.Results;
            
            o.loader = args.loader;
            if ischar(args.targets)
                o.targets = {args.targets};
            else
                o.targets = args.targets;
            end
            
            o.parameters = args.parameters;
            o.increment = args.increment;
        end
        
        function test = reanalyze(o,model,signature)
            test = reanalyze@Analyzer(o, model, signature); % Check whether this analyzers signature has changed
            if ~test 
                % Check if Scanner has reset/changed target field to force loading
                test = model.is_changed(o.targets{1});
            end
            if ~test && length(o.targets) > 1
                % Check the source field (2nd target) against the current loader source
                % filename.
                source = o.loader.get_source(o.parameters{:});
                test = ~strcmp(source, model.get(o.targets{2}));
            end
        end
        
        function result = analyze(o, model, ~, ~)
            [result, data, source, timestamp] = o.loader.load(o.parameters{:});
            
            switch result
                case Result.SUCCESS
                    if o.increment > 0
                        o.loader.increment(o.increment);
                    end
                    model.set(o.targets{1}, data);

                    if length(o.targets) > 1
                        % Set source filename to 2nd target, if provided
                        model.set(o.targets{2}, source);
                    end
                    if length(o.targets) > 2
                        % Set source timestamp to 2nd target, if provided
                        model.set(o.targets{3}, timestamp);
                    end                    
                    
                case Result.SKIP
                    if o.increment > 0
                        o.loader.increment(o.increment);
                    end

                    if length(o.targets) > 1
                        model.set(o.targets{2}, '');
                    end
                    if length(o.targets) > 2
                        model.set(o.targets{3}, '');
                    end                    
        
            end
        end

        function skip(o, ~, ~, ~)
            if o.increment > 0
                % Increment here.  This is reached if reanalyze returns
                % false, and so loading is being skipped.
                
                o.loader.increment(o.increment);
            end
        end
    end
    
end

