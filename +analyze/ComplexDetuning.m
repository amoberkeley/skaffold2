classdef ComplexDetuning < Analyzer
    % Calculates probe detuning from cavity resonance from the transmission
    % of phase-modulated sidebands, recorded in the heterodyne signal.
    % This method makes use of both the amplitude and phase information
    % contained in the heterodyne signal, which is an improvement over the
    % previous method, which just compared the power in each sideband.
    %
    % The result is something like a digital, heterodyne Pound-Drever-Hall 
    % error signal, represented in the complex plane.  By using information
    % in both quadratures, the ambiguity of which solution branch ('close' 
    % or 'far') to use is avoided, so the detuning can be continuously
    % estimated over any range.  Additionally, the imaginary part of the
    % solution directly gives a measure of the cavity half-linewidth (as 
    % measured in frequency space for the current probe polarization.  This
    % may differ from the cavity decay rate due to linear birefringence of
    % the cavity mirrors.)
    %
    % This calculation is insensitive to drifts in
    % the LO phase below the analysis band-width, because they are common to both
    % sidebands.  The modulation phase is recovered from the heterodyne
    % carrier.  The cavity linewidth is only needed to estimate the
    % cavity phase delay of the modulation signal, giving the correct
    % rotation in the complex plane.
    
    properties (SetAccess = protected)
        source      % Name of the field holding the demodulated sideband amplitudes
        targets     % Names of target fields for demodulated data
        steps       % Time steps in which to calculate mean
        fMod        % Modulation frequency
        phase
        rbGain
    end
    
    methods
        function o = ComplexDetuning(varargin)
            % Creates a new ComplexDetuning analyzer
            %
            % Usage:
            %   obj = analze.ComplexDetuning(source, targets, 'key',value,...)
            %
            % Parameters:
            %   source - {'aRed', 'aBlue'}
            %       DataModel fields for the raw heterodyne signal
            %   targets - {'deltaPcWf', 'kappaWf'}
            %       Destination fields for deltaPc waveform, and optionally 
            %       cavity half-linewidth waveform, respectively
            %
            % Keys:
            %   steps - {'target', [tStart, tStop],...}
            %       Time-steps over which to compute average detuning. 
            %       Specified as cell array of DataModel target fields
            %       followed by corresponding time window.
            %   fMod - double
            %       Sideband modulation frequency          
            %   rbGain - double
            %       Relative (power) detection efficiency for red vs. blue sideband
            
            p = inputParser();
            p.addRequired('source',@(x) iscellstr(x))
            p.addRequired('targets', @(x) ~isempty(x) && (ischar(x) || iscellstr(x)));
            p.addParameter('steps', {}, @(x) isstruct(x) || iscell(x));
            p.addParameter('fMod',2e6,@isscalar)
            p.addParameter('phase',0,@isscalar)
            p.addParameter('rbGain', NaN, @(x) x > 0);
            p.parse(varargin{:});
            args = p.Results;
            
            o.source = args.source;
            if ~iscellstr(args.targets)
                o.targets = {args.targets};
            else
                o.targets = args.targets;
            end
            
            if ~isstruct(args.steps)
                o.steps = struct(args.steps{:});
            end

            % Check that step definitions are valid
            stepTargets = fieldnames(o.steps);
            for i=1:numel(stepTargets)
                stepRange = o.steps.(stepTargets{i});
                assert(isnumeric(stepRange) && numel(stepRange)==2, 'DemodSidelock:InvalidStepRange', 'Invalid step range ''%s''', stepTargets{i});
            end
            
            o.fMod = args.fMod;
            o.phase = args.phase;
            o.rbGain = args.rbGain;
                        
            o.set_source_fields(o.source);
        end
        
        % Implements abstract analyze()
        function result = analyze(o, model, ~, ~)
            aRed = model.get(o.source{1});
            aBlue = sqrt(o.rbGain) * model.get(o.source{2});

            % Calculate complex detuning from normalized difference of sideband states.
            detRatio = o.fMod .* (exp(1j*o.phase)*aRed - exp(-1j*o.phase)*aBlue)./(exp(1j*o.phase)*aRed + exp(-1j*o.phase)*aBlue);
            
            deltaPcWf = real(detRatio); % Real part is detuning
            model.set(o.targets{1}, deltaPcWf);
            if length(o.targets) > 1
                model.set(o.targets{2}, imag(detRatio));  % Imaginary part is cavity half-linewidth
            end

            % Calculate averages within step ranges.  Average after
            % calculating deltaPc above.  Averaging aRed or aBlue
            % independently prevents a time-varying LO-phase from canceling
            % out properly in the ratio.
            stepTargets = fieldnames(o.steps);
            for i=1:numel(stepTargets)
                stepRange = o.steps.(stepTargets{i});
                model.set(stepTargets{i}, deltaPcWf.slice_x(stepRange).mean().data);
            end   
            
            result = Result.SUCCESS;
        end
    end
end

