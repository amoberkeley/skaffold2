classdef RescaleVco < analyze.DecimateRescale
    % Rescales VCO tuning frequency data
    %
    % In order to plot averaged heterodyne data vs. VCO frequency data, you
    % should choose 'bw' to be twice the heterodyne low-pass BW.
  
    methods
        function o = RescaleVco(source,varargin)
            %Create a new RescaleVco analyzer
            %
            %Usage:
            %  o = analyze.RescaleVco(source,'key',value,...)
            %
            %Parameters:
            %  source - DataModel source field containing signal in volts
            %
            %Keys (default value first):
            %  target - Destination fields for result (defaults to
            %    'source')
            %    bw - 100e3 | number
            %       Low-pass filter BW for the VCO frequency.
            %    scaleFun - 1 | function
            %       Function to convert voltage to frequency.
                        
            o = o@analyze.DecimateRescale(source, varargin{:});
        end
    end      
end

