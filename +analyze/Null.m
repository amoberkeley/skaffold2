classdef Null < Analyzer
    %analyze.Null performs no analysis.
    %
    %DataModel returned by analyze.Null.analyze() is the identical model
    %that was passed.
    
    properties (SetAccess = protected)
    end
    
    methods
        function result = analyze(~,~,~,~)
            result = Result.SUCCESS;            
        end
    end
end

