classdef Manipulate < Analyzer
    % Use to manipulate analyzers within an analysis pattern
    %
    % Use a Manipulate analyzer if you want to change the behavior of an
    % analyzer within an analysis pattern.
    %
    % Example:
    %
    %   sa = analyze.Spectrogram('raw','rawSpectrogram','tWin',0.1);
    %   % Makes a FourierChunkAnalyzer with 100 ms window time
    %   % analyzes 'raw' field of DataModel
    %
    %   am = analyze.Manipulate(sa, @(x) x.source='other')
    %   % On analyze, changes source field of fa to 'other'
    %
    %   b = analyze.Block()
    %   b.add(sa);
    %   b.add(am);
    %   b.add(sa);
    %   result = b.analyze(model);
    %
    %   % This pattern transforms 'raw', then 'other' field of DataModel
    
    properties (SetAccess = protected)
        object
        command
    end
    
    methods
        function o = Manipulator(object, varargin)
            %Creates a new Manipulator analyzer
            %
            %Usage:
            %
            %    o = AnalyzerManipulator(analyzer)
            %
            %  Make a new AnalyzerManipulator with null command
            %
            %    o = AnalyzerManipulator(analyzer,command)
            %
            %  Specifies command to execute on analyze()
            
            
            p = inputParser;
            p.addRequired('object',@(x) isa(x,'handle'));
            p.addOptional('command',@(x) 0, @(x) isa(x,'function_handle'));
            p.parse(object, varargin{:});
            args = p.Results;
            
            o.object = args.object;
            o.command = args.command;
        end
        
        function test = reanalyze(~,~,~)
            % Force analyzer to run, so manipulation always occurs
            test = true;
        end
        
        function result = analyze(o, ~, ~, ~)
            % Manipulate object.  Does nothing to DataModel.
            o.command(o.object);
            result = Result.SUCCESS;
        end
    end
end

