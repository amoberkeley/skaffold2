classdef DemodulateMean < Analyzer

    properties (SetAccess = protected)
        source      % Name of the field holding the heterodyne signal
        targets     % Names of target fields for demodulated complex amlitude
        range       % Time range in which to calculate waveforms
        freq        % Demodulation frequencies
        freqPerPoint
        phase       % Initial demodulation phase at t=t0
        t0          % Define time reference for demodulation     
    end
    
    methods
        function o = DemodulateMean(varargin)
            % Creates a new DemodulateMean analyzer
            %
            % Usage:
            %   obj = analze.DemodulateMean(source, targets, 'key', value,...)
            %
            % Parameters:
            %   sources - 'raw'
            %       Name of DataModel fields for the raw heterodyne signal 
            %   targets - {'data1', 'data2',...}
            %       Destination fields for demodulated complex waveforms
            %
            % Keys:
            %   freq - double
            %       Vector of demodulation frequencies, corresponding to
            %       list of targets
            %   range - [] | 2-element double array
            %       Start and stop time for analysis.  If empty, analyzes 
            %       entire source.
            
            p = inputParser();
            p.addRequired('source', @ischar);
            p.addRequired('targets', @(x) ~isempty(x) && (ischar(x) || iscellstr(x)));
            p.addParameter('freq', [], @(x) isnumeric(x) && numel(x) > 0);
            p.addParameter('freqPerPoint', [], @(x) isnumeric(x) && numel(x) > 0);
            p.addParameter('phase', 0, @(x) isnumeric(x));
            p.addParameter('t0', 0, @(x) isnumeric(x));            
            p.addParameter('range', [], @(x) isnumeric(x) && numel(x)==0 || numel(x)==2);
            p.parse(varargin{:});
            args = p.Results;
            
            o.source = args.source;
            if ~iscellstr(args.targets)
                o.targets = {args.targets};
            else
                o.targets = args.targets;
            end
            
            o.freq = args.freq;
            o.freqPerPoint = args.freqPerPoint;
            o.phase = args.phase;
            o.t0 = args.t0;            
            o.range = args.range;
            
            o.set_source_fields({o.source});
        end
        
        % Implements abstract analyze()
        function result = analyze(o, model, ~, point)
            wf = model.get(o.source);
            if ~isempty(o.range)
                wf = wf.slice_x(o.range);
            end            
            
            demodFreq = o.freq;
            if ~isempty(o.freqPerPoint)
                demodFreq = o.freqPerPoint(:, point);
            end

            for idx=1:numel(demodFreq)
                f = demodFreq(idx);

                nFullCycles = floor((wf.get_x_end - wf.x0) * f);
                wf.slice_x([wf.x0, wf.x0 + nFullCycles/f]);

                t = wf.get_x()' - o.t0;

                sbAmp = mean(wf.data .* exp(1i * (2*pi*f*t + o.phase)));
                model.set(o.targets{idx}, sbAmp);
            end
            
            result = Result.SUCCESS;
        end
    end
end

