classdef FitSidebands < Aggregator
    
    properties (SetAccess = protected)
        source        % Name of aggregate DataModel field containing Waveform or to fit
        targets       % Name of single DataFrame field in which to store fit results        
        range         % Range of data to include in fit
        guessFm       % Initial guess frequency
        guessGm       % Initial guess linewidth
        boundsF
        boundsG
        method        % Fit method (fit to single Waveform, or adaptively average cell array of waveforms)
        minAvg        % Minimum number of averages for adapative method
        threshold     % Fit threshold (percentage of variance accounted for by fit)
        suppressDrive % n x 2 matrix where each row is a frequency band to linearly interpolate in spectrum
        plot          % Plot fit results or not
        plotName
        plotRange     % Plot axis bounds
        ifField
        
        fitModel = @(params,f) params(4) + params(1)./(1 + ((f - params(2)) ./ (params(3)/2)).^2)
    end
    
    methods
        function o = FitSidebands(varargin)
            %Builds a new FitSidebands aggregator/analyzer
            %
            %Usage:
            
            p = inputParser();
            p.addRequired('source',@ischar);  
            p.addRequired('targets',@(x) iscellstr(x) && ~isempty(x));
            p.addParameter('range',[],...
                @(x) isnumeric(x) && numel(x)==2);
            p.addParameter('guessFm',0,@(x) isscalar(x) && isreal(x) && x >= 0);
            p.addParameter('guessGm',1e3,@(x) isscalar(x) && isreal(x) && x >= 0);
            p.addParameter('boundsF',[],...
                @(x) isnumeric(x) && numel(x)==0 || numel(x)==2);         
            p.addParameter('boundsG',[0.01e3, 100e3],...
                @(x) isnumeric(x) && numel(x)==0 || numel(x)==2);         
            p.addParameter('method','avg',@(x) ismember(x, {'avg','adaptive'}));
            p.addParameter('minAvg',3,@(x) isscalar(x) && x>0);
            p.addParameter('threshold',0.4,@(x) isscalar(x) && isreal(x) && x>=0);
            p.addParameter('suppressDrive',[],@(x) isnumeric(x) && numel(x)==0 || size(x,2)==2);
            p.addParameter('plot',false,@(x) islogical(x) || ischar(x));
            p.addParameter('plotRange',[],...
                @(x) isnumeric(x) && numel(x)==0 || numel(x)==2);         
            p.addParameter('if','',@ischar);
            p.parse(varargin{:});
            args = p.Results;
            
            o.source = args.source;
            o.targets = args.targets;
            o.range = args.range;
            o.guessFm = args.guessFm;
            o.guessGm = args.guessGm;
            o.boundsF = args.boundsF;
            o.boundsG = args.boundsG;
            o.threshold = args.threshold;
            o.suppressDrive = args.suppressDrive;
            o.method = args.method;
            o.minAvg = args.minAvg;
            o.plot = args.plot;
            o.plotRange = args.plotRange;
            o.ifField = args.if;
            
            if ischar(o.plot)
                o.plotName = o.plot;
                o.plot = true;
            else
                o.plotName = 'Fit Sideband';
            end
            
            if o.guessFm <= 0
                o.guessFm = mean(o.range);
            end
            if isempty(o.boundsF)
                o.boundsF = [o.range(1), o.range(2)];
            end           
        end
        
        function result = run(o,aggModel,loop,point,model,varargin)
            result = Result.SUCCESS;
            if ~isempty(o.ifField)
                valid = aggModel.get(o.ifField);
                good = valid(loop, point);
            else
                good = true;
            end

            r2 = 0;
            Fm = NaN;
            Gm = NaN;
            
            if good && aggModel.has(o.source)
                switch o.method
                    case 'avg'
                        [fit, r2] = o.analyze_avg(aggModel, loop, point);
                    case 'adaptive'
                        [fit, r2] = o.analyze_adaptive(aggModel, loop, point);
                end

                if (r2 > o.threshold)
                    Fm = fit(2);
                    Gm = abs(fit(3));
                end
            end
            
            % Set results back on single DataFrame, to be aggregated
            % further, or saved in the DataScan
            model.set(o.targets{1}, Fm);
            if length(o.targets) > 1
                model.set(o.targets{2}, Gm);
            end            
            if length(o.targets) > 2
                model.set(o.targets{3}, r2);
            end
        end        
        
        function [fit, r2] = analyze_adaptive(o,model, loop, point)
            agg = model.get(o.source);

            if ~isempty(o.plotRange)  
                sum = 0*agg{1}.slice_x(o.plotRange(1), o.plotRange(2));
            else
                sum = 0*agg{1};
            end            

            for index = 1:length(agg)
                if ~isempty(o.plotRange)  
                    sum = sum + agg{index}.slice_x(o.plotRange(1), o.plotRange(2));
                else
                    sum = sum + agg{index};
                end  
                if index < min(o.minAvg, length(agg))
                    continue;
                end

                wf = o.suppress_drive(sum/index);
                [fit, r2] = o.do_fit(wf);        
                
                if o.plot
                    o.debug_plot(wf, fit);
                end
                
                if o.verify_fit(fit, r2)
                    if index > 1
                        fprintf('Fit accepted with %d average\n', index);
                    end
                    break;
                end
            end
        end
        
        function [fit, r2] = analyze_avg(o,model, ~, point)
            wf = model.get(o.source);
            if iscell(wf)
                wf = wf{max(1, length(wf))};
            end
            
            assert(isa(wf,'Waveform'),'skaffold:InputError',...
                'analyze.FitSidebands expects ''source'' to refer to a Waveform.');
            
            if ~isempty(o.plotRange)  
                wf = wf.slice_x(o.plotRange(1), o.plotRange(2));
            end            

            wf = o.suppress_drive(wf);
            [fit, r2] = o.do_fit(wf);                        
            if o.plot
                o.debug_plot(wf, fit);                  
            end
        end
        
        function wf = suppress_drive(o,wf)
            if ~isempty(o.suppressDrive)
                suppressRange = [];
                for n = 1:size(o.suppressDrive,1)% loop over rows
                    [imin, imax] = wf.get_index_bounds(o.suppressDrive(n, 1), o.suppressDrive(n, 2));
                    suppressRange = [suppressRange, imin:imax];
                end
                
                includeRange = setdiff(1:size(wf.data), suppressRange);
                wf.data(suppressRange) = interp1(includeRange, wf.data(includeRange), suppressRange, 'linear');
            end            
        end
        
        function [fit, r2] = do_fit(o,wf)
            if ~isempty(o.range)  
                wf = wf.slice_x(o.range(1), o.range(2));
            end               
            wfmax = max(wf.data);
            guesses = [1, o.guessFm, o.guessGm, 0.1];
            lowerbound = [0.1, o.boundsF(1), o.boundsG(1), 0];
            upperbound = [2, o.boundsF(2), o.boundsG(2), .6];
            
            opts = optimoptions(@lsqcurvefit,'Display','off');
            wfScaled = double(wf.data/wfmax);
            
            [fit,~,residual] = lsqcurvefit(o.fitModel, guesses, double(wf.get_x'), wfScaled, lowerbound, upperbound, opts);
            
            sse = sum(residual.^2);
            sst = sum((wfScaled - mean(wfScaled)).^2);
            r2 = 1 - sse*(length(wfScaled)-1)/(sst*(length(wfScaled) - 4));   
            fit(1) = fit(1) * wfmax;
            fit(4) = fit(4) * wfmax;
        end
        
        function result = verify_fit(o, fit, r2)
            result = (r2 > o.threshold);
        end
        
        function debug_plot(o,wf, fit)
            % Plotting for debugging          
             x = linspace(o.range(1), o.range(2), 300);
             yfit = o.fitModel(fit,x);
             
             f = findfigure(o.plotName);
             clf(f);
             hAxes = axes('Parent',f);
             hold(hAxes,'on');
             plot(hAxes, wf.get_x(), wf.data);
             plot(hAxes, x,yfit,'r-');
             xlim(hAxes, o.plotRange);
             hold(hAxes,'off');
             pause(0.1);
        end
    end
    
end

