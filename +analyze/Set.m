classdef Set < analyze.Lambda
    % Sets specified `target` field to value `val`.
    
    methods
        function o = Set(target, val, varargin)
            o = o@analyze.Lambda({}, target, @() val, varargin{:});
        end
    end
end
