classdef FluorFit < Analyzer
    
    properties (SetAccess = protected)
        loader
        timestamp     % Name of DataModel field containing iteration timestamp
        target        % Name of DataModel field in which to store results
        
        roi_cen
        roi_span
        roi_vertices
        roi_imrect
        
        sigma
        pixel_size
        magnification
        
        doPlot
        warn
    end
            
    properties (Transient,SetAccess = protected)
        hFigure
        hAxes        
    end
    
    methods
        function o = FluorFit(varargin)
            
            p = inputParser();
            p.addRequired('loader',@(x) isa(x, 'Loader'));
            p.addRequired('timestamp',@ischar);
            p.addRequired('target',@ischar);
            p.addParameter('cen',[],@(x) numel(x) == 2);
            p.addParameter('span',[],@(x) numel(x) == 2);
            p.addParameter('plot',false,@islogical);
            p.addParameter('warn',false,@islogical);
            p.parse(varargin{:});
            args = p.Results;
            
            o.loader = args.loader;
            o.timestamp = args.timestamp;
            o.target = args.target;
            
            o.roi_cen = args.cen;
            o.roi_span = args.span;
            
            o.doPlot = args.plot;
            o.warn = args.warn;
            
            o.roi_vertices = [
                o.roi_cen(1) - ceil(o.roi_span(1)/2) + 1, o.roi_cen(2) - ceil(o.roi_span(2)/2) + 1;
                o.roi_cen(1) - ceil(o.roi_span(1)/2), o.roi_cen(2) + floor(o.roi_span(2)/2);
                o.roi_cen(1) + floor(o.roi_span(1)/2), o.roi_cen(2) + floor(o.roi_span(2)/2);
                o.roi_cen(1) + floor(o.roi_span(1)/2), o.roi_cen(2) - ceil(o.roi_span(2)/2) + 1;
            ];
        
            o.roi_imrect = 0;

            o.set_source_fields({o.timestamp});
        end
        
        function result = analyze(o, model, ~, ~)
            if ~isempty(o.timestamp)
                ts = model.get(o.timestamp);

                [result, img, ~, ~] = o.loader.load_timestamp(ts);
                if result == Result.SKIP
                    model.set(o.target, o.emptyStruct());                
                    result = Result.SUCCESS;
                    return
                end
            else
                [result, img, ~] = o.loader.load();
                if result == Result.SKIP
                    model.set(o.target, o.emptyStruct());                
                    result = Result.SUCCESS;
                    return
                elseif result == Result.OUT_OF_DATA
                    model.set(o.target, o.emptyStruct());                
                    return                        
                end

                o.loader.increment();
            end
            
%             img.abs = (img.sig - img.bkg);
            try
                img.abs = img.tof;
            catch
                img.abs = double(img.sig);
            end
           
            % Get ROI
            roi_x = o.roi_cen(1)+(1:o.roi_span(1))-ceil(o.roi_span(1)/2);
            roi_z = o.roi_cen(2)+(1:o.roi_span(2))-ceil(o.roi_span(2)/2);           
            roi_tof = img.abs(roi_x, roi_z);
            odzero = -log(roi_tof);	
            
            if o.doPlot
                if isempty(o.hFigure) || ~ishandle(o.hFigure)
                    o.hFigure = findfigure('Absorption fits');
                    clf(o.hFigure);
%                     o.hFigure.Position = [100,400,850,600];
                end

                subplotLocations={1:3,4,5,6};
                for index = 1:numel(subplotLocations)
                    if length(o.hAxes) < index || ~ishandle(o.hAxes(index))
                        o.hAxes(index) = subplot(2, 3, subplotLocations{index}, 'Parent', o.hFigure);
                    end
                end                
                
                ax = o.hAxes(1);
                cla(ax);
                e3.colorplot(ax, 1:size(img.abs,1), 1:size(img.abs,2), img.abs');
                colormap(ax, 'false2');                
                caxis(ax, [0.4, 1.1]);

                o.roi_imrect = imrect(ax, [
                    o.roi_cen(1) - ceil(o.roi_span(1)/2) + 1,...
                    o.roi_cen(2) - ceil(o.roi_span(2)/2) + 1,...
                    o.roi_span(1), o.roi_span(2)]);
                
%                 patch(o.roi_vertices(1:end,1), o.roi_vertices(1:end,2), [.2 .2 .8],...
%                     'EdgeColor', [.2 .2 .8],'LineWidth',1,'FaceAlpha',0.4, 'Parent', ax);
            else
                o.hAxes = cell(1,4);
            end
            
            w = [];
            if ~o.warn
                w = warning ('off','all');
            end
            
            [resulth, sliceh] = o.fit1D_h(roi_x, odzero, o.hAxes(3));
            [resultv, slicev] = o.fit1D_v(roi_z, odzero, o.hAxes(4));

            if ~o.warn
                warning(w);
            end
            
            resultc = o.countN(roi_tof);
            
            names = [fieldnames(resulth); fieldnames(resultv); fieldnames(resultc); fieldnames(img)];
            result = cell2struct([struct2cell(resulth); struct2cell(resultv); struct2cell(resultc); struct2cell(img)], names, 1);
            
            model.set(o.target, result);
            if numel(o.sliceTargets)>=1
                model.set(o.sliceTargets{1}, Waveform(roi_x, sliceh));
            end
            if numel(o.sliceTargets)>=2
                model.set(o.sliceTargets{2}, Waveform(roi_z, slicev));
            end
            result = Result.SUCCESS;
        end
        
        function [result, h_slice] = fit1D_h(o, roi_x, odzero, ax) 
            h_slice = nansum(odzero,2);
            h_mask = ~sum(~isnan(odzero),2);
            h_slice(h_mask) = NaN;

            if ishandle(ax)
                cla(ax)
                hold(ax,'on');
                plot(ax, roi_x, h_slice);
                xlabel(ax,'horizontal slice');
            end

            try
                guess_cen = find(h_slice==max(h_slice),1);
                guess = [0, 100, roi_x(guess_cen), o.roi_span(1)/2];
                [fit_params,fit_residuals,~,fit_covar] = nlinfit(roi_x(~h_mask), h_slice(~h_mask),@analyze.AbsorptionFit.Gauss1D,guess);
                ci = nlparci(fit_params,fit_residuals,'covar',fit_covar);
                fit_err = abs(ci(:,1) - ci(:,2))'/2;

                % lb = [-2, .001, roi_x_cen - roi_x_span/2, .1];
                % ub = [10, 10, roi_x_cen + roi_x_span/2, roi_x_span];
                % [fitParams,resnorm,residual,exitflag] = lsqcurvefit(@Gauss1D, guess, roi_x(~h_mask), h_slice(~h_mask), lb, ub);

                if ishandle(ax)
                    plot(ax, roi_x, analyze.AbsorptionFit.Gauss1D(fit_params, roi_x));
                end
            catch ME
                warning(['Error performing 1D horizontal fit: ', ME.message]);
                fit_params = zeros(1,4);
                fit_err = zeros(1,4);
            end
            
            result = struct();
            result.x0_1d = fit_params(3);
            result.Sx_1d = abs(fit_params(4))/sqrt(2);	% Convert fit width to rms width (units of pixels)
            result.dSx_1d = fit_err(4)/sqrt(2);	% Convert fit width to rms width (units of pixels)
            result.Wx_1d = result.Sx_1d*o.pixel_size/o.magnification;					%scale to centimeters
            result.dWx_1d = result.dSx_1d*o.pixel_size/o.magnification;					%scale to centimeters           
            result.Na_h = sqrt(pi)*fit_params(2)*abs(fit_params(4))*o.pixel_size^2/(o.sigma);
            result.dNa_h = sqrt(pi)*o.pixel_size^2/(o.sigma)*(fit_err(2)*abs(fit_params(4)) + fit_params(2)*fit_err(4));
            
            if ishandle(ax)
                str = {...
                    sprintf('Na: %.3g \\pm %.3g', result.Na_h, result.dNa_h),...
                    sprintf('Sx: %.2g \\pm %.2g', result.Sx_1d, result.dSx_1d)...
                    };
                text(o.roi_cen(1)-.4*o.roi_span(1), 1.0, str, 'BackgroundColor','white','EdgeColor','black','Parent',ax);
            end
        end
        
        function [result, v_slice] = fit1D_v(o, roi_z, odzero, ax) 
            v_slice = nansum(odzero,1)';
            v_mask = ~sum(~isnan(odzero),1);
            v_slice(v_mask) = NaN;
            
            if ishandle(ax)
                cla(ax)
                hold(ax,'on');
                %set(ax, 'YScale','log');
                plot(ax, roi_z, v_slice);
                xlabel(ax,'vertical slice');
            end

            try
                guess_cen = find(v_slice==max(v_slice),1);
                guess = [0, 100, roi_z(guess_cen), o.roi_span(2)/2];
                [fit_params,fit_residuals,~,fit_covar] = nlinfit(roi_z(~v_mask), v_slice(~v_mask),@analyze.AbsorptionFit.Gauss1D,guess);
                ci = nlparci(fit_params,fit_residuals,'covar',fit_covar);
                fit_err = abs(ci(:,1) - ci(:,2))'/2;

                % lb = [-2, .001, roi_z_cen - roi_z_span/2, .1];
                % ub = [10, 10, roi_z_cen + roi_z_span/2, roi_z_span];
                % [fitParams,resnorm,residual,exitflag] = lsqcurvefit(@Gauss1D, guess, roi_y(~v_mask), v_slice(~v_mask), lb, ub);

                if ishandle(ax)
                    plot(ax, roi_z, analyze.AbsorptionFit.Gauss1D(fit_params, roi_z));
                end
            catch ME
                warning(['Error performing 1D vertical fit: ', ME.message]);
                fit_params = zeros(1,4);
                fit_err = zeros(1,4);                
            end     
            
            result = struct();
            result.z0_1d = fit_params(3);
            result.Sz_1d = abs(fit_params(4))/sqrt(2);
            result.dSz_1d = fit_err(4)/sqrt(2);
            result.Wz_1d = result.Sz_1d*o.pixel_size/o.magnification;					%scale to centimeters
            result.dWz_1d = result.dSz_1d*o.pixel_size/o.magnification;					%scale to centimeters            
            result.Na_v = sqrt(pi)*fit_params(2)*abs(fit_params(4))*o.pixel_size^2/(o.sigma);
            result.dNa_v = sqrt(pi)*o.pixel_size^2/(o.sigma)*(fit_err(2)*abs(fit_params(4)) + fit_params(2)*fit_err(4));            
            
            if ishandle(ax)
                str = {...
                    sprintf('Na: %.3g \\pm %.3g', result.Na_v, result.dNa_v),...
                    sprintf('Sz: %.2g \\pm %.2g', result.Sz_1d, result.dSz_1d)...
                    };
                text(o.roi_cen(2)-.4*o.roi_span(2), 1.0, str, 'BackgroundColor','white','EdgeColor','black','Parent',ax);
            end            
        end
       
        function result = countN(o, roi_tof)
            MINTRANS = .01;	% cut off for minimum transmission (omit pixels from analysis where transmission < mintrans
            MAXTRANS = 1.5;    % cut of center of Region of Interest

            od_lim = roi_tof;
            od_lim(od_lim > MAXTRANS) = MAXTRANS;
            od_lim(od_lim < MINTRANS) = MINTRANS;
            od = -log(od_lim);

            result = struct();
            odsum = nansum(od(:));
            result.Na_sum = odsum/o.sigma*(o.pixel_size/o.magnification)^2;
        end
        
    end
   
    methods(Static)
        
        function Z = Gauss1D(params,x)
            Z = params(1) + params(2)*exp(-((x(:)-params(3))/params(4)).^2);
        end        
        
        function s = emptyStruct()
            s = struct(...
                'Sx_2d', NaN, 'dSx_2d', NaN, 'Sz_2d', NaN, 'dSz_2d', NaN, ...
                'x0_2d', NaN, 'dx0_2d', NaN, 'z0_2d', NaN, 'dz0_2d', NaN, ...
                'Wx_2d', NaN, 'dWx_2d', NaN, 'Wz_2d', NaN, 'dWz_2d', NaN, ...
                'Na_2d', NaN, 'dNa_2d', NaN, 'Na_offset', NaN, 'dNa_offset', NaN,...
                'x0_1d', NaN, 'Sx_1d', NaN, 'dSx_1d', NaN, 'Wx_1d', NaN, ...
                'dWx_1d', NaN, 'Na_h', NaN, 'dNa_h', NaN, 'z0_1d', NaN, 'Sz_1d', NaN, 'dSz_1d', NaN,...
                'Wz_1d', NaN, 'dWz_1d', NaN, 'Na_v', NaN, 'dNa_v', NaN, 'Na_sum', NaN...
            );
        end
    end
end

