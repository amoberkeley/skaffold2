classdef FindRange < Analyzer
    % Finds the first range in which the input data meet a given set of
    % requirements.
    
    properties (SetAccess = protected)
        sources       % Names of DataModel fields containing data to search
        target        % Name of DataModel field to which the result should be saved
        start         % Function describing the requirements for the range to begin
        stop          % Function describing the requirements for the range to end, or desired range length
        range         % Data range in which to search
        rangeField    % Name of DataModel field containing range in which to search
        rangeStart    % Earliest point at which to start searching
        rangeStartField % Name of DataModel field containing earliest point at which to start searching
        minLength     % Minimum length of the range
        waitTime      % Time to wait before starting to search for a range
        successField  % Field to write failures to rather than throwing errors
    end
    
    methods
        function o = FindRange(sources, target, start, stop, varargin)
            % Builds a new FindRange analyzer
            %
            % Usage:
            %    o = analyze.FindRange(xField, yField, target, start, stop)
            %    o = analyze.FindRange(..., options)
            %
            % Parameters:
            %   xField - Name(s) of DataModel field(s) containing data to
            %     search and operate on
            %   target - Name of DataModel field to which the resulting
            %     range should be saved
            %   start - Function describing the requirements for the range
            %     to begin
            %   stop - Function describing the requirements for the range
            %     to stop, or desired length of range (in seconds)
            %
            % Options:
            %   range - Data range in which to search
            %   rangeField - Name of DataModel field containing range in
            %     which to search
            %   rangeStart - Earliest point at which to start searching
            %   rangeStartField - Name of DataModel field containing 
            %     earliest point at which to start searching
            %   minLength - Minimum length of the range
            %   waitTime - Time to wait before starting to search for the
            %     range
            %   successField - Field to write failures to rather than throwing
            %     errors
            
            p = inputParser();
            p.addRequired('sources', @(x) ischar(x) || iscellstr(x));
            p.addRequired('target', @ischar);
            p.addRequired('start', @(x) isa(x, 'function_handle'));
            p.addRequired('stop', @(x) isa(x, 'function_handle') || (isnumeric(x) && numel(x) == 1));
            p.addParameter('range', [], @(x) ...
                (isnumeric(x) && numel(x) == 0 || numel(x) == 2) ||...
                (iscell(x) && all(cellfun(@(y) isnumeric(y) && numel(y) == 2, x))));
            p.addParameter('rangeField', '', @ischar);
            p.addParameter('rangeStart', [], @isnumeric);
            p.addParameter('rangeStartField', '', @ischar);
            p.addParameter('minLength', 0, @(x) isnumeric(x) && numel(x) == 1);
            p.addParameter('waitTime', 0, @(x) isnumeric(x) && numel(x) == 1);
            p.addParameter('successField', '', @ischar);
            
            p.parse(sources, target, start, stop, varargin{:});
            args = p.Results;
            
            if iscell(args.sources)
                o.sources = args.sources;
            else
                o.sources = {args.sources};
            end
            
            o.target = args.target;
            o.start = args.start;
            o.stop = args.stop;         
            o.range = args.range;
            o.rangeField = args.rangeField;
            o.rangeStart = args.rangeStart;
            o.rangeStartField = args.rangeStartField;
            o.minLength = args.minLength;
            o.waitTime = args.waitTime;
            o.successField = args.successField;
            
            o.set_source_fields(o.sources);
        end
        
        function result = analyze(o, model, ~, point)
            srcs = num2cell(cellfun(@model.get, o.sources));

            rng = o.range;
            if ~isempty(o.rangeField)
                rng = get(model, o.rangeField);
            end
            if iscell(rng)
                rng = rng{point};
            end
            
            rngStart = o.rangeStart;
            if ~isempty(o.rangeStartField)
                rngStart = get(model, o.rangeStartField);
            end
            if iscell(rngStart)
                rngStart = rngStart{point};
            end
            rngStart = max(rngStart);
            
            for i = 1:length(srcs)
                assert(isa(srcs{i}, 'Waveform'), 'skaffold:InputError', ...
                    'All FindRange source fields must be Waveforms.');
                    
                if ~isempty(rng)
                    srcs{i} = srcs{i}.slice_x(min(rng) + o.waitTime, max(rng));
                elseif ~isempty(rngStart)
                    st = min(rngStart + o.waitTime, srcs{i}.get_x_end);
                    srcs{i} = srcs{i}.slice_x(st, srcs{i}.get_x_end);
                end
            end
            
            xdata = srcs{1}.get_x;
            dx = srcs{1}.dx;
            
            assert(all(cellfun(@(wf) all(wf.get_x == xdata), srcs)), 'skaffold:InputError', ...
                'All FindRange source fields must share the same x data.');
            
            ydata = cellfun(@(wf) wf.data, srcs, 'UniformOutput', false);
            
            startAfter = o.start(ydata{:});
            startAt = xdata(find(startAfter, 1));
            
            defaultRange = [startAt, startAt+max(o.minLength, dx)];
            
            result = Result.SUCCESS;
            
            if isempty(startAt)
                if ~isempty(o.successField)
                    model.set(o.successField, 0);
                else
                    warning('skaffold:InputError', ...
                        ['No suitable starting value for ' o.target ' was found for the desired range.']);
                end
                model.set(o.target, [0, 0]); % TODO: find a more suitable failsafe
                return
            end
            
            if isnumeric(o.stop)
                stopAt =  startAt + o.stop;
            else
                stopBefore = o.stop(ydata{:}) & (xdata >= (startAt + o.minLength))';
                stopAt = xdata(find(stopBefore, 1));

                if isempty(stopAt)
                    if ~isempty(o.successField)
                        model.set(o.successField, 0);
                    else
                        warning('skaffold:InputError', ...
                            ['No suitable stopping value for ' o.target ' was found for the desired range.']);
                    end
                    model.set(o.target, defaultRange);
                    return
                end
            end
            
            if ~isempty(o.successField)
                model.set(o.successField, 1);
            end
            
            model.set(o.target, [startAt, stopAt]);
            
        end
        
    end
    
end
