classdef Heterodyne < Analyzer
    %Performs averaging and automatic quadrature rotation on a heterodyne signal
    %
    %This analyzer requires the signal processing toolbox
    
    properties (SetAccess = protected)
        source       % Name of the field holding the heterodyne signal
        targets      % Names of target fields for demodulated data
        range
        filterBw     % BW for quadrature identification
        carrierFreq  % Beat carrier frequency
        doRotation   % If true, performs rotation
        pulseMask
    end
    
    methods
        function o = Heterodyne(varargin)
            % Creates a new Heterodyne analyzer
            %
            % Usage:
            %   obj = analyze.Heterodyne(source,[targets],'key',value,...)
            %
            % Parameters:            
            %   source - string
            %       name of DataModel field containing raw signal
            %   targets - {'iQuad','qQuad','hetMag','hetPhase', 'iAvg','qAvg'}
            %       Destination fields for demodulated data
            %
            % Keys:
            %   range - [] | 2-element double array
            %       Time range for demodulation.  If empty, demodulates 
            %       entire source.            
            %   filterBw - double
            %       Averaging BW
            %   carrierFreq - double
            %       Heterodyne carrier frequency
            %   doRotation - logical
            %       Whether to rotate carrier phase to 0
            
            p = inputParser();
            p.addRequired('source',@ischar)
            p.addOptional('targets',{'iQuad','qQuad','hetMag','hetPhase','iAvg','qAvg'},@iscellstr);
            p.addParameter('range', [], @(x) isnumeric(x) && numel(x)==0 || numel(x)==2);
            p.addParameter('pulseMask',[],@(x) isnumeric(x) && (isempty(x) || size(x,2)==2));
            p.addParameter('filterBw',10e3,@isscalar)
            p.addParameter('carrierFreq',10e6,@isscalar)
            p.addParameter('doRotation',true,@(x) x || ~x)
            p.parse(varargin{:});
            args = p.Results;
            
            o.source = args.source;
            o.targets = args.targets;
            o.range = args.range;
            o.filterBw = args.filterBw;
            o.carrierFreq = args.carrierFreq;
            o.doRotation = args.doRotation;
            o.pulseMask = args.pulseMask;
            
            o.set_source_fields({o.source});
        end
        
        function result = analyze(o, model, ~, ~)
            %Performs the heterodyne conversion.
            %
            %This is a three-step process.  First, the heterodyne signal
            %is demodulated at the carrier frequency, yielding the in-phase
            %(I) and quadrature-phase (Q) signal components.  Next, the
            %low-frequency instantaneous carrier amplitude and phase are
            %determined.  Finally, the quadratures are rotated such that I
            %reflects the amplitude modulation and Q reflects the phase
            %modulation.
            %
            %The third step is optional, toggled by o.doRotation.
            %
            %The entire process should take roughly 1 s on a 2.4 GHz
            %machine, for data sources having 3.2 million elements.
            %
            %Returns:
            %  model - A HeterodyneDataModel object containing the
            %          following fields:
            %    iQuad - The in-phase quadrature
            %    qQuad - The quadrature-phase quadrature
            %    hetAmp - The carrier amplitude
            %    hetPhase - The carrier phase
            %    iAvg - The low-pass filtered in-phase quadrature
            %    qAvg - The low-pass filtered quadrature-phase quadrature
            
            wf = model.get(o.source);
            if ~isempty(o.range)
                wf = wf.slice_x(o.range(1), o.range(2));
            end            
            
            t0 = wf.x0;
            
            [cQuad,quadDt] = o.demod(wf.data,o.carrierFreq,t0,wf.dx);
            [mag,phase,cAvg,avgDt] = o.average(cQuad,o.filterBw,quadDt);
            if o.doRotation
                if ~isempty(o.pulseMask)
                    phase = o.mask_phase(phase,t0,avgDt,o.pulseMask);
                end
                [cQuad] = o.rotate(cQuad,phase,avgDt,quadDt);
            end
                       
            if ~isempty(o.targets{1})
                model.set(o.targets{1}, Waveform(t0,quadDt,real(cQuad)));
            end
            if length(o.targets) > 1 && ~isempty(o.targets{2})
                model.set(o.targets{2}, Waveform(t0,quadDt,imag(cQuad)));
            end
            if length(o.targets) > 2 && ~isempty(o.targets{3})
                model.set(o.targets{3}, Waveform(t0,avgDt,mag));
            end
            if length(o.targets) > 3 && ~isempty(o.targets{4})
                model.set(o.targets{4}, Waveform(t0,avgDt,phase));
            end
            if length(o.targets) > 4 && ~isempty(o.targets{5})
                model.set(o.targets{5}, Waveform(t0,avgDt,real(cAvg)));
            end
            if length(o.targets) > 5 && ~isempty(o.targets{6})
                model.set(o.targets{6}, Waveform(t0,avgDt,imag(cAvg)));
            end
            
            result = Result.SUCCESS;
        end
    end
    
    methods (Static)
        
        function [cRaw,quadDt] = demod(source,carrierFreq,t0,dt)
            % Performs I/Q demodulation
            %
            % Defines the I quadrature to be in phase with a cosine wave
            % beginning from time = 0.  The Q quadrature is thus in-phase
            % with a sine wave beginning from time = 0.
            %
            % iRaw and qRaw are filtered and downsampled to the carrier 
            % frequency.  This is accomplished via a call to a hacked 
            % version of MATLAB's builtin decimate(y,r,'fir') function.  
            % The method has been hacked to use an 8-pole Butterworth
            % filter with cutoff frequency at 1.0*carrier frequency.  The 
            % filter is applied twice, in forward and reverse directions with
            % reflecting boundary conditions, yielding no phase distortion
            % of the result.
            %
            % Note that iRaw and qRaw are returned as column vectors,
            % irregardless of the shape of source.
            
            n = length(source);
            carrierOmega = 2*pi*carrierFreq;    % Only do this calculation once
            r = round(1/dt/carrierFreq);
            quadDt = r*dt;
            
            % Make the cos and sin arrays
            t = linspace(t0, t0+dt*(n-1), n);
            cMask = exp(1i*carrierOmega*t);

            cMask = source.*cMask';
            % Matlab's IIR 2-way Chebyshev filter has non-unity gain,
            % so use FIR filter.  FIR filter also has advangtage of being
            % significantly faster in this application (by x4)
            cRaw = e3.decimate(cMask,r);       % Filter and downsample, requires Signal Processing Toolbox
        end
        
        function [mag,phase,cAvg,avgDt] = average(cRaw,bw,dt)
            % Creates low-sample-frequency waveforms of I/Q, and
            % calculates low-frequency semi-instantaneous amplitude and
            % phase.
            %
            % As for HeterodyneAnalyzer.demod, calls MATLAB's decimate()
            % function, but with filter frequency at 0.8/dt.
            
            % This process should take a negligible amount of time when
            % compared to demod().  This is because the source is already
            % 1/8 length, and decimate() operates in O(n) time.
            r = round(1/dt/bw/2);           % Number of points over which to average
            avgDt = r*dt;
            cAvg = e3.decimate(cRaw,r);  % Filter and downsample
            mag = real(cAvg).^2 + imag(cAvg).^2;
            phase = unwrap(angle(cAvg));
        end
        
        function [phase] = mask_phase(phase, t0, phaseDt, mask)
            
            t = t0 + (0:phaseDt:(length(phase)-1)*phaseDt);
            
            iMaskEdge = (mask - t0) / phaseDt;
            iMaskEdge(:,1) = floor(iMaskEdge(:,1));
            iMaskEdge(:,2) = ceil(iMaskEdge(:,2));

            for idx=1:size(mask,1)
                phase(iMaskEdge(idx, 1):iMaskEdge(idx,2)) = nan;
            end
            nanMask = isnan(phase);
            
            phase(nanMask) = interp1(t(~nanMask),phase(~nanMask),t(nanMask),'linear');                
        end
        
        function [cRot] = rotate(cRaw,phase,phaseDt,quadDt)
            % Automatically rotates quadratures such that I corresponds to
            % carrier amplitude and Q corresponds to carrier phase.
            
            % We can ignore the time 0 for these operations
            ti = 0:quadDt:(length(cRaw)-1)*quadDt;           % Time lookup array
            phasei = winterp1(0,phaseDt,phase,ti,'linear');         
            
            % winterp1 NaN fills end of interpolated data.  This causes problems for taking FFTs and 
            % filtering the rotated data, so to avoid NaNs for convenience,
            % we will back fill the end of the array with the last finite
            % value
            lasti = find(~isnan(phasei),1,'last');  
            phasei((lasti+1):end) = phasei(lasti);
            
            % Could use nearest-neighbor for speed increase (currently
            % using linear interpolation)
            cRot = cRaw .* exp(-1i * phasei); % Perform rotation
        end
    end
    
end

