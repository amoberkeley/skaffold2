classdef QuadratureFFT < Analyzer
    %QUADRATUREFFTANALYZER Creates maps of FFTs verses quadrature angle
    %
    %Note that complex quadrature maps can be efficiently
    %reconstructed using a phaseList equal to [0 pi/2], then
    %rotating the resulting complex FFT after averaging.  Power
    %maps must, however, be calculated before averaging.
    
    properties
        phaseList
        iSource
        qSource
        target
        range     % Time range 
        type      % One of 'complex', 'power', 'complexDensity', or 'powerDensity'
    end
    
    methods
        function o = QuadratureFFT(iSource,qSource,target,phaseList,varargin) 
            %Constructs a new QuadratureFFT Analyzer
            %
            %Usage:
            %  obj = analyze.QuadratureFFT(iSource,qSource,target,phaseList,'key',value,...)
            %
            %Parameters:
            %  iSource - Source of I quadrature
            %  qSource - Source of Q quadrature
            %  target - DataModel field in which to store quadrature map spectrogram
            %  phaseList - numeric vector
            %    Quadrature angles (in radians) at which to make the FFT
            %
            %Keys:
            %  range - [] | 2-element double array
            %    Start and stop time for transform  If empty, transforms 
            %               entire source.
            %  type - 'complex' | 'power' | 'complexDensity' | 'powerDensity'

            p = inputParser;
            p.addRequired('iSource',@ischar);
            p.addRequired('qSource',@ischar);
            p.addRequired('target',@ischar);
            p.addRequired('phaseList',@(x) isnumeric(x) && isvector(x));
            p.addParameter('range',[], @(x) isnumeric(x) && numel(x)==0 || numel(x)==2);
            p.addParameter('type','powerDensity',...
                @(x) any(strcmp(x,{'complex','power','complexDensity','powerDensity'})));          
            p.parse(iSource,qSource,target,phaseList,varargin{:});   
            r = p.Results;
            
            o.iSource = r.iSource;
            o.qSource = r.qSource;
            o.target = r.target;
            o.phaseList = r.phaseList;
            o.range = r.range;
            o.type = r.type;
            
            o.set_source_fields({o.iSource,o.qSource});            
        end
        
        function result = analyze(o, model, ~, ~)
            %Creates a quadrature map of the time-averaged spectrogram
            %
            %In the returned Waveform, rows correspond to frequency and
            %columns correspond to quadrature angle.
            
            %First angle
            s = o.get_quadrature(model,o.phaseList(1));
            s = s.slice_x(o.range(1),o.range(2));
            %Perform the transformation
            yf = s.fft(o.type);          
            for iAngle = 2:length(o.phaseList)
                s = o.get_quadrature(model,o.phaseList(iAngle));
                s = s.slice_x(o.range(1),o.range(2));
                yf.data(:,iAngle) = s.fft(o.type).data(:);
            end
   
            model.set(o.target, yf);         
            result = Result.SUCCESS;           
        end
        
        function rot = get_quadrature(o,model,theta)
            %Returns the time-domain data at the specified quadrature angle
            %
            %Usage:
            %  rot = obj.get_quadrature(model,theta)
            %
            %Parameters:
            %  model - DataModel containing fiels <obj.iSource> and
            %    <obj.qSource>
            %  theta - Quadrature angle (in radians)
            
            si = model.get(o.iSource);
            sq = model.get(o.qSource);
            
            rot = si*cos(theta) + sq*sin(theta);
        end
    end
    
end

