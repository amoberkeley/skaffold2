classdef FFT < Analyzer
    %Makes Fourier transform.
    
    properties (SetAccess = protected)
        source    % DataModel field to transform
        target    % DataModel field for result
        range     % Time range start        
        rangeField
        zeroPadRange
        type      % One of 'complex', 'power', 'complexDensity', or 'powerDensity'
    end
    
    methods
        function o = FFT(source,target,varargin)
            %Constructs a new FFT analyzer
            %
            %Usage:
            %
            %  o = analyze.FFT(source,target,'key',value,...)
            %Parameters:
            %  source - Name of the DataModel field to transform
            %  target - Name(s) of the DataModel field(s) to store transform in
            %
            %Keys (default first):
            %  range - [] | 2-element double array
            %    Start and stop time for transform  If empty, transforms 
            %               entire source.
            %  type - 'complex' | 'power' | 'complexDensity' | 'powerDensity'
                        
            p = inputParser();
            p.addRequired('source', @ischar);
            p.addRequired('target', @(x) ischar(x) || iscellstr(x));
            p.addParameter('range', [], @(x) isnumeric(x) && numel(x)==0 || numel(x)==2);
            p.addParameter('rangeField', '', @ischar);
            p.addParameter('type', 'powerDensity', ...
                @(x) all(ismember(x, {'complex', 'power', 'complexDensity', 'powerDensity'})));
            p.addParameter('zeroPadRange', [], @(x) isnumeric(x) && numel(x)==0 || numel(x)==2);
            p.parse(source,target,varargin{:});
            args = p.Results;
            
            o.source = args.source;

            if ischar(args.target)
                o.target = {args.target};
            else
                o.target = args.target;
            end            

            o.range = args.range;
            o.rangeField = args.rangeField;

            o.zeroPadRange = args.zeroPadRange;
            
            if ischar(args.type)
                o.type = {args.type};
            else
                o.type = args.type;
            end
            
            o.set_source_fields({o.source});
        end
        
        function result = analyze(o, model, ~, ~)
            wf = model.get(o.source);
            
            rng = [];
            if ~isempty(o.range)
                rng = o.range;
            elseif ~isempty(o.rangeField)
                rng = get(model, o.rangeField);
            end

            if ~isempty(o.zeroPadRange)
                if o.zeroPadRange(1) > rng(1)
                    rng(1) = o.zeroPadRange(1);
                end

                if o.zeroPadRange(2) < rng(2)
                    rng(2) = o.zeroPadRange(2);
                end
            end
            
            % Slice, if requested
            if ~isempty(rng)
                wf = wf.slice_x(rng(1), rng(2));  
            end

            if ~isempty(o.zeroPadRange)
                wfLength = wf.get_x_end - wf.x0;
                wfLengthNew = o.zeroPadRange(2) - o.zeroPadRange(1);

                nPointsNew = ceil((wfLengthNew - wfLength) / wf.dx);
                wf.data = [wf.data; zeros(nPointsNew, 1)];
            end
            
            % Perform the transformation
            fft = wf.fft(o.type{:});

            % Save output
            for i=1:length(o.target)
                model.set(o.target{i}, fft(i));
            end
            result = Result.SUCCESS;
        end
    end    
end

