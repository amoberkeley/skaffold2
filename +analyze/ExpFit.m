classdef ExpFit < analyze.Fit
    %Fits data in a DataModel to an exponential.
    
    methods
        function o = ExpFit(xField, yField, target, varargin)
            %Builds a new ExpFit analyzer
            %
            %Usage:
            %    o = analyze.ExpFit(xField,yField,target,[fitOptions],)
            %    o = analyze.ExpFit(...,fitOptions)
            %Parameters:
            %   xField - Name of DataModel field holding x data; or, set to
            %     '', in which case x data will be extracted from y
            %     Waveform.
            %   yField - Name of DataModel field holding y data
            %   fitOptions - Cell array of options to pass to nlfit
            %   range - Data range to use in fit
            
            o = o@analyze.Fit(xField, yField, target,...
                @(y0, A, f, x) analyze.ExpFit.expModel(y0, A, f, x), @analyze.ExpFit.expGuess,...
                varargin{:});
        end
    end
    
    methods (Static) 
        function result = expModel(y0, A, f, x)
            x0 = min(x);
            result = y0+A*exp(-(x-x0)*f);
        end
        
        function guess = expGuess(x, y)
            %guess = [min(y), max(y)-min(y), x(find((y-min(y))<(max(y)-min(y))/3,1,'first'))-min(x) ];
            xSpan = max(x)-min(x);
            if 0 == xSpan
                warn('Bad x-values!')
                xSpan = 0.1;
            end
            
            guess = [min(y), max(y)-min(y), 4/xSpan];
        end        
    end
end

