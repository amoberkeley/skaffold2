classdef DispersiveShift < Analyzer
    % calculates dispersive shift
    %   
    
    properties (SetAccess = protected)
        fitFields
        target
        emptyNamespace
        errTarget
    end
    
    methods
        function o = DispersiveShift(fitFields, target, varargin)
            %Construct a DispersiveShift analyzer
            %
            %Usage:
            %  obj = analyze.DispersiveShift(fitFields)
            %
            %Parameters:
            %  fitFields - Name of fields containing nlfit object, storing
            %    result of cavity resonance fit (center frequency must be
            %    in nlfit field 'x0')
            p = inputParser;
            p.addRequired('fitFields', @(x) ischar(x) || iscellstr(x))
            p.addRequired('target', @ischar);
            p.addParameter('emptyNamespace', 'empty', @ischar)
            p.addParameter('errTarget', '', @ischar)
            p.parse(fitFields, target, varargin{:});
            args = p.Results;
            
            o.fitFields = args.fitFields;
            o.target = args.target;
            o.emptyNamespace = args.emptyNamespace;
            o.errTarget = args.errTarget;
            
            sources = cell(2*length(o.fitFields), 1);
            for index=1:length(o.fitFields)
                sources{2*index - 1} = o.fitFields{index};
                sources{2*index} = [o.emptyNamespace,'_',o.fitFields{index}];
            end
            o.set_source_fields(sources);
        end
        
        function result = analyze(o, model, ~, ~)
            empty = DataNamespace(model, o.emptyNamespace);
            
            dn = zeros(size(o.fitFields));
            dnErr = zeros(size(o.fitFields));
            for index=1:length(o.fitFields)
                field = o.fitFields{index};
                dn(index) = model.get(field).x0 - empty.get(field).x0;
                dnErr(index) = sqrt(model.get(field).dx0^2 + empty.get(field).dx0^2);
            end
            
            weights = 1 ./ dnErr.^2;
            dnMean = sum(dn .* weights) ./ sum(weights); % mean(dn);
            model.set(o.target, dnMean);
            
            if ~isempty(o.errTarget)
                n = numel(o.fitFields);
                if n > 1
                    sigmaSquared = sum((dn - dnMean).^2) / (n-1);
                    sumWeights = sum(weights) / n;
                    sumSquaredWeights = sum(weights.^2) / n;
                    dnUncertainty = sqrt(sigmaSquared * sumSquaredWeights / n / sumWeights.^2); % std(dn);
                else
                    dnUncertainty = dnErr;
                end
                model.set(o.errTarget, dnUncertainty);
            end

            result = Result.SUCCESS;
        end
    end
    
end

