classdef LorFit < analyze.Fit
    %Fits data in a DataModel to a lorentzian.
    
    methods
        function o = LorFit(xField, yField, target, varargin)
            %Builds a new LorFit analyzer
            %
            %Usage:
            %    o = analyze.LorFit(xField,yField,target,[fitOptions],)
            %    o = analyze.LorFit(...,fitOptions)
            %Parameters:
            %   xField - Name of DataModel field holding x data; or, set to
            %     '', in which case x data will be extracted from y
            %     Waveform.
            %   yField - Name of DataModel field holding y data
            %   fitOptions - Cell array of options to pass to nlfit
            %   range - Data range to use in fit
            
            o = o@analyze.Fit(xField, yField, target,...
                @(y0, A, G, x0, x) analyze.LorFit.lorModel(y0, A, G, x0, x), @analyze.LorFit.lorGuess,...
                varargin{:});
        end
    end
    
    methods (Static) 
        function result = lorModel(y0, A, G, x0, x)
            result = y0+A*G/2./sqrt((x-x0).^2 + G^2/4);
        end
        
        function guess = lorGuess(x,y)
            high = max(y);
            low = min(y);
            if high > 10 && low < 1
                low = 1;
            end
            
            guess = [min(y), high-low, (max(x)-min(x))/4, mean(x(y==max(y))) ];
        end        
    end
end

