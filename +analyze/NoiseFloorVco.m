classdef NoiseFloorVco < Analyzer
    % Applies arbitrary functions to properties of a Data Model
    
    properties (SetAccess = protected)
        source
        target
        
        range
        lp
        scaleFun
    end
    
    methods
        
        function o = NoiseFloorVco(source, target, varargin)
            % Constructs a NoiseFloorVco Analyzer
            
            p = inputParser();
            
            p.addRequired('source', @ischar);
            p.addRequired('target', @ischar);
            p.addOptional('range', [], @isnumeric);
            p.addOptional('lp', nan, @(x) isscalar(x) && isnumeric(x));
            p.addParameter('scaleFun', @(x) x, @(x) isa(x, 'function_handle'));
            
            p.parse(source, target, varargin{:});
            args = p.Results;
            
            o.source = args.source;
            o.target = args.target;
            o.range = args.range;
            o.lp = args.lp;
            o.scaleFun = args.scaleFun;
            
            o.set_source_fields({o.source});
            
        end
        
        function r = analyze(o, model, ~, ~)
            % Calculates the noise floor on the vco signal, given the supplied
            % scaling function
            
            vco = model.get(o.source);
            if ~isnan(o.lp)
                vco = vco.decimate(floor(1/(o.lp*vco.dx)));
            end
            if ~isempty(o.range)
                vco = vco.slice_x(o.range);
            end
            
            vco = o.scaleFun(vco);
            
            noiseDist = fitdist(vco.data, 'Normal');
            noiseFloor = noiseDist.('sigma');
        
            model.set(o.target, noiseFloor);
            
            r = Result.SUCCESS;
            
        end
        
    end
    
end
