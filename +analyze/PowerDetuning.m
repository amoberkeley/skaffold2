classdef PowerDetuning < Analyzer

    properties (SetAccess = protected)
        source     % Name of the fields holding the demodulated sideband power
        target     % Names of target fields for demodulated data
        steps       % Time steps in which to calculate mean
        fMod        % Modulation frequency
        kappa
        rbGain
        close       % true if the lock point is within the sidebands, false otherwise
    end
    
    methods
        function o = PowerDetuning(varargin)
            % Creates a new PowerDetuning analyzer
            %
            % Usage:
            %   obj = analze.PowerDetuning(sources, target, 'key', value,...)
            %
            % Parameters:
            %   sources - {'raw','qQuad'}
            %       Cell array with names of DataModel fields for the
            %       demodulated sideband power
            %   targets - 'deltaPcWf'
            %       Destination fields for deltaPc waveform, and optionally 
            %       cavity half-linewidth waveform, respectively
            %
            % Keys:
            %   steps - {'target', [tStart, tStop],...}
            %       Time-steps over which to compute average detuning. 
            %       Specified as cell array of DataModel target fields
            %       followed by corresponding time window.
            %   fMod - double
            %       Sideband modulation frequency          
            %   kappa - double
            %       Broadened cavity half-linewidth, accounting for birefringence (cyclic)
            %   rbGain - double
            %       Relative (power) detection efficiency for red vs. blue sideband
            %   close - true if the lock point is within the sidebands,
            %               false otherwise
            
            p = inputParser();
            p.addRequired('source', @iscellstr);
            p.addRequired('target', @ischar);
            p.addParameter('steps', {}, @(x) isstruct(x) || iscell(x));
            p.addParameter('fMod', NaN, @isscalar)      
            p.addParameter('kappa', NaN, @(x) x > 0);
            p.addParameter('rbGain', NaN, @(x) x > 0);
            p.addParameter('close', true, @islogical);
            p.parse(varargin{:});
            args = p.Results;
            
            o.source = args.source;
            o.target = args.target;
            
            if ~isstruct(args.steps)
                o.steps = struct(args.steps{:});
            end

            % Check that step definitions are valid
            stepTargets = fieldnames(o.steps);
            for i=1:numel(stepTargets)
                stepRange = o.steps.(stepTargets{i});
                assert(isnumeric(stepRange) && numel(stepRange)==2, 'SidelockPower:InvalidStepRange', 'Invalid step range ''%s''', stepTargets{i});
            end            
            
            o.fMod = args.fMod;
            o.kappa = args.kappa;
            o.rbGain = args.rbGain;
            o.close = args.close;
            
            o.set_source_fields(o.source);
        end
        
        % Implements abstract analyze()
        function result = analyze(o, model, ~, ~)
            redWf = model.get(o.source{1});
            blueWf = o.rbGain*model.get(o.source{2});

            % Compute sideband imbalance
            r = (blueWf-redWf)./(blueWf+redWf);
            if o.close
                deltaPcWf = real(-o.fMod + sqrt(o.fMod^2 - r.^2*(o.fMod^2 + o.kappa^2)))./r;
            else
                deltaPcWf = real(-o.fMod - sqrt(o.fMod^2 - r.^2*(o.fMod^2 + o.kappa^2)))./r;
            end          
                
            model.set(o.target, deltaPcWf);

            % Calculate averages within step ranges. 
            stepTargets = fieldnames(o.steps);
            for i=1:numel(stepTargets)
                stepRange = o.steps.(stepTargets{i});

                powerRed = redWf.slice_x(stepRange).mean().data;
                powerBlue = blueWf.slice_x(stepRange).mean().data;

                % Compute sideband imbalance
                r = (powerBlue-powerRed)./(powerBlue+powerRed);
                if o.close
                    deltaPc = (-o.fMod + sqrt(o.fMod^2 - r.^2*(o.fMod^2 + o.kappa^2)))./r;
                else
                    deltaPc = (-o.fMod - sqrt(o.fMod^2 - r.^2*(o.fMod^2 + o.kappa^2)))./r;
                end          
               
                model.set(stepTargets{i}, real(deltaPc));
            end   
            
            result = Result.SUCCESS;
        end
    end
end

