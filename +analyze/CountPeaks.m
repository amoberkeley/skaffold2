classdef CountPeaks < Analyzer
    % Converts voltage from heterodyne detector to heterodyne watts
    
    properties (SetAccess = protected)
        sources           % Fields to rescale
        targets           % Fields to store results in, if not sources

        bw               % bandwidth with which to bin counts
        threshold        % threshold for something to look like a peak
    end
    
    methods
        function o = CountPeaks(source, target, bw, threshold)
            % Constructs a new CountPeaks analyzer
            %
            %Usage:
            %  o = analyze.CountPeaks(source, target, bw)
            %
            %Parameters:
            %  source - DataModel source fields containing signals in volts
            %  target - Destination fields for result
            %  bw     - Bondwidth with which to bin counts

            p = inputParser;
            p.addRequired('source', @(s) ischar(s) || iscellstr(s));
            p.addRequired('target', @(s) ischar(s) || iscellstr(s));
            p.addRequired('bw', @isreal);
            p.addRequired('threshold', @isreal);
            p.parse(source, target, bw, threshold);
            args = p.Results;
            
            if iscell(args.source)
                o.sources = args.source;
            else
                o.sources = {args.source};
            end
           
            if iscell(args.target)
                o.targets = args.target;
            else
                o.targets = {args.target};
            end
            
            o.bw = args.bw;
            o.threshold = args.threshold;

            o.set_source_fields(o.sources);
        end
        
        function result = analyze(o, model, ~, ~)
            for i_source = 1:length(o.sources)
                wf = model.get(o.sources{i_source});
                
                t = wf.get_x();
                f_sample = 1 / wf.dx;
                
                n_dec = ceil(f_sample / o.bw);
                
                t_filt = t(1:n_dec:end);
                data_filt = zeros(size(t_filt));
                
                is_high = 0;
                for i = 1:(numel(t_filt)-1)
                    bin_count = 0;
                    for j = 1:n_dec
                        if wf.data((i-1)*n_dec + j) > o.threshold
                            if 0 == is_high
                                is_high = 1;
                            end
                        else
                            if 1 == is_high
                                bin_count = bin_count + 1;
                                is_high = 0;
                            end
                        end
                    end
                    
                    data_filt(i + 1) = bin_count;
                end
                
                model.set(o.targets{i_source}, Waveform(wf.x0, wf.dx*n_dec, data_filt'));
            end
            
            result = Result.SUCCESS;
        end
    end
    
end

