classdef PulsedOptimalFilter < Analyzer
   
    properties (SetAccess = protected)
        source       % source field
        target
        
        range
        tRep
        nRep
        
        nPoints
        
        phase
        modelF
        modelG
        modelX
        modelDt
        modelSin
        modelCos
    end
    
    methods
        function o = PulsedOptimalFilter(source,target,varargin)
            p = inputParser;
            p.addRequired('source', @(x) ischar(x) && ~isempty(x) );
            p.addRequired('target', @(x) ischar(x) && ~isempty(x) );

            p.addParameter('range',[], @(x) isnumeric(x) && (numel(x)==0 || numel(x)==2));
            p.addParameter('nRep', 1, @(x) isscalar(x) && x >= 1);
            p.addParameter('tRep', 0, @(x) isscalar(x) && x >= 0);         
            
            p.addParameter('phase', 3*pi/2, @(x) isscalar(x) && isreal(x));
            p.addParameter('modelF', [], @(x) ~isempty(x) && all(x>0));
            p.addParameter('modelG', [], @(x) ~isempty(x) && all(x>0));
            
            p.parse(source,target,varargin{:})
            v = p.Results;
            
            o.source = v.source;
            o.target = v.target;
           
            o.range = v.range;
            o.tRep = v.tRep;
            o.nRep = v.nRep;
            
            o.phase = v.phase;
            o.modelF = v.modelF;
            o.modelG = v.modelG;            
            o.modelDt = 0;
            o.nPoints = min(length(o.modelF), length(o.modelG));           
                        
            if o.nRep > 1 && o.tRep == 0
                error('skaffold:FFTSlice', 'tRep must be > 0 if nRep > 1.');
            end

            o.set_source_fields({o.source});
        end
        
        function build_model_functions(o, dt)
            o.modelDt = dt;
            iStart = round(o.range(1)/o.modelDt) + 1;
            iStop = round(o.range(2)/o.modelDt) + 1;
            iLength = iStop - iStart;
            o.modelX = (0:iLength) * dt;
            
            for point = 1:o.nPoints
                o.modelSin(:,point) = 1*sin(2*pi*o.modelF(point)*o.modelX + o.phase).*exp(-pi*o.modelX*o.modelG(point));
                o.modelCos(:,point) = 1*cos(2*pi*o.modelF(point)*o.modelX + o.phase).*exp(-pi*o.modelX*o.modelG(point));
            end            
        end
        
        function result = analyze(o, model, ~, point)
            result = Result.SUCCESS;

            if point > o.nPoints
                error('skaffold:PulseOptimalFilter', 'Analyzer must be configured with model parameters for each point.');
            end
            
            data = model.get(o.source);
            
            if o.modelDt ~= data.dx
                o.build_model_functions(data.dx);
            end
            
            response = zeros(1,o.nRep,'like',complex(0));
            for iRep = 0:o.nRep-1
                slice = data.slice_x(o.range(1) + iRep * o.tRep, o.range(2) + iRep * o.tRep);
                q1 = sum(slice.data .* o.modelSin(:,point));
                q2 = sum(slice.data .* o.modelCos(:,point));
                response(iRep+1) = complex(q1, q2);
            end               
            
            model.set(o.target, response);
        end
    end   
end

