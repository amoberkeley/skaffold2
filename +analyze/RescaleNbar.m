classdef RescaleNbar < Analyzer
    % Calculates the mean photon number from the heterodyne carrier
    % magnitude
    
    properties (SetAccess = protected)
        source
        psd
        targets
        steps
        
        bw
        range
        
        fCar        % Carrier beat frequency        
        snRange % Range over which to evaluate shot noise
        edet    % Heterodyne detection efficiency
        kappaN  % Natural cavity linewidth (cyclic)        
        
        debug   %If 'all', prints debug messages
    end
    
    methods
        function o = RescaleNbar(source,targets,varargin)
            %Constructs a RescaleNbar analyzer
            %
            %Usage:
            %  o = analyze.RescaleNbar(source,targets,step,'key',value,...)
            %
            %Parameters:
            %  source - DataModel source field containing raw heterodyne
            %           signal FFT
            %  targets - Destination fields for deltaPc, nBar, and SN,
            %            respectively
            %  step - Time step interval in which to calculate results
            %
            %Keys:
            %  kappaN     - Natural cavity linewidth (cyclic units)
            %  snRange    - 2-element array indicating range of detuning
            %               range over which to evaluate shot noise
            %  eDet       - Heterodyne detection efficiency            
            %  debug     - 'none' | 'all'
            %                If 'all', prints update messages to screen

            p = inputParser;
            p.addRequired('source', @ischar);
            p.addRequired('psd', @ischar);
            p.addRequired('targets', @(x) ~isempty(x) && (ischar(x) || iscellstr(x)));
            
            p.addParameter('fCar', NaN, @isscalar)
            p.addParameter('snRange', [], @(x) isnumeric(x) && numel(x)==2);
            p.addParameter('range', [], @(x) isnumeric(x) && numel(x)==0 || numel(x)==2);
            p.addParameter('steps', {}, @(x) isstruct(x) || iscell(x));

            p.addParameter('bw', NaN, @isscalar);
            p.addParameter('kappaN', NaN, @(x) x > 0);
            p.addParameter('edet', NaN, @(x) x > 0);
            p.addParameter('debug', false, @islogical);
            p.parse(source,targets,varargin{:});
            args = p.Results;
            
            o.source = args.source;
            o.psd = args.psd;
            if ~iscellstr(args.targets)
                o.targets = {args.targets};
            else
                o.targets = args.targets;
            end
                        
            o.range = args.range;
            
            if ~isstruct(args.steps)
                o.steps = struct(args.steps{:});
            end

            % Check that step definitions are valid
            stepTargets = fieldnames(o.steps);
            for i=1:numel(stepTargets)
                stepRange = o.steps.(stepTargets{i});
                assert(isnumeric(stepRange) && numel(stepRange)==2, 'SidelockPower:InvalidStepRange', 'Invalid step range ''%s''', stepTargets{i});
            end                       
            
            o.bw = args.bw;
            o.fCar = args.fCar;
            o.kappaN = args.kappaN;
            o.snRange = args.snRange;
            o.edet = args.edet;
            
            o.debug = args.debug;
            
            o.set_source_fields({o.source});
        end
        
        function result = analyze(o, model, ~, ~)
            wf = model.get(o.source);
            
            % Slice, if requested
            if ~isempty(o.range)
                wf = wf.slice_x(o.range);
            end            
            
            noiseSpec = model.get(o.psd);
            
            Ssn = noiseSpec.slice_x(o.fCar + o.snRange).mean(1).data/2;
            Ssn = Ssn + noiseSpec.slice_x(o.fCar - o.snRange).mean(1).data/2;
          
            nBarWf = (wf - o.bw*Ssn)/(o.edet*Ssn*8*pi*o.kappaN);           
            model.set(o.targets{1}, nBarWf);

            % Calculate averages within step ranges. 
            stepTargets = fieldnames(o.steps);
            for i=1:numel(stepTargets)
                stepRange = o.steps.(stepTargets{i});

                nBar = nBarWf.slice_x(stepRange).mean().data;
                model.set(stepTargets{i}, nBar);
            end               
            
            result = Result.SUCCESS;
        end
    end
end

