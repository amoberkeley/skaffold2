classdef ATanFit < analyze.Fit
    %Fits data in a DataModel to an arctangent.
    
    methods
        function o = ATanFit(xField, yField, target, sign, varargin)
            %Builds a new ATanFit analyzer
            %
            %Usage:
            %    o = analyze.ATanFit(xField,yField,target,[fitOptions],)
            %    o = analyze.ATanFit(...,fitOptions)
            %Parameters:
            %   xField - Name of DataModel field holding x data; or, set to
            %     '', in which case x data will be extracted from y
            %     Waveform.
            %   yField - Name of DataModel field holding y data
            %   target - Name of DataModel field in which to store results.
            %   sign - Sign of the arctangent: +1 for rising, -1 for
            %     falling.
            %   fitOptions - Cell array of options to pass to nlfit.
            %   range - Data range to use in fit.
            
            o = o@analyze.Fit(xField, yField, target,...
                @(y0, y1, A, G, x0, x) analyze.ATanFit.atanModel(y0, y1, A, G, x0, x), ...
                @(x, y) analyze.ATanFit.atanGuess(x, y, sign),...
                varargin{:});
        end
    end
    
    methods (Static) 
        function result = atanModel(y0, y1, A, G, x0, x)
            result = y0 + y1*x + A * G/2 * atan(2 * (x - x0) / G);
        end
        
        function guess = atanGuess(x, y, sign)
            high = max(y);
            low = min(y);
            if high > 10 && low < 1
                low = 1;
            end
            
            if mod(numel(y), 2) ~= 0
                medy = median(y);
            else
                medy = median(y(1:end-1));
            end
            
            G = (max(x)-min(x))/8;
            
            guess = [(high+low)/2, pi/10, 2*sign*(high-low)/(pi*G), G, median(x(y==medy))];
        end        
    end
end

