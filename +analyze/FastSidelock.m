classdef FastSidelock < Analyzer
    % Calculates the probe detuning and mean photon number in small
    % timesteps
    
    properties (SetAccess = protected)
        source
        targets
         
        step
        window
        windowFunction
        sidebandBW
        carrierBW
        range
        
        fCar    % Carrier frequency
        fMod    % Modulation frequency
        kappa   % Broadened cavity linewidth (cyclic)
        kappaN  % Natural cavity linewidth (cyclic)        
        rbGain  % Relative detection gain of red vs. blue sideband
        snRange % Range over which to evaluate shot noise
        edet    % Heterodyne detection efficiency
        close   % true if the lock point is within the sidebands, false otherwise
        
        debug   %If 'all', prints debug messages
    end
    
    methods
        function o = FastSidelock(source,targets,varargin)
            %Constructs a FastSidelock analyzer
            %
            %Usage:
            %  o = analyze.FastSidelock(source,targets,step,'key',value,...)
            %
            %Parameters:
            %  source - DataModel source field containing raw heterodyne
            %           signal FFT
            %  targets - Destination fields for deltaPc, nBar, and SN,
            %            respectively
            %  step - Time step interval in which to calculate results
            %
            %Keys:
            %  fCar       - Carrier frequency
            %  fMod       - Modulation frequency
            %  kappa      - Broadened cavity linewidth (cyclic)
            %  kappaN     - Natural cavity linewidth (cyclic)
            %  rbGain     - Relative detection gain of red vs. blue sideband
            %  snRange    - 2-element array indicating range of detuning
            %               range over which to evaluate shot noise
            %  eDet       - Heterodyne detection efficiency            
            %  close      - true if the lock point is within the sidebands,
            %               false otherwise
            %  update     - 'none' | 'all'
            %                If 'all', prints update messages to screen
            %  sbWindow - scalar (20e3)
            %     Width of frequency band in which to sum sideband
            %     power
            %  carWindow - scalar (20e3)
            %     Width of frequency band in which to sum carrier
            %     power            

            p = inputParser;
            p.addRequired('source', @ischar);
            p.addRequired('targets', @(x) ~isempty(x) && (ischar(x) || iscellstr(x)));
            p.addRequired('step',@(x) isscalar(x) && x > 0);
            
            p.addParameter('window',NaN,@isscalar);
            p.addParameter('windowFunction',@hamming,@(x) isa(x, 'function_handle'));          

            p.addParameter('range',[],@(x) isnumeric(x) && numel(x)==0 || numel(x)==2);            
            p.addParameter('carrierBW', 5e3, @(x) x > 0);
            p.addParameter('sidebandBW', 100e3, @(x) x > 0);
            
            p.addParameter('fCar', NaN, @(x) x > 0);
            p.addParameter('fMod', NaN, @(x) x > 0);
            p.addParameter('kappa', NaN, @(x) x > 0);
            p.addParameter('kappaN', NaN, @(x) x > 0);
            p.addParameter('rbGain', NaN, @(x) x > 0);
            p.addParameter('snRange', [], @(x) isnumeric(x) && numel(x)==2);
            p.addParameter('edet', NaN, @(x) x > 0);
            p.addParameter('close', true, @islogical);
            p.addParameter('debug', false, @islogical);
            p.parse(source,targets,varargin{:});
            args = p.Results;
            
            o.source = args.source;
            if ~iscellstr(args.targets)
                o.targets = {args.targets};
            else
                o.targets = args.targets;
            end
            
            o.step = args.step;
            if ~isnan(args.window)
                o.window = args.window;
            else
                o.window = 4*o.step;
            end
            o.windowFunction = args.windowFunction;
            o.sidebandBW = args.sidebandBW;
            o.carrierBW = args.carrierBW;
            o.range = args.range;
            
            o.fCar = args.fCar;
            o.fMod = args.fMod;
            o.kappa = args.kappa;
            o.kappaN = args.kappaN;
            o.rbGain = args.rbGain;
            o.snRange = args.snRange;
            o.edet = args.edet;
            o.close = args.close;
            
            o.debug = args.debug;
            
            o.set_source_fields({o.source});
        end
        
        function result = analyze(o, model, ~, ~)
            data = model.get(o.source);
            
            % Slice, if requested
            if ~isempty(o.range)
                data = data.slice_x(o.range); 
            end            
            
            [dataSpec, tSpec] = data.spectrogram(o.step, 'powerDensity', o.window, o.windowFunction);
            t0 = min(tSpec);
            dt = mean(diff(tSpec));
            
            if ~isempty(o.targets{1})
                powerBlue = o.rbGain * dataSpec.slice_x(o.fCar + o.fMod + o.sidebandBW*[-1/2, 1/2]).sum(1).data;
                powerRed = dataSpec.slice_x(o.fCar - o.fMod + o.sidebandBW*[-1/2, 1/2]).sum(1).data;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
                % Compute sideband imbalance
                r = (powerBlue-powerRed)./(powerBlue+powerRed);
                if o.close
                    deltaPc = (-o.fMod + sqrt(o.fMod^2 - r.^2*(o.fMod^2 + o.kappa^2)))./r;
                else
                    deltaPc = (-o.fMod - sqrt(o.fMod^2 - r.^2*(o.fMod^2 + o.kappa^2)))./r;
                end          
%                deltaPc(imag(deltaPc) ~= 0) = nan; % Suppress imaginary points
                deltaPc = real(deltaPc);
                
                model.set(o.targets{1}, Waveform(t0, dt, deltaPc'));               
            end

            if length(o.targets) > 1
                % Calculate SN power time points
                powerSN = dataSpec.slice_x(o.fCar + o.snRange).mean(1).data/2;
                powerSN = powerSN + dataSpec.slice_x(o.fCar - o.snRange).mean(1).data/2;

                % Calculate carrier power time points
                powerCarrier = dataSpec.slice_x(o.fCar + o.carrierBW*[-1/2, 1/2]);
                nWindow = powerCarrier.length();
                powerCarrier = powerCarrier.sum(1).data;
                
                % Convert to photon number
                nbar = (powerCarrier./powerSN - nWindow)*dataSpec.dx/8/pi/o.kappaN/o.edet;

                model.set(o.targets{2}, Waveform(t0, dt, nbar'));
                if length(o.targets) > 2
                    model.set(o.targets{3}, Waveform(t0, dt, powerSN'));
                end
            end

            result = Result.SUCCESS;
        end
    end
end

