classdef Namespace < Analyzer
    % Runs analyzer on DataModel namespace
    %
    
    properties (SetAccess = protected)
        namespace
        analyzer
    end
    
    methods
        function o = Namespace(namespace, analyzer)
            %Constructs a new analyze.Namespace.
            %
            %Usage:
            %
            %    obj = analyze.Namespace(namespace, analyzer)
            %
            %  Creates a Namespace analyzer to run the given analyzer on a
            %  namespace of the DataModel
            %
            %Params:
            %  namespace - Namespace prefix
            %  analyzer - Analyzer to run on namespace
            %
            
            assert(ischar(namespace),'skaffold:ArgumentError',...
                'Input ''namespace'' to analyze.Namespace() must be a string');
            assert(isa(analyzer,'Analyzer') || isa(analyzer,'Reporter'),'skaffold:TypeError',...
                'Input ''analyzer'' to analyze.Namespace() must be an Analyzer or Reporter.');
            
            o.namespace = namespace;
            o.analyzer = analyzer;
                                    
            sig = class(o);
            if ~isempty(o.namespace)
                sig = [sig,'_',o.namespace];
            end
            o.set_signature(sig);
        end
        
        function result = run(o,model,varargin)
            p = inputParser;
            p.addOptional('loop',0,@(x) isnumeric(x) && isscalar(x));
            p.addOptional('point',0,@(x) isnumeric(x) && isscalar(x));
            p.addOptional('frame',[],@(x) isempty(x) || isa(x, 'DataModel'));
            p.addParameter('force',false,@islogical);
            p.addParameter('skip',false,@islogical);            
            p.parse(varargin{:});
            args = p.Results;            
            
            % If this Namespace analyzer's signature has changed, force its
            % analyzer to run, regardless of its signature.  This keeps things
            % consistent if the namespace is changed.
            sig = model.pop_signature();
            force = args.force || o.reanalyze(model, sig);

            subspace = DataNamespace(model, o.namespace);
            result = o.analyzer.run(subspace, args.loop, args.point, 'force', force, 'skip', args.skip);
        end
                
        function signatures = collect_signatures(o)
            signatures = {o.signature()};
            if isa(o.analyzer, 'Analyzer')
                signatures = [signatures, o.analyzer.collect_signatures()];
            end
        end
        
        function result = analyze(~,~,~,~)
            % Not used in Namespace analyzer
            error('skaffold:UsageError',...
                'analyze.Namespace does not support analyze()');   
        end        
    end
    
end

