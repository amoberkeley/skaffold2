function se = nanstderr(x, d)
%NANSTDERR Standard Error on the Mean, excluding NaNs
%    Calculates the standard error of the mean for values in x along
%    dimension d, excluding elements which are NaN.
%
%Usage:
%  se = nanstderr(x,[d])
%
%Parameters:
%  x - Vector or matrix of sample values
%  d - Dimension along which to calculate.  Defaults to first non-singleton
%     dimension
%
    if nargin < 2
        d = find(size(x) ~= 1, 1);    
    end
    se = nanstd(x, [], d)./sqrt(sum(~isnan(x), d));
end