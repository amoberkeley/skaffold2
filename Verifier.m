classdef Verifier < handle
    %Abstract base class for Verifier objects
    %
    %See also VerifyingAnalyzer.
    
    methods (Abstract)
        %Tests model, returning test state and message
        %
        %Should have form:
        %  [test,message] = obj.verify(model)
        %
        %Parameters:
        %  model - DataModel to verify
        %
        %Returns:
        %  test - True if test passed, false otherwise
        %  message - Message indicating nature of test failure
        [test,message] = verify(obj,model,loop,point)
    end
    
    methods
        
        function [valid,message] = verify_aggregate(o,model)
            error('%s.verify_aggregate is not implemented', class(o));
        end
        
        function [hHist] = plot_aggregate(o,ax,model)
            error('%s.plot_aggregate is not implemented', class(o));
        end
        
        function key = get_key(o)
            %Returns a string key for this object.
            %
            %Default implementation returns the object's class name.
            key = class(o);
        end
    end
    
end

