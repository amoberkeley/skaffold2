classdef DataScan < DataModel & matlab.mixin.CustomDisplay
    
    properties (Constant, Access=protected)
        TYPE_SCALAR = 1
        TYPE_COMPLEX = 2
        TYPE_VECTOR = 3
        TYPE_COMPLEX_VECTOR = 4
        TYPE_WAVEFORM = 5
        TYPE_COMPLEX_WAVEFORM = 6
        TYPE_STRING = 7
        TYPE_STRUCT = 8
        
        dataGroup = 'scan'
        analysisGroup = 'analysis'
        loaderGroup = 'load'
        signatureField = 'meta_signatures'
   
        scanRegistry = containers.Map
        
        DEBUG = 0
        timeout = 10
    end
    
    properties (SetAccess = protected)
        file
        readonly
        maxLoops        
        useSingle
        includeFields
        excludeFields
        metaFields = {'meta_signatures'}
        
        nLoops = 0
        nPoints = 1

        signatures
    end
    
    properties (Access = protected, Transient)
        fileId = H5ML.id
        dataGroupId = H5ML.id
        analysisGroupId = H5ML.id
        loaderGroupId = H5ML.id
        fieldAttrId = H5ML.id
        complexMemType = H5ML.id
        complexFileType = H5ML.id        

        scalarMemType
        scalarFileType        
        
        datasets
        types
        
        timer
    end
    
    methods
        function o = DataScan(file,varargin)
            p = inputParser;
            p.addRequired('file', @ischar);
            p.addOptional('nPoints', 1, @(x) isscalar(x) && x > 0);
            p.addParameter('readonly', false, @islogical);
            p.addParameter('maxLoops', 0, @(x) isscalar(x) && x >= 0);
            p.addParameter('include', {}, @iscellstr);
            p.addParameter('exclude', {}, @iscellstr);
            p.addParameter('precision', 'single', @(x) ismember(x, {'single','double'}));
            p.parse(file,varargin{:});
            args = p.Results;            
                       
            o.file = args.file;
            o.readonly = args.readonly;
            o.includeFields = args.include;
            o.excludeFields = args.exclude;
            o.nPoints = args.nPoints;
            o.nLoops = 0;
            
            if args.maxLoops > 0
                o.maxLoops = args.maxLoops;
            else
                o.maxLoops = H5ML.get_constant_value('H5S_UNLIMITED');
            end
            
            if strcmp(args.precision, 'single')
                o.useSingle = true;
                o.scalarFileType = 'H5T_IEEE_F32LE';
            else
                o.useSingle = false;
                o.scalarFileType = 'H5T_IEEE_F64LE';
            end            
            o.scalarMemType = 'H5T_NATIVE_DOUBLE';
            
            o.datasets = containers.Map;
            o.types = containers.Map;
            o.signatures = containers.Map;
            
            % Create / Load HDF5 file
            
            DataScan.register(o.file, o);
            
            if exist(o.file, 'file') ~= 2
                assert(~o.readonly, 'skaffold:ArgumentError',...
                    'HDF5 file must already exist, if ''readonly'' is set in DataScan.');
                
                path = fileparts(o.file);
                if exist(path, 'dir') ~= 7
                    mkdir(path);
                end
                
                o.create_file();
            else
                o.open_file();
            end
            
            o.load_file();
            
            cb = @(t,e) o.close_file();
            o.timer = timer('TimerFcn', cb,'StartDelay',o.timeout);
            start(o.timer);
            
            warning( 'off', 'MATLAB:lang:badlyScopedReturnValue' );
        end
        
        function flush(o)
            H5F.flush(o.fileId, 'H5F_SCOPE_GLOBAL');
        end
        
        function touch(o)
            if o.fileId.identifier == -1
                if o.DEBUG
                    fprintf('Reopening DataScan at ''%s''\n', o.file);
                end
                o.open_file();
                o.load_file();
            end
            stop(o.timer);
            start(o.timer);
        end
        
        function delete(o)
            if ~isempty(o.timer)
                stop(o.timer);
            end
            o.close_file();
        end         

        %% Frame I/O methods
        function write_frame(o,loop,point,model,key)
            o.touch();
            fields = model.fields();
            if ~isempty(o.includeFields)
                fields = intersect(fields, o.includeFields);
            end
            if ~isempty(o.excludeFields)
                fields = setdiff(fields, o.excludeFields);
            end

            if nargin > 4
                % Clear the signature field for this model while writing,
                % to approximate an atomic write operation. (Force complete
                % reanalysis if script is interrupted during writing)

                if ~o.has(o.signatureField)
                    o.init_dataset(o.signatureField, key);
                end
                o.write_field(loop, point, o.signatureField, {''});
            end             

            for index = 1:length(fields)
                % Update datasets with modified fields

                field = fields{index};
                data = model.get(field);
                if ~o.has(field)
                    o.init_dataset(field, data);
                end
                if model.is_changed(field)
                    try
                        o.write_field(loop, point, field, data);
                    catch me
                        switch me.identifier 
                            case 'MATLAB:imagesci:hdf5dataset:illegalArrayAccess'
                                % this error is raised when the size of the
                                % datatype has changed in the analysis.  Offer to
                                % delete and rebuild this dataset.

                                prompt = sprintf(['Datatype has changed for field ''%s'' in HDF5 DataScan.\n'...
                                    ' Do you want to delete and recreate the dataset for this field?'],field);
                                res = input(prompt, 's');
                                if ~isequal(res, 'y')
                                    error('Analysis canceled.');
                                end

                                fprintf('Rebuilding...\n');
                                o.unset(field);
                                o.init_dataset(field, data);
                                o.write_field(loop, point, field, data);
                        end
                    end                        
                end
            end
            
            if nargin > 4
                % Now that all updates are written, set analysis key to
                % reflect latest calculations
                
                o.write_field(loop, point, o.signatureField, {key});
            end
 
            model.clear_changed();
        end
        
        function model = read_frame(o,loop,point)
            o.touch();
            sigs = {};
            if o.has(o.signatureField)
                key = o.read_field(loop, point, o.signatureField);
                
                if o.signatures.isKey(key)
                    sigs = o.signatures(key);
                end
            end
            model = DataScanFrame(o, loop, point, 'signatures', sigs);            
        end
        
        function clear_frame(o,loop,point)
            o.touch();
            fields = o.datasets.keys();

            for index = 1:length(fields)
                % Update datasets with modified fields
                o.clear_field(loop, point, fields{index});
            end
        end        
        
        function test = has_frame(o,loop,point)
            test = loop <= o.nLoops && point <= o.nPoints;
        end
        
        %% Signature methods
        function key = register_signatures(o, sigs)
            o.touch();
            % Saves the set of Analyzer signatures in the DataScan, and
            % returns a key to be referenced by each individual DataFrame
            
            % Calculate hash of signatures, for use as dataset name for
            % this set of signatures
            key = md5(strjoin(sigs,''));
            
            if ~o.signatures.isKey(key)
                % If no match, create new dataset and save signatures.
                % Assume if there is a match, the signature list is already
                % stored in the HDF5 analysis group.                
                o.signatures(key) = sigs;

                % Create dataset and write signatures
                type_id = H5T.copy ('H5T_C_S1');
                H5T.set_size(type_id, 'H5T_VARIABLE');
                space_id = H5S.create_simple(1, length(sigs), length(sigs));
                dset_id = H5D.create(o.analysisGroupId, key ,type_id,space_id,'H5P_DEFAULT');
                H5D.write(dset_id, 'H5ML_DEFAULT', 'H5S_ALL', 'H5S_ALL', 'H5P_DEFAULT', sigs);
                H5D.close(dset_id);
            end
        end
        
        %% Loader Data methods
        function data = get_loader_data(o, field)
            o.touch();
            data = [];
                
            if ~H5L.exists(o.loaderGroupId, field,'H5P_DEFAULT')
                return;
            end
            
            dset = H5D.open(o.loaderGroupId, field);            
            data = H5D.read(dset, 'H5ML_DEFAULT', 'H5S_ALL', 'H5S_ALL', 'H5P_DEFAULT');
            H5D.close(dset);
        end
        
        function set_loader_data(o, field, data)
            o.touch();
            if H5L.exists(o.loaderGroupId, field,'H5P_DEFAULT')
                dset = H5D.open(o.loaderGroupId, field);

                H5D.set_extent(dset, length(data));
            else
                % Assume 1D vector of integers
                dcpl = H5P.create('H5P_DATASET_CREATE');             
                H5P.set_chunk(dcpl, 1000);
                space_id = H5S.create_simple(1, length(data), H5ML.get_constant_value('H5S_UNLIMITED'));              
                dset = H5D.create(o.loaderGroupId, field, 'H5T_STD_I32LE', space_id, 'H5P_DEFAULT', dcpl, 'H5P_DEFAULT');
            end
            
            H5D.write(dset, 'H5ML_DEFAULT', 'H5S_ALL', 'H5S_ALL', 'H5P_DEFAULT', int32(data));
            H5D.close(dset);
        end

        %% Field I/O methods

        function data = read_field(o,loop,point,field)
            o.touch();
            if ~o.datasets.isKey(field)
                error('skaffold:ArgumentError', 'Unrecognized field ''%s''.', field);
            end
                        
            dataset = o.datasets(field);
            mem_space_id = H5S.create_simple(1, 1, []);
            file_space_id = H5D.get_space(dataset);
            [~, dims, ~] = H5S.get_simple_extent_dims(file_space_id);
            dims = fliplr(dims);
            if any([loop, point] > dims)
                data = NaN;
                return;
            end
            
            coords = [loop, point]-1;
            H5S.select_elements(file_space_id, 'H5S_SELECT_SET', fliplr(coords));
            data = H5D.read(dataset, 'H5ML_DEFAULT', mem_space_id, file_space_id, 'H5P_DEFAULT');
            
            data = DataScan.unpack(o.types(field), data);
        end

        function write_field(o,loop,point,field,data)
            o.touch();                                    
            if ~o.datasets.isKey(field)
                error('skaffold:ArgumentError', 'Unrecognized field ''%s''.', field);
            end
            
            dataset = o.datasets(field);
            mem_space_id = H5S.create_simple(2, [1,1], []);
            file_space_id = H5D.get_space(dataset);
            [~, dims, ~] = H5S.get_simple_extent_dims(file_space_id);
            dims = fliplr(dims);
            if any([loop, point] > dims)
                loops = max(dims(1), loop);
                points = max(dims(2), point);
                o.nLoops = max(o.nLoops, loops);
                o.nPoints = max(o.nPoints, points);
                
                H5D.set_extent(dataset, fliplr([loops, points]));
                file_space_id = H5D.get_space(dataset); % Reload new file space.  Otherwise bad things happen...
            end
            coords = [loop, point]-1;
            H5S.select_elements(file_space_id, 'H5S_SELECT_SET', fliplr(coords));

            data = DataScan.pack(o.types(field), data, o.useSingle);    
            H5D.write(dataset, 'H5ML_DEFAULT', mem_space_id, file_space_id, 'H5P_DEFAULT', data);
        end
                     
        function clear_field(o,loop,point,field)
            o.touch();
            if ~o.datasets.isKey(field)
                error('skaffold:ArgumentError', 'Unrecognized field ''%s''.', field);
            end
                        
            dataset = o.datasets(field);
            mem_space_id = H5S.create_simple(1, 1, []);
            file_space_id = H5D.get_space(dataset);
            [~, dims, ~] = H5S.get_simple_extent_dims(file_space_id);
            dims = fliplr(dims);
            if any([loop, point] > dims)
                return;
            end
            
            coords = [loop, point]-1;
            H5S.select_elements(file_space_id, 'H5S_SELECT_SET', fliplr(coords));
            data = H5D.read(dataset, 'H5ML_DEFAULT', mem_space_id, file_space_id, 'H5P_DEFAULT');            
            data = DataScan.clear(o.types(field), data);
            H5D.write(dataset, 'H5ML_DEFAULT', mem_space_id, file_space_id, 'H5P_DEFAULT', data);
        end
        
        %% Aggregate access methods
        function [model, selection] = select(o,varargin)
            % Selects aggregated data field from DataScan, with optional
            % data ranges specified per field. Returns aggregate DataModel
            % containing requested fields, and logical array indicating 
            % selected iterations.
            % 
            % Usage:
            %   scan.select('field1','field2',[min2, max2],'field3', ...);
            %
            % Parameters:
            %   A variable number of field names, optionally followed by a
            %   range selection vector to apply to that field.  Data will
            %   be returned for fields which satisfy all given selection
            %   ranges.
            
            selection = true(o.nLoops, o.nPoints);
            selected = struct();
            index = 1;
            while index < nargin
                field = varargin{index};
                selected.(field) = o.get(field);
                
                index = index + 1;
                if nargin <= index || ischar(varargin{index})
                    continue;
                end
                
                range = varargin{index};                
                selection = selection & (selected.(field) > min(range) & selected.(field) < max(range));
                index = index + 1;
            end
            
            model = DataFrame();
            fieldnames = fields(selected);
            for index = 1:length(fieldnames)
                data = selected.(fieldnames{index});
                data(~selection) = NaN;
                model.set(fieldnames{index}, data);
            end
        end

    %% Abstract implementations:
        function data = get(o,field,loops,points,~)
            o.touch();
            
            if ~o.datasets.isKey(field)
                error('skaffold:ArgumentError', 'Unrecognized field ''%s''.', field);
            end

            dataset = o.datasets(field);
            file_space_id = H5D.get_space(dataset);
            [~, dims, ~] = H5S.get_simple_extent_dims(file_space_id);
            dims = fliplr(dims);
          
            if nargin > 2
                if nargin < 3 || isempty(loops) || (ischar(loops) && loops == ':')
                    loops = 1:dims(1);
                elseif ~isrow(loops)
                    % Ensure loops is a row vector, which combvec expects.
                    loops = reshape(loops,1,length(loops));
                end
                
                % Convert logical array to indices
                if islogical(loops)
                    loops = find(loops);
                end
                
                if nargin < 4 || isempty(points) || (ischar(points) && points == ':')
                    points = 1:dims(2);
                elseif ~isrow(points)
                    % Ensure points is a row vector, which combvec expects.
                    points = reshape(points,1,length(points));
                end
                
                mem_space_id = H5S.create_simple(2, fliplr([length(loops), length(points)]), []);
                
                coords = combvec(loops, points)' - 1;
                H5S.select_elements(file_space_id, 'H5S_SELECT_SET', fliplr(coords)');
                
                if H5S.select_valid(file_space_id) <= 0
                    error('skaffold:IndexError', 'Selection out of range ''%s''.', field);                    
                end
            
                data = H5D.read(dataset, 'H5ML_DEFAULT', mem_space_id, file_space_id, 'H5P_DEFAULT');
            else                
                data = H5D.read(dataset, 'H5ML_DEFAULT', 'H5S_ALL', 'H5S_ALL', 'H5P_DEFAULT');
            end            
            
            data = DataScan.unpack(o.types(field), data);
        end
        
        function set(o,field,values)
            o.touch();
            
            if ~o.has(field)
                o.init_dataset(field, values(1,1));
            end
            
            dataDims = size(values);
            
            dataset = o.datasets(field);
            file_space_id = H5D.get_space(dataset);
            [~, dims, ~] = H5S.get_simple_extent_dims(file_space_id);
            dims = fliplr(dims);
            if any(dataDims > [o.nLoops, o.nPoints])
                error('skaffold:UsageError',...
                    'Dimensions of new field data must match current dimesions of DataScan');   
            end           
            if any(dims < [o.nLoops, o.nPoints])               
                H5D.set_extent(dataset, fliplr([o.nLoops, o.nPoints]));
            end
            
            data = DataScan.pack(o.types(field), values, o.useSingle);            
            H5D.write(dataset, 'H5ML_DEFAULT', 'H5S_ALL', 'H5S_ALL', 'H5P_DEFAULT', data);             
        end
        
        function unset(o,field)
            % Remove dataset from HDF5 group.  The HDF5 library does not
            % currently support reclaiming this space, except using the
            % h5repack utility.
            
            o.touch();
            if o.datasets.isKey(field)
                H5D.close(o.datasets(field));
                o.datasets.remove(field);
                o.types.remove(field);
            end
            if H5L.exists(o.dataGroupId, field, 'H5P_DEFAULT')
                H5L.delete(o.dataGroupId, field, 'H5P_DEFAULT');
            end
        end
        
        function test = has(o,field)
            o.touch();
            test = o.datasets.isKey(field);
        end
        
        function test = is_excluded(o,field)
            test = ismember(field, o.excludeFields);
        end
        
        function fieldnames = fields(o)
            o.touch();
            fieldnames = setdiff(o.datasets.keys(), o.metaFields);
        end
    end
   
    methods (Access=protected)
        function create_file(o)
            fcpl = H5P.create('H5P_FILE_CREATE');
            fapl = H5P.create('H5P_FILE_ACCESS');
            o.fileId = H5F.create(o.file,'H5F_ACC_EXCL',fcpl,fapl);
        end
        
        function open_file(o)
            if o.readonly
                access = 'H5F_ACC_RDONLY';
            else
                access = 'H5F_ACC_RDWR';
            end
            o.fileId = H5F.open(o.file, access, 'H5P_DEFAULT');
        end
        
        function load_file(o)
            o.dataGroupId = DataScan.open_create_group(o.fileId, o.dataGroup);
            o.analysisGroupId = DataScan.open_create_group(o.fileId, o.analysisGroup);
            o.loaderGroupId = DataScan.open_create_group(o.fileId, o.loaderGroup);
            
            o.create_datatypes();
            o.open_datasets();
            o.read_signatures();            
        end
        
        function close_file(o)
            if o.DEBUG                
                fprintf('Closing DataScan at ''%s''\n', o.file);
            end
            o.close_datasets();
            
            if o.dataGroupId.identifier >= 0
                H5G.close(o.dataGroupId);
                o.dataGroupId = H5ML.id;
            end
            if o.analysisGroupId.identifier >= 0
                H5G.close(o.analysisGroupId);
                o.analysisGroupId = H5ML.id;
            end
            if o.loaderGroupId.identifier >= 0            
                H5G.close(o.loaderGroupId);
                o.loaderGroupId = H5ML.id;
            end
            if o.fileId.identifier >= 0
                H5F.close(o.fileId);
                o.fileId = H5ML.id;
            end
        end
        
        %% Signature methods
        function read_signatures(o)
            function [status,o] = iter_datasets(~, name, o)
                status = 0;
                
                % Read analyzer signatures from HDF5 analysis dataset    
                dset_id = H5D.open(o.analysisGroupId, name, 'H5P_DEFAULT');
                o.signatures(name) = H5D.read(dset_id, 'H5ML_DEFAULT', 'H5S_ALL', 'H5S_ALL', 'H5P_DEFAULT');
                H5D.close(dset_id);
            end

            H5L.iterate(o.analysisGroupId, 'H5_INDEX_NAME', 'H5_ITER_NATIVE', 0, @iter_datasets, o);            
        end

        %% Datatype Creation
        function create_datatypes(o)
            memType = H5T.copy(o.scalarMemType);
            memSize = H5T.get_size(memType);

            fileType = H5T.copy(o.scalarFileType);
            fileSize = H5T.get_size(fileType);

            % Complex datatype
            o.complexMemType = H5T.create('H5T_COMPOUND',2*memSize);
            H5T.insert(o.complexMemType,'real',0,memType);
            H5T.insert(o.complexMemType,'imag',memSize,memType);

            o.complexFileType = H5T.create('H5T_COMPOUND',2*fileSize);
            H5T.insert(o.complexFileType,'real',0,fileType);
            H5T.insert(o.complexFileType,'imag',fileSize,fileType);
        end
        
        function [memType, fileType] = create_vector_datatype(o,dims,complex)
            if ~complex
                memType = H5T.array_create (o.scalarMemType,length(dims),fliplr(dims),[]);            
                fileType = H5T.array_create (o.scalarFileType,length(dims),fliplr(dims),[]);
            else
                memType = H5T.array_create (o.complexMemType,length(dims),fliplr(dims),[]);            
                fileType = H5T.array_create (o.complexFileType,length(dims),fliplr(dims),[]);
            end
        end
        
        function [memType, fileType] = create_waveform_datatype(o, dims, complex)
            doubleMemType = H5T.copy(o.scalarMemType);
            doubleMemSize = H5T.get_size(doubleMemType);
            doubleFileType = H5T.copy(o.scalarFileType);
            doubleFileSize = H5T.get_size(doubleFileType);

            if ~complex
                arrayMemType = H5T.array_create (o.scalarMemType,length(dims),fliplr(dims),[]);
                arrayMemSize = H5T.get_size(arrayMemType);          
                arrayFileType = H5T.array_create (o.scalarFileType,length(dims),fliplr(dims),[]);
                arrayFileSize = H5T.get_size(arrayFileType);
            else
                arrayMemType = H5T.array_create (o.complexMemType,length(dims),fliplr(dims),[]);
                arrayMemSize = H5T.get_size(arrayMemType);
                arrayFileType = H5T.array_create (o.complexFileType,length(dims),fliplr(dims),[]);
                arrayFileSize = H5T.get_size(arrayFileType);
            end
            
            memType = H5T.create('H5T_COMPOUND',2*doubleMemSize+arrayMemSize);
            H5T.insert(memType,'x0',0,doubleMemType);
            H5T.insert(memType,'dx',doubleMemSize,doubleMemType);
            H5T.insert(memType,'data',2*doubleMemSize,arrayMemType);
            
            fileType = H5T.create('H5T_COMPOUND',2*doubleFileSize+arrayFileSize);
            H5T.insert(fileType,'x0',0,doubleFileType);
            H5T.insert(fileType,'dx',doubleFileSize,doubleFileType);
            H5T.insert(fileType,'data',2*doubleFileSize,arrayFileType);
            
            H5T.pack(fileType);
        end
        
        function [memType, fileType] = create_struct_datatype(o, struct)
            doubleMemType = H5T.copy(o.scalarMemType);
            doubleMemSize = H5T.get_size(doubleMemType);
            doubleFileType = H5T.copy(o.scalarFileType);
            doubleFileSize = H5T.get_size(doubleFileType);

            complexMemSize = H5T.get_size(o.complexMemType);
            complexFileSize = H5T.get_size(o.complexFileType);

            strMemType = H5T.copy ('H5T_C_S1');
            H5T.set_size(strMemType, 'H5T_VARIABLE');
            strMemSize = H5T.get_size(strMemType);
            
            fieldnames = fields(struct);
            memOffsets = zeros(size(fieldnames));
            fileOffsets = zeros(size(fieldnames));
            for i=1:length(fieldnames)
                data = struct.(fieldnames{i});
                if ischar(data)
                    memTypes(i) = strMemType;
                    memOffsets(i) = strMemSize;
                    fileTypes(i) = strMemType;
                    fileOffsets(i) = strMemSize;
                elseif ~isreal(data)
                    memTypes(i) = o.complexMemType;
                    memOffsets(i) = complexMemSize;
                    fileTypes(i) = o.complexFileType;
                    fileOffsets(i) = complexFileSize;
                else
                    memTypes(i) = doubleMemType;
                    memOffsets(i) = doubleMemSize;
                    fileTypes(i) = doubleFileType;
                    fileOffsets(i) = doubleFileSize;
                end
            end
            
            memType = H5T.create('H5T_COMPOUND', sum(memOffsets));
            fileType = H5T.create('H5T_COMPOUND', sum(fileOffsets));  
            
            memOffsets = [0; cumsum(memOffsets(1:end-1))];
            fileOffsets = [0; cumsum(fileOffsets(1:end-1))];               
            
            for i=1:length(fieldnames)
                H5T.insert(memType, fieldnames{i}, memOffsets(i), memTypes(i));
                H5T.insert(fileType, fieldnames{i}, fileOffsets(i), fileTypes(i));    
            end
        end
        
        %% Dataset creation
     
        function init_dataset(o, field, data)
            % Create datasets in HDF5 file to match fields in DataModel.
            % Detects field types, and creates dataset with appropriate
            % datatype to match
        
            if o.has(field), return; end; % Assume existing datasets have correct type
                
            if ischar(data)
                fileType = H5T.copy('H5T_C_S1');
                H5T.set_size(fileType, 'H5T_VARIABLE');            
                type = o.TYPE_STRING;           
            
            elseif isa(data, 'Waveform')
                complex = ~isreal(data.data);            
                if complex
                    type = o.TYPE_COMPLEX_WAVEFORM;                    
                else
                    type = o.TYPE_WAVEFORM;
                end
                [~, fileType] = o.create_waveform_datatype(size(data.data), complex);

            elseif isstruct(data)
                [~, fileType] = o.create_struct_datatype(data);
                type = o.TYPE_STRUCT;          
            
            elseif isnumeric(data) && isscalar(data)
                complex = ~isreal(data);                    
                if complex
                    fileType = o.complexFileType;
                    type = o.TYPE_COMPLEX;
                else
                    fileType = o.scalarFileType;
                    type = o.TYPE_SCALAR;
                end
            
            elseif isnumeric(data) && ismatrix(data)
                complex = ~isreal(data);
                if complex
                    type = o.TYPE_COMPLEX_VECTOR;
                else
                    type = o.TYPE_VECTOR;
                end
                [~, fileType] = o.create_vector_datatype(size(data), complex);
            
            else
                error('Skaffold:TypeError',...
                    ['Unknown data type conversion for field ''', field, ''' to HDF5']);
            end
            
            o.create_dataset(field, fileType, type);
        end        
        
        function create_dataset(o,name,fileType,type)
            % Creates dataset given HDF5 fileType, and DataScan field type,
            % which is saved as an attribute of the dataset.
            
            [dapl, dcpl] = DataScan.get_dataset_properties(type, fileType);
            
            dims = [max(1, o.nLoops), o.nPoints];
            maxdims = [o.maxLoops, o.nPoints];
            space_id = H5S.create_simple(2, fliplr(dims), fliplr(maxdims));              
            
            dset = H5D.create(o.dataGroupId, name, fileType, space_id, 'H5P_DEFAULT', dcpl, dapl);
            o.datasets(name) = dset;
            o.types(name) = type;            
            
            space_id = H5S.create_simple(1, 1, 1);           
            typeAttr = H5A.create(dset, 'type', 'H5T_STD_U16BE', space_id, 'H5P_DEFAULT', 'H5P_DEFAULT');
            H5A.write(typeAttr,'H5ML_DEFAULT',uint16(type));
        end
        
        function open_dataset(o, field)
            % Open dataset from HDF5 file.  First checks 'type' attribute
            % to determine the dataset access property list configuration
            
            % Load 'type' from dataset attribute
            try
                typeAttr = H5A.open_by_name(o.dataGroupId, field, 'type');
                type = H5A.read(typeAttr,'H5ML_DEFAULT');
            catch e
                warning('skaffold:DataWarning',...
                'HDF5 dataset ''%s'' does not have a type attribute.  Skipping dataset.', field);
                return
            end
            
            % Load copy of datatype from dataset
            dset = H5D.open(o.dataGroupId, field);
            fileType = H5D.get_type(dset);
            H5D.close(dset);
            
            % Get dataset access property list
            [dapl, ~] = DataScan.get_dataset_properties(type, fileType);
            
            % Open dataset
            dset = H5D.open(o.dataGroupId, field, dapl);
            o.datasets(field) = dset;
            o.types(field) = type;
            
            % Check dataset size, and adjust nLoops and nPoints to to be at
            % least as large
            file_space_id = H5D.get_space(dset);
            [~, dims, ~] = H5S.get_simple_extent_dims(file_space_id);
            dims = fliplr(dims);
            o.nLoops = max(o.nLoops, dims(1));
            
            if o.nPoints ~= dims(2)
                warning('skaffold:DataScan',...
                    'DataScan.nPoints = %d does not match dimensions of HDF5 database, with nPoints = %d. DataScan.nPoints will be set to the larger of the two.', ...
                    o.nPoints, dims(2));
                o.nPoints = max(o.nPoints, dims(2)); 
            end
        end
               
        function open_datasets(o)
            % Open all datasets in the data group of the HDF5 file.  Each
            % dataset corresponds to a DataModel field.
            
            function [status,o] = iter_datasets(~, name, o)
                % Iterates through datasets in group, opening them if:
                %   1) It is a metadata field, used by DataScan internally
                %   2) It is not a member of the 'exclude' list in the DataScan
                %   3) If not empty, it is a member of the 'include' list
                %      in the DataScan
                status = 0;
                if ~ismember(name, o.metaFields) && (ismember(name, o.excludeFields) ||...
                    ~(isempty(o.includeFields) || ismember(name, o.includeFields)))
                    return;
                end                
                o.open_dataset(name);
            end

            H5L.iterate(o.dataGroupId, 'H5_INDEX_NAME', 'H5_ITER_NATIVE', 0, @iter_datasets, o);
        end
        
        function close_datasets(o)
            % Close all datasets.  This happens automatically when the
            % DataScan object is deleted.
            
            fieldNames = o.datasets.keys();
            for i=1:length(fieldNames)
                name = fieldNames{i};
                dset = o.datasets(name);
                if dset.identifier >= 0
                    H5D.close(dset);
                    o.datasets(name) = H5ML.id;
                end
            end
        end
        
        function s = getHeader(o)
            name = matlab.mixin.CustomDisplay.getClassNameForHeader(o);
            rwLabel = {'R/W','RO'};
            precLabel = {'double','single'};
            s = [...
                sprintf('%s for file ''%s'' (%s):\n\n', name, o.file, rwLabel{o.readonly+1}),...
                sprintf('\tnLoops = %d x nPoints = %d, %s precision\n', o.nLoops, o.nPoints, precLabel{o.useSingle+1}),...
                ];
        end
        
        function propgrp = getPropertyGroups(o)          
            fields = o.fields();
            if ~isempty(fields)
                fieldDesc = struct();
                for idx=1:length(fields)
                    fieldDesc.(fields{idx}) = o.getFieldDesc(fields{idx});
                end
                propgrp(1) = matlab.mixin.util.PropertyGroup(fieldDesc,'Fields');
            else
                propgrp(1) = matlab.mixin.util.PropertyGroup({},'Fields');
            end
        end
        
        function desc = getFieldDesc(o, field)
            type = o.types(field);
            desc = 'Unknown';
            switch type
                case DataScan.TYPE_SCALAR
                    desc = 'Scalar';

                case DataScan.TYPE_VECTOR
                    desc = 'Vector';
                    
                case DataScan.TYPE_COMPLEX
                    desc = 'Complex Scalar';
                    
                case DataScan.TYPE_COMPLEX_VECTOR
                    desc = 'Complex Vector';
                    
                case DataScan.TYPE_WAVEFORM
                    desc = 'Waveform';
                    
                case DataScan.TYPE_COMPLEX_WAVEFORM
                    desc = 'Complex Waveform';                    
                    
                case DataScan.TYPE_STRING
                    desc = 'String';
                    
                case DataScan.TYPE_STRUCT
                    desc = 'Struct';
            end                
        end
    end    

    methods (Static)     
        function close_all()
            registry = DataScan.scanRegistry;
            openFiles = registry.keys();
            for i=1:length(openFiles)
                scan = registry(openFiles{i});
                delete(scan);
                registry.remove(openFiles{i});
            end
            
            H5.garbage_collect();            
        end
        
        function delete_scan(file)
            if exist(file, 'file') ~= 2
                return
            end
            
            res = input('Are you sure you want to clear the HDF5 storage (y/n)?', 's');
            if ~isequal(res, 'y')
                error('Aborting for lack of confirmation');
            end
            
            DataScan.close_all(); % Close all open DataScans, to make sure all resources are released
            H5.close(); % Close the HDF5 library to ensure all resources are released

            if exist(file, 'file') == 2
                delete(file);
                if exist(file, 'file') == 2
                    error('Failed to delete HDF5 file. Make sure all references are closed, and try again.');
                end
            end
            
            H5.open(); % reopen the library
        end        
    end
    
    methods (Static, Access = protected)
        function data = pack(type, data, useSingle)
            % Convert data from MATLAB native types to the structures the
            % HDF5 library expects.
            
            switch type
                case DataScan.TYPE_SCALAR
                    if useSingle
                        data = single(data);
                    end

                case DataScan.TYPE_VECTOR
                    if useSingle
                        data = single(data);
                    end
                    
                case DataScan.TYPE_COMPLEX
                    if useSingle
                        data = single(data);
                    end
                    data = struct('real', real(data), 'imag' ,imag(data));
                    
                case DataScan.TYPE_COMPLEX_VECTOR
                    if useSingle
                        data = single(data);
                    end                    
                    data = struct('real', real(data), 'imag', imag(data));
                    
                case DataScan.TYPE_WAVEFORM
                    if isa(data, 'Waveform')
                        data = struct(data);
                    end
                    if useSingle
                        data.x0 = single(data.x0);
                        data.dx = single(data.dx);
                        data.data = single(data.data);
                    end
                    
                case DataScan.TYPE_COMPLEX_WAVEFORM
                    if isa(data, 'Waveform')
                        data = struct(data);
                    end
                    if useSingle
                        data.x0 = single(data.x0);
                        data.dx = single(data.dx);
                        data.data = single(data.data);
                    end
                    data.data = struct('real', real(data.data),'imag', imag(data.data));
                    
                case DataScan.TYPE_STRING
                    if ~iscellstr(data)
                        data = {data};
                    end
                    
                case DataScan.TYPE_STRUCT
                    fieldnames = fields(data);
                    for i = 1:length(fieldnames)
                        if ischar(data(1).(fieldnames{i}))
                            data.(fieldnames{i}) = cellstr(data.(fieldnames{i}));
                        elseif useSingle && isnumeric(data(1).(fieldnames{i}))
                            data.(fieldnames{i}) = single([data.(fieldnames{i})]);
                        end
                    end
            end            
        end
        
        function data = unpack(type, data)
            switch type
                case DataScan.TYPE_SCALAR
                    data = double(data);
                
                case DataScan.TYPE_VECTOR
                    data = double(data);
                    
                case DataScan.TYPE_COMPLEX
                    data = double(complex(data.real, data.imag));
                    
                case DataScan.TYPE_COMPLEX_VECTOR
                    data = double(complex(data.real, data.imag));
                    
                case DataScan.TYPE_WAVEFORM
                    nPoints = size(data.x0, 2);
                    nd = ndims(data.data) - (nPoints > 1);
                    idx = repmat({':'}, 1, nd);

                    wfs(nPoints) = Waveform;
                    for index = 1:nPoints
                        %Assume different loops of the same point have the
                        % same x0 and dx values, and combine them into a
                        % single waveform, with the loops indexed in the
                        % highest dimension.
                        wfs(index) = Waveform(double(data.x0(1,index)), double(data.dx(1,index)), double(data.data(idx{:},index)));
                    end
                    data = wfs;                    
                    
                case DataScan.TYPE_COMPLEX_WAVEFORM
                    nPoints = size(data.x0, 2);
                    nd = ndims(data.data.real) - (nPoints > 1);
                    idx = repmat({':'}, 1, nd);

                    wfs(nPoints) = Waveform;
                    for index = 1:nPoints
                        %Assume different loops of the same point have the
                        % same x0 and dx values, and combine them into a
                        % single waveform, with the loops indexed in the
                        % highest dimension.
                        wfs(index) = Waveform(double(data.x0(1,index)), double(data.dx(1,index)), ...
                            double(complex(data.data.real(idx{:},index), data.data.imag(idx{:},index))));
                    end
                    data = wfs;                    
                    
                case DataScan.TYPE_STRING
                    if iscellstr(data) && length(data) == 1
                        data = data{1};
                    end               
                    
                case DataScan.TYPE_STRUCT
                    fieldnames = fields(data);
                    for i = 1:length(fieldnames)
                        if isnumeric(data.(fieldnames{i}))
                            data.(fieldnames{i}) = double(data.(fieldnames{i}));
                        end
                    end                    
            end            
        end
        
        function data = clear(type, data)
            switch type
                case DataScan.TYPE_SCALAR
                    data = data*0;

                case DataScan.TYPE_VECTOR
                    data = data*0;
                
                case DataScan.TYPE_COMPLEX
                    data.real = data.real*0;
                    data.imag = data.imag*0;
                    
                case DataScan.TYPE_COMPLEX_VECTOR
                    data = data*0;
                    
                case DataScan.TYPE_WAVEFORM
                    data.x0 = 0*data.x0;
                    data.dx = 0*data.dx;
                    data.data = 0*data.data;
                    
                case DataScan.TYPE_COMPLEX_WAVEFORM
                    data.x0 = 0*data.x0;
                    data.dx = 0*data.dx;
                    data.data.real = 0*data.data.real;
                    data.data.imag = 0*data.data.imag;
                    
                case DataScan.TYPE_STRING
                    data = {''};
                    
                case DataScan.TYPE_STRUCT
                    fieldnames = fields(data);
                    for i = 1:length(fieldnames)
                        if ischar(data.(fieldnames{i}))
                            data.(fieldnames{i}) = '';
                        elseif isnumeric(data.(fieldnames{i}))
                            data.(fieldnames{i}) = data.(fieldnames{i})*0;
                        end
                    end                    
            end            
        end
        
        function register(file, scan)
            % Create local copy of constant class property handle.  This is
            % necessary for matlab to be able to modify the object
            % referenced by the handle.
            registry = DataScan.scanRegistry;
            
            p = inputParser;
            p.addRequired('file', @ischar);
            p.addRequired('scan', @(x) isempty(x) || isa(x, 'DataScan'));
            p.parse(file, scan);
            args = p.Results;
            
            if exist(args.file, 'file')
                file = args.file;
            elseif ~isempty(which(args.file))
                file = which(args.file);
            else
                file = args.file;
            end
            
            openFiles = registry.keys();
            for i=1:length(openFiles)
                scan = registry(openFiles{i});
                if DataScan.DEBUG
                    fprintf('Closing previously opened DataScan at ''%s''\n', file);
                end
                delete(scan);
                registry.remove(openFiles{i});
            end
                        
            H5.garbage_collect();
            
            if ~isempty(args.scan)            
                registry(file) = args.scan;
            else
                registry.remove(file);
            end
        end

        function groupId = open_create_group(parent, name)
            if H5L.exists(parent, name,'H5P_DEFAULT')
                groupId = H5G.open(parent, name, 'H5P_DEFAULT');
            else
                groupId = H5G.create(parent, name, 'H5P_DEFAULT','H5P_DEFAULT','H5P_DEFAULT');
            end            
        end        
        
        function [dapl, dcpl] = get_dataset_properties(type, fileType)            
            % Tunes chunking and cacheing parameters based on dataset type.
            
            dcpl = H5P.create('H5P_DATASET_CREATE');            
            dapl = H5P.create('H5P_DATASET_ACCESS');
            
            maxCache = 20*1024*1024; % 20 MB limit
            
            dataSize = H5T.get_size(fileType);
            switch type
                case DataScan.TYPE_SCALAR
                    chunk_dims = [200, 1];                   
                case DataScan.TYPE_COMPLEX
                    chunk_dims = [200, 1];
                case DataScan.TYPE_VECTOR
                    chunk_dims = [10, 1];
                case DataScan.TYPE_COMPLEX_VECTOR
                    chunk_dims = [10, 1];
                case DataScan.TYPE_WAVEFORM
                    chunk_dims = [10, 1];
                case DataScan.TYPE_COMPLEX_WAVEFORM
                    chunk_dims = [10, 1];
                case DataScan.TYPE_STRING
                    chunk_dims = [100, 1];
                case DataScan.TYPE_STRUCT
                    chunk_dims = [100, 1];
                otherwise
                    chunk_dims = [100, 1];                 
            end
            
            ncache = 20;
            nslots = 2003; % prime value closest to 20 * 100
            nbytes = prod(chunk_dims) * dataSize * ncache;
            nbytes = min(nbytes, maxCache);
            H5P.set_chunk(dcpl, fliplr(chunk_dims));   
            H5P.set_chunk_cache(dapl, nslots, nbytes, 0.9)
        end        
    end    
end
