function out = nanmean(X, varargin)

if ~isempty(varargin)
    out = mean(X, varargin{:}, 'omitnan');
else
    out = mean(X, 1, 'omitnan');
end

end