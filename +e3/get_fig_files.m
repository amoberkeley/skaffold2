function files = get_fig_files(runname, name, ftype)
    init_skaffold;
    
    if nargin < 3
        ftype = {'png'};
    end
    
    if ~iscell(ftype)
        ftype = {ftype};
    end
    
    if nargin < 2
        name = runname;
        runname = [];
    end
    
    local_file = {};
    if ~isempty(runname)
        for i = 1:length(ftype)
            local_file{end+1} = sprintf('%s_%s.%s', runname, name, ftype{i});
        end
    end
    
    if exist('fig_files','var') && isfield(fig_files, name)
        files = [local_file, fig_files.(name)];
    else
        files = local_file;
    end
end