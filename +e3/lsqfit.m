function [b,varargout] = lsqfit(x,y,f,b0,fixed,lb,ub,err,conf,opt)
%  LSQFIT Nonlinear least-squares regression. Code derived from 'nlfit' script 
%     written by Matt Grau at https://github.com/mgrau/matlab/blob/master/nlfit.m
%
%     BETA = LSQFIT(X,Y,MODELFUN,BETA0) estimates the coefficients of a
%     nonlinear regression function, using least squares estimation.
%     
%     BETA = LSQFIT(X,Y,MODELFUN,BETA0,FIXED) estimates the coefficients of a
%     nonlinear regression function, holding BETA(i) fixed when FIXED(i) is 
%     nonzero.
%     
%     BETA = LSQFIT(X,Y,MODELFUN,BETA0,FIXED,LB,UB) estimates the coefficients of a
%     nonlinear regression function, with lower bound LB and uppper bound UB.
%
%     BETA = LSQFIT(X,Y,MODELFUN,BETA0,FIXED,LB,UB,ERR) estimates the coefficients of a
%     nonlinear regression function, with weights 1/ERR.
%  
%     [BETA,ERR] = LSQFIT(X,Y,MODELFUN,BETA0) returns the fitted
%     coefficients BETA, and 1-sigma errors ERR.
%     
%     [BETA,ERR,CI] = LSQFIT(X,Y,MODELFUN,BETA0) returns the fitted
%     coefficients BETA, 1-sigma errors ERR, and 0.95 confidence interval CI.
%     
%     [BETA,ERR,CI] = LSQFIT(X,Y,MODELFUN,BETA0,FIXED,LB,UB,ERR,ALPHA) returns the 
%     fitted coefficients BETA, 1-sigma errors ERR, and ALPHA confidence 
%     interval CI.
%

%% Default arguments.
if nargin<10
    opt = [];
end
if nargin<9
    conf = 0.95; % default to 0.95 confidence interval
end
if nargin<8
    err = [];
end
if nargin<7
    ub = [];
end
if nargin<6
    lb = [];
end
if nargin<5
    fixed = [];
end

if isempty(opt)
    % no options given, standard is to set Display to off and shutdown
    % warnings so that automatic fitting is facilitated
%     opt = optimset('Display','off');
    opt = optimset('Display','off','TolFun',1e-9);
    warning('off', 'all');
end
if isempty(err)
    err = 1+0*y;
end
if isempty(ub)
    ub = inf+0*b0; % default to +inf for upper bound
end
if isempty(lb)
    lb = -inf+0*b0; % default to -inf for lower bound
end
if isempty(fixed)
    fixed = 0*b0; % default to not fixing any parameters
end
if (size(fixed)~=size(b0)) | (size(lb)~=size(b0)) | (size(ub)~=size(b0))
    error('Parameters b0, fixed, lb and ub must have the same size!');
end
if any(lb(~fixed)==ub(~fixed))
    error('Lower Bound and Upper Bound cannot be the same!');
end
for i=1:length(lb)
    a = min(lb(i),ub(i));
    ub(i) = max(lb(i),ub(i));
    lb(i) = a;
end

weights = 1./err; % Calculate normalized weights
weights = weights/nansum(weights(:));
weights(~isfinite(weights)) = 0;

yWeighted = y.*weights;

var = err.^2;
var(isnan(err) | err < eps()) = inf;

fixed = logical(fixed); % make the fixed array ones and zeros;
           
%Scaling
yScale = max(y(:))-min(y(:));     % yOff = mean(y);
if any(b0==0)
    warning('nlfit:ScaleWarning',...
        'One or more initial model parameters are 0, initial value may be badly scaled.');
end
bScale = abs(b0.*(b0~=0) + (b0==0));  %Scale 0 parameters by 1

%Scaled fit model
b0Scaled = b0./bScale;
lbScaled = lb./bScale;
ubScaled = ub./bScale;

% yScale = max(yWeighted(:));
scaledWeights = weights/yScale;
% jWeights = repmat(scaledWeights(:),1,sum(~fixed));
jWeights = scaledWeights(:) * bScale(~fixed);
function [y, J] = f_fixed(beta,z)
    b = b0.*fixed;
    b(~fixed) = beta.*bScale(~fixed);
    if nargout > 1
        [y, J] = f(b,z);
        y = y.*scaledWeights;
        J = J(:,~fixed).*jWeights;
    else
        y = f(b,z).*scaledWeights;
    end
end

n = length(y);
p = numel(b0Scaled(~fixed));
v = n-p;
    
if all(fixed)
    b = b0;
    
    err_out = 0*b0;
    ci_out = 0*b0;
else
    % Fitting
    % generate a function, f_fixed, that is the same as f but hold certain
    % parameters at constant values, and accepts a shorter range of not fixed
    % parameters.

    % do fit using lsqcurvefit
    [beta,resnorm,residual,exitFlag,output,lambda,J] = ...
        lsqcurvefit(@f_fixed, b0Scaled(~fixed), x, yWeighted/yScale, lbScaled(~fixed), ubScaled(~fixed), opt);
    
    % turn warnings back on
    warning('on', 'all');

   % Calculation modified from nlparci
   % Estimate covariance matrix from Jacobian   
%     [~,R] = qr(J,0);
%     Rinv = R\eye(size(R));
%     diag_info = sum(Rinv.*Rinv',2);
    rmse = norm(residual) / sqrt(v);
    covar = inv(J'*J) * rmse^2;
    diag_info = diag(covar);
    
    % 1-sigma standard error from the residuals and jacobian.
%     se = sqrt(diag_info) * rmse;
    se = sqrt(diag_info);
    fiterr = se * tinv((1 + erf(1/sqrt(2)))/2,v);

    % Calculate confidence interval
    delta = se * tinv((1 + conf)/2,v);
    ci = [(beta(:) - delta), (beta(:) + delta)];
    
    beta = beta.*bScale(~fixed);
    fiterr = fiterr.*bScale(~fixed)';
    ci = ci.*[bScale(~fixed); bScale(~fixed)]';
    covar_out = full(covar).*(bScale(~fixed).*bScale(~fixed)');

    % Output conditioning
    b = b0.*fixed;
    b(~fixed) = beta;

    err_out = 0*b0;
    err_out(~fixed) = fiterr;

    if size(b,1)==1
        ci_out = [b; b];
        ci_out(:,~fixed) = ci';
    else
        ci_out = [b, b];
        ci_out(~fixed,:) = ci;
    end
end

totalSS = sum((yWeighted(:) - mean(yWeighted(:))).^2); % Calculate (weighted) total sum of squares
residuals = (yWeighted - f(b,x).*weights);
resSS = sum(residuals(:).^2); % Calculate (weighted) residual sum of squares

out = cell(1,5);
out{1,1} = err_out;
out{1,2} = ci_out;
out{1,3} = sum((f(b,x)-y).^2./var)/(n-p); % Calculate reduced chi-squared
out{1,4} = covar_out;
out{1,5} = 1 - resSS/totalSS; % Calculate coefficient of deterimination, R2
out{1,6} = 1 - resSS/totalSS*(n-1)/(n-p); % Calculate adjusted coefficient of deterimination

if nargout>1
    varargout = out(1,1:nargout-1);
end

end
