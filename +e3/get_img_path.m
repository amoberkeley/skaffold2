function img_path = get_img_path(date)
    init_skaffold;
    tokens = regexpi(date, '(\d+)[\\/](.*)[\\/](\d+)' , 'tokens');
    if ~isempty(tokens)
        months = {'jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec'};
        monthnum = find(strncmpi(tokens{1}{2},months,3));
        if ~isempty(monthnum)
            tokens{1}{2} = monthnum;
            tokens{1}{3} = str2num(tokens{1}{3});
            pattern = regexprep(['%s' filesep '%02d' filesep '%02d'],'\\','\\\\'); % Escape forward slash for Windows...
            date = sprintf(pattern, tokens{1}{:});
        end
    end
    img_path = fullfile(imgRoot, date);
end