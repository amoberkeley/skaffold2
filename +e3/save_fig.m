function figfile = save_fig( dataname, plotname, varargin )

    p = inputParser;
    p.addRequired('dataname',@ischar);
    p.addRequired('plotname',@ischar);
    p.addOptional('size', [11 8.5], @(x) isnumeric(x) && numel(x)==0 || numel(x)==2);
    p.addOptional('fig', [], @(x) isempty(x) || (isscalar(x)&&ishandle(x)));
    p.addOptional('type','png',@ischar);
    p.addParameter('dpi', 200, @(x) isscalar(x) && x>0);
    p.addParameter('subdir', false, @islogical);    
    p.addParameter('backend', 'builtin', @(x) ismember(x, {'builtin','export_fig'}));
    p.parse(dataname, plotname, varargin{:});
    args = p.Results;

    if isempty(args.fig)
        args.fig = gcf;
    end
    runname = strrep(args.dataname, '.mat', '');
        
    if args.subdir        
        assert(~isempty(args.plotname),'plotname must be provided for saving in a subdirectory!');
        if ~exist(runname, 'dir')
            result = mkdir(pwd(), runname);
            assert(result, 'Failed creating subdirectory for plots');
        end        
        
        figfile = fullfile(runname, args.plotname);
    else
        figfile = runname;
        if ~isempty(args.plotname)
            figfile = [figfile, '_', args.plotname];
        end        
    end    
    
    if strcmp(args.type,'epsc')
        ext = 'eps';
    else
        ext = args.type;
    end
    figfile = [figfile, '.', ext];

    
    fprintf('Saving plot to file ''%s''\n', figfile);
    set(args.fig,'PaperUnits','inches','PaperSize',args.size,'PaperPosition',[0, 0, args.size],'Color','w')
    
    switch (args.backend)
        case 'export_fig'
            export_fig(figfile, ['-',args.type], sprintf('-r%d',args.dpi), '-nocrop', args.fig);
            
        otherwise
            switch (args.type)
                case 'eps'
                    print(args.fig, '-deps2', sprintf('-r%d',args.dpi), figfile, '-painters');

                case 'epsc'
                    print(args.fig, '-depsc2', sprintf('-r%d',args.dpi), figfile, '-painters', '-loose');

                case 'pdf'
                    print(args.fig, '-dpdf', sprintf('-r%d',args.dpi), figfile, '-painters', '-loose');

                case 'png'
                    print(args.fig, '-dpng', sprintf('-r%d',args.dpi), figfile, '-opengl');

                otherwise
                    saveas(args.fig, figfile, args.type);
            end
    end
end

