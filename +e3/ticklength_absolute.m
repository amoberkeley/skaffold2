function ticklength_absolute(handle, length)
%Set axes tick length in absolute units (Inches)
%
%Usage:
%  e3.ticklength_absolute(handle, length)
%
%Arguments:
%  handle - Axes or figure handle.  For figure, acts on all child axes
%  length - Desired tick length, in inches

    assert(ishghandle(handle), 'Invalid graphics handle');
    
    switch get(handle,'type')
        case 'figure'
            axs = findall(handle, 'type', 'axes');
            for idx=1:numel(axs)
                e3.ticklength_absolute(axs(idx), length);
            end
            
        case 'axes'
            ax = handle;
            oldUnits = ax.Units;
            ax.Units = 'Inches';
            axScale = max(ax.Position(3:4));
            ax.TickLength(1) = length / axScale;
            ax.Units = oldUnits;        
            
        case 'colorbar'
            cb = handle;
            oldUnits = cb.Units;
            cb.Units = 'Inches';
            axScale = max(cb.Position(3:4));
            cb.TickLength(1) = length / axScale;
            cb.Units = oldUnits;                 
            
%             cb.TickLength(1) = length;
            
        otherwise
            error('Graphics handle must be axis or figure');
            
    end           
    
end