function [fit, z, dN, outliers, agg] = contrast_h5(nPoints, nLoops, z0, dca, varargin)
%CONTRAST Loads and fits contrast data
%
%Usage:
%  c = contrast_h5(nPoints, nLoops, z0, dca)
%  [c, z, dn] = contrast_h5(..., 'key', value, ...)
%
%Parameters:
%  nPoints - Integer number of unique cloud positions
%  nLoops - Number of repeated loops over these positions
%  z0 - The position (in um) of the first position
%  dca - The cavity-atomic-resonance detuning (in MHz)
%
%Keys (default first):
%  carrierFreq - 10e6 || double
%    Heterodyne carrier frequency (in Hz)
%  vcoBw - 20e3 || double
%    Low-pass filter BW (in Hz)
%  sweepRange - {[0 0.005] [0.007 0.012]} || cell array of 2-element arrays
%    List of time ranges corresponding to each sweep
%  scaleFun - @(x) -255-389*x || function handle
%    Function to convert voltage on second channel into VCO tuning
%    frequency
%  positionFile - '+e3\positionsAll.txt' || path name
%    Location of file containing list of positions (in um)
%  g0 - 13.0 || double
%    Value of single-photon - single-atom coupling (MHz)
%  noLoad - false || true
%    If true and data have already been loaded, remake contrast plot
%    without reloading data
%  ignore - [] || array of deltaN indices to ignore
%
%Returns:
%  c - A nlfit object to the following model:
%        y0 + C*y0*sin(2*pi*k*z + phi0)
%      where the variables are the following:
%        y0 - mean dispersive shift (in MHz)
%        C - contrast (1 is maximum)
%        k - spatial overlap cyclic frequency (in um^-1)
%        phi0 - spatial overlap phase offset (in rad)
%  Na - Atom number
%  z - A list of spatial positions (in um)
%  dn - A corresponding list of dispersive shifts (in MHz)

persistent pathname
persistent runname
persistent data
persistent analysisPath

global absorptionCenCav
global absorptionSpanCav

p = inputParser;

p.addRequired('nPoints', @(x) uint32(x) == x && isscalar(x) && x>4);
p.addRequired('nLoops', @(x) uint32(x) == x && isscalar(x) && x>= 1);
p.addRequired('z0', @(x) isscalar(x) && isreal(x));
p.addRequired('dca', @(x) isscalar(x) && isreal(x));
p.addParameter('carrierFreq', 10e6, @(x) isscalar(x) && isreal(x));
p.addParameter('vcoBw', 20e3, @(x) isscalar(x) && isreal(x));
p.addParameter('sweepRange', {[0.0001 0.0049], [0.0071, 0.0119]});
p.addParameter('scaleFun', @(x) -258.2-380.2*x, @(x) isa(x, 'function_handle'));
p.addParameter('positionFile', ['+e3', filesep, 'positionsAll.txt'], @ischar);
p.addParameter('g0', 13.0, @(x) isscalar(x) && isreal(x));
p.addParameter('noLoad', false, @(x) isscalar(x) && (islogical(x) || isa(x, 'DataModel')));
p.addParameter('ignore', [], @(x) all(uint32(x) == x));
p.addParameter('outlierThreshold', 3.0, @(x) isscalar(x) && isreal(x));
p.addParameter('Na', [], @(x) isempty(x) || numel(x) == 2);
p.addParameter('cguess', [0.9 1600 0.1 1.0], @(x) isempty(x) || numel(x) == 4 || numel(x) == 5);
p.addParameter('verifiers', {verify.Range('deltaN', [0.5, -15])}, ...
    @(x) iscell(x) && all(cellfun(@(vr) any(strcmp(superclasses(vr), 'Verifier')), x)));
p.addParameter('maxDeltaNDiff', 3, @(x) isscalar(x) && isreal(x));
p.addParameter('plotFits', false, @islogical);
p.addParameter('rawName', 'raw', @ischar);
p.addParameter('vcoName', 'vco', @ischar);
p.addParameter('includeLinearTerm', false, @islogical);

p.parse(nPoints, nLoops, z0, dca, varargin{:});
r = p.Results;

live = 1;

init_skaffold;
init_absorption;

empty_vcoName = ['empty_' r.vcoName];

if ~isempty(r.cguess)
    if r.includeLinearTerm
        assert(5 == numel(r.cguess), 'Argement cguess must be provided and have 5 elements when includeLinearTerm is set to true.')
    else
        assert(4 == numel(r.cguess), 'Argement cguess must have 4 elements when includeLinearTerm is set to false (default).')
    end
end

%Load the z positions
assert(logical(exist(r.positionFile, 'file')), 'skaffold:FileNotFound', ...
	'Could not find positions file ''%s''.', r.positionFile)
z = load(r.positionFile);
z = z(:, 1);                            %Extract first column
iz0 = find(abs(z-z0) == min(abs(z-z0))); %Find the closest z to z0
z = repmat(z(iz0:iz0+nPoints-1), 1, nLoops).';

if isempty(pathname)
    pathname = fullfile(dataRoot, datestr(now(), 'yyyy/mmm/dd'));
    if exist(pathname, 'dir') ~=7
        pathname = [pwd '\'];
    end
end

if isa(r.noLoad, 'DataModel')
    data = r.noLoad;
    r.noLoad = true;
end

if r.plotFits
    hetVsVcoAxis = report.Axes({...
        report.Plot('sweepUpFitWf', 'lineProperties', {'LineStyle', ':', 'Color', 'k', 'LineWidth', 1.5}), ...
        report.Plot('sweepDownFitWf', 'lineProperties', {'LineStyle', '--', 'Color', 'k', 'LineWidth', 1.5}), ...
        report.Plot('empty_sweepUpFitWf', 'lineProperties', {'LineStyle', ':', 'Color', 'k', 'LineWidth', 1.5}), ...
        report.Plot('empty_sweepDownFitWf', 'lineProperties', {'LineStyle', '--', 'Color', 'k', 'LineWidth', 1.5}), ...
        report.Plot('empty_hetMag', 'xField', empty_vcoName, 'lineProperties', {'Color', 'r'})...
        report.Plot('hetMag', 'xField', r.vcoName), ...
        }, 'xLabel', 'Probe tuning (MHz)', 'yLabel', 'Het. Mag.', 'axisCommand', 'tight');
else
    hetVsVcoAxis = report.Axes({...
        report.Plot('empty_hetMag', 'xField', empty_vcoName, 'lineProperties', {'Color', 'r'})...
        report.Plot('hetMag', 'xField', r.vcoName), ...
        }, 'xLabel', 'Probe tuning (MHz)', 'yLabel', 'Het. Mag.', 'axisCommand', 'tight');
end

if r.dca > 0
    ylims = [-0.5 inf];
else
    ylims = [-inf 0.5];
end

% Construct reporters
if isempty(data) || ~r.noLoad

    % Construct analyzers
    gsl = loader.GageHDF5(pathname, '', 'skip', []);

    %Reset the pathname
    pathname = gsl.get_path();

    analysisPath = strrep(gsl.get_root(), dataRoot, analysisRoot);
    if exist(analysisPath, 'dir') ~= 7
        mkdir(analysisPath);
    end
    runname = gsl.get_run();
    dataname = fullfile(analysisPath, [runname '.mat']);   
    
    %This is the reporter we'll use for iteration data
    pr = report.Figure('Contrast single-file plots', {...
        report.Axes({...
        report.Plot(empty_vcoName, 'lineProperties', {'Color', 'r'})...
        report.Plot(r.vcoName), ...
        }, 'xLabel', 'Time (s)', 'yLabel', 'Probe tuning (MHz)'), ...                 
        hetVsVcoAxis;...               
        report.Axes({...
        report.Plot('empty_hetMag', 'lineProperties', {'Color', 'r'})...
        report.Plot('hetMag'), ...
        }, 'xLabel', 'Time (s)', 'yLabel', 'Het. Mag.'), ...
        report.Axes({...
        report.Plot('empty_hetPhase', 'lineProperties', {'Color', 'r'})...
        report.Plot('hetPhase'), ...
        }, 'xLabel', 'Time (s)', 'yLabel', 'Het. Phase')...
        }, 'axesProperties', {'FontSize', 11, 'Box', 'on'}, 'save', e3.get_fig_files('single'));

    
    plotAgg = report.Figure('Contrast aggregated plots', {...
        report.PointPlot('deltaN', 'yLim', ylims, 'xValues', z(1, :)), ...
        ...report.Axes({...
        ...report.PointPlot('deltaNUp', 'LineProperties', {'Marker', 'x'}), ...
        ...report.PointPlot('deltaNDown', 'LineProperties', {'Marker', '+'})}, ...
        ...'yLabel', 'separate up / down', 'yLim', ylims) ...
        }, 'save', e3.get_fig_files('agg'));
   
    plotAtoms = report.Figure('Atom plots', {...
        report.PointPlot('tof.Na_2d', 'ylabel', 'N_a 2D', 'YLim', [0, 3000]), ...
        report.PointPlot('tof.Na_h', 'ylabel', 'N_a Horiz.', 'YLim', [0, 3000]), ...
        report.PointPlot('tof.Sx_1d', 'ylabel', 'S_x 1D', 'YLim', [0, 15]), ...
        }, 'figureProperties', {'PaperPosition', [0 0 11 8.5]}, 'save', e3.get_fig_files('atoms'));

    %ahet loads and performs default analysis on heterodyne data
    ahet = analyze.Block({
        analyze.RescaleHeterodyne(r.rawName, 'transimpedance', 28.8e3, 'gain', 30.1, 'impedance', 50), ...
        analyze.Heterodyne(r.rawName, 'carrierFreq', r.carrierFreq, 'filterBw', r.vcoBw/2), ...
        analyze.RescaleVco(r.vcoName, 'bw', r.vcoBw, 'scaleFun', r.scaleFun), ...
        analyze.Lambda({r.vcoName, 'hetMag'}, r.vcoName,  @(vco, hm) vco.slice([1, length(hm)])),... 
        analyze.LorFit(r.vcoName, 'hetMag', 'sweepUpFit', 'range', r.sweepRange{1}, 'targetWf', 'sweepUpFitWf'), ...
        analyze.LorFit(r.vcoName, 'hetMag', 'sweepDownFit', 'range', r.sweepRange{2}, 'targetWf', 'sweepDownFitWf')...
        });

    adl = loader.Andor(imgRoot, 'offset', 0);
    tof = analyze.AbsorptionFit(adl, 'timestamp', 'tof', 'cen', absorptionCenCav, 'span', absorptionSpanCav, ...
        'pixel_size', 2.86e-4, 'cross_section', 2.9e-9, 'plot', true, 'threshold', 1200);
    
    %ap first runs ahet, then performs FFT, lock, and report anlyses
    %Load the with atoms trace
    ap = analyze.Block({...
        analyze.LoadH5(gsl, {'source', 'timestamp'}), ...
        ahet, ... % Load the atoms data
    	analyze.Namespace('empty', ahet), ... %Load the empty cavity
        analyze.DispersiveShift({'sweepUpFit'}, 'deltaNUp'), ...
        analyze.DispersiveShift({'sweepDownFit'}, 'deltaNDown'), ...
        analyze.DispersiveShift({'sweepUpFit', 'sweepDownFit'}, 'deltaN'), ...
        analyze.Lambda({'deltaNUp', 'deltaNDown'}, 'deltaNDiff', @(dnUp, dnDown) abs(dnUp - dnDown)), ...
        tof, pr, ...
        report.Print('deltaN = %0.4f (%s)\n', {'deltaN', 'source'})
    });
    
    aggregator = aggregate.Block({
            aggregate.Verifier('valid', {...
                r.verifiers{:}, ...
                verify.Range('deltaNDiff', [0, r.maxDeltaNDiff]), ...
            }), ...
            aggregate.Collect({'deltaN', 'deltaNUp', 'deltaNDown'}, 'if', 'valid'), ...
            aggregate.Collect('tof'), ...
            plotAtoms, plotAgg...
        });

    % Set up scanner
    scanner = Scanner(ap, nPoints, nLoops, aggregator, 'wait', true, ...
        'loader', gsl, 'aggregateFile', dataname);
    
    agg = scanner.get_aggregate();
    agg.set('z', z);
    agg.set('dca', dca);
    agg.set('nPoints', nPoints);
    agg.set('nLoops', nLoops);
    
    % Analyze data
    [res, data, ~] = scanner.run();
    if res ~= Result.SUCCESS
        error('Analysis failed');
    end

end

data.set('z', z);
agg = data;

%% Fit contrast
dN = data.get('deltaN');

mask = true(size(dN));
mask(r.ignore) = false;

if ~isempty(r.Na)
    Na = data.get('tof.Na_2d');
    badNa = Na > max(r.Na) | Na < min(r.Na);
    mask(badNa) = false;
end

if r.includeLinearTerm
    z1 = min(z(1, :));
    z2 = max(z(1, :));
    cmodel = @(C, Na1, Na2, k, phi0, z) ((Na2+Na1)/2 + (z - (z2+z1)/2)*(Na2-Na1)/(z2-z1) + C*(Na1+Na2)/2*sin(2*pi*k*z + phi0))*r.g0^2/dca/2;
else
    cmodel = @(C, Na, k, phi0, z) (Na + C*Na*sin(2*pi*k*z + phi0))*r.g0^2/dca/2;
end

%cguess = [0.9 1600 0.1 1.0]; % for when the fit struggles, input own
fit = nlfit.from(z(mask), dN(mask), cmodel, r.cguess);

outliers = find(abs(dN - fit.of(z)) > r.outlierThreshold);

%% Make plot
fig = ufigure('Contrast plot');
clf(fig);

figSize = [6 4];
fig.Units = 'inches';
fig.Position(3:4) = figSize;

ax = axes('Parent', fig, 'NextPlot', 'add');
zp = linspace(min(z(:)), max(z(:)), 101);

grid on;

hFit = plot(ax, zp, fit.of(zp));
hData = plot(ax, z(mask), dN(mask), 'ok', 'MarkerFaceColor', [0.5 0.5 1.0]);
hIgnore = plot(ax, z(~mask), dN(~mask), 'xr', 'MarkerSize', 8, 'LineWidth', 1); 

if r.includeLinearTerm
    hl = legend([hData, hFit], {sprintf('Data at \\Delta_{ca} = %0.0f GHz', dca/1e3), ...
        sprintf('Fit: C = %4.2f \\pm %4.2f\n    Na = %4.0f \\pm %4.0f', ...
        fit.C, fit.dC/2, (fit.Na1 + fit.Na2)/2, (fit.dNa1 + fit.dNa2)/2)}, 'Location', 'southwest');
else
    hl = legend([hData, hFit], {sprintf('Data at \\Delta_{ca} = %0.0f GHz', dca/1e3), ...
        sprintf('Fit: C = %4.2f \\pm %4.2f\n    Na = %4.0f \\pm %4.0f', ...
        fit.C, fit.dC/2, fit.Na, fit.dNa/2)}, 'Location', 'southwest');
end

set(hl, 'FontName', 'Consolas');
xlabel(ax, 'Position (\mum)');
ylabel(ax, 'Dispersive shift (MHz)');

%%

e3.save_fig(fullfile(analysisPath, runname), 'agg', figSize, fig);
