function structOut = structequal(structIn, structOut)
    for x=1:numel(fieldnames(structIn))
        a=fieldnames(structIn);
        structOut.(a{x})=structIn.(a{x});
    end
end