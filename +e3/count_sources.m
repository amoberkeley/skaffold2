function [maxNum, fileNums] = count_sources(pathname, varargin)
    p = inputParser;
    p.addRequired('pathname',@ischar);
    p.addParameter('useDir',0,@(x) isscalar(x) && isreal(x) && x > 0);
    p.addParameter('start',1,@(x) isscalar(x) && isreal(x) && x > 0);
    p.addParameter('wildcardPattern','AS_CH*.sig',@ischar);
    p.addParameter('pattern','AS_CH\d\d-(\d*).sig',@ischar);
    p.addParameter('existPattern','AS_CH01-%05d.sig',@ischar);
    p.parse(pathname,varargin{:});
    r = p.Results;
    maxNum = 0;
    
    if r.useDir
        fileList = dir(fullfile(r.pathname, wildcardPattern));
        if isempty([fileList.name])
            return
        end
        fileNums = cellfun(@(x) str2num(x{1}), regexp([fileList.name], r.pattern,'tokens'));
        maxNum = max(fileNums);
    else
        % Network optimized search for latest signal file matching
        % existPattern, beginning checking with file number start
        startNum = r.start;
        for i = startNum:100000
            if exist(fullfile(r.pathname, sprintf(r.existPattern, i)), 'file')
                maxNum = i;
            else
                return;
            end
        end
    end
end