function [ax, index, cols, rows] = tileplot(nPoints, point, varargin)
%Tile subplots in a figure, either vertically or horizontally, determining
% row and column number automatically for a given number of subplots
%
%Usage:
%  [ax, index, cols, rows] = e3.tileplot(nPoints, point,'key',value,...)
%
%Arguments:
%  nPoints - Integer number of unique cloud positions
%
%Parameters (default first):
%  rows - [] || double
%    Manually choose the number of rows to use.
%  cols - [] || double
%    Manually choose the number of columns to use.
%  tile - 'horiz' || {'horiz', 'vert'}
%    Should subsequent points be tiled horizontally or vertically
%
%Returns:
%  ax - The handle of the subplot axes
%  index - The MATLAB index of the subplot
%  cols - The number of columns in the figure
%  rows - The number of rows in the figure

    p = inputParser;
    p.addRequired('nPoints',@(x) isscalar(x) && x > 0);
    p.addRequired('point',@(x) isscalar(x) && x > 0);
    p.addParameter('rows',[],@(x) isempty(x) || (isscalar(x) && x > 0));
    p.addParameter('cols',[],@(x) isempty(x) || (isscalar(x) && x > 0));
    p.addParameter('tile', 'horiz', @(x) ismember(x, {'horiz','vert'}));
    p.addParameter('parent', [], @(x) ishandle(x));
    p.addParameter('margins', {}, @(x) iscell(x));
    p.KeepUnmatched = 1;
    p.parse(nPoints, point, varargin{:});
    args = p.Results;
    extra = struct2params(p.Unmatched);

    if ~isempty(args.parent)
        hParent = args.parent;
    else
        hParent = gcf();
    end
    
    if ~isempty(args.rows)
        rows = args.rows;
    elseif ~isempty(args.cols)
        rows = ceil(nPoints / args.cols);
    else
        rows = ceil(sqrt(args.nPoints));
    end
    
    if ~isempty(args.cols)
        cols = args.cols;
    else
        cols = ceil(nPoints / rows);
    end
    
    switch args.tile
        case 'horiz'
            index = point;
            
        case 'vert'
            ir = mod(point-1, rows)+1;
            ic = ceil(point/rows);
            index = (ir-1) * cols + ic;
    end
    
    if isempty(args.margins)
        ax = subplot(rows, cols, index, 'Parent', hParent, 'NextPlot', 'add', extra{:});
    else
        ax = subtightplot(rows, cols,index, args.margins{:}, 'Parent', hParent, 'NextPlot', 'add', extra{:});
    end    
end