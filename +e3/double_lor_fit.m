function [fit, ci, r2] = double_lor_fit(wf, varargin)
%Fits a double lorentzian model to the waveform(s) in the first parameter.  
%
%  model = y0 + A1*G1^2/4./((x-x1).^2 + G1^2/4) + A2*G2^2/4./((x-x2).^2 + G2^2/4)
%
%If the Waveform is multidimensional, it will treat the first dimension as the frequency axis,
%and individually fit each row of the higher dimesions.  The results
%returned with have one fewer dimensions, with the shape of the second and
%higher dimesions of the original waveform.
%
%Usage:
%  [fitObj, fitStruct, r2] = e3.lor_fit(data,'key',value,...)
%
%Parameters:
%  data - Waveform containing spectrum to fit
%
%Keys (default first):
%  f - [] || double
%    Frequency guess.  Defaults to position of data maximum.
%  g - [] || double
%    Linewidth guess.  Defaults to 1/4 of the data range.
%  sn - double
%    Shot-noise level. If provided, this is excluded from the fit
%    parameters, and the amplitude is returned in units of SN.  Otherwise
%    the SN level will be extracted from the fit.
%  range - [] || [fStart, fStop]
%    Frequency range for fit
%  plot - boolean
%    Specify whether to plot the fit result over the data.  This defaults
%    to true for 1-d Waveforms when no output arguments are specified,
%    otherwise it defaults to false.  For multi-dimensional Waveforms, this
%    will show 'pages' of 20 fit results, 'pause'ing in between.
%
%Returns:
%  fitObj - An nlfit object
%  fitStruct - A struct array with the fit parameters
%  r2 - A measure of the goodness of fit (roughly the percentage of the
%       variance of the data which the fit accounts for).

    p = inputParser;
    p.addRequired('data',@(x) isa(x, 'Waveform'));
    p.addParameter('range', [], @(x) isnumeric(x) && numel(x)==0 || numel(x)==2);
    p.addParameter('f', [], @(x) all(x > 0) || isempty(x));
    p.addParameter('g', [], @(x) all(x > 0) || isempty(x));
    p.addParameter('sn', false, @(x) isscalar(x) && (x > 0 || x == false));
    p.addParameter('fDiv',[], @(x) all(x > 0) || isempty(x));
    p.addParameter('plot', false, @(x) islogical(x) || ishandle(x));
    p.addParameter('warn', false, @(x) islogical(x));
    p.parse(wf, varargin{:});
    args = p.Results;
            
    if isempty(args.range)
        range = [wf.x0, wf.get_x_end()];
    else
        wf = wf.slice_x(min(args.range), max(args.range));
        range = args.range;
    end
    
    x = wf.get_x()';   
    
    warnState(1) = warning('query','stats:nlinfit:IllConditionedJacobian');
    warnState(2) = warning('query','stats:nlinfit:IterationLimitExceeded');
    warnState(3) = warning('query','stats:nlinfit:ModelConstantWRTParam');
    if ~(args.warn)
        warning('off','stats:nlinfit:IllConditionedJacobian')
        warning('off','stats:nlinfit:IterationLimitExceeded')
        warning('off','stats:nlinfit:ModelConstantWRTParam')
    end
    
    model = @double_lor_model;
    
    if isvector(wf.data)
        y = wf.data(:);
        [guess, fixed, lb, ub] = make_guess(x, y, args);
        [fit, ci, r2] = do_fit(x, y, model, guess, fixed, lb, ub);
        
        if ishandle(args.plot) || args.plot || (nargout == 0)
            make_plot(x, y, fit, ci, model, range, args.plot);
        end
        
%         fitStruct =  struct();
%         for iParam = 1:length(fitObj.parameters)
%             fitStruct.(fitObj.parameters{iParam}) = fitObj.(fitObj.parameters{iParam});
%             fitStruct.(['d', fitObj.parameters{iParam}]) = fitObj.(['d' fitObj.parameters{iParam}]);
%         end          
    else
        dims = size(wf.data);
        r2 = zeros([dims(2:end),1]);
        nPoints = prod(dims(2:end));
        for point=1:nPoints
            y = wf.data(:,point);
            [guess, fixed, lb, ub] = make_guess(x, y, args);

            try
                [fit(:,point), ci(:,:,point), r2(point)] = do_fit(x, y, model, guess, fixed, lb, ub);
            catch e
                fprintf('Error fitting: %s', e.getReport());
            end               
        end
        if length(dims) > 2
            fit = reshape(fit, [size(fit,1),dims(2:end)]);
        end
        
        if args.plot
            make_plots(x, wf.data, fit, range);
        end

%         fitStruct =  struct();
%         for point=1:nPoints
%             if ~fitObj(point).isvalid()
%                 for iParam = 1:length(fitObj(1).parameters)
%                     fitStruct(point).(fitObj(1).parameters{iParam}) = nan;
%                     fitStruct(point).(['d', fitObj(1).parameters{iParam}]) = nan;
%                 end                         
%                 continue;
%             end
%             for iParam = 1:length(fitObj(point).parameters)
%                 fitStruct(point).(fitObj(point).parameters{iParam}) = fitObj(point).(fitObj(point).parameters{iParam});
%                 fitStruct(point).(['d', fitObj(point).parameters{iParam}]) = fitObj(point).(['d' fitObj(point).parameters{iParam}]);
%             end          
%         end
%         if length(dims) > 2
%             fitStruct = reshape(fitStruct, dims(2:end));
%         end
    end
    
    for i=1:length(warnState)
        warning(warnState(i).state, warnState(i).identifier);
    end
end

function [yn, J] = double_lor_model(p, xn)
    % p = [y0,A1,A2,G1,G2,x1,x2]
    
    A1 = p(2);
    A2 = p(3);
    g1 = p(4)/2;
    g2 = p(5)/2;
    dx1 = xn-p(6);
    dx2 = xn-p(7);
    
	yn = p(1) + A1./(1 + (dx1/g1).^2) ...
            + A2./(1 + (dx2/g2).^2);
        
    if nargout > 1   % two output arguments, calculate Jacobian
        J = [...
            ones(numel(xn),1),... (1 + A1./(1 + (dx1/g1).^2) + A2./(1 + (dx2/g2).^2)),... % dF/dy0
            1./(1 + (dx1(:)/g1).^2),... % dF/dA1
            1./(1 + (dx2(:)/g2).^2),... % dF/dA2
            A1.*g1.*dx1(:).^2./(g1.^2 + dx1(:).^2).^2,... % dF/dG1
            A2.*g2.*dx2(:).^2./(g2.^2 + dx2(:).^2).^2,... % dF/dG2
            2*A1.*g1.^2.*dx1(:)./(g1.^2 + dx1(:).^2).^2,... % dF/dx1
            2*A2.*g2.^2.*dx2(:)./(g2.^2 + dx2(:).^2).^2 ... % dF/dx2
        ];  % Jacobian of the function evaluated at x
    end

end


function [guess, fixed, lb, ub] = make_guess(x, y, args)
    fixed = false(1,7);
    
    if isempty(args.f)
        guessF = min(x) + (max(x) - min(x))*[.25,.75];
    else
        guessF = args.f;
    end

    if isempty(args.g)
        guessG = [(max(x)-min(x))/4, (max(x)-min(x))/4];
    else
        guessG = args.g;
    end

    if islogical(args.sn) && ~args.sn
        sn = min(y);
    else
        sn = args.sn;    
        fixed(1) = true;
    end
    
    guessA = max(y(:));
    
    guess = [sn, guessA, guessA, guessG, min(guessF), max(guessF)];
    lb = [0, 0, 0, 0, 0, min(x), min(x)];
    ub = [inf, inf, inf, (max(x)-min(x))/2, (max(x)-min(x))/2, max(x), max(x)];
    
    if ~isempty(args.fDiv)
%         lb(7) = args.fDiv;
%         ub(6) = args.fDiv;
        guess(2) = max(y(x < args.fDiv));
        guess(3) = max(y(x > args.fDiv));
    end
    
end

function [fit, ci, R2] = do_fit(x, y, model, guess, fixed, lb, ub)

%     yMax = max(y);
    total = sum(y);
    sn = guess(1);
    peakPower = sum(y-sn);
    
    yScaled = y / peakPower;

    yMax = 1;
    yScaled = y/yMax;
    guess(1:3) = guess(1:3)/yMax;
    lb(1:3) = lb(1:3)/yMax;
    ub(1:3) = ub(1:3)/yMax;

    opts = optimoptions(@lsqcurvefit,'Jacobian','on','MaxIter',1e4); %,'Display','off','DerivativeCheck','on',
    [fit, stderr, ci, X2, R2, adjR2] = e3.lsqfit(x, yScaled, model, guess, fixed, lb, ub, [], 0.95, opts);
    
    fit(1:3) = fit(1:3)*yMax;
    stderr(1:3) = stderr(1:3)*yMax;
    ci(:,1:3) = ci(:,1:3)*yMax;
    
%     rScale = max(residual)*yMax;                       %y-data scale
%     pScale = repmat(fit,length(residual),1);   %parameters scale
%     R = residual./rScale;                         %rescaled residuals
%     P = ones(size(fit));                       %rescaled result
%     J = jacobian.*pScale./rScale;
%     ci = nlparci(P,R,'jacobian',J,'alpha',1-confidence);
    %Scale back into output units
%     ci = ci.*repmat(fit.',1,2);
    
end

function make_plot(x, y, fit, ci, model, range, ax)
    modelX = linspace(min(range), max(range),500);

    if (ishandle(ax))
        cla(ax)
    else
        fig = ufigure('e3.double_lor_fit');
        clf(fig);
        ax = axes('Parent', fig);
        hold on
    end
    hold(ax, 'on')
    plot(ax,x,y);
    plot(ax, modelX, model(fit, modelX),'Color','r','Linewidth',2)
    yl=ylim(ax);
    plot(ax, modelX, model(ci(2,:), modelX),':','Color','k','Linewidth',1)
    plot(ax, modelX, model(ci(1,:), modelX),':','Color','k','Linewidth',1)
    ylim(ax,yl);
    xlim(ax, sort(range));
end

function make_plots(x, y, fit, ci, model, range)
    modelX = linspace(min(range), max(range),500);
    
    fig=ufigure('e3.double_lor_fit');
        
    [~, steps] = size(y);
    panels = ceil(steps / 16);

    for panel=1:panels
        clf(fig)    
        for iRow=1:4
            for iCol=1:4
                sp = (iRow-1)*4 + iCol;
                step = (panel-1)*16 + sp;
                if step > steps
                    break
                end
                ax=subplot(4,4,sp,'Parent',fig);
                hold(ax,'on');
                plot(ax, x, y(:,step));
                plot(ax, modelX, model(fit(step,:), modelX),'Color','r','Linewidth',2)
                plot(ax, modelX, model(ci(step,:,2), modelX),':','Color','k','Linewidth',1)
                plot(ax, modelX, model(ci(step,:,1), modelX),':','Color','k','Linewidth',1)
                xlim(ax,sort(range));
            end
            if step > steps
                break
            end
        end
        if step > steps
            break
        end
        pause
    end
end
