function h = colorplot(varargin)

    [ax,args,~] = axescheck(varargin{:});

    if isempty(ax)
        ax = gca();
    end

    p = inputParser;
    p.addRequired('x', @(x) isvector(x));
    p.addRequired('y', @(x) isvector(x));
    p.addRequired('c', @(x) ismatrix(x));
    p.addParameter('interp', false, @(x) islogical(x));
    p.KeepUnmatched = 1;
    p.parse(args{:});
    args = p.Results;
    extra = struct2params(p.Unmatched);
    
    x = args.x;
    y = args.y;
    c = args.c;
    
    if args.interp
        h = pcolor(ax, x, y, c);
        shading(ax, 'interp');
        set(h,'edgecolor','none', extra{:})
    else
        dx = diff(x);
        assert( all(max(abs(dx - dx(1))/dx(1)) < 10e-5),...
            'X-values must be evenly spaced for e3.colorplot, without interpolation');
        dy = diff(y);
        assert( all(max(abs(dy - dy(1))/dy(1)) < 10e-8),...
            'Y-values must be evenly spaced for e3.colorplot, without interpolation');
        
        xRange = [min(x(:)'), max(x(:)')];
        yRange = [min(y(:)'), max(y(:)')];
        h = imagesc(xRange, yRange, c, 'Parent', ax, extra{:});
        set(ax,'YDir','normal');
    end
end
