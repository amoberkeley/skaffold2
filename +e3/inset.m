function [hInset] = inset(varargin)
%Helper function to create inset plot
%
%Usage:
%  [hInset] = e3.inset([ax], pos, ...)
%
%Arguments:
%  ax - Parent axes (default gca())
%  pos - Position of inset, in relative units of 'parent' axes
%
%Parameters:
%  ... All parameters passed to underlying call to 'axes'
%
%Returns:
%  hInset - The handle of the inset axes

    [ax,args,~] = axescheck(varargin{:});

    if isempty(ax)
        ax = gca();
    end

    p = inputParser;
    p.addRequired('pos', @(x) isvector(x) && numel(x) == 4);
    p.KeepUnmatched = 1;
    p.parse(args{:});
    args = p.Results;
    extra = struct2params(p.Unmatched);
    
    parentUnits = ax.Units;
    rel = ax.Position;
    insetPos = [rel(1) + rel(3)*args.pos(1),...
        rel(2) + rel(4)*args.pos(2),...
        rel(3)*args.pos(3), rel(4)*args.pos(4)];
    hInset = axes('Units', parentUnits, 'Position', insetPos, 'Layer','top','NextPlot','add', extra{:});

end
