function nBar = nBar(powerSpectralDensity, carrierFreq, carrierBW, snRange, kappa, edet)
    % Calculates the mean cavity photon occupation.
    % 
    % Arguments:
    %   - powerSpectralDensity: The power spectral density of the
    %   heterodyne signal over the desired time span, calculated using,
    %   e.g., Waveform.fft('powerDensity').
    %   - carrierFreq: The heterodyne beat frequency, in hertz.
    %   - carrierBW: The bandwidth over which to integrate the carrier
    %   signal, in hertz.
    %   - snRange: The range over which to calculate the shotnoise spectral
    %   density, as a doublet [fMin, fMax], in hertz offset from the
    %   carrier frequency. Will be reflected  about the carrier frequency.
    %   - kappa: The cavity linewidth, in hertz.
    %   - edet: The total heterodyne detection efficiency.
    %
    Pcar = powerSpectralDensity.slice_x(carrierFreq + [-carrierBW/2, carrierBW/2]).sum.data ...
        * powerSpectralDensity.dx;
    
    Ssn_blue = powerSpectralDensity.slice_x(carrierFreq + snRange).mean.data;
    Ssn_red = powerSpectralDensity.slice_x(carrierFreq - snRange).mean.data;
    Ssn = (Ssn_blue + Ssn_red) / 2;
    
    nBar = (Pcar - Ssn * carrierBW) ./ (4 * Ssn * kappa * edet);
end