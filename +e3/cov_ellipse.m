function h=cov_ellipse(varargin)
% COV_ELLIPSE - plot an error ellipse defining a confidence region determined from a covariance matrix
%    COV_ELLIPSE(C) - Given a 2x2 covariance matrix (positive definite), plot the
%    associated error ellipse, at the origin. It returns a graphics handle
%    of the ellipse that was drawn. 
%
%    COV_ELLIPSE(C,V0) - Plot the ellipse centered at V0, a 2-element vector.
%
%    COV_ELLIPSE(...,'Property1',Value1,'Name2',Value2,...) sets the
%    values of specified properties, including:
%      'conf' - A value betwen 0 and 1 specifying the confidence interval.
%        the default is 0.95.
%       All remaining properties are passed to the 'plot' function

    [ax,args,~] = axescheck(varargin{:});

    if isempty(ax)
        ax = gca();
    end

    p = inputParser;
    p.addRequired('C', @(x) all(size(x) == [2,2]));
    p.addOptional('v0', [0, 0], @(x) isvector(x) && numel(x) == 2);
    p.addParameter('conf', 0.68, @(x) x > 0 && x < 1);
    p.KeepUnmatched = 1;
    p.parse(args{:});
    args = p.Results;
    plotOpts = struct2params(p.Unmatched);

    C = args.C;
    v0 = args.v0;
    conf = args.conf;

    [eigvec, eigval] = eig(C); % Solve eigensystem

    % Make sure the matrix is positive definite
    if ~all(diag(eigval) > 0)
        warning('The covariance matrix must be positive definite (it has non-positive eigenvalues)')
        return
    end

    % Compute quantile for the desired percentile
    k = sqrt(chi2inv(conf, 2)); % r=2 is the number of dimensions (degrees of freedom)
    
    phase = linspace(0, 2*pi, 100);

    v = k * [cos(phase'),sin(phase')] * sqrt(eigval) * eigvec'; % Transformation

    h1=plot(ax, v(:,1) + v0(1), v(:,2) + v0(2), plotOpts{:});
    
    if nargout
        h=h1;
    end
end

