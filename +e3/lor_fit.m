function [fitObj, fitStruct, r2] = lor_fit(wf, varargin)
%Fits a lorentzian model to the waveform(s) in the first parameter.  
%
%  model = y0+A*G^2/4./((x-x0).^2 + G^2/4)
%
%If the Waveform is multidimensional, it will treat the first dimension as the frequency axis,
%and individually fit each row of the higher dimesions.  The results
%returned with have one fewer dimensions, with the shape of the second and
%higher dimesions of the original waveform.
%
%Usage:
%  [fitObj, fitStruct, r2] = e3.lor_fit(data,'key',value,...)
%
%Parameters:
%  data - Waveform containing spectrum to fit
%
%Keys (default first):
%  f - [] || double
%    Frequency guess.  Defaults to position of data maximum.
%  g - [] || double
%    Linewidth guess.  Defaults to 1/4 of the data range.
%  sn - double
%    Shot-noise level. If provided, this is excluded from the fit
%    parameters, and the amplitude is returned in units of SN.  Otherwise
%    the SN level will be extracted from the fit.
%  range - [] || [fStart, fStop]
%    Frequency range for fit
%  plot - boolean
%    Specify whether to plot the fit result over the data.  This defaults
%    to true for 1-d Waveforms when no output arguments are specified,
%    otherwise it defaults to false.  For multi-dimensional Waveforms, this
%    will show 'pages' of 20 fit results, 'pause'ing in between.
%
%Returns:
%  fitObj - An nlfit object
%  fitStruct - A struct array with the fit parameters
%  r2 - A measure of the goodness of fit (roughly the percentage of the
%       variance of the data which the fit accounts for).

    p = inputParser;
    p.addRequired('data',@(x) isa(x, 'Waveform'));
    p.addParameter('range', [], @(x) isnumeric(x) && numel(x)==0 || numel(x)==2);
    p.addParameter('f', [], @(x) all(x > 0) || isempty(x));
    p.addParameter('g', [], @(x) all(x > 0) || isempty(x));
    p.addParameter('sn', false, @(x) isscalar(x) && (x > 0 || x == false));
    p.addParameter('plots', false, @(x) islogical(x) || ishandle(x));
    p.addParameter('warn', false, @(x) islogical(x));
    p.parse(wf, varargin{:});
    args = p.Results;

    sn = args.sn;
    fitSn = (islogical(sn) && ~sn);
    
    if fitSn
        model = @(y0,A,G,x0,x) y0+A*G^2/4./((x-x0).^2 + G^2/4);
    else
        model = @(A,G,x0,x) (sn)*(1+A*G^2/4./...
            ((x-x0).^2 + G^2/4));
    end
            
    if isempty(args.range)
        range = [wf.x0, wf.get_x_end()];
    else
        wf = wf.slice_x(min(args.range), max(args.range));
        range = args.range;
    end
    
    x = wf.get_x()';   
    
    warnState(1) = warning('query','stats:nlinfit:IllConditionedJacobian');
    warnState(2) = warning('query','stats:nlinfit:IterationLimitExceeded');
    warnState(3) = warning('query','stats:nlinfit:ModelConstantWRTParam');
    if ~(args.warn)
        warning('off','stats:nlinfit:IllConditionedJacobian')
        warning('off','stats:nlinfit:IterationLimitExceeded')
        warning('off','stats:nlinfit:ModelConstantWRTParam')
    end
    
    if isvector(wf.data)
        y = wf.data(:);
        guess = make_guess(x, y, args, fitSn);
        [fitObj, r2] = do_fit(x, y, model, guess);
        fitObj.G = abs(fitObj.G); % Make sure linewidth is positive
        
        if ishandle(args.plots) || args.plots || (nargout == 0)
            
            make_plot(wf, fitObj, range, args.plots);
        end
        
        fitStruct =  struct();
        for iParam = 1:length(fitObj.parameters)
            fitStruct.(fitObj.parameters{iParam}) = fitObj.(fitObj.parameters{iParam});
            fitStruct.(['d', fitObj.parameters{iParam}]) = fitObj.(['d' fitObj.parameters{iParam}]);
        end          
    else
        dims = size(wf.data);
        r2 = zeros([dims(2:end),1]);
        nPoints = prod(dims(2:end));
        for point=1:nPoints
            y = wf.data(:,point);
            guess = make_guess(x, y, args, fitSn);
            try
                [fitObj(point), r2(point)] = do_fit(x, y, model, guess);
                fitObj(point).G = abs(fitObj(point).G); % Make sure linewidth is positive
            catch e
                fprintf('Error fitting: %s', e.getReport());
            end               
        end
        if length(dims) > 2
            fitObj = reshape(fitObj, dims(2:end));
        end
        
        if args.plots
            make_plots(wf, fitObj, range);
        end

        fitStruct =  struct();
        for point=1:nPoints
            if ~fitObj(point).isvalid()
                for iParam = 1:length(fitObj(1).parameters)
                    fitStruct(point).(fitObj(1).parameters{iParam}) = nan;
                    fitStruct(point).(['d', fitObj(1).parameters{iParam}]) = nan;
                end                         
                continue;
            end
            for iParam = 1:length(fitObj(point).parameters)
                fitStruct(point).(fitObj(point).parameters{iParam}) = fitObj(point).(fitObj(point).parameters{iParam});
                fitStruct(point).(['d', fitObj(point).parameters{iParam}]) = fitObj(point).(['d' fitObj(point).parameters{iParam}]);
            end          
        end
        if length(dims) > 2
            fitStruct = reshape(fitStruct, dims(2:end));
        end
    end
    
    for i=1:length(warnState)
        warning(warnState(i).state, warnState(i).identifier);
    end
end

function guess = make_guess(x, y, args, fitSn)
  
    if isempty(args.f)
        guessF = mean(x(y==max(y)));
    else
        guessF = args.f;
    end
    
    if isempty(args.g)
        guessG = (max(x)-min(x))/4;
    else
        guessG = args.g;
    end
    
    if fitSn
        guessSN = min(y);
        guessA = max(y)-guessSN;
        guess = [guessSN, guessA, guessG, guessF];
    else
        guessA = max(y)/args.sn - 1;
        guess = [guessA, guessG, guessF];
    end
end

function [fit, r2] = do_fit(x, y, model, guess)
    fit = nlfit.from(x, y, model, guess);
    r2 = 1 - sum(fit.residual.^2)/(length(y) - 4)/var(y);        
end

function make_plot(wf, fitObj, range, ax)
    modelX = linspace(min(range), max(range),500);

    if ~ishandle(ax)
        fig=ufigure('e3.lor_fit');
        clf(fig)
        ax = axes('Parent',fig);
    end
    hold(ax,'on')
    wf.plot(ax); %I changed this from plot(ax,wf) on 4/5/2017 - Emma Dowd
    plot(ax, modelX, fitObj.of(modelX),'Color','r','Linewidth',2)
    xlim(ax, sort(range));
end

function make_plots(wf, fitObj, range)
    modelX = linspace(min(range), max(range),500);
    
    fig = ufigure('e3.lor_fit');
    clf(fig);
        
    [~, steps] = size(wf.data);
    panels = ceil(steps / 16);

    for panel=1:panels
        clf(fig)    
        for y=1:4
            for x=1:4
                step = (panel-1)*16 + (y-1)*4 + x;
                if step > steps
                    break
                end
                ax=subplot(4,4,x+(y-1)*4,'Parent',fig);
                hold(ax,'on')
                wf.select(step).plot(ax);
                plot(ax,modelX, fitObj(step).of(modelX),'Color','r','Linewidth',2)
                xlim(ax,sort(range));
            end
            if step > steps
                break
            end
        end
        if step > steps
            break
        end
        pause
    end
end
