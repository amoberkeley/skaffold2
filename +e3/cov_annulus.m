function [P] = cov_annulus(varargin)
% adapted from https://www.mathworks.com/matlabcentral/answers/3540-ring-annulis-patch

    [ax,args,~] = axescheck(varargin{:});

    if isempty(ax)
        ax = gca();
    end
    
    p = inputParser;
    p.addRequired('outerCov', @(x) all(size(x) == [2,2]));
    p.addRequired('outerOffset', @(x) isvector(x) && numel(x) == 2);
    p.addOptional('innerCov', [], @(x) all(size(x) == [2,2]) || isempty(x));
    p.addOptional('innerOffset', [], @(x) (isvector(x) && numel(x) == 2) || isempty(x));
    p.addParameter('conf', 0.68, @(x) x > 0 && x < 1);
    p.addParameter('FaceColor',[1,.5,.5]);
    p.addParameter('LineStyle','none');
    p.addParameter('EdgeColor',[]);
    p.KeepUnmatched = 1;
    p.parse(args{:});
    args = p.Results;
    plotOpts = struct2params(p.Unmatched);

    % Compute quantile for the desired percentile
    k = sqrt(chi2inv(args.conf, 2)); % r is the number of dimensions (degrees of freedom)
    
    phase = linspace(0, 2*pi, 200);

    [outerVec, outerVal] = eig(args.outerCov); % Solve eigensystem

    % Make sure the matrix is positive definite
    if ~all(diag(outerVal) > 0)
        warning('The covariance matrices must be positive definite (there are non-positive eigenvalues!)')
        return
    end
    
    vOuter = k * [cos(phase'),sin(phase')] * sqrt(outerVal) * outerVec'; % Transformation
    X = args.outerOffset(1) + vOuter(:,1);
    Y = args.outerOffset(2) + vOuter(:,2);
    
    if isempty(args.EdgeColor)
        args.EdgeColor = args.FaceColor;
    end
    
    if isempty(args.innerCov)
        P = patch(ax,X,Y,args.FaceColor,'LineStyle',args.LineStyle,'EdgeColor',args.EdgeColor,plotOpts{:});
    else
        [innerVec, innerVal] = eig(args.innerCov); % Solve eigensystem

        % Make sure the matrix is positive definite
        if ~all(diag(innerVal) > 0)
            warning('The covariance matrices must be positive definite (there are non-positive eigenvalues!)')
            return
        end
        
        if isempty(args.innerOffset)
            args.innerOffset = args.outerOffset;
        end

        vInner = k * [cos(phase'),sin(phase')] * sqrt(innerVal) * innerVec'; % Transformation

        x = args.innerOffset(1) + vInner(:,1);
        y = args.innerOffset(2) + vInner(:,2);
        P = patch(ax,[x; X],[y; Y],args.FaceColor,'LineStyle','none',plotOpts{:});
        
        L(1) = line(ax,x,y,'Color',args.EdgeColor,'LineStyle',args.LineStyle);
        L(2) = line(ax,X,Y,'Color',args.EdgeColor,'LineStyle',args.LineStyle);
        setappdata(P,'Edges',L);  
        addlistener(P, 'EdgeColor', 'PostSet', @edgec);
        addlistener(P, 'LineStyle', 'PostSet', @lnstl);
    end
end

function [] = edgec(hProp,event)
    hObj = event.AffectedObject;
    c = get(hObj,'EdgeColor');

    edges = getappdata(hObj, 'Edges');
    set(edges,'Color',c);
%         for idx=1:length(edges)
%             if ~ishandle(edges(idx))
%                 continue;
%             end
%             set(edges(idx),'Color',c);
%         end    
end

function [] = lnstl(hProp,event)
    hObj = event.AffectedObject;
    st = get(hObj,'LineStyle');
    set(hObj,'LineStyle','none')
    edges = getappdata(hObj, 'Edges');
    set(edges,'LineStyle',st);
end