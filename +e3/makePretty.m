function makePretty(varargin)
    p = inputParser;
    p.addOptional('figure', gcf, @ishandle);
    p.addParameter('width', 3.75, @isnumeric);
    p.addParameter('height', 3, @isnumeric);
    p.addParameter('textFontSize', 10, @isnumeric);
    p.addParameter('axFontSize', 8, @isnumeric);
    p.addParameter('legendFontSize', 8, @isnumeric);
    p.addParameter('axLineWidth', 0.5, @isnumeric);
    
    p.addParameter('FontName', 'Helvetica', @ischar);
    p.addParameter('Latex', [], @(x) islogical(x) || numel(x) == 0);
    p.addParameter('TickLength', 0.05, @islogical);
    
    p.parse(varargin{:});
    args = p.Results;
    
    f = args.figure;
    
    % If the option ignoreLatex is set, don't change the
    % interpreter if it's already set to latex. Otherwise, set the
    % interpreter to tex.
    textProps = {'Units', 'normalized','Interpreter', 'tex',...
                   'FontSize', args.textFontSize, 'FontName', args.FontName};
               
    latexProps = {'units', 'normalized','Interpreter', 'latex',...
                        'FontSize', args.textFontSize};

    axProps = {'LineWidth',args.axLineWidth,'Layer','top',...
        'ClippingStyle','rectangle','Units','Inches','FontSize',args.axFontSize};
    
    cbProps = {'LineWidth',args.axLineWidth,...
        'Units','Inches','FontSize',args.axFontSize};
        
    if numel(args.Latex) == 0 % no change in interpreter      
    elseif args.Latex % Set interpreter to Latex
       axProps = [axProps,'TickLabelInterpreter','latex'];
       cbProps = [cbProps,'TickLabelInterpreter','latex'];
       textProps = latexProps;
    else % Set interpreter to tex
       axProps = [axProps,'TickLabelInterpreter','tex'];
       cbProps = [cbProps,'TickLabelInterpreter','tex'];
       latexProps = textProps;
    end
             
    for ax = findall(f, 'type', 'axes')'
        set(ax, axProps{:});
        for current = findall(ax, 'type', 'text')'
            if isLocked(current); continue; end
            if strcmpi('latex', get(current, 'interpreter'))
                set(current, latexProps{:});
            else
                set(current, textProps{:});
            end
        end
               
        e3.ticklength_absolute(ax, args.TickLength)
    end    
    
    for cb = findall(f, 'type', 'colorbar')'
        set(cb, cbProps{:});
        label = cb.Label;
        if strcmpi('latex', get(label, 'interpreter'))
            set(label, latexProps{:});
        else
            set(label, textProps{:});
        end
        
        e3.ticklength_absolute(cb, args.TickLength)
    end

    for current = findall(f, 'type', 'legend')'
        if strcmpi('latex', get(current, 'interpreter'))
            set(current, latexProps{:},'FontSize', args.legendFontSize);
        else
            set(current, textProps{:},'FontSize', args.legendFontSize);
        end
    end
    
    for current = findall(f, 'type', 'textbox')'
        if strcmpi('latex', get(current, 'interpreter'))
            set(current, latexProps{:});
        else
            set(current, textProps{:});
        end
    end        

%     options = struct('width', p.Results.width,...
%                      'height', p.Results.height,...
%                      'LockAxes',1,'Color','rgb','Resolution',300);
%     e4tools.display.applytofig(gcf,options);
    
end

function locked = isLocked(h)
    locked = strcmp(get(h, 'tag'), 'locked');
end
