function data_path = get_data_path_h5(date, runname)
    % Returns GageScope data path for requested date, runname, and channel
    if nargin < 3
        channel = 1;
    end
    init_skaffold;
    data_path = fullfile(dataRoot, date, sprintf('%s_gs', runname));
end