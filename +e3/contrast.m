function [fit, z, dN, outliers, agg] = contrast(nPoints,nLoops,z0,dca,varargin)
%CONTRAST Loads and fits contrast data
%
%Usage:
%  c = contrast(nPoints,nLoops,z0,dca)
%  [c, z, dn] = ...
%  ... = contrast(...,'key',value,...)
%
%Parameters:
%  nPoints - Integer number of unique cloud positions
%  nLoops - Number of repeated loops over these positions
%  z0 - The position (in um) of the first position
%  dca - The cavity-atomic-resonance detuning (in MHz)
%
%Keys (default first):
%  carrierFreq - 10e6 || double
%    Heterodyne carrier frequency (in Hz)
%  vcoBw - 20e3 || double
%    Low-pass filter BW (in Hz)
%  sweepRange - {[0 0.005] [0.007 0.012]} || cell array of 2-element arrays
%    List of time ranges corresponding to each sweep
%  scaleFun - @(x) -255-389*x || function handle
%    Function to convert voltage on second channel into VCO tuning
%    frequency
%  triggerPattern - [32, 1] || vector
%  positionFile - '+e3\positionsAll.txt' || path name
%    Location of file containing list of positions (in um)
%  g0 - 13.0 || double
%    Value of single-photon - single-atom coupling (MHz)
%  noLoad - false || true
%    If true and data have already been loaded, remake contrast plot
%    without reloading data
%  ignore - [] || array of deltaN indices to ignore
%
%Returns:
%  c - A nlfit object to the following model:
%        y0 + C*y0*sin(2*pi*k*z + phi0)
%      where the variables are the following:
%        y0 - mean dispersive shift (in MHz)
%        C - contrast (1 is maximum)
%        k - spatial overlap cyclic frequency (in um^-1)
%        phi0 - spatial overlap phase offset (in rad)
%  Na - Atom number
%  z - A list of spatial positions (in um)
%  dn - A corresponding list of dispersive shifts (in MHz)

persistent pathname
persistent runname
persistent data
persistent analysisPath

p = inputParser;
p.addRequired('nPoints',@(x) uint32(x)==x && isscalar(x) && x>4);
p.addRequired('nLoops',@(x) uint32(x)==x && isscalar(x) && x>= 1);
p.addRequired('z0',@(x) isscalar(x) && isreal(x));
p.addRequired('dca',@(x) isscalar(x) && isreal(x));
p.addParameter('carrierFreq',10e6,@(x) isscalar(x) && isreal(x));
p.addParameter('vcoBw',20e3,@(x) isscalar(x) && isreal(x));
p.addParameter('sweepRange',{[0.0001 0.0049], [0.0071, 0.0119]});
p.addParameter('scaleFun',@(x) -255-389*x,@(x) isa(x,'function_handle'));
p.addParameter('positionFile',['+e3',filesep,'positionsAll.txt'],@ischar);
p.addParameter('g0',13.0,@(x) isscalar(x) && isreal(x));
p.addParameter('triggerPattern',[16,1],@(x) isvector(x) && all(x>0));
p.addParameter('noLoad',false,@(x) isscalar(x) && (islogical(x) || isa(x,'DataModel')));
p.addParameter('ignore',[],@(x) all(uint32(x)==x));
p.addParameter('outlierThreshold',3.0,@(x) isscalar(x) && isreal(x));
p.addParameter('Na',[],@(x) isempty(x) || numel(x) == 2);
p.addParameter('verifiers',{verify.Range('deltaN', [0,-30])},...
    @(x) iscell(x) && all(cellfun(@(vr) any(strcmp(superclasses(vr), 'Verifier')), x)));
p.parse(nPoints,nLoops,z0,dca,varargin{:});
r = p.Results;

live = 1;

init_skaffold;

%Load the z positions
assert(logical(exist(r.positionFile,'file')),'skaffold:FileNotFound',...
    'Could not find positions file ''%s''.',r.positionFile)
z = load(r.positionFile);
z = z(:,1);                            %Extract first column
iz0 = find(abs(z-z0)==min(abs(z-z0))); %Find the closest z to z0
z = repmat(z(iz0:iz0+nPoints-1),1, nLoops).';

if isempty(pathname)
    pathname = fullfile(dataRoot, datestr(now(),'yyyy/mmm/dd'));
    if exist(pathname, 'dir') ~=7
        pathname = [pwd '\'];
    end
end

if isa(r.noLoad,'DataModel')
    data = r.noLoad;
    r.noLoad = true;
end

% Construct reporters
if isempty(data) || ~r.noLoad

    % Construct analyzers
    gsl = loader.Gagescope(pathname,'','triggerPattern',r.triggerPattern,...
        'skip',[],'defaultRange',[0 max(r.sweepRange{2})+1e-3]);

    %Reset the pathname
    pathname = gsl.get_path(1);

    analysisPath = strrep(gsl.get_root(),dataRoot,analysisRoot);
    if exist(analysisPath, 'dir') ~= 7
        mkdir(analysisPath);
    end
    runname = gsl.get_run();
    dataname = fullfile(analysisPath, [runname '.mat']);   
    
    %This is the reporter we'll use for iteration data
    pr = report.Figure('Contrast single-file plots', {...
        report.Axes({...
             report.Plot('empty_vco','lineProperties',{'Color','r'})...
             report.Plot('vco'),...
        },'xLabel','Time (s)','yLabel','Probe tuning (MHz)'),...                 
        report.Axes({...
             report.Plot('empty_hetMag','xField','empty_vco','lineProperties',{'Color','r'})...
             report.Plot('hetMag','xField','vco'),...
        },'xLabel','Probe tuning (MHz)','yLabel','Het. Mag.','axisCommand','tight');...
        report.Axes({...
             report.Plot('empty_hetMag','lineProperties',{'Color','r'})...
             report.Plot('hetMag'),...
        },'xLabel','Time (s)','yLabel','Het. Mag.'),...
        report.Axes({...
             report.Plot('empty_hetPhase','lineProperties',{'Color','r'})...
             report.Plot('hetPhase'),...
        },'xLabel','Time (s)','yLabel','Het. Phase')...
     },'axesProperties', {'FontSize',11,'Box','on'},'save',e3.get_fig_files('single'),'raise','update');

    plotAgg = report.Figure('Contrast aggregated plots', {...
         report.PointPlot('deltaN')...
    },'save',e3.get_fig_files('agg'),'raise','update');
   
    plotAtoms = report.Figure('Atom plots', {...
      report.PointPlot('tof.Na_2d','ylabel','N_a 2D','YLim',[0,5000]),...
      report.PointPlot('tof.Na_h','ylabel','N_a Horiz.','YLim',[0,5000]),...
      report.PointPlot('tof.Sx_1d','ylabel','S_x 1D','YLim',[0,8]),...
    },'figureProperties',{'PaperPosition',[0 0 11 8.5]},'save',e3.get_fig_files('atoms'));

    %ahet loads and performs default analysis on heterodyne data
    ahet = analyze.Block({
    	analyze.Load(gsl, {'raw', 'source', 'timestamp'}, {'channel', 1}),...
    	analyze.RescaleHeterodyne('raw', 'transimpedance', 28.8e3, 'gain', 30.1, 'impedance',50),...
    	analyze.Heterodyne('raw', 'carrierFreq', r.carrierFreq, 'filterBw', r.vcoBw/2),...
    	analyze.Load(gsl, {'vco', 'vcoSource'}, {'channel', 2}, 'increment', true),...
    	analyze.RescaleVco('vco','bw',r.vcoBw, 'scaleFun', r.scaleFun),...
    	analyze.LorFit('vco', 'hetMag', 'sweepUpFit', 'range', r.sweepRange{1}),...
    	analyze.LorFit('vco', 'hetMag', 'sweepDownFit', 'range', r.sweepRange{2})...
    });

    adl = loader.Andor(imgRoot,'offset',0);
    tof = analyze.AbsorptionFit(adl,'timestamp','tof','cen',[320,28],'span',[54,22],...
        'pixel_size',2.86e-4,'cross_section',2.9e-9,'plot',true, 'threshold', 1000);
    
    %ap first runs ahet, then performs FFT, lock, and report anlyses
    %Load the with atoms trace
    ap = analyze.Block({...
        ahet,... % Load the atoms data
    	analyze.Namespace('empty', ahet),... %Load the empty cavity
        analyze.DispersiveShift({'sweepUpFit', 'sweepDownFit'},'deltaN'),...
        tof,pr,...
        report.Print('deltaN = %0.4f (%s)\n',{'deltaN', 'source'})
    });
    
    aggregator = aggregate.Block({
            aggregate.Verifier('valid',{...
                r.verifiers{:}...
            }),...
            aggregate.Collect('deltaN','if','valid'),...
            aggregate.Collect('tof'),...
            plotAtoms,plotAgg...
        });

    % Set up scanner
    scanner = Scanner(ap, nPoints, nLoops, aggregator, 'wait', true,...
        'loader', gsl, 'aggregateFile', dataname);
    
    agg = scanner.get_aggregate();
    agg.set('z', z);
    agg.set('dca', dca);
    agg.set('nPoints',nPoints);
    agg.set('nLoops',nLoops);
    
    % Analyze data
    [res, data, ~] = scanner.run();
    if res ~= Result.SUCCESS
        error('Analysis failed');
    end

end

data.set('z', z);
agg = data;

%% Fit contrast
dN = data.get('deltaN');

mask = true(size(dN));
mask(r.ignore) = false;

if ~isempty(r.Na)
    Na = data.get('tof.Na_2d');
    badNa = Na > max(r.Na) | Na < min(r.Na);
    mask(badNa) = false;
end

cmodel = @(C,Na,k,phi0,z) (Na + C*Na*sin(2*pi*k*z + phi0))*r.g0^2/dca/2;

cguess = [0.8 2000 1/4 1.0]; % for when the fit struggles, input own
fit = nlfit.from(z(mask), dN(mask), cmodel, cguess);

outliers = find(abs(dN - fit.of(z)) > r.outlierThreshold);

%% Make plot
fig = ufigure('Contrast plot');
clf(fig);

figSize = [6 4];
fig.Units = 'inches';
fig.Position(3:4) = figSize;

ax = axes('Parent',fig,'NextPlot','add');
zp = linspace(min(z(:)),max(z(:)),101);

grid on;

hFit = plot(ax, zp, fit.of(zp));
hData = plot(ax, z(mask), dN(mask),'ok','MarkerFaceColor',[0.5 0.5 1.0]);
hIgnore = plot(ax, z(~mask), dN(~mask), 'xr','MarkerSize',8,'LineWidth',1); 

hl = legend([hData, hFit], {sprintf('Data at \\Delta_{ca} = %0.0f GHz',dca/1e3),...
    sprintf('Fit: C = %4.2f \\pm %4.2f\n    Na = %4.0f \\pm %4.0f',...
        fit.C, fit.dC/2, fit.Na, fit.dNa/2)},'Location','southwest');
set(hl,'FontName','Consolas');
xlabel(ax, 'Position (\mum)');
ylabel(ax, 'Dispersive shift (MHz)');

%%

e3.save_fig(fullfile(analysisPath, runname),'agg',figSize,fig);
