function phi = phaseN(x, d, center)
% N-dimensional version of phase(x).  Unwraps phase along dimension d
    if nargin < 2 || isempty(d)
        d = find(size(x) > 1);
    end
    
    if nargin < 3
      center = false;
    end
    
    if isempty(d)
        d = 1;
    end

    phi = unwrap(angle(x), [], d);
    
    if center
        offset = 2*pi*round(nanmean(phi, d)/(2*pi));
        repsize = ones(1, ndims(x));
        repsize(d) = size(x,d);
        phi = phi - repmat(offset, repsize);
    end
end