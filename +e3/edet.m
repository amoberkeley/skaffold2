function [edet, emm, epath] = edet(Pcav,Pdet,PLO,Vbeat)
%EDET Utility function to calculate Heterodyne detection efficiency
%
%Usage:
%  e3.edet(Pcav,Pdet,PLO,Vbeat)
%  [edet, emm, epath] = ...
%
%Parameters:
%  Pcav - Probe power leaving cavity (in nW)
%  Pdet - Probe power measured at detector (in nW)
%  PLO - LO power at detector (in mW)
%  Vbeat - Heterodyne beat frequency amplitude (in mV)
%

    assert(Pcav > 1, 'Pcav should be given in nW');
    assert(Pdet > 1, 'Pdet should be given in nW');
    assert(Pcav > Pdet, 'Pcav must be greater than Pdet');
    assert(PLO > .5, 'PLO should be given in mW');
    assert(Vbeat > 1, 'Vbeat should be given in mV');

    epath = Pdet / Pcav;
    
    TI_gain = 28.8e3; % V / W
    
    emm = (Vbeat * 1e-3 /TI_gain)^2 * 2 / (PLO * 1e-3 * Pdet * 1e-9);

    cavity_losses = .31;
    dichroic_losses = 0.984; % 0.97 until 2019-08-28
    quantum_efficiency = .58; % 24Jan2018 - Changed to calibrated QE from shotnoise spectrum - JK
    
    edet = epath * emm * 1/2 * cavity_losses * dichroic_losses * quantum_efficiency;
    
    if nargout == 0
        fprintf('ePath: %.3f\n', epath);
        fprintf('eMM: %.3f\n', emm);
        fprintf('eDet: %.3f\n', edet);
    end
end