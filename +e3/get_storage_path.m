function storage_path = get_storage_path(date, file)
    init_skaffold;
    if exist('storageRoot','var') && ~isempty(storageRoot)
        storage_path = fullfile(storageRoot, date, file);
    else
        storage_path = file;
    end
end