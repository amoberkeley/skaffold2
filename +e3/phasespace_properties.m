function s = phasespace_properties(quad)
    s = struct();
    s.cov = cov(real(quad), imag(quad));

    s.semiMin = 4*min(eig(s.cov).^.5);
    s.semiMaj = 4*max(eig(s.cov).^.5);
    s.semiAvg = (s.semiMin + s.semiMaj)/2;

    s.ecc = sqrt(1-s.semiMin^2/s.semiMaj^2);            
end