%% Skaffold 2 analysis script
%
% Intended for this skaffold2 commit:
% 98650c1dfec5fbbca68c5133023702f4e3c461f7
%
% deltaPC = <0>MHz
% ateVolt = <8.225>V, imgVolt = <3.39>V, wavVolt =  <1.097>V
% fRF = <880>kHz
% lowEvap = <150>mV
% odtPwr = <6.6> V (initial)
% odtAPwr = <5.9> V
%
% Notes:
% Probe on resonance
%
 
date = '2014/Sep/30';
run = 'test1';
nPoints = 1;
tStart = 0.001;
tStop = 0.011;
edet = .080;
filesToSkip = []; 

%% Control Flags
single = 0; % Run analyzer on single iteration
reset = 1; % Reset analyzers and aggregation
reset_hard = 0; % Delete HDF5 file before resetting
live = 1; % Turn on to wait for additional data, off to exit on completion

%% Build file paths

nLoops = Scanner.SCAN_ALL;
filenumber = 1; %start at 1 to load all

pathname = e3.get_data_path(date, run);
storage = e3.get_storage_path(date, sprintf('%s.h5', run));
statename = sprintf('%s_state.mat', run);
dataname = sprintf('%s_%d_%dms.mat', run, round(tStart*1e3), round(tStop*1e3));
figfiles = {sprintf('%s_%d_%dms.png', run, round(tStart*1e3), round(tStop*1e3))};

init_skaffold; % Sets dataRoot via local config

%% Build Scanner 
if reset || reset_hard || single || ~exist(statename,'file')
    %% Construct reporters
    plotAgg = report.Figure('Aggregated plots', {...
          report.Plot('rawFft','xLabel','Frequency (Hz)','yLabel','Het PSD (W^2/Hz)',...
            'axesProperties',{'YLim',[0 2e-21],'XLim', 10e6 + [-180e3, 254e3 + 180e3]}),...
          report.Plot('deltaPc','xLabel','Loop','ylabel','\Delta_{pc} (Hz)',...
            'axesProperties',{'YLim', 0e3 + [-500e3, +500e3]},...
            'lineProperties',{'Marker','.','LineStyle','none'}),...
          report.Plot('nBar','xLabel','Loop','ylabel','nBar',...
            'axesProperties',{'YLim',[0,6]},...
            'lineProperties',{'Marker','.','LineStyle','none'}),...      
          report.Plot('dVbar','xLabel','Loop','ylabel','dVbar',...
            'axesProperties',{'YLim',[-7,0]},...
            'lineProperties',{'Marker','.','LineStyle','none'}),...         
        },'figureProperties',{'PaperPosition',[0 0 11 8.5]},...
        'update',true, 'save',figfiles);

    plotSingle = report.Figure('Single-file plots',{...
          report.Plot('hetMag','xLabel','Time (s)'),...
          report.Plot('vco','xLabel','Time (s)','axesProperties',{'YLim',[-390 -370]}),...
        },'figureProperties',{'PaperPosition',[0 0 11 8.5]}, 'update',true);

    %% Construct analyzers
    gsl = loader.Gagescope(pathname, filenumber,'dataRate',80e6,'skip',filesToSkip,...
        'defaultRange',[0, tStop + 1e-3],'triggerPattern',[32,1]);
    
    ahet = analyze.Block();
    ahet.add(analyze.Load(gsl, {'raw', 'source', 'timestamp'}, {'channel', 1}));
    ahet.add(analyze.RescaleHeterodyne('raw', 'transimpedance', 28.8e3, 'gain', 30.1, 'impedance',1e6));
    ahet.add(analyze.Load(gsl, {'vco', 'vcoSource'}, {'channel', 2}, 'increment', true));
    ahet.add(analyze.RescaleVco('vco','bw',20e3, 'scaleFun', @(x) -255.2-396*x));       

    % Only average last half of time window for VCO data, to be less
    % sensitive to transients in the sidelock at the beginning of the trace
    ahet.add(analyze.Mean('vco', 'vcoBar', 'range', [(tStart+tStop)/2,tStop]));
    ahet.add(analyze.Heterodyne('raw','carrierFreq',10e6,'filterBw',10e3));
    
    ahet.add(analyze.FFT('raw','rawFft','range', [tStart, tStop], 'type', 'powerDensity'));
    ahet.add(analyze.HetModSidelock('rawFft',{'deltaPc','nBar','sn'},...
        'fCar',10e6,'fMod',2.87e6,'kappa',2.15e6,'kappaN',1.82e6,'rbGain',1.14,...
        'snRange',[4e5 5.5e5],'edet',edet,'close',true));
    ahet.add(analyze.Slice('rawFft','rawFft', 10e6 + [-3.2e6, 3.2e6])); 

    ap = analyze.Block({...
        ahet,... %Load with atoms data
        analyze.Namespace('empty', ahet),...    %Load empty cavity data
        analyze.VcoShift('vcoBar','dVbar'),...  %Rest of the analysis
        analyze.FFT('qQuad',{'qQuadFft','qQuadPower'},'range', [tStart, tStop],'type',{'complex','power'}),...
        analyze.Slice('qQuadPower','qQuadPower',[0,400e3]),...
        analyze.Slice('qQuadFft','qQuadFft',[0,400e3]),...
        analyze.FFT('iQuad',{'iQuadFft','iQuadPower'},'range', [tStart, tStop],'type',{'complex','power'}),...
        analyze.Slice('iQuadFft','iQuadFft',[0,400e3]),...
        analyze.Slice('iQuadPower','iQuadPower',[0,400e3]),...        
        plotSingle,...
    });
    
    %% Construct Aggregators
    verifiers = {...
        verify.Range('vcoBar',-385 + [-100, 100]),...
        verify.Range('dVbar',[-7 -1]),...
        verify.Range('deltaPc',[-5000e3, 5000e3]),...
        verify.Range('nBar',[.5, 6]),...
    };

    aggregator = aggregate.Block({...
        aggregate.Verifier('valid',verifiers),...
    	aggregate.Collect({'deltaPc','empty_deltaPc',...
            'nBar','empty_nBar','vcoBar','dVbar'}),...
    	aggregate.Mean({'vco','rawFft','empty_rawFft','qQuad','iQuad','qQuadFft','iQuadFft','qQuadPower','iQuadPower'},'if','valid'),...
        plotAgg...
    });         

    %% Create Scanner
    scanner = Scanner(ap, nPoints, nLoops, aggregator,...
        'loader', gsl, 'wait', logical(live), 'state', statename, 'aggregateFile', dataname,...
        'storage', storage, 'exclude', {'raw','empty_raw'}, 'clearStorage', logical(reset_hard));
else
    %% Load saved Scanner
    scanner = Scanner.load_state(statename);
    gsl = scanner.get_loader();
    scanner.set_n_loops(nLoops);
    gsl.set_skip_files(filesToSkip);
end

%% Set aggregate constants fields
agg = scanner.get_aggregate();
agg.set('edet',edet);
agg.set('nPoints',nPoints);

%% Analyze data
[res, data, scan] = scanner.run();

if single
    fprintf('nbar = %0.2f\tDelta_pc = %0.0f kHz\tvcoBar = %0.2f\tdVbar = %0.2f\n',...
        data.get('nBar'),data.get('deltaPc')/1e3,data.get('vcoBar'),data.get('dVbar'));
end