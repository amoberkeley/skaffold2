function data_path = get_andor_path(date, runname)
    % Returns Andor image path for requested date and runname
    init_skaffold;
    data_path = fullfile(dataRoot, date, sprintf('%s_andor', runname));
end