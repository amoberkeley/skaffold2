function [ax, index] = gridplot(cols, rows, col, row, varargin)
%Helper function to place subplots in a 2-d grid
%
%Usage:
%  [ax, index, cols, rows] = e3.gridplot(cols, rows, col, row,'key',value,...)
%
%Arguments:
%  rows - Total number of rows
%  cols - Total number of columns
%  row - Current row index
%  col - Current column index
%
%Parameters (default first):
%  parent - gcf() || [figure handle]
%    Parent figure
%
%Returns:
%  ax - The handle of the subplot axes
%  index - The MATLAB index of the subplot

    p = inputParser;
    p.addRequired('cols',@(x) isscalar(x) && x > 0);
    p.addRequired('rows',@(x) isscalar(x) && x > 0);
    p.addRequired('col',@(x) isvector(x) && all(x > 0));
    p.addRequired('row',@(x) isvector(x) && all(x > 0));
    p.addParameter('parent', [], @(x) ishandle(x));
    p.addParameter('margins', {}, @(x) iscell(x));
    p.KeepUnmatched = 1;    
    p.parse(cols, rows, col, row, varargin{:});
    args = p.Results;
    extra = struct2params(p.Unmatched);

    if ~isempty(args.parent)
        hParent = args.parent;
    else
        hParent = gcf();
    end
    
    index = (row-1)*cols + col;
    if isempty(args.margins)
        ax = subplot(rows, cols, index, 'Parent', hParent, 'NextPlot', 'add', extra{:});
    else
        ax = subtightplot(rows, cols,index, args.margins{:}, 'Parent', hParent, 'NextPlot', 'add', extra{:});
    end
end