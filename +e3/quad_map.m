function [map, phaseList] = quad_map(iFFT, qFFT, varargin)
%Generates average quadrature map given single-shot, complex I and Q
%quadrature spectra
%
%Usage:
%  [map] = e3.quad_map(iFFT, qFFT, [phaseList],'key',value,...)
%
%Parameters:
%  iFFT - Single-shot complex FFTs of iQuad
%  qFFT - Single-shot complex FFTs of qQuad
%  phaseList (optional) - List of quadrature phases to calculate (in radians)
%
%Keys (default first):
%  range - [] || [fStart, fStop]
%    Frequency range to calculate
%  type - Output format, one of:
%    'complex' - Returns the complex scientific transform
%    'complexDensity' - Same as above, except normalized by the
%              square root of the Fourier bandwidth
%    'power' - Returns the square magnitude of the complex
%              transform
%    'powerDensity' - As above, but normalized by the Fourier
%              bandwidth
%
%Returns:
%  map - 2D quadrature map
%  phaseList - List of quadrature phases calculated
   
    p = inputParser;
    p.addRequired('iFFT',@(x) isa(x, 'Waveform'));
    p.addRequired('qFFT',@(x) isa(x, 'Waveform'));
    p.addOptional('phaseList',linspace(0,2*pi,100),@(x) isnumeric(x) && isvector(x));
    p.addParameter('range',[], @(x) isnumeric(x) && numel(x)==0 || numel(x)==2);
    p.addParameter('type','powerDensity',...
        @(x) any(strcmp(x,{'complex','power','complexDensity','powerDensity'})));        
    p.addParameter('plot', false, @(x) islogical(x) || ishandle(x));    
    p.parse(iFFT,qFFT,varargin{:});   
    args = p.Results;

    phaseList = args.phaseList;

    if ~isempty(args.range)
        [iStart, iStop] = iFFT.get_index_bounds(min(args.range), max(args.range));
    else
        iStart = 1;
        iStop = iFFT.length();
    end

    extraDims = repmat({':'}, 1, max(0,ndims(iFFT.data)-2));  
    map = Waveform(iFFT.x0+(iStart-1)*iFFT.dx,iFFT.dx,[]);
    
    % Index in reverse order, to allocate full map.data array in first iteration
    for iPhase = length(args.phaseList):-1:1
        theta = args.phaseList(iPhase);
        s = iFFT.data(iStart:iStop,:,extraDims{:})*cos(theta)...
            + qFFT.data(iStart:iStop,:,extraDims{:})*sin(theta);
        
        switch args.type
            case 'complexDensity'
                sr = s/sqrt(1/map.dx);                
            case 'complex'
                sr = s;
            case 'power'
                sr = s .* conj(s);
            case 'powerDensity'
                sr = s .* conj(s)*map.dx;
        end
        
        map.data(:,iPhase,extraDims{:}) = mean(sr,2);
    end
    
    if ishandle(args.plot) || args.plot || (nargout == 0)
        make_plot(map, phaseList, args.plot);
    end
end

function fig = make_plot(map, phaseList, ax)
    
    if ishandle(ax)
        cla(ax);
        hold(ax,'on');
    else
        fig = ufigure('Quadrature Map');
        clf(fig)
        ax = axes();
        hold(ax,'on');
    end
    
    f = map.get_x();    
    mapPlot = map.log();
    minRange = min(mapPlot.data(:));
    maxRange = max(mapPlot.data(:));

    e3.colorplot(ax, f(:), phaseList, squeeze(mapPlot.data)'); 
    
    caxis(ax, [minRange maxRange])
    xlim(ax, [min(f), max(f)]);
    ylim(ax, [min(phaseList), max(phaseList)]);
    ylabel(ax, 'Phase')
    xlabel(ax, 'Frequency (Hz)')
end