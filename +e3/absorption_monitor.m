
function data = absorption_monitor(varargin)

p = inputParser;
p.addParameter('files', {'andor.h5'}, @iscellstr);
p.addParameter('pos', 'cav', @(x) ismember(x, {'cav', 'evap', 'evap_part', 'op', 'wu', 'mot', 'umot', 'link4', 'link28'}));
p.addParameter('statename', '', @ischar);
p.addParameter('reset', false, @islogical);
p.addParameter('mlPath', 'E:\Machine Learning\exp_output.txt', @ischar);
p.parse(varargin{:});
r = p.Results;

init_skaffold;
init_absorption;

%files = {'andor.txt', 'andor-sig.txt', 'andor-ref.txt', 'andor-bkg.txt'};
files = r.files;
threshold = NaN;
axisRangeSx = [0 20];

switch r.pos
    case 'cav'
        global absorptionCenCav
        global absorptionSpanCav
        
        pixel_size = 2.86e-4; % at cavity
        cen = absorptionCenCav;
        span = absorptionSpanCav;
        axisRangeNa = 4000;
        axisRangeSx = [0 8];
        threshold = 1500;
        
    case 'op'
        global absorptionCenOP
        global absorptionSpanOP
        
        pixel_size = 1.46e-3;
        cen = absorptionCenOP;
        span = absorptionSpanOP;
        axisRangeNa = 3e6; %2.5e6;
        axisRangeSx = [30, 50];
        threshold = 1500;

    case 'wu'
        global absorptionCenWU
        global absorptionSpanWU
        
        pixel_size = 1.46e-3;
        cen = absorptionCenWU;
        span = absorptionSpanWU;
        axisRangeNa = 8e6;
        axisRangeSx = [0, 70];
        threshold = 1000;
        
    case 'evap'
        global absorptionCenEvap
        global absorptionSpanEvap
        
        pixel_size = 1.46e-3;
        cen = absorptionCenEvap;
        span = absorptionSpanEvap;
        axisRangeSx = [7, 12];
        axisRangeNa = 1e6;
        threshold = 900;

    case 'link4'
        global absorptionCenLink4
        global absorptionSpanLink4
        
        pixel_size = 1.46e-3;
        cen = absorptionCenLink4;
        span = absorptionSpanLink4;
        axisRangeSx = [5, 10];
        axisRangeNa = 7e5;
        
    case 'link28'
        global absorptionCenLink28
        global absorptionSpanLink28
        
        pixel_size = 2.86e-4;
        cen = absorptionCenLink28;
        span = absorptionSpanLink28;
        axisRangeSx = [5, 10];
        axisRangeNa = 7e5;
end

plotAtoms = report.Figure('Atom plots', {...
    report.LoopPlot('tof.Na_2d', 'ylabel', 'N_a 2D', 'YLim', [0, axisRangeNa], ...
    'LineProperties', {'LineStyle', '-', 'LineWidth', 3}), ...
    report.LoopPlot('tof.Na_sum', 'ylabel', 'N_a sum', 'YLim', [0, axisRangeNa], ...
    'LineProperties', {'LineStyle', '-', 'LineWidth', 3}), ...
    report.LoopPlot('tof.Sx_1d', 'ylabel', 'S_x 1D', 'yLim', axisRangeSx, ...
    'LineProperties', {'LineStyle', '-', 'LineWidth', 3}), ...
    ...report.Axes({...
    ...report.LoopPlot('tof.x0_2d', 'LineProperties', {'LineStyle', '-', 'LineWidth', 3}), ...
    ...report.LoopPlot('tof.z0_2d', 'LineProperties', {'LineStyle', '-', 'LineWidth', 3}), ...
    ...}, 'yLabel', 'position'), ...
    });

adl = loader.AndorMonitor(imgRoot, 'monitor', files);
tof = analyze.AbsorptionFit(adl, '', 'tof', 'cen', cen, 'span', span, ...
    'pixel_size', pixel_size, 'cross_section', 2.9e-9, 'plot', true, 'threshold', threshold);

ap = analyze.Block({...
    tof, ...
    report.Print('Na: %.3g, Sx: %.3g, Sz: %.3g\n', ...
    {'tof.Na_h', 'tof.Sx_1d', 'tof.Sz_1d'}), ...
    analyze.Lambda({'tof.Na_sum'}, 'cost', @(Na) -Na), ...
    report.Write(r.mlPath, 'cost = %f\n', {'cost'}), ...
    });

aggregator = aggregate.Block({
    aggregate.Collect('tof'), ...
    plotAtoms...
    });

% Set up scanner
if isempty(r.statename)
    scanner = Scanner(ap, 1, Scanner.SCAN_ALL, aggregator, 'wait', true, 'loader', adl);
elseif ~exist(r.statename, 'file') || r.reset
    scanner = Scanner(ap, 1, Scanner.SCAN_ALL, aggregator, 'wait', true, 'loader', adl, 'state', r.statename);
else
    scanner = Scanner.load_state(r.statename);
    scanner.set_n_loops(Scanner.SCAN_ALL);
end

% Analyze data
[~, data, ~] = scanner.run();

end
