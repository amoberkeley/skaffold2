function [fit, se, ci, r2, model] = lor_fit2(wf, varargin)
%Fits a lorentzian model to the waveform(s) in the first parameter.  
%
%  model = y0+A*G^2/4./((x-x0).^2 + G^2/4)
%
%If the Waveform is multidimensional, it will treat the first dimension as the frequency axis,
%and individually fit each row of the higher dimesions.  The results
%returned with have one fewer dimensions, with the shape of the second and
%higher dimesions of the original waveform.
%
%Usage:
%  [fitObj, fitStruct, r2] = e3.lor_fit(data,'key',value,...)
%
%Parameters:
%  data - Waveform containing spectrum to fit
%
%Keys (default first):
%  f - [] || double
%    Frequency guess.  Defaults to position of data maximum.
%  g - [] || double
%    Linewidth guess.  Defaults to 1/4 of the data range.
%  sn - double
%    Shot-noise level. If provided, this is excluded from the fit
%    parameters, and the amplitude is returned in units of SN.  Otherwise
%    the SN level will be extracted from the fit.
%  range - [] || [fStart, fStop]
%    Frequency range for fit
%  plot - boolean
%    Specify whether to plot the fit result over the data.  This defaults
%    to true for 1-d Waveforms when no output arguments are specified,
%    otherwise it defaults to false.  For multi-dimensional Waveforms, this
%    will show 'pages' of 20 fit results, 'pause'ing in between.
%
%Returns:
%  fitObj - An nlfit object
%  fitStruct - A struct array with the fit parameters
%  r2 - A measure of the goodness of fit (roughly the percentage of the
%       variance of the data which the fit accounts for).

    p = inputParser;
    p.addRequired('data',@(x) isa(x, 'Waveform'));
    p.addParameter('range', [], @(x) isnumeric(x) && numel(x)==0 || numel(x)==2);
    p.addParameter('f', [], @(x) all(x > 0) || isempty(x));
    p.addParameter('g', [], @(x) all(x > 0) || isempty(x));
    p.addParameter('sn', false, @(x) isscalar(x) && (x > 0 || x == false));
    p.addParameter('plot', false, @(x) islogical(x) || ishandle(x));
    p.addParameter('warn', false, @(x) islogical(x));
    p.addParameter('struct', false, @islogical);
    p.parse(wf, varargin{:});
    args = p.Results;
            
    if isempty(args.range)
        range = [wf.x0, wf.get_x_end()];
    else
        wf = wf.slice_x(min(args.range), max(args.range));
        range = args.range;
    end
    
    x = wf.get_x()';   
    
    warnState(1) = warning('query','stats:nlinfit:IllConditionedJacobian');
    warnState(2) = warning('query','stats:nlinfit:IterationLimitExceeded');
    warnState(3) = warning('query','stats:nlinfit:ModelConstantWRTParam');
    if ~(args.warn)
        warning('off','stats:nlinfit:IllConditionedJacobian')
        warning('off','stats:nlinfit:IterationLimitExceeded')
        warning('off','stats:nlinfit:ModelConstantWRTParam')
    end
    
    model = @lor_model;    
    labels = {'y0', 'A', 'G', 'x0'};
    
    if isvector(wf.data)
        
        fit = zeros([4, 1]);
        se = zeros([4, 1]);
        ci = zeros([2, 4]);
        
        y = wf.data(:);
        [guess, fixed, lb, ub] = make_guess(x, y, args);
        [fit(:,1), se(:,1), ci(:,:), r2] = do_fit(x, y, model, guess, fixed, lb, ub);
        
        if ishandle(args.plot) || args.plot || (nargout == 0)
            make_plot(x, y, fit, range, args.plot);
        end
    
    else
        dims = size(wf.data);
        nPoints = prod(dims(2:end));
        
        fit = zeros([4, dims(2:end)]);
        se = zeros([4, dims(2:end)]);
        ci = zeros([2, 4, dims(2:end)]);
        r2 = zeros([dims(2:end),1]);
        for point=1:nPoints
            y = wf.data(:,point);
            [guess, fixed, lb, ub] = make_guess(x, y, args);

            try
                [fit(:,point), se(:,point), ci(:,:,point), r2(point)] = do_fit(x, y, model, guess, fixed, lb, ub);
            catch e
                fprintf('Error fitting: %s', e.getReport());
            end               
        end
        if length(dims) > 2
            fit = reshape(fit, [size(fit,1),dims(2:end)]);
        end
        
        if args.plot
            make_plots(x, wf.data, fit, range);
        end

    end
        
    if args.struct            
        fit = params_to_struct(labels, fit, se);
    end
   
    for i=1:length(warnState)
        warning(warnState(i).state, warnState(i).identifier);
    end
end

function [yn, J] = lor_model(p, xn)
    [y0,A,G,x] = deal_num(p);
    
    g = G/2;
    dx = xn-x;
    
	yn = y0 + A./(1 + (dx/g).^2);
        
    if nargout > 1   % two output arguments, calculate Jacobian
        J = [...
            ones(numel(xn),1),... % dF/dy0
            1./(1 + (dx(:)/g).^2),... % dF/dA
            A.*g.*dx(:).^2./(g.^2 + dx(:).^2).^2,... % dF/dG
            2*A.*g.^2.*dx(:)./(g.^2 + dx(:).^2).^2,... % dF/dx
        ];  % Jacobian of the function evaluated at x
    end

end

function [guess, fixed, lb, ub] = make_guess(x, y, args)
    fixed = false(1,4);
    
    if isempty(args.f)
        guessF = mean(x(y==max(y)));        
%         guessF = mean(x);
    else
        guessF = args.f;
    end

    if isempty(args.g)
        guessG = (max(x)-min(x))/4;
    else
        guessG = args.g;
    end

    if islogical(args.sn) && ~args.sn
        sn = min(y);
    else
        sn = args.sn;    
        fixed(1) = true;
    end
    
    guessA = max(y(:)) - sn;
    
    guess = [sn, guessA, guessG, min(guessF)];
    lb = [];
    ub = [];
end

function [fit, stderr, ci, R2] = do_fit(x, y, model, guess, fixed, lb, ub)
    opts = optimoptions(@lsqcurvefit,'Jacobian','on','Display','off'); %,'Display','off','DerivativeCheck','on'
    [fit, stderr, ci, ~, R2, ~] = e3.lsqfit(x, y, model, guess, fixed, lb, ub, [], 0.95, opts);    
end

function make_plot(x, y, fit, range, ax)
    modelX = linspace(min(range), max(range),500);
  
    if ~ishandle(ax)
        fig=ufigure('e3.lor_fit2');
        clf(fig)
        ax = axes('Parent',fig);
    end
    hold(ax, 'on')
    
    plot(ax,x,y);
    plot(ax, modelX, lor_model(fit, modelX),'Color','r','Linewidth',2)
    xlim(ax, sort(range));
end

function make_plots(x, y, fit, range)
    modelX = linspace(min(range), max(range),500);
    
    fig = ufigure('e3.lor_fit2');
    clf(fig);
        
    [~, steps] = size(y);
    panels = ceil(steps / 16);

    for panel=1:panels
        clf(fig)    
        for iRow=1:4
            for iCol=1:4
                step = (panel-1)*16 + (iRow-1)*4 + iCol;
                if step > steps
                    break
                end
                ax=subplot(4,4,iCol+(iRow-1)*4,'Parent',fig);
                hold(ax,'on')
                plot(ax, x, y(:,step));
                plot(ax, modelX, lor_model(fit(:,step), modelX),'Color','r','Linewidth',2)
                xlim(ax,sort(range));
            end
            if step > steps
                break
            end
        end
        if step > steps
            break
        end
        pause
    end
end
      
function s = params_to_struct(labels, params, err)    
    dims = size(params);
    num = prod(dims(2:end));
    s = struct();
    for idx=1:length(labels)
        [s(1:num).(labels{idx})] = deal_num(params(idx,1:num));
        [s(1:num).([labels{idx}, '_err'])] = deal_num(err(idx,1:num));
    end
    if length(dims) > 2
        s = reshape(s, dims(2:end));
    end
end