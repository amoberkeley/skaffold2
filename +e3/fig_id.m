function h = fig_id(varargin)
    p = inputParser;
    p.addRequired('fig', @ishandle);
    p.addRequired('date', @isstr);
    p.addRequired('runname', @isstr);
    p.addParameter('position', [0.0, 0.0, 0.2, 0.3], @isnumeric);
    p.addParameter('size', 8, @isnumeric);
    p.KeepUnmatched = 1;    
    
    p.parse(varargin{:});
    r = p.Results;
    extra = struct2params(p.Unmatched);
    

    figtitle = sprintf('%s: %s',r.date,r.runname);
    
    h = annotation('textbox', r.position,'String',figtitle, 'FitBoxToText', 'on','LineStyle','none',...
         'VerticalAlignment','bottom','HorizontalAlignment','left', extra{:}, 'FontSize', r.size);
    
end
