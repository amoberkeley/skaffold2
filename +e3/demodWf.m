function out = demodWf(wf, f, bw, phi, t0)
    %DEMOD Demodulate a signal at a given frequency.

    if nargin < 3
        bw = f/2;
    end   
    if nargin < 4
        phi = 0;
    end
    if nargin < 5
        t0 = 0;
    end
    
    t = wf.get_x()' - t0;
    r = ceil(1/wf.dx/bw);

    carrierPhase = 2*pi*f*t + phi; % Phase evolution, offset by modulation phase
    demod = wf .* exp(1i * carrierPhase); % Demodulate heterodyne signal at sideband frequency
    out = demod.decimate(r); % Low pass filter and decimate to recover time-varying complex state of sideband
end



