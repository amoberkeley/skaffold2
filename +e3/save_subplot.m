function figfile = save_subplot( dataname, plotname, varargin )

    p = inputParser;
    p.addRequired('dataname',@ischar);
    p.addRequired('plotnames',@iscellstr);
    p.addOptional('size', [11 8.5], @(x) isnumeric(x) && numel(x)==0 || numel(x)==2);
    p.addOptional('fig', [], @(x) isempty(x) || (isscalar(x)&&ishandle(x)));
    p.addOptional('type','png',@ischar);
    p.addParameter('dpi', 200, @(x) isscalar(x) && x>0);
    p.addParameter('subdir', false, @islogical);    
    p.parse(dataname, plotname, varargin{:});
    args = p.Results;

    if isempty(args.fig)
        args.fig = gcf;
    end
    runname = strrep(args.dataname, '.mat', '');
        
    if args.subdir        
        assert(~isempty(args.plotname),'plotname must be provided for saving in a subdirectory!');
        if ~exist(runname, 'dir')
            result = mkdir(pwd(), runname);
            assert(result, 'Failed creating subdirectory for plots');
        end        
        
        figfile = fullfile(runname, args.plotname);
    else
        figfile = runname;
        if ~isempty(args.plotname)
            figfile = [figfile, '_', args.plotname];
        end        
    end    
    
    if strcmp(args.type,'epsc')
        ext = 'eps';
    else
        ext = args.type;
    end
    figfile = [figfile, '.', ext];

    fig = args.fig;
    
    hAxes = fig.Children;
    
    for idx=1:length(hAxes)
        tempFig = figure();
        tempAx = copyobj(hAxes(idx), tempFig);
        set(tempAx, 'Position', get(0, 'DefaultAxesPosition')); 
        save_fig(args.fig, figfile, args.size, args);
    end
end


function save_fig(fig, figfile, size, args)
    fprintf('Saving plot to file ''%s''\n', figfile);
    set(fig,'PaperUnits','inches','PaperSize',size,'PaperPosition',[0, 0, size])
    
    switch (args.type)
        case 'eps'
            print(fig, '-deps2', sprintf('-r%d',args.dpi), figfile);
            
        case 'epsc'
            print(fig, '-depsc2', sprintf('-r%d',args.dpi), figfile);

        case 'pdf'
            print(fig, '-dpdf', sprintf('-r%d',args.dpi), figfile);
            
        case 'png'
            print(fig, '-dpng', sprintf('-r%d',args.dpi), figfile);
            
        otherwise
            saveas(fig, figfile, args.type);
    end
end
