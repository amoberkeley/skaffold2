function [fit, ci, r2] = lor_fit_power(wf, varargin)
%Fits a lorentzian model to the waveform(s) in the first parameter.  
%
%  model = y0+A*G^2/4./((x-x0).^2 + G^2/4)
%
%If the Waveform is multidimensional, it will treat the first dimension as the frequency axis,
%and individually fit each row of the higher dimesions.  The results
%returned with have one fewer dimensions, with the shape of the second and
%higher dimesions of the original waveform.
%
%Usage:
%  [fitObj, fitStruct, r2] = e3.lor_fit(data,'key',value,...)
%
%Parameters:
%  data - Waveform containing spectrum to fit
%
%Keys (default first):
%  f - [] || double
%    Frequency guess.  Defaults to position of data maximum.
%  g - [] || double
%    Linewidth guess.  Defaults to 1/4 of the data range.
%  sn - double
%    Shot-noise level. If provided, this is excluded from the fit
%    parameters, and the amplitude is returned in units of SN.  Otherwise
%    the SN level will be extracted from the fit.
%  range - [] || [fStart, fStop]
%    Frequency range for fit
%  plot - boolean
%    Specify whether to plot the fit result over the data.  This defaults
%    to true for 1-d Waveforms when no output arguments are specified,
%    otherwise it defaults to false.  For multi-dimensional Waveforms, this
%    will show 'pages' of 20 fit results, 'pause'ing in between.
%
%Returns:
%  fitObj - An nlfit object
%  fitStruct - A struct array with the fit parameters
%  r2 - A measure of the goodness of fit (roughly the percentage of the
%       variance of the data which the fit accounts for).

    p = inputParser;
    p.addRequired('data',@(x) isa(x, 'Waveform'));
    p.addParameter('range', [], @(x) isnumeric(x) && numel(x)==0 || numel(x)==2);
    p.addParameter('sn', [], @(x) ~isempty(x) && isnumeric(x) && all(x > 0));
    p.addParameter('f', [], @(x) all(x > 0) || isempty(x));
    p.addParameter('g', [], @(x) all(x > 0) || isempty(x));
    p.addParameter('plots', false, @(x) islogical(x) || ishandle(x));
    p.addParameter('warn', false, @(x) islogical(x));
    p.parse(wf, varargin{:});
    args = p.Results;
    
    model = @lor_power_model;
            
    if isempty(args.range)
        range = [wf.x0, wf.get_x_end()];
    else
        wf = wf.slice_x(min(args.range), max(args.range));
        range = args.range;
    end
    
    x = wf.get_x()';   
    
    warnState(1) = warning('query','stats:nlinfit:IllConditionedJacobian');
    warnState(2) = warning('query','stats:nlinfit:IterationLimitExceeded');
    warnState(3) = warning('query','stats:nlinfit:ModelConstantWRTParam');
    if ~(args.warn)
        warning('off','stats:nlinfit:IllConditionedJacobian')
        warning('off','stats:nlinfit:IterationLimitExceeded')
        warning('off','stats:nlinfit:ModelConstantWRTParam')
    end
    
    opts = optimoptions(@lsqcurvefit,'Jacobian','on','Display','off'); %,'DerivativeCheck','on'
    if isvector(wf.data)
        y = wf.data(:);

        [guess, fixed, lb, ub] = make_guess(x, y, args);

        [fit, stderr, ci, ~, r2] = ...
            e3.lsqfit(x, y, model, guess, fixed, lb, ub, [], 0.95, opts);        
        
        if ishandle(args.plots) || args.plots || (nargout == 0)
            make_plot(x, y, fit, ci, model, range, args.plots);
        end
    
    else
        dims = size(wf.data);
        
        fit = nan([4, dims(2:end)]);
        stderr = nan([4, dims(2:end)]);
        ci = nan([2, 4, dims(2:end)]);
        r2 = nan([dims(2:end),1]);

        nPoints = prod(dims(2:end));
        for point=1:nPoints
            y = wf.data(:,point);
            [guess, fixed, lb, ub] = make_guess(x, y, args);
            try
                [fit(:,point), stderr(:,point), ci(:,:,point), ~, r2(point)] = ...
                    e3.lsqfit(x, y, model, guess, fixed, lb, ub, [], 0.95, opts);
            catch e
                fprintf('Error fitting: %s', e.getReport());
            end               
        end
        if length(dims) > 2
            fit = reshape(fit, [size(fit,1),dims(2:end)]);
        end
        
%         if args.plots
%             make_plots(wf, fitObj, range);
%         end

    end
    
    for i=1:length(warnState)
        warning(warnState(i).state, warnState(i).identifier);
    end
end


function [yn, J] = lor_power_model(p, xn)
    % p = [y0,P,G,x0]
    
    P = p(2);
    g = p(3)/2;
    dx = xn-p(4);
    
	yn = p(1) + P/pi./g./(1 + (dx/g).^2);
        
    if nargout > 1   % two output arguments, calculate Jacobian
        J = [...
            ones(numel(xn),1),... % dF/dy0
            1/pi./g./(1 + (dx(:)/g).^2),... % dF/dP
            P/2/pi.*(dx(:).^2 - g.^2)./(g.^2 + dx(:).^2).^2,... % dF/dG
            2/pi*P.*g.*dx(:)./(g.^2 + dx(:).^2).^2,... % dF/dx0
        ];  % Jacobian of the function evaluated at x
    end

end

function [guess, fixed, lb, ub] = make_guess(x, y, args)
    fixed = false(1,4);
    
    if isempty(args.f)
        guessF = min(x) + (max(x) - min(x))/2;
    else
        guessF = args.f;
    end

    if isempty(args.g)
        guessG = (max(x)-min(x))/4;
    else
        guessG = args.g;
    end

    sn = args.sn;    
    fixed(1) = true;

    dx = mean(diff(x));
    
    totalP = sum(y)*dx;
    totalSN = sn*length(y)*dx;
    guessP = totalP - totalSN;
    fixed(2) = true;
    
    guess = [sn, guessP, guessG, guessF];
    lb = [0, 0, 0, min(x)];
    ub = [inf, inf, (max(x)-min(x))/2, max(x)];
    
end

function make_plot(x, y, fit, ci, model, range, ax)
    modelX = linspace(min(range), max(range),500);

    if ~ishandle(ax)
        fig = ufigure('e3.lor_fit_power');
        clf(fig);
        ax = axes('Parent', fig);
    end
    hold(ax, 'on')
    plot(ax, x, y);
    plot(ax, modelX, model(fit, modelX),'Color','r','Linewidth',2)
    yl=ylim(ax);
    plot(ax, modelX, model(ci(2,:), modelX),':','Color','k','Linewidth',1)
    plot(ax, modelX, model(ci(1,:), modelX),':','Color','k','Linewidth',1)
    ylim(ax, yl);
    xlim(ax, sort(range));
end

function make_plots(wf, fitObj, range)
    modelX = linspace(min(range), max(range),500);
    
    fig=ufigure('e3.lor_fit');
    clf(fig);
        
    [~, steps] = size(wf.data);
    panels = ceil(steps / 16);

    for panel=1:panels
        clf(fig);    
        for y=1:4
            for x=1:4
                step = (panel-1)*16 + (y-1)*4 + x;
                if step > steps
                    break
                end
                ax=subplot(4,4,x+(y-1)*4,'Parent',fig);
                hold(ax,'on')
                plot(ax,wf.select(step));
                plot(ax,modelX, fitObj(step).of(modelX),'Color','r','Linewidth',2)
                xlim(ax,sort(range));
            end
            if step > steps
                break
            end
        end
        if step > steps
            break
        end
        pause
    end
end
