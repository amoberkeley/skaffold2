function m = mask_model(f, mask)

    function y = maskFunc(p, x)
        y = f(p,x);
        y = y(mask);
    end

    m = @maskFunc;

%     @(p, x) maskFunc(f(p, x));
%             maskFunc = @(x) x(dataMask);    % Input - (nDatum x nMech); Output - (nValidDatum x 1)
%             maskedModel = @(pn,x) maskFunc(model_simul(pn,x)); % Input - pn: (1 x nParams), x: (nDatum x 1); Output - (nValidDatum x 1)
end
