function [fit, ci, r2, hFig] = fit_asymmetry(wf, varargin)

    p = inputParser;
    p.addRequired('wf',@(x) isa(x, 'Waveform'));
    p.addParameter('carrier', 0, @(x) isscalar(x) && x > 0);
    p.addParameter('range', [], @(x) isnumeric(x) && numel(x)==2);
    p.addParameter('invert', false, @islogical);
    p.addParameter('conf', 0.68, @(x) isscalar(x) && x > 0 && x< 1);
    p.addParameter('f', [], @(x) all(x > 0) || isempty(x));
    p.addParameter('g', [], @(x) all(x > 0) || isempty(x));
    p.addParameter('sn', [], @(x) isnumeric(x) && numel(x) <= 2 && all(x > 0));
    p.addParameter('plot', false, @(x) islogical(x) || isgraphics(x,'figure') ||...
        (numel(x) == 2 && all(isgraphics(x,'axes'))));
    p.addParameter('struct', false, @islogical);
    p.addParameter('warn', false, @(x) islogical(x));
    p.parse(wf, varargin{:});
    args = p.Results; 
        
    warnState(1) = warning('query','stats:nlinfit:IllConditionedJacobian');
    warnState(2) = warning('query','stats:nlinfit:IterationLimitExceeded');
    warnState(3) = warning('query','stats:nlinfit:ModelConstantWRTParam');
    if ~(args.warn)
        warning('off','stats:nlinfit:IllConditionedJacobian')
        warning('off','stats:nlinfit:IterationLimitExceeded')
        warning('off','stats:nlinfit:ModelConstantWRTParam')
    end

    blueWf = wf.slice_x(args.carrier + args.range);
    blueX = blueWf.get_x() - args.carrier;
    
    redWf = wf.slice_x(args.carrier - args.range);
    redX = redWf.get_x() - args.carrier;
    
    fitN = min(redWf.length(), blueWf.length());
    x = [redX(1:fitN); blueX(1:fitN)]';
    
    labels = {'yR', 'yB', 'A', 'n', 'G', 'x0', 'sgn'};
            
    opts = optimoptions(@lsqcurvefit,'Display','off','Jacobian','on','MaxFunEvals',3000,'MaxIter',3000); %,'DerivativeCheck','on'    
    if isvector(wf.data)        
        fit = nan([7, 1]);
        stderr = nan([7, 1]);
        ci = nan([2, 7]);
        
        y = [redWf.data(1:fitN), blueWf.data(1:fitN)];

        [guess, fixed, lb, ub] = make_guess(x, y, args);

        [fit(:,1), stderr(:,1), ci(:,:), ~, r2] = e3.lsqfit(x, y, @asymmetry_model, guess,...
            fixed, lb, ub, [], args.conf, opts);
        if ishandle(args.plot) || args.plot || (nargout == 0)
            if isgraphics(args.plot,'figure')
                hFig = args.plot;
                clf(hFig);
                hAxes(1) = e3.gridplot(2, 1, 1, 1, 'Parent', hFig);
                hAxes(2) = e3.gridplot(2, 1, 2, 1, 'Parent', hFig);
            elseif all(isgraphics(args.plot,'axes'))
                hAxes = args.plot;
                hFig = hAxes(1).Parent;
            else
                hFig = figure;
                clf(hFig);
                hAxes(1) = e3.gridplot(2, 1, 1, 1, 'Parent', hFig);
                hAxes(2) = e3.gridplot(2, 1, 2, 1, 'Parent', hFig);
            end

            do_plot(hAxes, fit, stderr, x, y, args);    

            xlabel(hAxes(1),'Frequency (Hz)');
            xlabel(hAxes(2),'Frequency (Hz)');            
        end   

    else
        dims = size(wf.data);
        fit = nan([7, dims(2:end)]);
        stderr = nan([7, dims(2:end)]);
        ci = nan([2, 7, dims(2:end)]);
        r2 = nan([dims(2:end),1]);
        
        if ~isempty(args.plot) && isgraphics(args.plot,'figure')
            hFig = args.plot;
            clf(hFig);
        elseif args.plot
            hFig = figure();
        else
            hFig = -1;
        end
        
        nPoints = prod(dims(2:end));
        for point=1:nPoints
            
            y = [redWf.data(1:fitN,point), blueWf.data(1:fitN,point)];
            
            [guess, fixed, lb, ub] = make_guess(x, y, args);
            try
                [fit(:,point), stderr(:,point), ci(:,:,point), ~, r2(point)] = ...
                    e3.lsqfit(x, y, @asymmetry_model, guess, fixed, lb, ub, [], args.conf, opts);
                
                if isgraphics(hFig,'figure')
                    hAxes(1) = e3.gridplot(2, nPoints, 1, point, 'Parent', hFig);
                    hAxes(2) = e3.gridplot(2, nPoints, 2, point, 'Parent', hFig);                   
                    
                    do_plot(hAxes, fit(:,point), stderr(:,point), x, y, args);
                end                                    
            catch e
                fprintf('Error fitting: %s', e.getReport());
            end
        end
        
        if ~isempty(args.plot) && isgraphics(hFig,'figure')
            xlabel(hAxes(1),'Frequency (Hz)');
            xlabel(hAxes(2),'Frequency (Hz)');            
        end        
    end

    if args.struct            
        fit = params_to_struct(labels, fit, stderr);
    end
    
    for i=1:length(warnState)
        warning(warnState(i).state, warnState(i).identifier);
    end
end

function [guess, fixed, lb, ub] = make_guess(x, y, args)
    fixed = false(1,7);
    fixed(7) = true;
    
    if args.invert
        sign = -1;
    else
        sign = 1;
    end
    
    % guess = [yR,yB,A,n,G,x0]
    guess = [min(y), 1, 1, diff(args.range)/4, mean(args.range), sign];
    lb = [0, 0, 0, 0, 0, min(args.range), -1];
    ub = [inf, inf, inf, inf, diff(args.range)/2, max(args.range), 1];
       
    if ~isempty(args.f)
        guess(6) = args.f;
    end

    if ~isempty(args.g)
        guess(5) = args.g;
    end

    if ~isempty(args.sn)
        if isscalar(args.sn)
            guess(1) = args.sn;
            guess(2) = args.sn;       
        else
            guess(1:2) = args.sn(1:2);
        end
        fixed(1:2) = true; 
    end

end

function [yn, J] = asymmetry_model(p, xn)
    % p = [yR,yB,A,n,G,x0,sgn(Temp)]
    
    yR = p(1);
    yB = p(2);
    A = p(3);
    nR = p(4)+(p(7)>0);
    nB = p(4)+(p(7)<0);
    g = p(5)/2;
    dxR = xn(:,1) + p(6);
    dxB = xn(:,2) - p(6);
    
    yn = [...
        yR.*(1 + A.*nR./((dxR./g).^2 + 1)),...
        yB.*(1 + A.*nB./((dxB./g).^2 + 1))
    ];
        
    if nargout > 1   % two output arguments, calculate Jacobian
        J = [...
            [(1 + A.*nR./((dxR./g).^2 + 1)); 0*dxB],... % dF/dyR,
            [0*dxR; (1 + A.*nB./((dxB./g).^2 + 1))],... % dF/dyB,
            [yR.*nR./((dxR./g).^2 + 1); yB.*nB./((dxB./g).^2 + 1)],... % dF/dA
            [yR.*A./((dxR./g).^2 + 1); yB.*A./((dxB./g).^2 + 1)],... % dF/dn
            A*g*[yR*nR./(dxR .* ((g./dxR).^2 + 1)).^2; yB.*nB./(dxB .* ((g./dxB).^2 + 1)).^2],... % dF/dG
            2*A*g.^2*[-yR*nR./(dxR.^3 .* ((g./dxR).^2 + 1).^2); yB.*nB./(dxB.^3 .* ((g./dxB).^2 + 1).^2)]... % dF/dx0
        ];  % Jacobian of the function evaluated at x
    end
end

function do_plot(hAxes, fit, stderr, x, y, args)
    ax1 = hAxes(1);
    ax2 = hAxes(2);
    
    fitX = [...
        linspace(-max(args.range), -min(args.range), 1000);        
        linspace(min(args.range), max(args.range), 1000);
    ]';
    fitData = asymmetry_model(fit, fitX);

    hold(ax1, 'on');
    plot(ax1, x(:,1), y(:,1)/fit(1),'LineWidth',1,'Color',[0,0,1,.3]);
    plot(ax1, fitX(:,1), fitData(:,1)/fit(1),'LineWidth',2,'Color',[1,0,0,1]);
    xlim(ax1, fliplr(-args.range))
    ax1.YLim(1) = 0;
    ylabel(ax1,'Het. PSD (rel. SN)')


    hold(ax2,'on');
    plot(ax2, x(:,2), y(:,2)/fit(2),'LineWidth',1,'Color',[0,0,1,.3]);
    plot(ax2, fitX(:,2), fitData(:,2)/fit(2),'LineWidth',2,'Color',[1,0,0,1]);
    xlim(ax2, args.range)
    ax2.YLim(1) = 0;
    ax2.YLim(2) = max(ax1.YLim(2), ax2.YLim(2));
    ax1.YLim(2) = ax2.YLim(2);
    
    str = {...
        sprintf('n = %.2g \\pm %.2g (1-\\sigma interval)', fit(4), stderr(4));
        sprintf('A = %.2g \\pm %.2g', fit(3), stderr(3));
        sprintf('f/2{\\pi} = %.2g \\pm %.2g', fit(6), stderr(6));
        sprintf('G/2{\\pi} = %.2g \\pm %.2g', fit(5), stderr(5));
        };
    
    text(0.05, 0.05, str,'FontSize',9,'Parent',ax1,'Units','normalized','VerticalAlignment','bottom');
end

function s = params_to_struct(labels, params, err) 

    dims = size(params);
    num = prod(dims(2:end));
    s = struct();
    for idx=1:length(labels)
        [s(1:num).(labels{idx})] = deal_num(params(idx,1:num));
        [s(1:num).([labels{idx}, '_err'])] = deal_num(err(idx,1:num));
    end
    if length(dims) > 2
        s = reshape(s, dims(2:end));
    end
end
