function h = subLabel(varargin)
    p = inputParser;
    p.addRequired('ax', @ishandle);
    p.addRequired('str', @isstr);
    p.addParameter('offset', [-0.4 0.25], @isnumeric);
    p.KeepUnmatched = 1;    
    
    p.parse(varargin{:});
    r = p.Results;
    extra = struct2params(p.Unmatched);
    
    oldUnits = r.ax.Units;
    r.ax.Units = 'inches';
    
%     pos = r.ax.OuterPosition;
%     pos = pos + [r.offset, -r.offset];

    pos = r.ax.Position;
    pos = pos + [r.offset, 0 0];

    h = annotation('textbox', [0,0,1,1],'String',r.str,'LineStyle','none',...
         'VerticalAlignment','top','HorizontalAlignment','left','Units','inches', extra{:});
    h.Position = pos;
    
    r.ax.Units = oldUnits;
end
