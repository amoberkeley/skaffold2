function path = get_analysis_path(date)
    init_skaffold;
    path = fullfile(analysisRoot, date);
end