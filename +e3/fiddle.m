function [] = fiddle(varargin)

persistent pathname
persistent runname
persistent data
persistent analysisPath

%%
p = inputParser;
p.addParameter('carrierFreq', 10e6, @(x) isscalar(x) && isreal(x));
p.addParameter('vcoBw', 20e3, @(x) isscalar(x) && isreal(x));
p.addParameter('sweepRange', {[0.0001 0.0049], [0.0071, 0.0119]});
p.addParameter('deltaNRange', [-20, 1]);
p.addParameter('scaleFun', @(x) -254.9-395.2*x, @(x) isa(x, 'function_handle'));
p.addParameter('rawName', 'raw', @ischar);
p.addParameter('vcoName', 'vco', @ischar);
p.addParameter('mlPath', 'E:\Machine Learning\exp_output.txt', @ischar);
p.addParameter('wwwPlotFile', 'W:\internal\e3\cicero_feedback.png', @ischar);
p.parse(varargin{:});
r = p.Results;

init_skaffold;

%%
if isempty(pathname)
    pathname = fullfile(dataRoot, datestr(now(), 'yyyy/mmm/dd'));
    if exist(pathname, 'dir') ~=7
        pathname = [pwd '\'];
    end
end

%%
if isempty(data)
    gsl = loader.GageHDF5(pathname, '', 'skip', []);

    pathname = gsl.get_path(); % reset pathname

    analysisPath = strrep(gsl.get_root(), dataRoot, analysisRoot);
    if exist(analysisPath, 'dir') ~= 7
        mkdir(analysisPath);
    end
    
    runname = gsl.get_run();
    dataname = fullfile(analysisPath, [runname '.mat']);
    
    plotShifts = report.Figure('ML plots', {...
        report.LoopPlot('deltaN', 'ylabel', '\Delta_N', 'YLim', r.deltaNRange), ...
        }, 'save', r.wwwPlotFile);

    %ahet loads and performs default analysis on heterodyne data
    ahet = analyze.Block({
        analyze.RescaleHeterodyne(r.rawName, 'transimpedance', 28.8e3, 'gain', 30.1, 'impedance', 1e6), ...
        analyze.RescaleVco(r.vcoName, 'target', 'vcoSlow', 'bw', 20e3, 'scaleFun', @(x) -258.2-380.2*x), ...
        analyze.Heterodyne(r.rawName, {'', '', 'hetMag', 'hetPhase'}, ...
            'range', [min(r.sweepRange{1}), max(r.sweepRange{2})+0.5e-3], 'carrierFreq', 10e6, 'filterBw', 10e3, 'doRotation', true), ...
        analyze.LorFit('vcoSlow', 'hetMag', 'sweepUpFit', 'range', r.sweepRange{1}), ...
        analyze.LorFit('vcoSlow', 'hetMag', 'sweepDownFit', 'range', r.sweepRange{2}), ...
        });
    
    ap = analyze.Block({...
        analyze.LoadH5(gsl, {'source', 'timestamp'}), ...
        ahet, ... % Load with atoms data
        analyze.Namespace('empty', ahet), ... % Load empty cavity data
        analyze.DispersiveShift({'sweepUpFit', 'sweepDownFit'}, 'deltaN', 'errTarget', 'deltaNErr'), ...
        analyze.Lambda({'deltaN', 'deltaNErr'}, 'badShot', @(dN, dNErr) 1-1*(dN > r.deltaNRange(1) && dN < r.deltaNRange(2) && dNErr < 1)), ...
        report.Write(r.mlPath, 'cost = %f\nuncer = %f\nbad = %d\n', {'deltaN', 'deltaNErr', 'badShot'}), ...
        });
    
    aggregator = aggregate.Block({
        aggregate.Collect({'deltaN', 'deltaNErr', 'badShot'}), ...
        plotShifts, ...
        });

    scanner = Scanner(ap, 1, Scanner.SCAN_ALL, aggregator, 'wait', true, ...
        'loader', gsl, 'aggregateFile', dataname);
    
    [res, data, ~] = scanner.run();
    if res ~= Result.SUCCESS
        error('Analysis failed');
    end

end

end
