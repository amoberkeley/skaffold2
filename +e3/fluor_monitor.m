
function data = fluor_monitor(varargin)

p = inputParser;
p.addParameter('filePath','andorimg.txt',@(x)ischar(x));
p.addParameter('pos','cav',@(x) ismember(x,{'cav','evap','op','wu','mot','umot'}));
p.parse(varargin{:});
r = p.Results;

init_skaffold;

files = {'andorimg.txt','SigImg.txt'};

switch r.pos
    case 'cav'
        pixel_size = 2.86e-4; % at cavity
        cen = [333,18];
        span = [40,20];
        axisRange = 4000;
        
    case 'op'
        % Optical Pumping
        pixel_size = 1.46e-3;
        cen = [190, 170];
        span = [230, 195];
        axisRange = 12e6;

    case 'wu'
        % Evap Trap
        pixel_size = 1.46e-3;
        cen = [170, 180];
        span = [200, 175];
        axisRange = 10e6;
        
    case 'evap'
        % Evap Trap
        pixel_size = 1.46e-3;
        cen = [150, 228];
        span = [170, 78];
        axisRange = 1e6;
        
    case 'mot'
        % Evap Trap
        pixel_size = 1.46e-3;
        cen = [350, 100];
        span = [250, 199];
        axisRange = 1e6;
        files = {'andorimg.txt','SigImg.txt'};
    
    case 'umot'
        % Evap Trap
        pixel_size = 1.46e-3;
        cen = [100, 100];
        span = [200, 199];
        axisRange = 1e5;
        files = {'andorimg.txt','SigImg.txt'};
end

plotAtoms = report.Figure('Atom plots', {...
  report.LoopPlot('tof.Na_sum','yLim',[0, 4e7],...
  'ylabel','N_{sum}','LineProperties',{'LineStyle','-','LineWidth',3}),...
});

adl = loader.AndorMonitor(imgRoot,'monitor',files);
tof = analyze.FluorFit(adl,'','tof','cen',cen,'span',span,'plot',true);

ap = analyze.Block({...
    tof,...
    report.Print('Na: %.3g\n',{'tof.Na_sum'})
});

aggregator = aggregate.Block({
        aggregate.Collect('tof'),...
        plotAtoms...
    });

% Set up scanner
scanner = Scanner(ap, 1, Scanner.SCAN_ALL, aggregator, 'wait', true, 'loader', adl);

% Analyze data
[res, data, ~] = scanner.run();
