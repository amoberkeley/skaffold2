function [i] = imax(varargin)

[~, i] = max(varargin{:});

end