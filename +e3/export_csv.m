function export_csv( filename, varargin )

    T = table();
    
    len = 0;
    
    if numel(varargin)==1 && isstruct(varargin{1})
        s = varargin{1};
        names = fieldnames(s);
        for idx=1:length(names)
            name = names{idx};
            data = s.(name);
            
            if ~len
                len = numel(data);
            end
            
            assert(len == numel(data), ['All columns must have the same length.'...
                '  Column ''%s'' has %d rows, while previous columns had %d'],...
                name, numel(data), len);
            
            T.(name) = data(:);
        end
    else
        for index=1:2:numel(varargin)
            name = varargin{index};
            data = varargin{index+1};

            if ~len
                len = numel(data);
            end
            
            assert(len == numel(data), ['All columns must have the same length.'...
                '  Column ''%s'' has %d rows, while previous columns had %d'],...
                name, numel(data), len);
                        
            T.(name) = data(:);
        end
    end

    writetable(T, filename)
end