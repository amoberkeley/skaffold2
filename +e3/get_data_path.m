function data_path = get_data_path(date, runname, channel)
    % Returns GageScope data path for requested date, runname, and channel
    if nargin < 3
        channel = 1;
    end
    init_skaffold;
    data_path = fullfile(dataRoot, date, sprintf('%s_CH%02d', runname, channel), 'Folder.00001');
end