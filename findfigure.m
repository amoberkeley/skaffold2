function varargout = findfigure(name,varargin)
%FINDFIGURE Creates or finds a figure uniquely identified by name
%
%Usage:
%  findfigure(name)
%       If the figure named already exists, returns handle.
%       Otherwise, creates the figure using default values.
%  findfigure(name,'PropertyName',PropertyValue,...)
%       Sets the given figure properties.
%  [h,existed] = findfigure(...)
%       Returns a handle to the figure in h.  existed is true or false
%       depending on whether the figure was in existence before execution.
%
%Provides a way to uniquely specify which figures to use from code, so that
%only that code bit overwrites the figure (usually accomplished with
%something like "figure(487)", which is a bit of a hack)
%
%Example:
%  h = ufigure('myfunction output') creates a new figure that is unlikely
%  to be overwritten by any function other than myfunction

if ~exist('name')
    hFind = findobj('-property','Name');
    names = get(hFind,'Name');
    ind = find(~cellfun(@isempty,names));
    varargout = {{names{ind}} hFind(ind)};
else
    hFind = findobj('Name',name);

    existed = false;
    if isempty(hFind)
        h = figure(varargin{:});
        set(h,'Name',name);
    else
        h = hFind(1);
        existed = true;
        if ~isempty(varargin)
            set(h,varargin{:});
        end
    end

    switch nargout
        case 0
            varargout = {};
        case 1
            varargout = {h};
        otherwise
            varargout = {h existed};
    end
end