function cicero_feedback_sweeps(varargin)

p = inputParser;
p.addParameter('pathname', '', @ischar);
p.addParameter('pause', 1, @(x) isscalar(x) && isreal(x) && x > 0);
p.addParameter('stateFile', 'cicero_feedback_state.mat', @ischar);
p.addParameter('overrideFile', 'C:\Documents and Settings\E3\My Documents\cicero_override.txt', @ischar);
p.addParameter('dataRoot', 'E:\Data\', @ischar);
p.addParameter('analysisRoot', 'E:\Analysis\', @ischar);
p.addParameter('logFile', '%s_feedback.log', @ischar);
p.addParameter('plotFile', '%s_feedback.png', @ischar);
p.addParameter('wwwPlotFile', 'W:\internal\e3\cicero_feedback.png', @ischar);
p.addParameter('showGui', true, @islogical);
p.parse(varargin{:});
r = p.Results;
    
%% Control parameters

% Manually set pathname (comment out for prompt)
% date = '2019/Mar/05';
% run = 'run1';
% r.pathname = e3.get_data_path(date, run);

% Resets skaffold analyzers, and triggers reanalysis of most recent
% 'startLoops' loops.  Necessary for any changes to sideband fit
% parameters to take effect.
resetAnalyzers = 1;

% Resets PID loops. Happens automatically when data path changes.  
% Otherwise, can tune all lock parameters without resetting.
resetLoops = 0;
       
%% Lock Parameters
deltaPcEnable = true;
deltaPcParameters = {...
    'setPoint', -2.15e6 / sqrt(3), ...
    'initial', 3.75, ...
    'range', [3.45 3.95], ...
    'gain', -5e-8, ...
    'tInt', 20};

sidebandEnable = false;
sidebandFmParameters = {...
    'setPoint', 129e3, ...
    'initial', 5.9103, ...%6.3428, ...
    'range', 6 + [-0.8, 0.8], ...
    'gain', 0.1 * -0.0758e-3, ...%0.18 * -0.0758e-3, ...
    'tInt', 30};

sweepEcEnable = false;
sweepEcParameters = {...
    'setPoint', -382.5, ...
    'initial', 3.2, ...
    'range', 3.2 + [-0.4, 0.4], ...
    'gain', 0.1 * -0.0758e-3, ...
    'tInt', 30};
        
%% Sideband fit parameters
sidebandAverage = 20; % number of previous shots to use in weighted, moving average
minAvg = 5; % Minimum number of interations to average for sideband fit
sidebandGuessFm = 129e3;
sidebandGuessGm = 4e3;
sidebandRange = sidebandGuessFm + [-5e3, 5e3]; % frequency range to use in sideband fit.  Should exclude other oscillators
plotRange = sidebandGuessFm + [-22e3, 22e3];

suppressDrive = []; % suppress_sidebands(driveFreq, driveRep, sidebandRange);

edet = .073;
nPoints = 1; % Doesn't currently support nPoints ~= 1
startLoops = sidebandAverage;
threshold = 0.3;

tDeltaPC = [0.018, 0.048];
tSidebandFm = [0.0065, 0.0115];

sweepStartStop = [0e-3, 5e-3];
tSweepRange = {sweepStartStop, 7e-3 + sweepStartStop};
        
%% Loop and Analyzer Methods
function pids = initialize_loops()
    % Create new FeedbackLoop objects
    pids = struct('deltaPc', 0, 'sidebandFm', 0, 'sweepEc', 0);
    if deltaPcEnable
        pids.deltaPc = FeedbackLoop('empty_deltaPc', 'slSetpoint', ...
            deltaPcParameters{:}, ...
            'tSample', 30, ... % Sampling interval (sequences length) do not change
            'tBW', 240);
    end

    if sidebandEnable
        pids.sidebandFm = FeedbackLoop('sidebandFm', 'odtPwr', ...
            sidebandFmParameters{:}, ...
            'tSample', 30, ... % Sampling interval (sequences length) do not change
            'tBW', 300);
    end
    
    if sweepEcEnable
        pids.sweepEc = FeedbackLoop('empty_freqSweepMean', 'probeFreqEC', ...
            sweepEcParameters{:});
    end
end


function tune_loops()
    % Update feedback parameters for FeedbackLoops, without reseting
    % history
    if deltaPcEnable
        state.pids(1).deltaPc.tune(deltaPcParameters{:});
    end
    if sidebandEnable
        state.pids(1).sidebandFm.tune(sidebandFmParameters{:});
    end
    if sweepEcEnable
        state.pids(1).sweepEc.tune(sweepEcParameters{:});
    end
end

function [scanner, gsl] = build_analyzers(filenumber, skipFiles)
    % Construct analyzers
    gsl = loader.GageHDF5(state.pathname, filenumber, 'skip', skipFiles);

    ahet = analyze.Block();
    
    ahet.add(analyze.RescaleHeterodyne('raw', 'transimpedance', 28.8e3, 'gain', 30.1, 'impedance', 1e6));
    ahet.add(analyze.FFT('raw', 'rawFft', 'range', tDeltaPC, 'type', 'powerDensity'));
    ahet.add(analyze.HetModSidelock('rawFft', {'deltaPc', 'nBar', 'sn'}, ...
        'fCar', 10e6, 'fMod', 2.87e6, 'kappa', 2.15e6, 'kappaN', 1.82e6, 'rbGain', 1.14, ...
        'snRange', [3.5e5 4.5e5], 'edet', edet, 'close', true));
    ahet.add(analyze.RescaleVco('vco', 'target', 'vcoSlow', 'bw', 20e3, 'scaleFun', @(x) -254.9-395.2*x));
    ahet.add(analyze.RescaleVco('vco', 'bw', 1e6, 'scaleFun', @(x) -254.9-395.2*x));

    if sweepEcEnable
        ahet.add(analyze.Heterodyne('empty_raw', {'', '', 'hetMagSweep', ''}, ...
            'range', [min(tSweepRange{1}), max(tSweepRange{2})+0.5e-3], ...
            'carrierFreq', 10e6, 'filterBw', 10e3, 'doRotation', false));
        ahet.add(analyze.LorFit('vcoSlow', 'hetMagSweep', 'sweepUpFit', 'range', tSweepRange{1}));
        ahet.add(analyze.LorFit('vcoSlow', 'hetMagSweep', 'sweepDownFit', 'range', tSweepRange{2}));
        ahet.add(analyze.Lambda('sweepUpFit', 'freqSweepUp', @(fobj) fobj.('x0'))); 
        ahet.add(analyze.Lambda('sweepDownFit', 'freqSweepDown', @(fobj) fobj.('x0'))); 
        ahet.add(analyze.Lambda({'freqSweepUp', 'freqSweepDown'}, 'freqSweepMean', @(f1, f2) 0.5 * (f1 + f2)));
    end
    
    analyzer = analyze.Block({ ...
        analyze.LoadH5(gsl, {'source', 'timestamp'}), ...
        ahet, ...
        analyze.Namespace('empty', ahet), ...
        });

    if sidebandEnable
        analyzer.add(analyze.Heterodyne('raw', 'carrierFreq', 10e6, 'filterBw', 10e3));
        analyzer.add(analyze.FFT('qQuad', 'qQuadPower', 'range', tSidebandFm, 'type', 'powerDensity'));
        analyzer.add(analyze.FFT('iQuad', 'iQuadPower', 'range', tSidebandFm, 'type', 'powerDensity'));            
    end

    verifiers = {...
        verify.Range('nBar', [0.1, 100]), ...
    };

    if deltaPcEnable
        analyzer.add(analyze.Lambda('empty_vco', 'vcoMin', @(vcoWf) min(vcoWf.slice_x(tDeltaPC).data)));
        analyzer.add(analyze.Lambda('empty_vco', 'vcoMax', @(vcoWf) max(vcoWf.slice_x(tDeltaPC).data)));
        verifiers{end+1} = verify.Range('vcoMin', [-390, -375]);
        verifiers{end+1} = verify.Range('vcoMax', [-390, -375]);
    end

    if sweepEcEnable
        verifiers{end+1} = verify.Range('empty_freqSweepUp', [-400, -360]); % TODO: tune
        verifiers{end+1} = verify.Range('empty_freqSweepDown', [-400, -360]); % TODO: tune
    end

    aggregator = aggregate.Block({...
        aggregate.Verifier('valid', verifiers), ...
        aggregate.Collect({'empty_deltaPc'}, 'if', 'valid'), ...
    });    

    if sidebandEnable
        aggregator.add(aggregate.Memory('qQuadPower', 'qQuadPowerCache', sidebandAverage, 'if', 'valid'));
        aggregator.add(analyze.FitSidebands('qQuadPowerCache', {'sidebandFm', 'sidebandGm', 'sidebandR2'}, 'threshold', threshold, ...
            'guessFm', sidebandGuessFm, 'guessGm', sidebandGuessGm, 'range', sidebandRange, ...
            'plot', true, 'plotRange', plotRange, 'suppressDrive', suppressDrive, ...
            'method', 'adaptive', 'minAvg', minAvg, 'if', 'valid'));
        aggregator.add(aggregate.Collect({'sidebandFm', 'sidebandR2'}, 'if', 'valid'));
    end
    
    if sweepEcEnable
        aggregator.add(aggregate.Collect({'empty_freqSweepDown', 'empty_freqSweepUp', 'empty_freqSweepMean'}, 'if', 'valid'));
    end

    scanner = Scanner(analyzer, nPoints, Scanner.SCAN_ALL, aggregator, 'loader', gsl, 'showGui', false);
end

function suppress = suppress_sidebands(driveFreq, driveRep, sidebandRange)
    suppress = driveFreq + [-50, 50];  % on-resonance drive frequency band to be suppressed in fits
    nSuppress = floor(diff(sidebandRange)/(2*driveRep));
    for index=1:nSuppress
       suppress = [
            suppress;...
            driveFreq + index*driveRep + [-50, 50];... % drive repitition sidebands to be suppressed in fits
            driveFreq - index*driveRep + [-50, 50];...           
        ];
    end        
end

%% Initialize UI
global skaffoldInterrupt
skaffoldInterrupt = false;
inLoop = true;

if r.showGui
    % Create GUI with stop button, which sets interrupt flag
    gui = stopGui();
else
    gui = [];
end

% Initiailze state variables
% State variables
state = struct('pathname', '');

%% BEGIN MAIN SCRIPT

% Load state from stateFile
if exist(r.stateFile, 'file')
    state = load(r.stateFile);
end

% Prompt user for data path
if isempty(r.pathname)
    % Use path dialog to get pathname
    pathname = pathname_dialog(state.pathname);
    if ~ischar(pathname) || isempty(pathname)
        return;
    end
else
    pathname = r.pathname;
end

% If selected pathname differs from saved state, force reset
if ~strcmp(pathname, state.pathname)
    resetAnalyzers = 1;
    resetLoops = 1;
    state.pathname = pathname;

    % Split parts of data path, and build target paths
    parts = regexpi(state.pathname, ['^(.+)\' filesep '(.+)_gs\' filesep '?'], 'tokens');
    state.dataPath = parts{1}{1};
    state.run = parts{1}{2};

    state.analysisPath = strrep(state.dataPath, r.dataRoot, r.analysisRoot);
    % Auto-create analysis directory, if it doesn't exist
    if exist(state.analysisPath, 'dir') ~= 7
        mkdir(state.analysisPath);
    end        

    state.plotFile = fullfile(state.analysisPath, sprintf(r.plotFile, state.run));       
    state.logFile = fullfile(state.dataPath, sprintf(r.logFile, state.run));
end

% Initialize new PID loops, or tune existing ones if no reset
if resetLoops || ~isfield(state, 'pids') || isempty(state.pids)
    state.pids = initialize_loops();        
else
    tune_loops();
end

if resetAnalyzers || ~isfield(state, 'scanner') || isempty(state.scanner)
    % Only read most recent 10 files, if initializing new scanner
    currentFile = e3.count_sources(state.pathname, ...
        'wildcardPattern', 'iteration*.h5', 'pattern', 'iteration(\d*).h5', 'existPattern', 'iteration%05d.h5');
    state.startFile = max(0, currentFile - nPoints*startLoops);

    fprintf('Starting at file AS_CH01-%05d.sig\n', state.startFile+1);

    numFiles = currentFile - state.startFile;
    nLoops = floor(numFiles/(nPoints));
    fprintf('Processing previous %d loops\n', nLoops);

    [state.scanner, state.gsl] = build_analyzers(state.startFile+1, []);
    state.scanner.run();        
    state.lastFile = state.gsl.fileNumber;

    update_locks();

    save_state();
end

update_plots();

%% Main Program loop    
fprintf('Entering main loop\n');

while ~skaffoldInterrupt
    if ~state.gsl.has_data()
        % If loader is set, and currently out of data, flush
        % the scan file to disk, then wait for new data.
        fprintf('Waiting for data... ');

        % Save state of Scanner
        save_state();

        while ~state.gsl.has_data() && ~skaffoldInterrupt
            pause(r.pause);
        end
        fprintf('processing file ''%s''\n', state.gsl.get_source());
    end

    if skaffoldInterrupt
        fprintf('Interrupted\n');
        break;
    end

    state.scanner.run();

    state.lastFile = state.gsl.fileNumber;

    if inLoop
        update_locks();
    end

    update_plots();

    save_state();
end

 %  if ishandle(gui)
 %      % Close 'stop' GUI
 %      close(gui);
 %  end

%% Methods    

function update_locks()
    agg = state.scanner.get_aggregate();
    iteration = floor(state.lastFile);
    logData = [];

 %      names = fieldnames(state.pids);
 %      for i = 1:length(names)
 %          lockName = names{i};
 %          lock = state.pids.(lockName);
 %      
 %          values = agg.get(lock.get_input_name());
 %          output = lock.update(values, iteration);
 %          fprintf('%2 = %0.4f\n', lock.get_output_name(), output);            
 %          cicero_override(lock.get_output_name(), output);
 %          logData = [logData, values(end), output];
 %      end

    if deltaPcEnable
        lockName = 'deltaPc';
        lock = state.pids.(lockName);

        values = agg.get(lock.get_input_name());
        output = lock.update(values, iteration);
        fprintf('%s = %0.4f, %s = %0.4f\n', lock.get_input_name(), values(end), lock.get_output_name(), output);            
        cicero_override(lock.get_output_name(), output);
        logData = [logData, values(end), output];
    end

    if sidebandEnable
        lockName = 'sidebandFm';
        lock = state.pids.(lockName);

        values = agg.get(lock.get_input_name());       
        r2 = agg.get('sidebandR2');
        output = lock.update(values, iteration);
        fprintf('%s = %0.4f, %s = %0.4f, %s = %0.4f\n', lock.get_input_name(), values(end), 'sidebandR2', r2(end), lock.get_output_name(), output);            
        cicero_override(lock.get_output_name(), output);   
        logData = [logData, values(end), output];
    end
    
    if sweepEcEnable
        lockName = 'sweepEc';
        lock = state.pids.(lockName);
        
        values = agg.get(lock.get_input_name());
        output = lock.update(values, iteration);
        fprintf('%s = %0.4f, %s = %0.4f\n', lock.get_input_name(), values(end), lock.get_output_name(), output);
        cicero_override(lock.get_output_name(), output);
        logData = [logData, values(end), output];
    end

    write_log(iteration, logData);
end

function fid = init_log()
    % Create log file, and write header to first line
    fid = fopen(state.logFile, 'a+t');
    if (fid >= 0) 
        fprintf(fid, 'timestamp, shot');

        names = fieldnames(state.pids);
        for i = 1:length(names)
            lock = state.pids.(names{i});
            if lock == 0
                continue;
            end
            fprintf(fid, ', %s, %s', lock.get_input_name(), lock.get_output_name());
        end
        fprintf(fid, '\n');
    end
end

function write_log(shot, logData)
    % Write data point to log file
    try
        if isempty(state.logFile)
            return;
        end

        if exist(state.logFile, 'file') ~= 2
            % If no log file, write header first
            fid = init_log();
        else
            fid = fopen(state.logFile, 'a+t');
        end

        if (fid >= 0)
            timestamp = datestr(clock, 31);
            logText = sprintf(', % .4g', logData);
            fprintf(fid, '''%s'', %d%s\n', timestamp, shot, logText);
            fclose(fid);
        end        
    catch e
        fprintf('Error writing log: %s', e.getReport());
    end        
end

function update_plots()
    h = ufigure('Feedback plots');
    names = fieldnames(state.pids);
    namesUsed = {};
    for i = 1:length(names)
        name = names{i};
        if state.pids.(name) ~= 0
            namesUsed{end+1} = name;
        end
    end
    
    nNamesUsed = length(namesUsed);
    for i = 1:nNamesUsed
        lockName = namesUsed{i};
        lock = state.pids.(lockName);
        if lock == 0
            continue;
        end

        subplot(nNamesUsed, 1, i);
        input = lock.get_input();     
        output = lock.get_output();    
        if isempty(input)
            continue;
        end

        [haxes, hLine1, hLine2] = plotyy(1:length(input), input, 1:length(output), output, 'plot', 'plot');
        set(hLine1, 'Marker', 'o')
        set(hLine1, 'LineStyle', '--')
        set(hLine2, 'Marker', 'o')
        set(hLine2, 'LineStyle', '--')
        ylabel(haxes(1), strrep(lock.get_input_name(), '_', ' ')) % label left y-axis
        ylabel(haxes(2), strrep(lock.get_output_name(), '_', ' ')) % label right y-axis
        xlabel(haxes(2), 'Iteration') % label x-axis
    end

    if ~isempty(state.plotFile)
        try
            saveas(h, state.plotFile, 'png');
        catch e
            fprintf('Error saving plot: %s', e.getReport());
        end            
    end

    if ~isempty(r.wwwPlotFile)
        try
            saveas(h, r.wwwPlotFile, 'png');
        catch e
            fprintf('Error saving plot: %s', e.getReport());
        end
    end        
end

function pathname = pathname_dialog(pathname)
    % Prompt user for data path
    if isempty(pathname)
        pathname = r.dataRoot;
    end

    [f, pathname] = uigetfile(['*', '.h5'], 'Choose GageScope data folder', [pathname, '']);

    if ischar(f)
        return;
    else  % Aborted UI
        pathname = false;
    end
end

function save_state()
    % Save script state to file
    save(r.stateFile, '-struct', 'state');
end

function cicero_override(name, value)
    % Write name = value update to cicero override file
    fid = fopen(r.overrideFile, 'at+');
    if (fid >= 0)
        fprintf(fid, '%s = %0.4f\n', name, value);
        fclose(fid);
    end
end

end