classdef Iterator < handle
    %Analyzes experimental parameter scans
    
    properties (Constant)
        SCAN_ALL = 0;
    end
    
    properties (SetAccess = protected)
        analyzer
        aggregator
        scan        
        readonly
        fields
        
        iterations
        loops
        points
        
        nPoints
        nLoops
        nIterations
    end
    
    methods
        function o = Iterator(aggregator, scan, varargin)
            %Create a new Iterator.
            %
            %Usage:
            %    o = Iterator(aggregator, scan, [points], [loops])
            %
            %Parameters:
            %  aggregator - Aggregator to apply each iteration.
            %  scan - DataScan object or path to HDF5 dataset
            %  points - Array of points to iterate over.  Defaults to all,
            %       if empty
            %  loops - Array of loops to iterate over.  Defaults to all, if
            %       empty
            %  readonly - Defaults true.  If true, opens scan readonly, and
            %       doesn't save any modifications to the individual
            %       iteration models.  If false, any changes to DataModels
            %       are saved back to the scan.
                        
            p = inputParser;
            p.addRequired('scan', @(x) isa(x,'DataScan') || ischar(x));
            p.addOptional('analyzer', [], @(x) isempty(x) || isa(x,'Analyzer'));            
            p.addOptional('aggregator', [], @(x) isempty(x) || isa(x,'Aggregator'));            
            p.addParameter('points', [], @(x) isvector(x) && all(x > 0));
            p.addParameter('loops', [], @(x) isvector(x) && all(x > 0));
            p.addParameter('iterations', [], @(x) (ismatrix(x) && islogical(x)) || (isvector(x) && all(uint32(x) == x)));
            p.addParameter('fields', {}, @iscellstr);
            p.addParameter('readonly', true, @islogical);
            p.parse(aggregator, scan, varargin{:});
            args = p.Results;
            
            o.analyzer = args.analyzer;
            o.aggregator = args.aggregator;
            o.readonly = args.readonly;
            o.fields = args.fields;
            
            if isa(args.scan, 'DataScan')
                o.scan = args.scan;
            elseif exist(args.scan, 'file') == 2
                o.scan = DataScan(args.scan, 'readonly', o.readonly, 'include', o.fields);
            else
                error('Skaffold:TypeError',...
                    'Iterator must be given either a DataScan or the path to an HDF5 dataset.');
            end
            
            o.nPoints = o.scan.nPoints;
            o.nLoops = o.scan.nLoops;
            o.nIterations = o.nLoops * o.nPoints;
            
            if ~isempty(args.iterations)
                if ~isempty(args.loops) || ~isempty(args.points)
                    warning('skaffold:Iterator',...
                        ['Iterator parameter ''iterations'' takes precedence over ''loops'' and ''points''.',...
                        'These parameters will be ignored']);
                end
                
                if islogical(args.iterations)
                    % Logical matrix provided for 'iterations'.  Ensure
                    % it's dimensions do not exceed the scan dimensions,
                    % and convert to a vector of linear indices.
                    
                    if any(size(args.iterations) > [o.nLoops, o.nPoints])
                        error('skaffold:Iterator',...
                            '''iterations'' dimensions exceed scan dimensions nLoops x nPoints = %d x %d.',...
                            o.nLoops, o.nPoints);
                    elseif any(size(args.iterations) < [o.nLoops, o.nPoints])
                        % Resize array to nLoops x nPoints
                        args.iterations(o.nLoops, o.nPoints) = false;
                    end
                                        
                    % Convert logical matrix to vector of linear indices.
                    o.iterations = find(args.iterations)';
                else
                    % Linear index vector provided for 'iterations'. Ensure
                    % no index value exceeds nIterations.
                    if any(args.iterations > o.nIterations)
                        error('skaffold:Iterator',...
                            'At least one value of ''iterations'' exceed scan dimensions nLoops x nPoints = %d', o.nIterations);
                    end
                    if iscolumn(args.iterations)
                        o.iterations = args.iterations';
                    else
                        o.iterations = args.iterations;
                    end
                end
            else
                % 'iterations' not specified, build list of iterations to
                % iterate from 'loops' and 'points', if provided, or
                % iterate all.
                
                if ~isempty(args.loops)
                    o.loops = args.loops;
                else
                    o.loops = 1:o.nLoops;
                end

                if ~isempty(args.points)
                    o.points = args.points;
                else
                    o.points = 1:o.nPoints;
                end
                
                % Build logical matrix of selected loops and points
                o.iterations = false(o.nPoints, o.nLoops);
                o.iterations(o.points, o.loops) = true;
                
                % Convert logical matrix to vector of linear indices
                o.iterations = find(o.iterations)';
            end
        end
        
        function [res, aggregate] = run(o, aggregate)
            if nargin < 2
                aggregate = DataFrame();
            end
            
            for iteration = o.iterations
                [loop, point] = ind2sub([o.nLoops, o.nPoints], iteration);

                model = o.scan.read_frame(loop, point);
                         
                if ~isempty(o.analyzer)
                    res = o.analyzer.run(model, loop, point);                
                end

                switch res
                    case Result.SUCCESS
                        if ~isempty(o.aggregator)
                            % Run aggregator on analyzed DataModel
                            o.aggregator.run(aggregate, loop, point, model);
                        end
                        if ~o.readonly
                           % Write any changes back to scan
                           o.scan.write_frame(loop, point, model);                            
                        end                                 
                        
                    case Result.SKIP                                        
                        if ~isempty(o.aggregator)
                            % Give aggregators a chance to cleanup, if
                            % skipping this iteration
                            o.aggregator.skip(aggregate, loop, point);
                        end
                        if ~isempty(o.scan)
                           % Write any changes back to the DataScan
                           o.scan.write_frame(loop, point, model);                            
                        end
                        % reset result, in case this is the last iteration
                        res = Result.SUCCESS; 

                    case Result.ABORT
                        if ~isempty(o.aggregator)
                            % Give aggregators a chance to cleanup, if
                            % skipping this iteration
                            o.aggregator.skip(o.aggregate, loop, point);
                        end
                        if ~isempty(o.scan)
                            o.scan.clear_frame(loop,point);
                        end
                        res = Result.SUCCESS; 
                        
                    case Result.FAIL
                        fprintf('Fatal error occured during analysis\n');
                        break
                        
                    case Result.OUT_OF_DATA
                        break
                end
            end
            
            if ~o.readonly
                % Flush DataScan buffers to disk
                o.scan.flush();
            end
            
            res = Result.SUCCESS;
        end
        
        function scan = get_scan(o)
            scan = o.scan;
        end
    end
end

