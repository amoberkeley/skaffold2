classdef DataScanFrame < DataFrame
    % Subclass of DataFrame which implements lazy-loading of data from a
    % DataScan, for efficient iteration of DataScan frames.  Also
    % implements 'pop_signature' to allow checking of Analyzer signatures
    % to avoid duplicate analysis.
    
    properties (SetAccess = protected)
        scan
        loop
        point
        changed
        signatures
        signatureIndex
    end

    methods
        function o = DataScanFrame(scan, loop, point, varargin)
            %Creates a new DataScanFrame
            %
            %Usage:
            %  o = DataScanFrame(scan, loop, point, ['Key', value],...)
           
            p = inputParser;
            p.addRequired('scan', @(x) isa(x, 'DataScan'));
            p.addRequired('loop',@(x) isscalar(x) && x >= 0);
            p.addRequired('point',@(x) isscalar(x) && x >= 0);
            p.addParameter('signatures',{}, @iscellstr);
            p.parse(scan, loop, point, varargin{:});
            args = p.Results; 
            
            o = o@DataFrame();
 
            o.scan = args.scan;
            o.loop = args.loop;
            o.point = args.point;
            o.changed = containers.Map();
            o.signatures = args.signatures;
            o.signatureIndex = 1;
        end

        function sig = pop_signature(o)
            if o.signatureIndex <= length(o.signatures)
                sig = o.signatures{o.signatureIndex};
                o.signatureIndex = o.signatureIndex + 1;
            else
                sig = '';
            end
        end
        
        function s = struct(o)
            o.load_all();
            s = struct@DataFrame(o);
        end
        
        function load_all(o)
            fieldnames = o.fields();
            for index = 1:length(fieldnames)
                field = fieldnames{index};
                o.get(field);
            end
        end
        
    % Abstract implementations:
        function value = get(o,field,~)
            parts = strsplit(field,'.');
            
            if (isfield(o.data, parts{1})) % Check if field is in local data cache
                value = o.data(1).(parts{1});
            elseif o.scan.has(parts{1}) % Field doesn't exist in local data, load from DataScan
                value = o.scan.read_field(o.loop, o.point, parts{1});
                o.data(1).(parts{1}) = value;
            elseif o.scan.is_excluded(parts{1}) % Field was excluded from DataScan, throw exception to allow Scanner to reload it
                error('skaffold:ExcludedField', field);
            else
                error('skaffold:ArgumentError', 'Unrecognized field ''%s''.', field);
            end
            
            if length(parts) > 1
                if ~isstruct(value)
                    error('skaffold:ArgumentError', 'Attempted to access sub-field of non-structure value ''%s''.', field);
                elseif ~isfield(value, parts{2})
                    error('skaffold:ArgumentError', 'Unrecognized sub-field ''%s''.', field);
                else
                    % Get struct array field as an array with the same shape as the struct
                    value = field2array(value, parts{2});
                end
            end            
        end
        
        function set(o,field,value)
            set@DataFrame(o, field, value);
            o.changed(field) = true;
        end
        
        function unset(o,field)
            unset@DataFrame(o, field);
            o.scan.clear_field(o.loop, o.point, field);
            o.changed(field) = false;
        end
        
        function test = has(o,field)
            parts = strsplit(field,'.');
            test = o.scan.has(parts{1});
        end
        
        function test = is_changed(o, field)
            test = o.changed.isKey(field) && o.changed(field);
        end
        
        function clear_changed(o)
            o.changed = containers.Map();
        end
        
        function flush_changes(o)
            o.scan.write_frame(o.loop, o.point, o);
            o.clear_changed();
        end
        
        function fieldnames = fields(o)
            fieldnames = union(fields@DataFrame(o), o.scan.fields());
        end
    end
        
    methods (Access = protected)
        function propgrp = getPropertyGroups(o)
            % TODO display fields in DataScan, that aren't cached in o.data
            if ~isempty(o.data)
                propgrp = matlab.mixin.util.PropertyGroup(o.data);
            else
                propgrp = matlab.mixin.util.PropertyGroup();
            end
        end
    end
end

