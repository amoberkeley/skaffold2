function hash = md5(string)
    persistent md;
    
    if isempty(md)
        import java.security.MessageDigest;
        md = MessageDigest.getInstance('MD5');
    end
    
    if isempty(string)
        hash = '';
    else
        hash = md.digest(uint8(string));
        hash = reshape(dec2hex(typecast(hash,'uint8'))',1,[]);    
    end
end