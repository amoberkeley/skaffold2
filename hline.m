function hLines=hline(varargin)
%Shortcut to plot a horizontal line(s).  
%
%Usage:
%  hLines = hline([ax],y,...)
%
%Parameters:
%  ax - Optional axis handle, if not provided defaults to current axis.
%  y - Scalar or vector of y-value(s)
%  ... - All additional parameters will be passed directly to 'plot'
%
%Returns:
%  hLines - Handles of created lines

    [ax,args,nargs] = axescheck(varargin{:});

    if isempty(ax)
        ax = gca();
    end
    
    y = args{1};
    if nargs > 1
        opts = args(2:end);
    else
        opts = {};
    end
    
    wasHeld=ishold(ax);
    hold(ax,'on');
    
    x=get(gca,'xlim');
    for idx=1:length(y)
        h(idx)=plot(ax, x, [y(idx) y(idx)], opts{:});
    end
    
    if ~wasHeld
        hold(ax,'off')
    end
    
%     set(h,'handlevisibility','off')

    listener = getappdata(ax, 'HLineListener');
    if isempty(listener)
        listener = addlistener(ax,'XLim','PostSet',@xLimListener);
        setappdata(ax, 'HLineListener',listener);
    end
    lines = getappdata(ax, 'HLines');
    if isempty(lines)
        lines = [];
    end
    lines = [lines, h];
    setappdata(ax,'HLines',lines);  
    
    if nargout
        hLines=h;
    end
end

function xLimListener(~,event)
    ax = event.AffectedObject;
    xl = get(ax, 'XLim');
    lines = getappdata(ax, 'HLines');
    invalid = false(size(lines));
    for idx=1:length(lines)
        if ~ishandle(lines(idx))
            invalid(idx) = true;
            continue;
        end
        set(lines(idx), 'XData', xl);
    end    
    setappdata(ax, 'HLines', lines(~invalid));
end
