function out = mat2code(arr, spec)

    if nargin < 2
        spec = '%.4g';
    end
    
    strArr = arrayfun(@(x) sprintf(spec, x), arr, 'UniformOutput', false);
    cellStr = join(join(strArr,', '), ';\n    ');
    str = ['[\n    ', cellStr{:}, '\n];\n'];
    fprintf(str)
    
    if nargout > 0
        out = str;
    end
end