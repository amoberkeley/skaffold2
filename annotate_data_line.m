function h = annotate_data_line(ax, type, xends, yends, varargin)
       
    [figX, figY, visible] = data2fig(ax, xends, yends);
    
    h = annotation(ax.Parent,type,figX,figY,'Visible',visible,'Units','normalized',varargin{:});
    setappdata(h, 'Coords', {xends, yends});

    ls = getappdata(ax, 'DataLineListener');
    if isempty(ls)
        ls = {};
        ls{1} = addlistener(ax,'XLim','PostSet',@resizeListener);
        ls{2} = addlistener(ax,'YLim','PostSet',@resizeListener);
        ls{3} = addlistener(ax,'Position','PostSet',@resizeListener);
%         ls{4} = addlistener(ax.Parent,'Position','PostSet',@resizeListener);
        setappdata(ax, 'DataLineListener',ls);
    end
    objs = getappdata(ax, 'DataLines');
    if isempty(objs)
        objs = [];
    end
    objs = [objs, h];
    setappdata(ax,'DataLines',objs);  
end

function [figX, figY, visible] = data2fig(ax, xends, yends)
    xl = ax.XLim;
    yl = ax.YLim;  
    if ax.XAxis.Scale == "log"
        xends = log10(xends);
        xl = log10(xl);
    end
    if ax.YAxis.Scale == "log"
        yends = log10(yends);
        yl = log10(yl);
    end
    
    % Normalize data units relative to axes
    normX = (xends - xl(1))/(xl(2) - xl(1));
    normY = (yends - yl(1))/(yl(2) - yl(1));   

    oldUnits = ax.Units;
    ax.Units = 'normalized';
    figX = ax.Position(1) + normX * ax.Position(3);
    figY = ax.Position(2) + normY * ax.Position(4);
    visible = all(figX > ax.Position(1) & figX < ax.Position(1) + ax.Position(3))...
        & all(figY > ax.Position(2) & figY < ax.Position(2) + ax.Position(4));
    ax.Units = oldUnits;
    
    figX = max(0, min(figX, 1));
    figY = max(0, min(figY, 1));
end

function resizeListener(~,event)
    ax = event.AffectedObject;
    lines = getappdata(ax, 'DataLines');
    for idx=1:length(lines)
        if ~ishandle(lines)
            continue;
        end
        coords = getappdata(lines(idx), 'Coords');
        [figX, figY, visible] = data2fig(ax, coords{1}, coords{2});
        set(lines(idx), 'X', figX);
        set(lines(idx), 'Y', figY);
        set(lines(idx), 'Visible', visible);
    end
end