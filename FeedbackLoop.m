classdef FeedbackLoop < handle
    
    properties (Access = protected)
        inputName  % input variable name
        outputName % control variable name
        dest       % output destination
        
        setPoint   % Target value for input variable
        gain       % Overall feedback gain
        tInt       % Integration response time
        range      % Ouput variable range
        
        nFilt      % Order of LP filter to apply to input data
        tSample    % Sample period (sets timescale for all other time parameters)
        tBW        % BW of input filter        
        aFilt      % Internal filter parameter vector
        bFilt      % Internal filter parameter vector
        
        input      % Input history vector
        errors     % Error history vector
        filtered   % Filtered error history vector
        output     % Output history vector
        index      % Current vector index
    end
    
    methods
        function o = FeedbackLoop(varargin)
            %Constructs a FeedbackLoop.
            %
            %Usage:
            %  obj = FeedbackLoop(inputName, outputName, 'key', value,...)
            %
            %Parameters:
            %  inputName - String name of input field
            %  outputName - String name of output field
            %
            %Keys:
            %  setPoint - scalar
            %    Target set point for input variable.  Defaults to 0
            %  initial - scalar
            %    Initial output parameter value.  Defaults to 0
            %  gain - scalar
            %    Overall gain of feedback loop
            %  tInt - scalar
            %    Integrator response time.  Smaller time means larger
            %    integrator contribution.
            %  range - [outputMax, outputMin]
            %    Defines output variable limits
            
            p = inputParser;
            p.addRequired('inputName', @ischar);
            p.addRequired('outputName', @ischar);
            
            p.addParameter('dest', 'cicero', @(x) ismember(x, {'labrad', 'cicero'}));
            
            p.addParameter('setPoint', 0, @(x) isscalar(x) && isreal(x));
            p.addParameter('initial', 0, @(x) isscalar(x) && isreal(x));
            p.addParameter('gain', 1, @(x) isscalar(x) && isreal(x));
            p.addParameter('tInt', 150, @(x) isscalar(x) && isreal(x));
            p.addParameter('range', [-10, 10], @(x) isreal(x) && isvector(x) && length(x) == 2);
            
            p.addParameter('nFilt', 2, @(x) isscalar(x) && isreal(x) && x > 0);
            p.addParameter('tSample', 30, @(x) isscalar(x) && isreal(x) && x > 0);
            p.addParameter('tBW', 10*60, @(x) isscalar(x) && isreal(x) && x > 0);
            
            p.addParameter('length', 1e4, @(x) isscalar(x) && isreal(x) && x > 0);
            p.parse(varargin{:});
            r = p.Results;            
            
            o.inputName = r.inputName;
            o.outputName = r.outputName;
            o.dest = r.dest;
            
            o.setPoint = r.setPoint;
            o.gain = r.gain;
            o.tInt = r.tInt;
            o.range = r.range;
                        
            o.nFilt = r.nFilt;
            o.tSample = r.tSample;
            o.tBW = r.tBW;
            
            % preallocate arrays
            o.input = nan(1, r.length);
            o.errors = nan(1, r.length);
            o.filtered = nan(1, r.length);
            o.output = nan(1, r.length);
            o.index = 1;
            
            o.input(1) = NaN;
            o.errors(1) = NaN;
            o.filtered(1) = 0;
            o.output(1) = r.initial;
                                   
            if o.nFilt > 0
                % Initialize low-pass filter for input
                fFilt = (2*o.tSample)/o.tBW;
                [o.bFilt, o.aFilt] = butter(o.nFilt, fFilt);
            end
        end
        
        function tune(o, varargin)
            % Adjust feedback parameters of the loop, without reseting its
            % history.  Can change 'setPoint', 'initial', 'gain', 'tInt',
            % and'range'.
            %
            % If the Set Point is modified, this will recalculate the last output value.
            
            p = inputParser;

            p.addParameter('setPoint', NaN, @(x) isnan(x) || (isscalar(x) && isreal(x)));
            p.addParameter('initial', NaN, @(x) isnan(x) || (isscalar(x) && isreal(x)));
            p.addParameter('gain', NaN, @(x) isnan(x) || (isscalar(x) && isreal(x)));
            p.addParameter('tInt', NaN, @(x) isnan(x) || (isscalar(x) && isreal(x)));
            p.addParameter('range', [], @(x) isempty(x) || (isreal(x) && isvector(x) && length(x) == 2));

            p.parse(varargin{:});
            r = p.Results;            

            if ~isnan(r.gain)
                o.gain = r.gain;
            end
            if ~isnan(r.tInt)
                o.tInt = r.tInt;
            end
            if ~isempty(r.range)
                o.range = r.range;
            end
            if ~isnan(r.setPoint)
                o.change_setpoint(r.setPoint);
            end                        
        end            
        
        function output = change_setpoint(o, setPoint)
            % Change set point of feedback loop.  This will recalculate the
            % last output value, based on the new set point.
            if setPoint == o.setPoint
                output = o.output(o.index);
                return;
            end
            
            o.setPoint = setPoint;
            o.errors(o.index) = o.input(o.index) - o.setPoint;

            if isnan(o.errors(o.index))
                output = o.output(o.index);
                return;
            end
            
            % Reset last output point to reflect new setPoint.  If gain is
            % tuned correctly, this should optimally adjust the current
            % output value, such that the error on the next sample is
            % small.   This should shift the lock point with minimal filter
            % response then.
            inter = o.output(o.index-1) + o.gain * (o.errors(o.index) - o.filtered(o.index-1))...
                + o.gain * o.tSample / o.tInt * (o.errors(o.index)); 
            
            % Coerce output to range
            o.output(o.index) = min(o.range(2), max(o.range(1), inter));
            
            output = o.output(o.index);
        end
        
        function inputName = get_input_name(o)
            inputName = o.inputName;
        end
        
        function outputName = get_output_name(o)
            outputName = o.outputName;
        end
        
        function dest = get_dest(o)
            dest = o.dest;
        end
        
        function input = get_input(o)
            input = o.input(1:o.index);
        end
        
        function errors = get_errors(o)
            errors = o.errors(1:o.index);
        end
        
        function filtered = get_filtered(o)
            filtered = o.filtered(1:o.index);
        end
        
        function output = get_output(o)
            output = o.output(1:o.index);
        end
        
        %TODO Implement in loop/out of loop
        function output = update(o, input, iteration)
            % Update loop with new input data.
            %
            % If 'input' is scalar, it is interperated as a single new
            % data point, and is appended to the loop history.
            % 
            % If 'input' is a vector, it is expected to be the full loop
            % input history, and only the new data points at the end are 
            % appeneded to the internal loop history.  All data points are
            % used in the input filter, but only the most recent is used to
            % calculate the new output value
            curIndex = o.index;

            if isscalar(input)
                % single new input point received, append to input vector
                finalIndex = curIndex + 1;
                o.input(finalIndex) = input;
                o.errors(finalIndex) = input - o.setPoint;
            elseif isvector(input)
                % full input vector, append new points
                finalIndex = max(length(input), iteration);
                offset = finalIndex - length(input);
                
                if offset > curIndex
                    o.input(curIndex+1:offset) = NaN;
                    o.errors(curIndex+1:offset) = NaN;
                    o.input(offset+1:finalIndex) = input;
                    o.errors(offset+1:finalIndex) = input - o.setPoint;
                else
                    o.input(curIndex+1:finalIndex) = input(curIndex-offset+1:end);
                    o.errors(curIndex+1:finalIndex) = input(curIndex-offset+1:end) - o.setPoint;
                end
            end
            
            nullIndices = isnan(o.errors(1:finalIndex));
            lastGood = find(~nullIndices, 1, 'last' );
            if ~isempty(lastGood)
                finalIndex = min(lastGood, finalIndex);
            else
                finalIndex = 0;
            end
            
            if isempty(finalIndex) || finalIndex <= curIndex
                output = o.output(curIndex);
                return
            end    
            
            % Linearly interpolate null values in errors, up to finalIndex.
            % The integrator won't see these anyway, but this is to keep
            % the filter from doing something crazy.
            if sum(nullIndices(2:finalIndex))
                if nullIndices(1)
                    % If first value is null, backfill initial null values with first non-null value
                    firstGood = find(~nullIndices,1,'first');
                    o.errors(1:firstGood-1) = o.errors(firstGood);
                    nullIndices(1:firstGood-1) = 0;
                end
                o.errors(nullIndices) = interp1(find(~nullIndices), o.errors(~nullIndices), find(nullIndices), 'linear');
            end
                
            if o.nFilt > 0
                % Filter error signal
                if (finalIndex-1 > o.nFilt)                    
                    % Pad signal with mirrored copy, to fix boundary
                    % conditions.
                    padded = [o.errors(finalIndex:-1:2),...
                        o.errors(2:finalIndex),...
                        o.errors(finalIndex:-1:2)];
                    
                    padded = filtfilt(o.bFilt, o.aFilt, padded);
                    o.filtered(2:finalIndex) = padded(finalIndex:2*(finalIndex-1));
                else
                    o.filtered(2:finalIndex) = mean(o.errors(2:finalIndex));
                end
            else
                o.filtered(1:finalIndex) = o.errors(1:finalIndex);
            end
            
            % If update contains more than 1 new point, backfill output
            % history with last output value
            if finalIndex - curIndex > 1
                o.output(curIndex+1:finalIndex-1) = o.output(curIndex);                
            end
            
            % recalculate the new output based on the state at the last
            % output and the most recent new point.  This avoids integrator
            % wind-up due to gaps in feedback due to any glitch in analysis
            %
            % Apply 'velocity' PID algorithm to calculate change in output
            inter = o.output(curIndex) + o.gain * (o.filtered(finalIndex) - o.filtered(curIndex))...
                + o.gain * o.tSample / o.tInt * (o.filtered(finalIndex));

            % Coerce output to range
            o.output(finalIndex) = min(o.range(2), max(o.range(1), inter));         
                        
            o.index = finalIndex;
            output = o.output(finalIndex);
        end
    end        
end

