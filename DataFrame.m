classdef DataFrame < DataModel & matlab.mixin.CustomDisplay
    % DataModel using a scalar structured array
    
    properties (SetAccess = protected)
        data
        uid
    end

    methods
        function o = DataFrame(varargin)
            %Creates a new DataFrame
            %
            %Usage:
            %  o = DataFrame(model)
           
            p = inputParser;
            p.addOptional('model', [], @(x) isempty(x) || isa(x, 'DataModel'));
            p.addOptional('uid', [], @ischar);
            p.parse(varargin{:});
            args = p.Results;    
            
            o.data = struct([]);
            o.uid = args.uid;
                        
            if ~isempty(args.model)
                o.copy_fields(args.model);
            end
        end
        
        function s = struct(o)
            s = o.data;
            fields = fieldnames(s);
            for ix=1:length(fields)
                fn = fields{ix};
                fo = s.(fn);
                if isa(fo,'Waveform')
                    s.(fn) = struct(fo);
                    s.(fn).('df__type') = class(fo);
                end
            end
        end
        
        function r = extract(o,varargin)
            r = DataFrame();
            for index=1:length(varargin)
                r.set(varargin{index},o.get(varargin{index}));
            end
        end
        
        function save(o,filename,varargin)
            if ~isempty(varargin)
                r = o.extract(varargin{:});
            else
                r = o;
            end
            
            if ~isempty(r.data)
                s = struct(r);
                save(filename,'-struct','s','-v7');
            else
                warning('skaffold:DataFrame', 'Attempt to save empty DataFrame');
            end
        end
        
        function unpack(o,varargin)
            if isempty(varargin)
                fields = o.fields();
                warning('Calling DataFrame.unpack without specifying a field list is dangerous!');
            else
                fields = varargin;
            end
            
            for idx=1:length(fields)
                name = fields{idx};
                if ~o.has(name)
                    warning('Missing field: ''%s''',name);
                    continue;
                end
                assignin('caller',name,o.get(name));
            end            
        end

        function pack(o,varargin)
            fields = varargin;
            
            for idx=1:length(fields)
                name = fields{idx};
                o.set(name, evalin('caller',name));
            end            
        end        
    end
    
	methods (Static)
        function model = load(filename,varargin)
            p = inputParser;
            p.addRequired('filename',@ischar);
            p.addOptional('fields',{},@iscellstr);
            p.addOptional('create',false,@islogical);
            p.addParameter('uid', [], @ischar);            
            p.parse(filename,varargin{:});
            r = p.Results;

            if exist(r.filename, 'file') ~= 2
                if r.create % Create empty dataframe
                    model = DataFrame([]);
                    model.uid = r.uid;
                    return
                else
                    error('Unable to load Dataframe: file does not exist ''%s''', r.filename);
                end
            end
            
            if ~isempty(r.fields)
                s = load(r.filename, r.fields{:});
            else
                s = load(r.filename);
            end
            model = DataFrame.from_struct(s);
            model.uid = r.uid;
        end
        
        function model = reload(model, filedate, filename, varargin)
            p = inputParser;
            p.addRequired('data',@(x) isempty(x) || isa(x, 'DataModel'));            
            p.addRequired('filedate',@ischar);
            p.addRequired('filename',@ischar);
            p.addOptional('fields',{},@iscellstr);
            p.parse(model,filedate,filename,varargin{:});
            r = p.Results;

            fileInfo = dir(filename);
            if ~isempty(fileInfo)
                modified = datestr(fileInfo.datenum);
            else
                error('Failed to load DataFrame: file ''%s%'' doesn''t exist',filename);
            end

            checkUid = [filedate, ',' filename, ',', modified];            
            if ~isa(model, 'DataFrame') || ~strcmp(model.uid, checkUid)
                model = DataFrame.load(r.filename, r.fields, 'uid', checkUid);
            end
        end
        
        function result = check_uid(model, checkUid)        
            result = isa(model, 'DataFrame') && strcmp(model.uid, checkUid);
        end
    end
    
    methods (Static, Access = protected)        
        function model = from_struct(s)
            model = DataFrame();
            model.data = s;
                        
            fields = fieldnames(s);
            for ix=1:length(fields)
                fn = fields{ix};
                if isstruct(s.(fn)) && isfield(s.(fn),'df__type')
                    fs = s.(fn);
                    switch (fs.df__type)
                        case 'Waveform'
                            model.data.(fn) = Waveform(fs.x0,fs.dx,fs.data);
                    end
                end
            end
        end
    end
    
    % Abstract implementations:
    methods
        function value = get(o,field,~)                       
            parts = strsplit(field,'.');
            if isfield(o.data, parts{1})
                value = o.data(1).(parts{1});
                if length(parts) > 1
                    if ~isstruct(value)
                        error('skaffold:ArgumentError', 'Invalid sub-field access ''%s''.', field);
                    elseif ~isfield(value, parts{2})
                        error('skaffold:ArgumentError', 'Unrecognized sub-field ''%s''.', field);
                    elseif numel(value)==1
                        value = value.(parts{2});
                    else
                        dims = size(value);
                        value = {value.(parts{2})};
                        [value{cellfun(@(x) isempty(x), value)}]=deal(nan);
                        value = cell2mat(value);
                        value = reshape(value, dims);
                    end
                end
            else
                error('skaffold:ArgumentError', 'Unrecognized field ''%s''.', field);
            end
        end
        
        function set(o,field,value)
            o.data(1).(field) = value;
        end
        
        function unset(o,field)
            if (isfield(o.data, field))
                o.data = rmfield(o.data, field);
            end            
        end
        
        function test = has(o,field)
            test = false;
            
            parts = strsplit(field,'.');
            if ~isfield(o.data, parts{1})
                return;
            elseif length(parts) > 1
                if ~isstruct(o.data(1).(parts{1})) || ~isfield(o.data(1).(parts{1}), parts{2})
                    return
                end
            end
            
            test = true;
        end
        
        function model = clone(o)
            model = DataFrame(o);
        end
        
        function fields = fields(o)
            fields = fieldnames(o.data);
        end
    end
    
    methods (Access = protected)
        function s = getHeader(o)
            name = matlab.mixin.CustomDisplay.getClassNameForHeader(o);
            if ~isscalar(o)
                dims = matlab.mixin.CustomDisplay.convertDimensionsToString(o);
                s = sprintf('%s %s array\n', dims, name);
            else
                s = sprintf('%s with fields:\n', name);            
            end            
        end
        function propgrp = getPropertyGroups(o)
%             if ~all(arrayfun(@(x) isempty(x.data), o))
            if isscalar(o) && ~isempty(o.data)
                propgrp = matlab.mixin.util.PropertyGroup(o.data);
            else
                propgrp = matlab.mixin.util.PropertyGroup();
            end                
        end        
    end
end

