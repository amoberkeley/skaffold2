function out = nansum(X, varargin)

out = sum(X, varargin{:}, 'omitnan');

end