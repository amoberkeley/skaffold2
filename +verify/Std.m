classdef Std < Verifier
    % Verifies that the standard deviation of a DataModel field falls within given range
    
    properties (SetAccess = protected)
        field    % Name of field to verify
        bounds   % 2-element array of bounds
        range    % 2-element array indicating range over which to verify        
    end
    
    methods
        function o = Std(field, bounds, varargin)
            %Constructs a Std verifier.
            %
            %Usage:
            %  obj = verify.Std(field,bounds,'key',value,...)
            %
            %Parameters:
            %  field - String name of DataModel field to verify
            %  bounds - 2-element array of bounds
            %
            %Keys:
            %  range - [] | 2-element array
            %    If not empty, only verifies data in specified time range


            p = inputParser;
            p.addRequired('field',@ischar)
            p.addRequired('bounds',@(x) isnumeric(x) && numel(x) == 2);
            p.addParameter('range', [], @(x) isnumeric(x) && (numel(x) == 0 || numel(x) == 2));
            p.parse(field,bounds,varargin{:});
            args = p.Results;
            
            o.field = args.field;
            o.bounds = args.bounds;
            o.range = args.range;            
        end
            
        
        function [test,message] = verify(o,model,~,~)
            %Verifies that the standard deviation stays within bounds.
            
            data = model.get(o.field);
        
            if isa(data,'Waveform')
                if ~isempty(o.range)
                    data = data.slice_x(min(o.range), max(o.range));
                end
                
                data = data.data;
            elseif ~isempty(o.range)
                error('skaffold:TypeError','DataModel field must be a Waveform if range is set in verify.Std.');
            end
            
            %Test bounds
            sample = std(data(:));
            test = ~(sample > max(o.bounds) || sample < min(o.bounds));
            message = '';

            if ~test
                message = sprintf('Standard deviation of ''%s'' is %0.4g, outside bounds [%0.4g, %0.4g].',...
                    o.field, sample, min(o.bounds), max(o.bounds));
            end           
        end
        
        function [sample] = sample_aggregate(o,model)            
            data = model.get(o.field);
        
            if isa(data,'Waveform')
                wf = data.condense();
                if ~isempty(o.range)
                    wf = wf.slice_x(min(o.range), max(o.range));
                end
                sample = squeeze(wf.std().data);

                if ndims(data) > 2
                    sample = permute(sample, circshift(1:ndims(sample), 2, 2));
                end
            else
                error('skaffold:TypeError','verify.Std.sample_aggregate currently only supports Waveforms');
            end            
        end

        function [valid,message] = verify_aggregate(o,model)
            %Verifies that the standard deviation stays within bounds.
            
            sample = o.sample_aggregate(model);
                               
            %Test bounds (assume loop and point are first two dims, check
            %   that all higher dimensions are within bounds)
            valid = ~all(sample(:,:,:) > max(o.bounds) | sample(:,:,:) < min(o.bounds), 3);

            message = sprintf('%s: Standard deviation of %d iterations outside of bounds [%0.4g, %0.4g].', ...
                o.field, sum(~valid(:)), min(o.bounds), max(o.bounds));
        end        
        
        function [] = plot_aggregate(o,ax,model)
            sample = o.sample_aggregate(model);

            min_range = o.bounds + range(o.bounds)/4*[-1,1];
            max_range = mean(o.bounds) + 3*range(o.bounds)*[-1,1];

            sample(sample<min(max_range) | sample>max(max_range)) = NaN;
            data_range = nanmean(sample(:)) + 5*nanstd(sample(:))*[-1,1];

            valid = ~all(sample(:,:,:) > max(o.bounds) | sample(:,:,:) < min(o.bounds), 3);
            nRejected = sum(~valid(:));            
            
            hold(ax, 'on');
            title(ax, o.field, 'Interpreter', 'none');
            hHist = histogram(ax, sample, min(100,numel(sample)), 'EdgeColor','none');
            str = sprintf('%d failed (%0.2f%%)', nRejected, 100*nRejected/numel(sample));
            hText = text(0.90, 0.90, str,'FontSize',9,'BackgroundColor',[1,1,1,.9],'Parent',ax,...
                'Units','normalized', 'VerticalAlignment', 'top', 'HorizontalAlignment', 'right'); 
            vline(ax, o.bounds, '--', 'Color',[1 0 0 1], 'LineWidth', 1)
            xlim(ax, minmax([min_range, data_range]));
        end        
    end
    
end

