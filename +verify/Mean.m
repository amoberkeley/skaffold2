classdef Mean < Verifier
    % Verifies that the mean of a DataModel field falls within given range
    
    properties (SetAccess = protected)
        field    % Name of field to verify
        bounds   % 2-element array of bounds
        range    % 2-element array indicating range over which to verify        
    end
    
    methods
        function o = Mean(field, bounds, varargin)
            %Constructs a Mean verifier.
            %
            %Usage:
            %  obj = verify.Mean(field,bounds,'key',value,...)
            %
            %Parameters:
            %  field - String name of DataModel field to verify
            %  bounds - 2-element array of bounds
            %
            %Keys:
            %  range - [] | 2-element array
            %    If not empty, only verifies data in specified time range


            p = inputParser;
            p.addRequired('field',@ischar)
            p.addRequired('bounds',@(x) isnumeric(x) && numel(x) == 2);
            p.addParameter('range', [], @(x) isnumeric(x) && (numel(x) == 0 || numel(x) == 2));
            p.parse(field,bounds,varargin{:});
            args = p.Results;
            
            o.field = args.field;
            o.bounds = args.bounds;
            o.range = args.range;            
        end
            
        
        function [test,message] = verify(o,model,~,~)
            %Verifies that the mean stays within bounds.
            
            data = model.get(o.field);
        
            if ~isempty(o.range)
                assert(isa(data, 'Waveform'),'skaffold:TypeError',...
                    'DataModel field must be a Waveform if range is set in verify.Mean.');
                
                data = data.slice_x(min(o.range), max(o.range)).data;
            elseif isa(data,'Waveform')
                data = data.data;
            end
            
            test = true;
            message = '';
            
            avg = mean(data);
            %Test mean
            if avg > max(o.bounds) || avg < min(o.bounds)
                test = false;
                message = sprintf('Mean of ''%s'' is %0.4g, outside bounds [%0.4g, %0.4g].',...
                    o.field, avg, min(o.bounds), max(o.bounds));
            end           
        end
    end
    
end

