classdef ComplexRange < Verifier
    % Verifies that the all values of a DataModel field falls within given range
    
    properties (SetAccess = protected)
        field    % Name of field to verify
        center   % Complex number defining center of complex range
        radius   % Real number defining radius of complex range
        range    % 2-element array indicating time range over which to verify        
    end
    
    methods
        function o = ComplexRange(field, bounds, varargin)
            %Constructs a ComplexRange verifier.
            %
            %Usage:
            %  obj = verify.ComplexRange(field,center,radius,'key',value,...)
            %
            %Parameters:
            %  field - String name of DataModel field to verify
            %  center - Complex number defining center of complex range
            %  radius - Real number defining radius of complex range
            %
            %Keys:
            %  range - [] | 2-element array
            %    If not empty, only verifies data in specified time range


            p = inputParser;
            p.addRequired('field', @ischar)
            p.addRequired('center', @(x) isscalar(x) && isnumeric(x));
            p.addRequired('radius', @(x) isscalar(x) && isreal(x));
            p.addParameter('range', [], @(x) isnumeric(x) && (numel(x) == 0 || numel(x) == 2));
            p.parse(field,bounds,varargin{:});
            args = p.Results;
            
            o.field = args.field;
            o.center = args.center;
            o.radius = args.radius;
            o.range = args.range;            
        end
            
        
        function [test,message] = verify(o,model,~,~)
            %Verifies that the mean stays within bounds.
            
            data = model.get(o.field);
        
            if ~isempty(o.range)
                assert(isa(data, 'Waveform'),'skaffold:TypeError',...
                    'DataModel field must be a Waveform if range is set in verify.Mean.');
                
                data = data.slice_x(min(o.range), max(o.range)).data;
            elseif isa(data,'Waveform')
                data = data.data;
            end
            
            test = true;
            message = '';
            
            %Test bounds
            deviations = abs(data - o.center);
            if max(deviations) > o.radius
                test = false;
                message = sprintf('Value of ''%s'' is more then %0.4g away from %s.',...
                    o.field, o.radius, num2str(o.center));
            end           
        end
    end
    
end

