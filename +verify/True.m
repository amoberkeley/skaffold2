classdef True < Verifier
    % Verifies that the all values of a DataModel field falls within given range
    
    properties (SetAccess = protected)
        field    % Name of field to verify
        invert   % Boolean describing whether to invert field value
    end
    
    methods
        function o = True(field, varargin)
            % Constructs a True verifier.

            p = inputParser;
            p.addRequired('field',@ischar)
            p.addParameter('invert', 0);
            
            p.parse(field, varargin{:});
            args = p.Results;
            
            o.field = args.field;
            o.invert = args.invert;            
        end
        
        function [test, message] = verify(o, model, ~, ~)
            % Verifies that the value is true.
            
            val = model.get(o.field);
            test = val ~= o.invert;
            
            falseOrTrue = {'false', 'true'};
            
            message = '';
            if ~test
                message = sprintf('Value of ''%s'' is %s.', o.field, falseOrTrue{val+1});
            end
        end

    end
    
end

