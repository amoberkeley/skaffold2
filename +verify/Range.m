classdef Range < Verifier
    % Verifies that the all values of a DataModel field falls within given range
    
    properties (SetAccess = protected)
        field    % Name of field to verify
        bounds   % 2-element array of bounds
        range    % 2-element array indicating range over which to verify        
    end
    
    methods
        function o = Range(field, bounds, varargin)
            %Constructs a Range verifier.
            %
            %Usage:
            %  obj = verify.Range(field,bounds,'key',value,...)
            %
            %Parameters:
            %  field - String name of DataModel field to verify
            %  bounds - 2-element array of bounds
            %
            %Keys:
            %  range - [] | 2-element array
            %    If not empty, only verifies data in specified time range


            p = inputParser;
            p.addRequired('field',@ischar)
            p.addRequired('bounds',@(x) isnumeric(x) && numel(x) == 2);
            p.addParameter('range', [], @(x) isnumeric(x) && (numel(x) == 0 || numel(x) == 2));
            p.parse(field,bounds,varargin{:});
            args = p.Results;
            
            o.field = args.field;
            o.bounds = args.bounds;
            o.range = args.range;            
        end
            
        
        function [test,message] = verify(o,model,~,~)
            %Verifies that the mean stays within bounds.
            
            data = model.get(o.field);
        
            if isa(data,'Waveform')
                if ~isempty(o.range)
                    data.slice_x(min(o.range), max(o.range));
                end
                
                data = data.data;
            elseif ~isempty(o.range)
                error('skaffold:TypeError','DataModel field must be a Waveform if range is set in verify.Mean.');
            end
            
            message = '';
            
            %Test bounds
            test = ~all(data(:) > max(o.bounds) | data(:) < min(o.bounds));
            if ~test
                if isscalar(data)
                    message = sprintf('Value of ''%s'' is %0.4g, outside bounds [%0.4g, %0.4g].',...
                    o.field, data, min(o.bounds), max(o.bounds));
                else
                    message = sprintf('Range of ''%s'' is [%0.4g, %0.4g], outside bounds [%0.4g, %0.4g].',...
                    o.field, min(data), max(data), min(o.bounds), max(o.bounds));
                end
            end
        end
        
        function [valid,message] = verify_aggregate(o,model)
            %Verifies that the mean stays within bounds.
            
            data = model.get(o.field);
        
            if isa(data,'Waveform')
                wf = data.condense();
                if ~isempty(o.range)
                    wf = wf.slice_x(min(o.range), max(o.range));
                end
                data = wf.data;

                if ndims(data) > 2
                    data = permute(data, circshift(1:ndims(data), 2, 2));
                end
            elseif ~isempty(o.range)
                error('skaffold:TypeError','DataModel field must be a Waveform if range is set in verify.Mean.');
            end
                       
            %Test bounds (assume loop and point are first two dims, check
            %   that all higher dimensions are within bounds)
            valid = ~all(data(:,:,:) > max(o.bounds) | data(:,:,:) < min(o.bounds), 3);
            nRejected = sum(~valid(:));

            message = sprintf('%s: Value of %d iterations outside of bounds [%0.4g, %0.4g].', ...
                o.field, nRejected, min(o.bounds), max(o.bounds));
        end        
        
        function [hHist] = plot_aggregate(o,ax,model)
            data = model.get(o.field);

            if isa(data,'Waveform')
                warning('verify.Range.plot_aggregate is not supported for Waveforms');
                return
            end

            valid = ~(data > max(o.bounds) | data < min(o.bounds));
            nRejected = sum(~valid(:));    
            
            min_range = o.bounds + range(o.bounds)/4*[-1,1];
            max_range = mean(o.bounds) + 3*range(o.bounds)*[-1,1];

            data(data<min(max_range) | data>max(max_range)) = NaN;
            data_range = nanmean(data(:)) + 5*nanstd(data(:))*[-1,1];        
            
            hold(ax, 'on');
            title(ax, o.field, 'Interpreter', 'none');
            hHist = histogram(ax, data, min(100,numel(data)), 'EdgeColor','none');
            str = sprintf('%d failed (%0.2f%%)', nRejected, 100*nRejected/numel(data));
%           legend(h, str);
            hText = text(0.90, 0.90, str,'FontSize',9,'BackgroundColor',[1,1,1,.9],'Parent',ax,...
                'Units','normalized', 'VerticalAlignment', 'top', 'HorizontalAlignment', 'right'); 
            vline(ax, o.bounds, '--', 'Color',[1 0 0 1], 'LineWidth', 1)
            xlim(ax, minmax([min_range, data_range]));
        end
    end
    
end

