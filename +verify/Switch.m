classdef Switch < Verifier

    properties (SetAccess = protected)
        verifiers
        modulus
    end
    
    methods
        function o = Switch(verifiers,varargin)
            %Constructs a Switch verifier.
            %
            %Usage:
            %  obj = verify.Switch(verifiers)
            %
            %Parameters:
            %  verifiers - Cell array of verifiers to be run on
            %              corresponding points
            %

            p = inputParser;
            p.addRequired('verifiers', @(x) iscell(x) && ~isempty(x) && all(cellfun(@(y) isa(y,'Verifier'),x)))
            p.addParameter('modulus', false, @islogical);
            p.parse(verifiers,varargin{:});
            args = p.Results;
            
            o.verifiers = args.verifiers;
            o.modulus = args.modulus;
        end
            
        
        function [test,message] = verify(o,model,loop,point)
            %Verifies that the mean stays within bounds.
                        
            if o.modulus
                iVerifier = mod(point-1, length(o.verifiers))+1;
                [test, message] = o.verifiers{iVerifier}.verify(model, loop, point);
            elseif point <= length(o.verifiers)
                [test, message] = o.verifiers{point}.verify(model, loop, point);
            else
                test = true;
                message = '';
            end
        end
    end
    
end

