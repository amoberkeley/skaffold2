function varargout = ufigure(name,varargin)
%UFIGURE Creates or focuses on a figure uniquely identified by name
%
%Usage:
%  ufigure(name)
%       If the figure named already exists, changes the graphics focus to
%       that figure.  Otherwise, creates the figure using default values.
%  ufigure(name,'PropertyName',PropertyValue,...)
%       Sets the given figure properties.
%  [h,existed] = ufigure(...)
%       Returns a handle to the figure in h.  existed is true or false
%       depending on whether the figure was in existence before execution.
%
%Provides a way to uniquely specify which figures to use from code, so that
%only that code bit overwrites the figure (usually accomplished with
%something like "figure(487)", which is a bit of a hack)
%
%Example:
%  h = ufigure('myfunction output') creates a new figure that is unlikely
%  to be overwritten by any function other than myfunction

if ~exist('name')
    hFind = findobj('-property','Name');
    names = get(hFind,'Name');
    ind = find(~cellfun(@isempty,names));
    varargout = {{names{ind}} hFind(ind)};
else
    hFind = findobj('Name',name);

    existed = false;
    if isempty(hFind)
        h = figure(varargin{:});
        set(h,'Name',name);
    else
        h = hFind(1);
        existed = true;
        if ~isempty(varargin)
            set(h,varargin{:});
        end
        figure(h);
    end

    switch nargout
        case 0
            varargout = {};
        case 1
            varargout = {h};
        otherwise
            varargout = {h existed};
    end
end