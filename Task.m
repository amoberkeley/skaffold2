classdef Task < handle
    % Base abstract class for a task that runs on a DataModel
    
    methods (Abstract)
        result = run(o, model, varargin)
    end
    
    methods
        function skip(o, model, varargin)
        end
    end
end

