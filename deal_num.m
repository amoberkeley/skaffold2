function varargout = deal_num(x)
    varargout = num2cell(x(:));
end